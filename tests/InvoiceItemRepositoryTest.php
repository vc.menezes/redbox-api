<?php

use App\Models\InvoiceItem;
use App\Repositories\InvoiceItemRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InvoiceItemRepositoryTest extends TestCase
{
    use MakeInvoiceItemTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var InvoiceItemRepository
     */
    protected $invoiceItemRepo;

    public function setUp()
    {
        parent::setUp();
        $this->invoiceItemRepo = App::make(InvoiceItemRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateInvoiceItem()
    {
        $invoiceItem = $this->fakeInvoiceItemData();
        $createdInvoiceItem = $this->invoiceItemRepo->create($invoiceItem);
        $createdInvoiceItem = $createdInvoiceItem->toArray();
        $this->assertArrayHasKey('id', $createdInvoiceItem);
        $this->assertNotNull($createdInvoiceItem['id'], 'Created InvoiceItem must have id specified');
        $this->assertNotNull(InvoiceItem::find($createdInvoiceItem['id']), 'InvoiceItem with given id must be in DB');
        $this->assertModelData($invoiceItem, $createdInvoiceItem);
    }

    /**
     * @test read
     */
    public function testReadInvoiceItem()
    {
        $invoiceItem = $this->makeInvoiceItem();
        $dbInvoiceItem = $this->invoiceItemRepo->find($invoiceItem->id);
        $dbInvoiceItem = $dbInvoiceItem->toArray();
        $this->assertModelData($invoiceItem->toArray(), $dbInvoiceItem);
    }

    /**
     * @test update
     */
    public function testUpdateInvoiceItem()
    {
        $invoiceItem = $this->makeInvoiceItem();
        $fakeInvoiceItem = $this->fakeInvoiceItemData();
        $updatedInvoiceItem = $this->invoiceItemRepo->update($fakeInvoiceItem, $invoiceItem->id);
        $this->assertModelData($fakeInvoiceItem, $updatedInvoiceItem->toArray());
        $dbInvoiceItem = $this->invoiceItemRepo->find($invoiceItem->id);
        $this->assertModelData($fakeInvoiceItem, $dbInvoiceItem->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteInvoiceItem()
    {
        $invoiceItem = $this->makeInvoiceItem();
        $resp = $this->invoiceItemRepo->delete($invoiceItem->id);
        $this->assertTrue($resp);
        $this->assertNull(InvoiceItem::find($invoiceItem->id), 'InvoiceItem should not exist in DB');
    }
}
