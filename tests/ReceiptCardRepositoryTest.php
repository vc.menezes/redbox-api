<?php

use App\Models\ReceiptCard;
use App\Repositories\ReceiptCardRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReceiptCardRepositoryTest extends TestCase
{
    use MakeReceiptCardTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ReceiptCardRepository
     */
    protected $receiptCardRepo;

    public function setUp()
    {
        parent::setUp();
        $this->receiptCardRepo = App::make(ReceiptCardRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateReceiptCard()
    {
        $receiptCard = $this->fakeReceiptCardData();
        $createdReceiptCard = $this->receiptCardRepo->create($receiptCard);
        $createdReceiptCard = $createdReceiptCard->toArray();
        $this->assertArrayHasKey('id', $createdReceiptCard);
        $this->assertNotNull($createdReceiptCard['id'], 'Created ReceiptCard must have id specified');
        $this->assertNotNull(ReceiptCard::find($createdReceiptCard['id']), 'ReceiptCard with given id must be in DB');
        $this->assertModelData($receiptCard, $createdReceiptCard);
    }

    /**
     * @test read
     */
    public function testReadReceiptCard()
    {
        $receiptCard = $this->makeReceiptCard();
        $dbReceiptCard = $this->receiptCardRepo->find($receiptCard->id);
        $dbReceiptCard = $dbReceiptCard->toArray();
        $this->assertModelData($receiptCard->toArray(), $dbReceiptCard);
    }

    /**
     * @test update
     */
    public function testUpdateReceiptCard()
    {
        $receiptCard = $this->makeReceiptCard();
        $fakeReceiptCard = $this->fakeReceiptCardData();
        $updatedReceiptCard = $this->receiptCardRepo->update($fakeReceiptCard, $receiptCard->id);
        $this->assertModelData($fakeReceiptCard, $updatedReceiptCard->toArray());
        $dbReceiptCard = $this->receiptCardRepo->find($receiptCard->id);
        $this->assertModelData($fakeReceiptCard, $dbReceiptCard->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteReceiptCard()
    {
        $receiptCard = $this->makeReceiptCard();
        $resp = $this->receiptCardRepo->delete($receiptCard->id);
        $this->assertTrue($resp);
        $this->assertNull(ReceiptCard::find($receiptCard->id), 'ReceiptCard should not exist in DB');
    }
}
