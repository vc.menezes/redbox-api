<?php

use App\Models\BulkOrderView;
use App\Repositories\BulkOrderViewRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BulkOrderViewRepositoryTest extends TestCase
{
    use MakeBulkOrderViewTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BulkOrderViewRepository
     */
    protected $bulkOrderViewRepo;

    public function setUp()
    {
        parent::setUp();
        $this->bulkOrderViewRepo = App::make(BulkOrderViewRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBulkOrderView()
    {
        $bulkOrderView = $this->fakeBulkOrderViewData();
        $createdBulkOrderView = $this->bulkOrderViewRepo->create($bulkOrderView);
        $createdBulkOrderView = $createdBulkOrderView->toArray();
        $this->assertArrayHasKey('id', $createdBulkOrderView);
        $this->assertNotNull($createdBulkOrderView['id'], 'Created BulkOrderView must have id specified');
        $this->assertNotNull(BulkOrderView::find($createdBulkOrderView['id']), 'BulkOrderView with given id must be in DB');
        $this->assertModelData($bulkOrderView, $createdBulkOrderView);
    }

    /**
     * @test read
     */
    public function testReadBulkOrderView()
    {
        $bulkOrderView = $this->makeBulkOrderView();
        $dbBulkOrderView = $this->bulkOrderViewRepo->find($bulkOrderView->id);
        $dbBulkOrderView = $dbBulkOrderView->toArray();
        $this->assertModelData($bulkOrderView->toArray(), $dbBulkOrderView);
    }

    /**
     * @test update
     */
    public function testUpdateBulkOrderView()
    {
        $bulkOrderView = $this->makeBulkOrderView();
        $fakeBulkOrderView = $this->fakeBulkOrderViewData();
        $updatedBulkOrderView = $this->bulkOrderViewRepo->update($fakeBulkOrderView, $bulkOrderView->id);
        $this->assertModelData($fakeBulkOrderView, $updatedBulkOrderView->toArray());
        $dbBulkOrderView = $this->bulkOrderViewRepo->find($bulkOrderView->id);
        $this->assertModelData($fakeBulkOrderView, $dbBulkOrderView->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBulkOrderView()
    {
        $bulkOrderView = $this->makeBulkOrderView();
        $resp = $this->bulkOrderViewRepo->delete($bulkOrderView->id);
        $this->assertTrue($resp);
        $this->assertNull(BulkOrderView::find($bulkOrderView->id), 'BulkOrderView should not exist in DB');
    }
}
