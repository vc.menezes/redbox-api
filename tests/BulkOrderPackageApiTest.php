<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BulkOrderPackageApiTest extends TestCase
{
    use MakeBulkOrderPackageTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBulkOrderPackage()
    {
        $bulkOrderPackage = $this->fakeBulkOrderPackageData();
        $this->json('POST', '/api/v1/bulkOrderPackages', $bulkOrderPackage);

        $this->assertApiResponse($bulkOrderPackage);
    }

    /**
     * @test
     */
    public function testReadBulkOrderPackage()
    {
        $bulkOrderPackage = $this->makeBulkOrderPackage();
        $this->json('GET', '/api/v1/bulkOrderPackages/'.$bulkOrderPackage->id);

        $this->assertApiResponse($bulkOrderPackage->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBulkOrderPackage()
    {
        $bulkOrderPackage = $this->makeBulkOrderPackage();
        $editedBulkOrderPackage = $this->fakeBulkOrderPackageData();

        $this->json('PUT', '/api/v1/bulkOrderPackages/'.$bulkOrderPackage->id, $editedBulkOrderPackage);

        $this->assertApiResponse($editedBulkOrderPackage);
    }

    /**
     * @test
     */
    public function testDeleteBulkOrderPackage()
    {
        $bulkOrderPackage = $this->makeBulkOrderPackage();
        $this->json('DELETE', '/api/v1/bulkOrderPackages/'.$bulkOrderPackage->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/bulkOrderPackages/'.$bulkOrderPackage->id);

        $this->assertResponseStatus(404);
    }
}
