<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactableApiTest extends TestCase
{
    use MakeContactableTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateContactable()
    {
        $contactable = $this->fakeContactableData();
        $this->json('POST', '/api/v1/contactables', $contactable);

        $this->assertApiResponse($contactable);
    }

    /**
     * @test
     */
    public function testReadContactable()
    {
        $contactable = $this->makeContactable();
        $this->json('GET', '/api/v1/contactables/'.$contactable->id);

        $this->assertApiResponse($contactable->toArray());
    }

    /**
     * @test
     */
    public function testUpdateContactable()
    {
        $contactable = $this->makeContactable();
        $editedContactable = $this->fakeContactableData();

        $this->json('PUT', '/api/v1/contactables/'.$contactable->id, $editedContactable);

        $this->assertApiResponse($editedContactable);
    }

    /**
     * @test
     */
    public function testDeleteContactable()
    {
        $contactable = $this->makeContactable();
        $this->json('DELETE', '/api/v1/contactables/'.$contactable->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/contactables/'.$contactable->id);

        $this->assertResponseStatus(404);
    }
}
