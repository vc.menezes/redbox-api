<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class KeyApiTest extends TestCase
{
    use MakeKeyTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateKey()
    {
        $key = $this->fakeKeyData();
        $this->json('POST', '/api/v1/keys', $key);

        $this->assertApiResponse($key);
    }

    /**
     * @test
     */
    public function testReadKey()
    {
        $key = $this->makeKey();
        $this->json('GET', '/api/v1/keys/'.$key->id);

        $this->assertApiResponse($key->toArray());
    }

    /**
     * @test
     */
    public function testUpdateKey()
    {
        $key = $this->makeKey();
        $editedKey = $this->fakeKeyData();

        $this->json('PUT', '/api/v1/keys/'.$key->id, $editedKey);

        $this->assertApiResponse($editedKey);
    }

    /**
     * @test
     */
    public function testDeleteKey()
    {
        $key = $this->makeKey();
        $this->json('DELETE', '/api/v1/keys/'.$key->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/keys/'.$key->id);

        $this->assertResponseStatus(404);
    }
}
