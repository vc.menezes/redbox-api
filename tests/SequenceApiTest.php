<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SequenceApiTest extends TestCase
{
    use MakeSequenceTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSequence()
    {
        $sequence = $this->fakeSequenceData();
        $this->json('POST', '/api/v1/sequences', $sequence);

        $this->assertApiResponse($sequence);
    }

    /**
     * @test
     */
    public function testReadSequence()
    {
        $sequence = $this->makeSequence();
        $this->json('GET', '/api/v1/sequences/'.$sequence->id);

        $this->assertApiResponse($sequence->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSequence()
    {
        $sequence = $this->makeSequence();
        $editedSequence = $this->fakeSequenceData();

        $this->json('PUT', '/api/v1/sequences/'.$sequence->id, $editedSequence);

        $this->assertApiResponse($editedSequence);
    }

    /**
     * @test
     */
    public function testDeleteSequence()
    {
        $sequence = $this->makeSequence();
        $this->json('DELETE', '/api/v1/sequences/'.$sequence->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/sequences/'.$sequence->id);

        $this->assertResponseStatus(404);
    }
}
