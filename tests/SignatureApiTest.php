<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SignatureApiTest extends TestCase
{
    use MakeSignatureTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSignature()
    {
        $signature = $this->fakeSignatureData();
        $this->json('POST', '/api/v1/signatures', $signature);

        $this->assertApiResponse($signature);
    }

    /**
     * @test
     */
    public function testReadSignature()
    {
        $signature = $this->makeSignature();
        $this->json('GET', '/api/v1/signatures/'.$signature->id);

        $this->assertApiResponse($signature->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSignature()
    {
        $signature = $this->makeSignature();
        $editedSignature = $this->fakeSignatureData();

        $this->json('PUT', '/api/v1/signatures/'.$signature->id, $editedSignature);

        $this->assertApiResponse($editedSignature);
    }

    /**
     * @test
     */
    public function testDeleteSignature()
    {
        $signature = $this->makeSignature();
        $this->json('DELETE', '/api/v1/signatures/'.$signature->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/signatures/'.$signature->id);

        $this->assertResponseStatus(404);
    }
}
