<?php

use App\Models\Tracking;
use App\Repositories\TrackingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TrackingRepositoryTest extends TestCase
{
    use MakeTrackingTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TrackingRepository
     */
    protected $trackingRepo;

    public function setUp()
    {
        parent::setUp();
        $this->trackingRepo = App::make(TrackingRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTracking()
    {
        $tracking = $this->fakeTrackingData();
        $createdTracking = $this->trackingRepo->create($tracking);
        $createdTracking = $createdTracking->toArray();
        $this->assertArrayHasKey('id', $createdTracking);
        $this->assertNotNull($createdTracking['id'], 'Created Tracking must have id specified');
        $this->assertNotNull(Tracking::find($createdTracking['id']), 'Tracking with given id must be in DB');
        $this->assertModelData($tracking, $createdTracking);
    }

    /**
     * @test read
     */
    public function testReadTracking()
    {
        $tracking = $this->makeTracking();
        $dbTracking = $this->trackingRepo->find($tracking->id);
        $dbTracking = $dbTracking->toArray();
        $this->assertModelData($tracking->toArray(), $dbTracking);
    }

    /**
     * @test update
     */
    public function testUpdateTracking()
    {
        $tracking = $this->makeTracking();
        $fakeTracking = $this->fakeTrackingData();
        $updatedTracking = $this->trackingRepo->update($fakeTracking, $tracking->id);
        $this->assertModelData($fakeTracking, $updatedTracking->toArray());
        $dbTracking = $this->trackingRepo->find($tracking->id);
        $this->assertModelData($fakeTracking, $dbTracking->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTracking()
    {
        $tracking = $this->makeTracking();
        $resp = $this->trackingRepo->delete($tracking->id);
        $this->assertTrue($resp);
        $this->assertNull(Tracking::find($tracking->id), 'Tracking should not exist in DB');
    }
}
