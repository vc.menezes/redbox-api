<?php

use App\Models\CommissionView;
use App\Repositories\CommissionViewRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommissionViewRepositoryTest extends TestCase
{
    use MakeCommissionViewTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CommissionViewRepository
     */
    protected $commissionViewRepo;

    public function setUp()
    {
        parent::setUp();
        $this->commissionViewRepo = App::make(CommissionViewRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCommissionView()
    {
        $commissionView = $this->fakeCommissionViewData();
        $createdCommissionView = $this->commissionViewRepo->create($commissionView);
        $createdCommissionView = $createdCommissionView->toArray();
        $this->assertArrayHasKey('id', $createdCommissionView);
        $this->assertNotNull($createdCommissionView['id'], 'Created CommissionView must have id specified');
        $this->assertNotNull(CommissionView::find($createdCommissionView['id']), 'CommissionView with given id must be in DB');
        $this->assertModelData($commissionView, $createdCommissionView);
    }

    /**
     * @test read
     */
    public function testReadCommissionView()
    {
        $commissionView = $this->makeCommissionView();
        $dbCommissionView = $this->commissionViewRepo->find($commissionView->id);
        $dbCommissionView = $dbCommissionView->toArray();
        $this->assertModelData($commissionView->toArray(), $dbCommissionView);
    }

    /**
     * @test update
     */
    public function testUpdateCommissionView()
    {
        $commissionView = $this->makeCommissionView();
        $fakeCommissionView = $this->fakeCommissionViewData();
        $updatedCommissionView = $this->commissionViewRepo->update($fakeCommissionView, $commissionView->id);
        $this->assertModelData($fakeCommissionView, $updatedCommissionView->toArray());
        $dbCommissionView = $this->commissionViewRepo->find($commissionView->id);
        $this->assertModelData($fakeCommissionView, $dbCommissionView->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCommissionView()
    {
        $commissionView = $this->makeCommissionView();
        $resp = $this->commissionViewRepo->delete($commissionView->id);
        $this->assertTrue($resp);
        $this->assertNull(CommissionView::find($commissionView->id), 'CommissionView should not exist in DB');
    }
}
