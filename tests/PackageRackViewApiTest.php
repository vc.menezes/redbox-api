<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackageRackViewApiTest extends TestCase
{
    use MakePackageRackViewTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePackageRackView()
    {
        $packageRackView = $this->fakePackageRackViewData();
        $this->json('POST', '/api/v1/packageRackViews', $packageRackView);

        $this->assertApiResponse($packageRackView);
    }

    /**
     * @test
     */
    public function testReadPackageRackView()
    {
        $packageRackView = $this->makePackageRackView();
        $this->json('GET', '/api/v1/packageRackViews/'.$packageRackView->id);

        $this->assertApiResponse($packageRackView->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePackageRackView()
    {
        $packageRackView = $this->makePackageRackView();
        $editedPackageRackView = $this->fakePackageRackViewData();

        $this->json('PUT', '/api/v1/packageRackViews/'.$packageRackView->id, $editedPackageRackView);

        $this->assertApiResponse($editedPackageRackView);
    }

    /**
     * @test
     */
    public function testDeletePackageRackView()
    {
        $packageRackView = $this->makePackageRackView();
        $this->json('DELETE', '/api/v1/packageRackViews/'.$packageRackView->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packageRackViews/'.$packageRackView->id);

        $this->assertResponseStatus(404);
    }
}
