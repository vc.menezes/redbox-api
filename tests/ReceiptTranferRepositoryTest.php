<?php

use App\Models\ReceiptTranfer;
use App\Repositories\ReceiptTranferRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReceiptTranferRepositoryTest extends TestCase
{
    use MakeReceiptTranferTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ReceiptTranferRepository
     */
    protected $receiptTranferRepo;

    public function setUp()
    {
        parent::setUp();
        $this->receiptTranferRepo = App::make(ReceiptTranferRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateReceiptTranfer()
    {
        $receiptTranfer = $this->fakeReceiptTranferData();
        $createdReceiptTranfer = $this->receiptTranferRepo->create($receiptTranfer);
        $createdReceiptTranfer = $createdReceiptTranfer->toArray();
        $this->assertArrayHasKey('id', $createdReceiptTranfer);
        $this->assertNotNull($createdReceiptTranfer['id'], 'Created ReceiptTranfer must have id specified');
        $this->assertNotNull(ReceiptTranfer::find($createdReceiptTranfer['id']), 'ReceiptTranfer with given id must be in DB');
        $this->assertModelData($receiptTranfer, $createdReceiptTranfer);
    }

    /**
     * @test read
     */
    public function testReadReceiptTranfer()
    {
        $receiptTranfer = $this->makeReceiptTranfer();
        $dbReceiptTranfer = $this->receiptTranferRepo->find($receiptTranfer->id);
        $dbReceiptTranfer = $dbReceiptTranfer->toArray();
        $this->assertModelData($receiptTranfer->toArray(), $dbReceiptTranfer);
    }

    /**
     * @test update
     */
    public function testUpdateReceiptTranfer()
    {
        $receiptTranfer = $this->makeReceiptTranfer();
        $fakeReceiptTranfer = $this->fakeReceiptTranferData();
        $updatedReceiptTranfer = $this->receiptTranferRepo->update($fakeReceiptTranfer, $receiptTranfer->id);
        $this->assertModelData($fakeReceiptTranfer, $updatedReceiptTranfer->toArray());
        $dbReceiptTranfer = $this->receiptTranferRepo->find($receiptTranfer->id);
        $this->assertModelData($fakeReceiptTranfer, $dbReceiptTranfer->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteReceiptTranfer()
    {
        $receiptTranfer = $this->makeReceiptTranfer();
        $resp = $this->receiptTranferRepo->delete($receiptTranfer->id);
        $this->assertTrue($resp);
        $this->assertNull(ReceiptTranfer::find($receiptTranfer->id), 'ReceiptTranfer should not exist in DB');
    }
}
