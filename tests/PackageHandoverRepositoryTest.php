<?php

use App\Models\PackageHandover;
use App\Repositories\PackageHandoverRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackageHandoverRepositoryTest extends TestCase
{
    use MakePackageHandoverTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PackageHandoverRepository
     */
    protected $packageHandoverRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packageHandoverRepo = App::make(PackageHandoverRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePackageHandover()
    {
        $packageHandover = $this->fakePackageHandoverData();
        $createdPackageHandover = $this->packageHandoverRepo->create($packageHandover);
        $createdPackageHandover = $createdPackageHandover->toArray();
        $this->assertArrayHasKey('id', $createdPackageHandover);
        $this->assertNotNull($createdPackageHandover['id'], 'Created PackageHandover must have id specified');
        $this->assertNotNull(PackageHandover::find($createdPackageHandover['id']), 'PackageHandover with given id must be in DB');
        $this->assertModelData($packageHandover, $createdPackageHandover);
    }

    /**
     * @test read
     */
    public function testReadPackageHandover()
    {
        $packageHandover = $this->makePackageHandover();
        $dbPackageHandover = $this->packageHandoverRepo->find($packageHandover->id);
        $dbPackageHandover = $dbPackageHandover->toArray();
        $this->assertModelData($packageHandover->toArray(), $dbPackageHandover);
    }

    /**
     * @test update
     */
    public function testUpdatePackageHandover()
    {
        $packageHandover = $this->makePackageHandover();
        $fakePackageHandover = $this->fakePackageHandoverData();
        $updatedPackageHandover = $this->packageHandoverRepo->update($fakePackageHandover, $packageHandover->id);
        $this->assertModelData($fakePackageHandover, $updatedPackageHandover->toArray());
        $dbPackageHandover = $this->packageHandoverRepo->find($packageHandover->id);
        $this->assertModelData($fakePackageHandover, $dbPackageHandover->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePackageHandover()
    {
        $packageHandover = $this->makePackageHandover();
        $resp = $this->packageHandoverRepo->delete($packageHandover->id);
        $this->assertTrue($resp);
        $this->assertNull(PackageHandover::find($packageHandover->id), 'PackageHandover should not exist in DB');
    }
}
