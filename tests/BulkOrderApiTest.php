<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BulkOrderApiTest extends TestCase
{
    use MakeBulkOrderTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBulkOrder()
    {
        $bulkOrder = $this->fakeBulkOrderData();
        $this->json('POST', '/api/v1/bulkOrders', $bulkOrder);

        $this->assertApiResponse($bulkOrder);
    }

    /**
     * @test
     */
    public function testReadBulkOrder()
    {
        $bulkOrder = $this->makeBulkOrder();
        $this->json('GET', '/api/v1/bulkOrders/'.$bulkOrder->id);

        $this->assertApiResponse($bulkOrder->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBulkOrder()
    {
        $bulkOrder = $this->makeBulkOrder();
        $editedBulkOrder = $this->fakeBulkOrderData();

        $this->json('PUT', '/api/v1/bulkOrders/'.$bulkOrder->id, $editedBulkOrder);

        $this->assertApiResponse($editedBulkOrder);
    }

    /**
     * @test
     */
    public function testDeleteBulkOrder()
    {
        $bulkOrder = $this->makeBulkOrder();
        $this->json('DELETE', '/api/v1/bulkOrders/'.$bulkOrder->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/bulkOrders/'.$bulkOrder->id);

        $this->assertResponseStatus(404);
    }
}
