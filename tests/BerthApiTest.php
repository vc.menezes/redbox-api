<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BerthApiTest extends TestCase
{
    use MakeBerthTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBerth()
    {
        $berth = $this->fakeBerthData();
        $this->json('POST', '/api/v1/berths', $berth);

        $this->assertApiResponse($berth);
    }

    /**
     * @test
     */
    public function testReadBerth()
    {
        $berth = $this->makeBerth();
        $this->json('GET', '/api/v1/berths/'.$berth->id);

        $this->assertApiResponse($berth->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBerth()
    {
        $berth = $this->makeBerth();
        $editedBerth = $this->fakeBerthData();

        $this->json('PUT', '/api/v1/berths/'.$berth->id, $editedBerth);

        $this->assertApiResponse($editedBerth);
    }

    /**
     * @test
     */
    public function testDeleteBerth()
    {
        $berth = $this->makeBerth();
        $this->json('DELETE', '/api/v1/berths/'.$berth->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/berths/'.$berth->id);

        $this->assertResponseStatus(404);
    }
}
