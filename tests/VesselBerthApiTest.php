<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VesselBerthApiTest extends TestCase
{
    use MakeVesselBerthTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateVesselBerth()
    {
        $vesselBerth = $this->fakeVesselBerthData();
        $this->json('POST', '/api/v1/vesselBerths', $vesselBerth);

        $this->assertApiResponse($vesselBerth);
    }

    /**
     * @test
     */
    public function testReadVesselBerth()
    {
        $vesselBerth = $this->makeVesselBerth();
        $this->json('GET', '/api/v1/vesselBerths/'.$vesselBerth->id);

        $this->assertApiResponse($vesselBerth->toArray());
    }

    /**
     * @test
     */
    public function testUpdateVesselBerth()
    {
        $vesselBerth = $this->makeVesselBerth();
        $editedVesselBerth = $this->fakeVesselBerthData();

        $this->json('PUT', '/api/v1/vesselBerths/'.$vesselBerth->id, $editedVesselBerth);

        $this->assertApiResponse($editedVesselBerth);
    }

    /**
     * @test
     */
    public function testDeleteVesselBerth()
    {
        $vesselBerth = $this->makeVesselBerth();
        $this->json('DELETE', '/api/v1/vesselBerths/'.$vesselBerth->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/vesselBerths/'.$vesselBerth->id);

        $this->assertResponseStatus(404);
    }
}
