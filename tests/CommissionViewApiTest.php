<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommissionViewApiTest extends TestCase
{
    use MakeCommissionViewTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCommissionView()
    {
        $commissionView = $this->fakeCommissionViewData();
        $this->json('POST', '/api/v1/commissionViews', $commissionView);

        $this->assertApiResponse($commissionView);
    }

    /**
     * @test
     */
    public function testReadCommissionView()
    {
        $commissionView = $this->makeCommissionView();
        $this->json('GET', '/api/v1/commissionViews/'.$commissionView->id);

        $this->assertApiResponse($commissionView->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCommissionView()
    {
        $commissionView = $this->makeCommissionView();
        $editedCommissionView = $this->fakeCommissionViewData();

        $this->json('PUT', '/api/v1/commissionViews/'.$commissionView->id, $editedCommissionView);

        $this->assertApiResponse($editedCommissionView);
    }

    /**
     * @test
     */
    public function testDeleteCommissionView()
    {
        $commissionView = $this->makeCommissionView();
        $this->json('DELETE', '/api/v1/commissionViews/'.$commissionView->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/commissionViews/'.$commissionView->id);

        $this->assertResponseStatus(404);
    }
}
