<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VesselTripDetailApiTest extends TestCase
{
    use MakeVesselTripDetailTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateVesselTripDetail()
    {
        $vesselTripDetail = $this->fakeVesselTripDetailData();
        $this->json('POST', '/api/v1/vesselTripDetails', $vesselTripDetail);

        $this->assertApiResponse($vesselTripDetail);
    }

    /**
     * @test
     */
    public function testReadVesselTripDetail()
    {
        $vesselTripDetail = $this->makeVesselTripDetail();
        $this->json('GET', '/api/v1/vesselTripDetails/'.$vesselTripDetail->id);

        $this->assertApiResponse($vesselTripDetail->toArray());
    }

    /**
     * @test
     */
    public function testUpdateVesselTripDetail()
    {
        $vesselTripDetail = $this->makeVesselTripDetail();
        $editedVesselTripDetail = $this->fakeVesselTripDetailData();

        $this->json('PUT', '/api/v1/vesselTripDetails/'.$vesselTripDetail->id, $editedVesselTripDetail);

        $this->assertApiResponse($editedVesselTripDetail);
    }

    /**
     * @test
     */
    public function testDeleteVesselTripDetail()
    {
        $vesselTripDetail = $this->makeVesselTripDetail();
        $this->json('DELETE', '/api/v1/vesselTripDetails/'.$vesselTripDetail->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/vesselTripDetails/'.$vesselTripDetail->id);

        $this->assertResponseStatus(404);
    }
}
