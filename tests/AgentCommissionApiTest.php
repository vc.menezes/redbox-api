<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AgentCommissionApiTest extends TestCase
{
    use MakeAgentCommissionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAgentCommission()
    {
        $agentCommission = $this->fakeAgentCommissionData();
        $this->json('POST', '/api/v1/agentCommissions', $agentCommission);

        $this->assertApiResponse($agentCommission);
    }

    /**
     * @test
     */
    public function testReadAgentCommission()
    {
        $agentCommission = $this->makeAgentCommission();
        $this->json('GET', '/api/v1/agentCommissions/'.$agentCommission->id);

        $this->assertApiResponse($agentCommission->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAgentCommission()
    {
        $agentCommission = $this->makeAgentCommission();
        $editedAgentCommission = $this->fakeAgentCommissionData();

        $this->json('PUT', '/api/v1/agentCommissions/'.$agentCommission->id, $editedAgentCommission);

        $this->assertApiResponse($editedAgentCommission);
    }

    /**
     * @test
     */
    public function testDeleteAgentCommission()
    {
        $agentCommission = $this->makeAgentCommission();
        $this->json('DELETE', '/api/v1/agentCommissions/'.$agentCommission->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/agentCommissions/'.$agentCommission->id);

        $this->assertResponseStatus(404);
    }
}
