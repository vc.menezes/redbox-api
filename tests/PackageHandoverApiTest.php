<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackageHandoverApiTest extends TestCase
{
    use MakePackageHandoverTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePackageHandover()
    {
        $packageHandover = $this->fakePackageHandoverData();
        $this->json('POST', '/api/v1/packageHandovers', $packageHandover);

        $this->assertApiResponse($packageHandover);
    }

    /**
     * @test
     */
    public function testReadPackageHandover()
    {
        $packageHandover = $this->makePackageHandover();
        $this->json('GET', '/api/v1/packageHandovers/'.$packageHandover->id);

        $this->assertApiResponse($packageHandover->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePackageHandover()
    {
        $packageHandover = $this->makePackageHandover();
        $editedPackageHandover = $this->fakePackageHandoverData();

        $this->json('PUT', '/api/v1/packageHandovers/'.$packageHandover->id, $editedPackageHandover);

        $this->assertApiResponse($editedPackageHandover);
    }

    /**
     * @test
     */
    public function testDeletePackageHandover()
    {
        $packageHandover = $this->makePackageHandover();
        $this->json('DELETE', '/api/v1/packageHandovers/'.$packageHandover->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packageHandovers/'.$packageHandover->id);

        $this->assertResponseStatus(404);
    }
}
