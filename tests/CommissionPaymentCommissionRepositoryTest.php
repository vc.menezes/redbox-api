<?php

use App\Models\CommissionPaymentCommission;
use App\Repositories\CommissionPaymentCommissionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommissionPaymentCommissionRepositoryTest extends TestCase
{
    use MakeCommissionPaymentCommissionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CommissionPaymentCommissionRepository
     */
    protected $commissionPaymentCommissionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->commissionPaymentCommissionRepo = App::make(CommissionPaymentCommissionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCommissionPaymentCommission()
    {
        $commissionPaymentCommission = $this->fakeCommissionPaymentCommissionData();
        $createdCommissionPaymentCommission = $this->commissionPaymentCommissionRepo->create($commissionPaymentCommission);
        $createdCommissionPaymentCommission = $createdCommissionPaymentCommission->toArray();
        $this->assertArrayHasKey('id', $createdCommissionPaymentCommission);
        $this->assertNotNull($createdCommissionPaymentCommission['id'], 'Created CommissionPaymentCommission must have id specified');
        $this->assertNotNull(CommissionPaymentCommission::find($createdCommissionPaymentCommission['id']), 'CommissionPaymentCommission with given id must be in DB');
        $this->assertModelData($commissionPaymentCommission, $createdCommissionPaymentCommission);
    }

    /**
     * @test read
     */
    public function testReadCommissionPaymentCommission()
    {
        $commissionPaymentCommission = $this->makeCommissionPaymentCommission();
        $dbCommissionPaymentCommission = $this->commissionPaymentCommissionRepo->find($commissionPaymentCommission->id);
        $dbCommissionPaymentCommission = $dbCommissionPaymentCommission->toArray();
        $this->assertModelData($commissionPaymentCommission->toArray(), $dbCommissionPaymentCommission);
    }

    /**
     * @test update
     */
    public function testUpdateCommissionPaymentCommission()
    {
        $commissionPaymentCommission = $this->makeCommissionPaymentCommission();
        $fakeCommissionPaymentCommission = $this->fakeCommissionPaymentCommissionData();
        $updatedCommissionPaymentCommission = $this->commissionPaymentCommissionRepo->update($fakeCommissionPaymentCommission, $commissionPaymentCommission->id);
        $this->assertModelData($fakeCommissionPaymentCommission, $updatedCommissionPaymentCommission->toArray());
        $dbCommissionPaymentCommission = $this->commissionPaymentCommissionRepo->find($commissionPaymentCommission->id);
        $this->assertModelData($fakeCommissionPaymentCommission, $dbCommissionPaymentCommission->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCommissionPaymentCommission()
    {
        $commissionPaymentCommission = $this->makeCommissionPaymentCommission();
        $resp = $this->commissionPaymentCommissionRepo->delete($commissionPaymentCommission->id);
        $this->assertTrue($resp);
        $this->assertNull(CommissionPaymentCommission::find($commissionPaymentCommission->id), 'CommissionPaymentCommission should not exist in DB');
    }
}
