<?php

use App\Models\Addressable;
use App\Repositories\AddressableRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AddressableRepositoryTest extends TestCase
{
    use MakeAddressableTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AddressableRepository
     */
    protected $addressableRepo;

    public function setUp()
    {
        parent::setUp();
        $this->addressableRepo = App::make(AddressableRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAddressable()
    {
        $addressable = $this->fakeAddressableData();
        $createdAddressable = $this->addressableRepo->create($addressable);
        $createdAddressable = $createdAddressable->toArray();
        $this->assertArrayHasKey('id', $createdAddressable);
        $this->assertNotNull($createdAddressable['id'], 'Created Addressable must have id specified');
        $this->assertNotNull(Addressable::find($createdAddressable['id']), 'Addressable with given id must be in DB');
        $this->assertModelData($addressable, $createdAddressable);
    }

    /**
     * @test read
     */
    public function testReadAddressable()
    {
        $addressable = $this->makeAddressable();
        $dbAddressable = $this->addressableRepo->find($addressable->id);
        $dbAddressable = $dbAddressable->toArray();
        $this->assertModelData($addressable->toArray(), $dbAddressable);
    }

    /**
     * @test update
     */
    public function testUpdateAddressable()
    {
        $addressable = $this->makeAddressable();
        $fakeAddressable = $this->fakeAddressableData();
        $updatedAddressable = $this->addressableRepo->update($fakeAddressable, $addressable->id);
        $this->assertModelData($fakeAddressable, $updatedAddressable->toArray());
        $dbAddressable = $this->addressableRepo->find($addressable->id);
        $this->assertModelData($fakeAddressable, $dbAddressable->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAddressable()
    {
        $addressable = $this->makeAddressable();
        $resp = $this->addressableRepo->delete($addressable->id);
        $this->assertTrue($resp);
        $this->assertNull(Addressable::find($addressable->id), 'Addressable should not exist in DB');
    }
}
