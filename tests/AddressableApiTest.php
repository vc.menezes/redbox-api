<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AddressableApiTest extends TestCase
{
    use MakeAddressableTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAddressable()
    {
        $addressable = $this->fakeAddressableData();
        $this->json('POST', '/api/v1/addressables', $addressable);

        $this->assertApiResponse($addressable);
    }

    /**
     * @test
     */
    public function testReadAddressable()
    {
        $addressable = $this->makeAddressable();
        $this->json('GET', '/api/v1/addressables/'.$addressable->id);

        $this->assertApiResponse($addressable->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAddressable()
    {
        $addressable = $this->makeAddressable();
        $editedAddressable = $this->fakeAddressableData();

        $this->json('PUT', '/api/v1/addressables/'.$addressable->id, $editedAddressable);

        $this->assertApiResponse($editedAddressable);
    }

    /**
     * @test
     */
    public function testDeleteAddressable()
    {
        $addressable = $this->makeAddressable();
        $this->json('DELETE', '/api/v1/addressables/'.$addressable->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/addressables/'.$addressable->id);

        $this->assertResponseStatus(404);
    }
}
