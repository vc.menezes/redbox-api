<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InvoiceReceiptApiTest extends TestCase
{
    use MakeInvoiceReceiptTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateInvoiceReceipt()
    {
        $invoiceReceipt = $this->fakeInvoiceReceiptData();
        $this->json('POST', '/api/v1/invoiceReceipts', $invoiceReceipt);

        $this->assertApiResponse($invoiceReceipt);
    }

    /**
     * @test
     */
    public function testReadInvoiceReceipt()
    {
        $invoiceReceipt = $this->makeInvoiceReceipt();
        $this->json('GET', '/api/v1/invoiceReceipts/'.$invoiceReceipt->id);

        $this->assertApiResponse($invoiceReceipt->toArray());
    }

    /**
     * @test
     */
    public function testUpdateInvoiceReceipt()
    {
        $invoiceReceipt = $this->makeInvoiceReceipt();
        $editedInvoiceReceipt = $this->fakeInvoiceReceiptData();

        $this->json('PUT', '/api/v1/invoiceReceipts/'.$invoiceReceipt->id, $editedInvoiceReceipt);

        $this->assertApiResponse($editedInvoiceReceipt);
    }

    /**
     * @test
     */
    public function testDeleteInvoiceReceipt()
    {
        $invoiceReceipt = $this->makeInvoiceReceipt();
        $this->json('DELETE', '/api/v1/invoiceReceipts/'.$invoiceReceipt->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/invoiceReceipts/'.$invoiceReceipt->id);

        $this->assertResponseStatus(404);
    }
}
