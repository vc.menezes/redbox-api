<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommissionPaymentApiTest extends TestCase
{
    use MakeCommissionPaymentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCommissionPayment()
    {
        $commissionPayment = $this->fakeCommissionPaymentData();
        $this->json('POST', '/api/v1/commissionPayments', $commissionPayment);

        $this->assertApiResponse($commissionPayment);
    }

    /**
     * @test
     */
    public function testReadCommissionPayment()
    {
        $commissionPayment = $this->makeCommissionPayment();
        $this->json('GET', '/api/v1/commissionPayments/'.$commissionPayment->id);

        $this->assertApiResponse($commissionPayment->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCommissionPayment()
    {
        $commissionPayment = $this->makeCommissionPayment();
        $editedCommissionPayment = $this->fakeCommissionPaymentData();

        $this->json('PUT', '/api/v1/commissionPayments/'.$commissionPayment->id, $editedCommissionPayment);

        $this->assertApiResponse($editedCommissionPayment);
    }

    /**
     * @test
     */
    public function testDeleteCommissionPayment()
    {
        $commissionPayment = $this->makeCommissionPayment();
        $this->json('DELETE', '/api/v1/commissionPayments/'.$commissionPayment->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/commissionPayments/'.$commissionPayment->id);

        $this->assertResponseStatus(404);
    }
}
