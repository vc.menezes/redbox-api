<?php

use App\Models\Signature;
use App\Repositories\SignatureRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SignatureRepositoryTest extends TestCase
{
    use MakeSignatureTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SignatureRepository
     */
    protected $signatureRepo;

    public function setUp()
    {
        parent::setUp();
        $this->signatureRepo = App::make(SignatureRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSignature()
    {
        $signature = $this->fakeSignatureData();
        $createdSignature = $this->signatureRepo->create($signature);
        $createdSignature = $createdSignature->toArray();
        $this->assertArrayHasKey('id', $createdSignature);
        $this->assertNotNull($createdSignature['id'], 'Created Signature must have id specified');
        $this->assertNotNull(Signature::find($createdSignature['id']), 'Signature with given id must be in DB');
        $this->assertModelData($signature, $createdSignature);
    }

    /**
     * @test read
     */
    public function testReadSignature()
    {
        $signature = $this->makeSignature();
        $dbSignature = $this->signatureRepo->find($signature->id);
        $dbSignature = $dbSignature->toArray();
        $this->assertModelData($signature->toArray(), $dbSignature);
    }

    /**
     * @test update
     */
    public function testUpdateSignature()
    {
        $signature = $this->makeSignature();
        $fakeSignature = $this->fakeSignatureData();
        $updatedSignature = $this->signatureRepo->update($fakeSignature, $signature->id);
        $this->assertModelData($fakeSignature, $updatedSignature->toArray());
        $dbSignature = $this->signatureRepo->find($signature->id);
        $this->assertModelData($fakeSignature, $dbSignature->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSignature()
    {
        $signature = $this->makeSignature();
        $resp = $this->signatureRepo->delete($signature->id);
        $this->assertTrue($resp);
        $this->assertNull(Signature::find($signature->id), 'Signature should not exist in DB');
    }
}
