<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AttachableApiTest extends TestCase
{
    use MakeAttachableTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAttachable()
    {
        $attachable = $this->fakeAttachableData();
        $this->json('POST', '/api/v1/attachables', $attachable);

        $this->assertApiResponse($attachable);
    }

    /**
     * @test
     */
    public function testReadAttachable()
    {
        $attachable = $this->makeAttachable();
        $this->json('GET', '/api/v1/attachables/'.$attachable->id);

        $this->assertApiResponse($attachable->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAttachable()
    {
        $attachable = $this->makeAttachable();
        $editedAttachable = $this->fakeAttachableData();

        $this->json('PUT', '/api/v1/attachables/'.$attachable->id, $editedAttachable);

        $this->assertApiResponse($editedAttachable);
    }

    /**
     * @test
     */
    public function testDeleteAttachable()
    {
        $attachable = $this->makeAttachable();
        $this->json('DELETE', '/api/v1/attachables/'.$attachable->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/attachables/'.$attachable->id);

        $this->assertResponseStatus(404);
    }
}
