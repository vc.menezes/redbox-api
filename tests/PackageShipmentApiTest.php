<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackageShipmentApiTest extends TestCase
{
    use MakePackageShipmentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePackageShipment()
    {
        $packageShipment = $this->fakePackageShipmentData();
        $this->json('POST', '/api/v1/packageShipments', $packageShipment);

        $this->assertApiResponse($packageShipment);
    }

    /**
     * @test
     */
    public function testReadPackageShipment()
    {
        $packageShipment = $this->makePackageShipment();
        $this->json('GET', '/api/v1/packageShipments/'.$packageShipment->id);

        $this->assertApiResponse($packageShipment->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePackageShipment()
    {
        $packageShipment = $this->makePackageShipment();
        $editedPackageShipment = $this->fakePackageShipmentData();

        $this->json('PUT', '/api/v1/packageShipments/'.$packageShipment->id, $editedPackageShipment);

        $this->assertApiResponse($editedPackageShipment);
    }

    /**
     * @test
     */
    public function testDeletePackageShipment()
    {
        $packageShipment = $this->makePackageShipment();
        $this->json('DELETE', '/api/v1/packageShipments/'.$packageShipment->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packageShipments/'.$packageShipment->id);

        $this->assertResponseStatus(404);
    }
}
