<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommodityCategoryApiTest extends TestCase
{
    use MakeCommodityCategoryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCommodityCategory()
    {
        $commodityCategory = $this->fakeCommodityCategoryData();
        $this->json('POST', '/api/v1/commodityCategories', $commodityCategory);

        $this->assertApiResponse($commodityCategory);
    }

    /**
     * @test
     */
    public function testReadCommodityCategory()
    {
        $commodityCategory = $this->makeCommodityCategory();
        $this->json('GET', '/api/v1/commodityCategories/'.$commodityCategory->id);

        $this->assertApiResponse($commodityCategory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCommodityCategory()
    {
        $commodityCategory = $this->makeCommodityCategory();
        $editedCommodityCategory = $this->fakeCommodityCategoryData();

        $this->json('PUT', '/api/v1/commodityCategories/'.$commodityCategory->id, $editedCommodityCategory);

        $this->assertApiResponse($editedCommodityCategory);
    }

    /**
     * @test
     */
    public function testDeleteCommodityCategory()
    {
        $commodityCategory = $this->makeCommodityCategory();
        $this->json('DELETE', '/api/v1/commodityCategories/'.$commodityCategory->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/commodityCategories/'.$commodityCategory->id);

        $this->assertResponseStatus(404);
    }
}
