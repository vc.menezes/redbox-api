<?php

use App\Models\ReceiptCheque;
use App\Repositories\ReceiptChequeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReceiptChequeRepositoryTest extends TestCase
{
    use MakeReceiptChequeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ReceiptChequeRepository
     */
    protected $receiptChequeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->receiptChequeRepo = App::make(ReceiptChequeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateReceiptCheque()
    {
        $receiptCheque = $this->fakeReceiptChequeData();
        $createdReceiptCheque = $this->receiptChequeRepo->create($receiptCheque);
        $createdReceiptCheque = $createdReceiptCheque->toArray();
        $this->assertArrayHasKey('id', $createdReceiptCheque);
        $this->assertNotNull($createdReceiptCheque['id'], 'Created ReceiptCheque must have id specified');
        $this->assertNotNull(ReceiptCheque::find($createdReceiptCheque['id']), 'ReceiptCheque with given id must be in DB');
        $this->assertModelData($receiptCheque, $createdReceiptCheque);
    }

    /**
     * @test read
     */
    public function testReadReceiptCheque()
    {
        $receiptCheque = $this->makeReceiptCheque();
        $dbReceiptCheque = $this->receiptChequeRepo->find($receiptCheque->id);
        $dbReceiptCheque = $dbReceiptCheque->toArray();
        $this->assertModelData($receiptCheque->toArray(), $dbReceiptCheque);
    }

    /**
     * @test update
     */
    public function testUpdateReceiptCheque()
    {
        $receiptCheque = $this->makeReceiptCheque();
        $fakeReceiptCheque = $this->fakeReceiptChequeData();
        $updatedReceiptCheque = $this->receiptChequeRepo->update($fakeReceiptCheque, $receiptCheque->id);
        $this->assertModelData($fakeReceiptCheque, $updatedReceiptCheque->toArray());
        $dbReceiptCheque = $this->receiptChequeRepo->find($receiptCheque->id);
        $this->assertModelData($fakeReceiptCheque, $dbReceiptCheque->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteReceiptCheque()
    {
        $receiptCheque = $this->makeReceiptCheque();
        $resp = $this->receiptChequeRepo->delete($receiptCheque->id);
        $this->assertTrue($resp);
        $this->assertNull(ReceiptCheque::find($receiptCheque->id), 'ReceiptCheque should not exist in DB');
    }
}
