<?php

use App\Models\StorageBlock;
use App\Repositories\StorageBlockRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StorageBlockRepositoryTest extends TestCase
{
    use MakeStorageBlockTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StorageBlockRepository
     */
    protected $storageBlockRepo;

    public function setUp()
    {
        parent::setUp();
        $this->storageBlockRepo = App::make(StorageBlockRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateStorageBlock()
    {
        $storageBlock = $this->fakeStorageBlockData();
        $createdStorageBlock = $this->storageBlockRepo->create($storageBlock);
        $createdStorageBlock = $createdStorageBlock->toArray();
        $this->assertArrayHasKey('id', $createdStorageBlock);
        $this->assertNotNull($createdStorageBlock['id'], 'Created StorageBlock must have id specified');
        $this->assertNotNull(StorageBlock::find($createdStorageBlock['id']), 'StorageBlock with given id must be in DB');
        $this->assertModelData($storageBlock, $createdStorageBlock);
    }

    /**
     * @test read
     */
    public function testReadStorageBlock()
    {
        $storageBlock = $this->makeStorageBlock();
        $dbStorageBlock = $this->storageBlockRepo->find($storageBlock->id);
        $dbStorageBlock = $dbStorageBlock->toArray();
        $this->assertModelData($storageBlock->toArray(), $dbStorageBlock);
    }

    /**
     * @test update
     */
    public function testUpdateStorageBlock()
    {
        $storageBlock = $this->makeStorageBlock();
        $fakeStorageBlock = $this->fakeStorageBlockData();
        $updatedStorageBlock = $this->storageBlockRepo->update($fakeStorageBlock, $storageBlock->id);
        $this->assertModelData($fakeStorageBlock, $updatedStorageBlock->toArray());
        $dbStorageBlock = $this->storageBlockRepo->find($storageBlock->id);
        $this->assertModelData($fakeStorageBlock, $dbStorageBlock->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteStorageBlock()
    {
        $storageBlock = $this->makeStorageBlock();
        $resp = $this->storageBlockRepo->delete($storageBlock->id);
        $this->assertTrue($resp);
        $this->assertNull(StorageBlock::find($storageBlock->id), 'StorageBlock should not exist in DB');
    }
}
