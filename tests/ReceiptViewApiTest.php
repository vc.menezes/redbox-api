<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReceiptViewApiTest extends TestCase
{
    use MakeReceiptViewTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateReceiptView()
    {
        $receiptView = $this->fakeReceiptViewData();
        $this->json('POST', '/api/v1/receiptViews', $receiptView);

        $this->assertApiResponse($receiptView);
    }

    /**
     * @test
     */
    public function testReadReceiptView()
    {
        $receiptView = $this->makeReceiptView();
        $this->json('GET', '/api/v1/receiptViews/'.$receiptView->id);

        $this->assertApiResponse($receiptView->toArray());
    }

    /**
     * @test
     */
    public function testUpdateReceiptView()
    {
        $receiptView = $this->makeReceiptView();
        $editedReceiptView = $this->fakeReceiptViewData();

        $this->json('PUT', '/api/v1/receiptViews/'.$receiptView->id, $editedReceiptView);

        $this->assertApiResponse($editedReceiptView);
    }

    /**
     * @test
     */
    public function testDeleteReceiptView()
    {
        $receiptView = $this->makeReceiptView();
        $this->json('DELETE', '/api/v1/receiptViews/'.$receiptView->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/receiptViews/'.$receiptView->id);

        $this->assertResponseStatus(404);
    }
}
