<?php

use App\Models\Berth;
use App\Repositories\BerthRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BerthRepositoryTest extends TestCase
{
    use MakeBerthTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BerthRepository
     */
    protected $berthRepo;

    public function setUp()
    {
        parent::setUp();
        $this->berthRepo = App::make(BerthRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBerth()
    {
        $berth = $this->fakeBerthData();
        $createdBerth = $this->berthRepo->create($berth);
        $createdBerth = $createdBerth->toArray();
        $this->assertArrayHasKey('id', $createdBerth);
        $this->assertNotNull($createdBerth['id'], 'Created Berth must have id specified');
        $this->assertNotNull(Berth::find($createdBerth['id']), 'Berth with given id must be in DB');
        $this->assertModelData($berth, $createdBerth);
    }

    /**
     * @test read
     */
    public function testReadBerth()
    {
        $berth = $this->makeBerth();
        $dbBerth = $this->berthRepo->find($berth->id);
        $dbBerth = $dbBerth->toArray();
        $this->assertModelData($berth->toArray(), $dbBerth);
    }

    /**
     * @test update
     */
    public function testUpdateBerth()
    {
        $berth = $this->makeBerth();
        $fakeBerth = $this->fakeBerthData();
        $updatedBerth = $this->berthRepo->update($fakeBerth, $berth->id);
        $this->assertModelData($fakeBerth, $updatedBerth->toArray());
        $dbBerth = $this->berthRepo->find($berth->id);
        $this->assertModelData($fakeBerth, $dbBerth->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBerth()
    {
        $berth = $this->makeBerth();
        $resp = $this->berthRepo->delete($berth->id);
        $this->assertTrue($resp);
        $this->assertNull(Berth::find($berth->id), 'Berth should not exist in DB');
    }
}
