<?php

use App\Models\CreditLimit;
use App\Repositories\CreditLimitRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreditLimitRepositoryTest extends TestCase
{
    use MakeCreditLimitTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CreditLimitRepository
     */
    protected $creditLimitRepo;

    public function setUp()
    {
        parent::setUp();
        $this->creditLimitRepo = App::make(CreditLimitRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCreditLimit()
    {
        $creditLimit = $this->fakeCreditLimitData();
        $createdCreditLimit = $this->creditLimitRepo->create($creditLimit);
        $createdCreditLimit = $createdCreditLimit->toArray();
        $this->assertArrayHasKey('id', $createdCreditLimit);
        $this->assertNotNull($createdCreditLimit['id'], 'Created CreditLimit must have id specified');
        $this->assertNotNull(CreditLimit::find($createdCreditLimit['id']), 'CreditLimit with given id must be in DB');
        $this->assertModelData($creditLimit, $createdCreditLimit);
    }

    /**
     * @test read
     */
    public function testReadCreditLimit()
    {
        $creditLimit = $this->makeCreditLimit();
        $dbCreditLimit = $this->creditLimitRepo->find($creditLimit->id);
        $dbCreditLimit = $dbCreditLimit->toArray();
        $this->assertModelData($creditLimit->toArray(), $dbCreditLimit);
    }

    /**
     * @test update
     */
    public function testUpdateCreditLimit()
    {
        $creditLimit = $this->makeCreditLimit();
        $fakeCreditLimit = $this->fakeCreditLimitData();
        $updatedCreditLimit = $this->creditLimitRepo->update($fakeCreditLimit, $creditLimit->id);
        $this->assertModelData($fakeCreditLimit, $updatedCreditLimit->toArray());
        $dbCreditLimit = $this->creditLimitRepo->find($creditLimit->id);
        $this->assertModelData($fakeCreditLimit, $dbCreditLimit->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCreditLimit()
    {
        $creditLimit = $this->makeCreditLimit();
        $resp = $this->creditLimitRepo->delete($creditLimit->id);
        $this->assertTrue($resp);
        $this->assertNull(CreditLimit::find($creditLimit->id), 'CreditLimit should not exist in DB');
    }
}
