<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReceiptChequeApiTest extends TestCase
{
    use MakeReceiptChequeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateReceiptCheque()
    {
        $receiptCheque = $this->fakeReceiptChequeData();
        $this->json('POST', '/api/v1/receiptCheques', $receiptCheque);

        $this->assertApiResponse($receiptCheque);
    }

    /**
     * @test
     */
    public function testReadReceiptCheque()
    {
        $receiptCheque = $this->makeReceiptCheque();
        $this->json('GET', '/api/v1/receiptCheques/'.$receiptCheque->id);

        $this->assertApiResponse($receiptCheque->toArray());
    }

    /**
     * @test
     */
    public function testUpdateReceiptCheque()
    {
        $receiptCheque = $this->makeReceiptCheque();
        $editedReceiptCheque = $this->fakeReceiptChequeData();

        $this->json('PUT', '/api/v1/receiptCheques/'.$receiptCheque->id, $editedReceiptCheque);

        $this->assertApiResponse($editedReceiptCheque);
    }

    /**
     * @test
     */
    public function testDeleteReceiptCheque()
    {
        $receiptCheque = $this->makeReceiptCheque();
        $this->json('DELETE', '/api/v1/receiptCheques/'.$receiptCheque->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/receiptCheques/'.$receiptCheque->id);

        $this->assertResponseStatus(404);
    }
}
