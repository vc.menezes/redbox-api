<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StorageBlockApiTest extends TestCase
{
    use MakeStorageBlockTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateStorageBlock()
    {
        $storageBlock = $this->fakeStorageBlockData();
        $this->json('POST', '/api/v1/storageBlocks', $storageBlock);

        $this->assertApiResponse($storageBlock);
    }

    /**
     * @test
     */
    public function testReadStorageBlock()
    {
        $storageBlock = $this->makeStorageBlock();
        $this->json('GET', '/api/v1/storageBlocks/'.$storageBlock->id);

        $this->assertApiResponse($storageBlock->toArray());
    }

    /**
     * @test
     */
    public function testUpdateStorageBlock()
    {
        $storageBlock = $this->makeStorageBlock();
        $editedStorageBlock = $this->fakeStorageBlockData();

        $this->json('PUT', '/api/v1/storageBlocks/'.$storageBlock->id, $editedStorageBlock);

        $this->assertApiResponse($editedStorageBlock);
    }

    /**
     * @test
     */
    public function testDeleteStorageBlock()
    {
        $storageBlock = $this->makeStorageBlock();
        $this->json('DELETE', '/api/v1/storageBlocks/'.$storageBlock->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/storageBlocks/'.$storageBlock->id);

        $this->assertResponseStatus(404);
    }
}
