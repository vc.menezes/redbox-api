<?php

use App\Models\BulkOrder;
use App\Repositories\BulkOrderRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BulkOrderRepositoryTest extends TestCase
{
    use MakeBulkOrderTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BulkOrderRepository
     */
    protected $bulkOrderRepo;

    public function setUp()
    {
        parent::setUp();
        $this->bulkOrderRepo = App::make(BulkOrderRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBulkOrder()
    {
        $bulkOrder = $this->fakeBulkOrderData();
        $createdBulkOrder = $this->bulkOrderRepo->create($bulkOrder);
        $createdBulkOrder = $createdBulkOrder->toArray();
        $this->assertArrayHasKey('id', $createdBulkOrder);
        $this->assertNotNull($createdBulkOrder['id'], 'Created BulkOrder must have id specified');
        $this->assertNotNull(BulkOrder::find($createdBulkOrder['id']), 'BulkOrder with given id must be in DB');
        $this->assertModelData($bulkOrder, $createdBulkOrder);
    }

    /**
     * @test read
     */
    public function testReadBulkOrder()
    {
        $bulkOrder = $this->makeBulkOrder();
        $dbBulkOrder = $this->bulkOrderRepo->find($bulkOrder->id);
        $dbBulkOrder = $dbBulkOrder->toArray();
        $this->assertModelData($bulkOrder->toArray(), $dbBulkOrder);
    }

    /**
     * @test update
     */
    public function testUpdateBulkOrder()
    {
        $bulkOrder = $this->makeBulkOrder();
        $fakeBulkOrder = $this->fakeBulkOrderData();
        $updatedBulkOrder = $this->bulkOrderRepo->update($fakeBulkOrder, $bulkOrder->id);
        $this->assertModelData($fakeBulkOrder, $updatedBulkOrder->toArray());
        $dbBulkOrder = $this->bulkOrderRepo->find($bulkOrder->id);
        $this->assertModelData($fakeBulkOrder, $dbBulkOrder->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBulkOrder()
    {
        $bulkOrder = $this->makeBulkOrder();
        $resp = $this->bulkOrderRepo->delete($bulkOrder->id);
        $this->assertTrue($resp);
        $this->assertNull(BulkOrder::find($bulkOrder->id), 'BulkOrder should not exist in DB');
    }
}
