<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackageDestinationApiTest extends TestCase
{
    use MakePackageDestinationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePackageDestination()
    {
        $packageDestination = $this->fakePackageDestinationData();
        $this->json('POST', '/api/v1/packageDestinations', $packageDestination);

        $this->assertApiResponse($packageDestination);
    }

    /**
     * @test
     */
    public function testReadPackageDestination()
    {
        $packageDestination = $this->makePackageDestination();
        $this->json('GET', '/api/v1/packageDestinations/'.$packageDestination->id);

        $this->assertApiResponse($packageDestination->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePackageDestination()
    {
        $packageDestination = $this->makePackageDestination();
        $editedPackageDestination = $this->fakePackageDestinationData();

        $this->json('PUT', '/api/v1/packageDestinations/'.$packageDestination->id, $editedPackageDestination);

        $this->assertApiResponse($editedPackageDestination);
    }

    /**
     * @test
     */
    public function testDeletePackageDestination()
    {
        $packageDestination = $this->makePackageDestination();
        $this->json('DELETE', '/api/v1/packageDestinations/'.$packageDestination->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packageDestinations/'.$packageDestination->id);

        $this->assertResponseStatus(404);
    }
}
