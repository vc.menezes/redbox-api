<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RemarkApiTest extends TestCase
{
    use MakeRemarkTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRemark()
    {
        $remark = $this->fakeRemarkData();
        $this->json('POST', '/api/v1/remarks', $remark);

        $this->assertApiResponse($remark);
    }

    /**
     * @test
     */
    public function testReadRemark()
    {
        $remark = $this->makeRemark();
        $this->json('GET', '/api/v1/remarks/'.$remark->id);

        $this->assertApiResponse($remark->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRemark()
    {
        $remark = $this->makeRemark();
        $editedRemark = $this->fakeRemarkData();

        $this->json('PUT', '/api/v1/remarks/'.$remark->id, $editedRemark);

        $this->assertApiResponse($editedRemark);
    }

    /**
     * @test
     */
    public function testDeleteRemark()
    {
        $remark = $this->makeRemark();
        $this->json('DELETE', '/api/v1/remarks/'.$remark->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/remarks/'.$remark->id);

        $this->assertResponseStatus(404);
    }
}
