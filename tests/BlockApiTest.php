<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BlockApiTest extends TestCase
{
    use MakeBlockTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBlock()
    {
        $block = $this->fakeBlockData();
        $this->json('POST', '/api/v1/blocks', $block);

        $this->assertApiResponse($block);
    }

    /**
     * @test
     */
    public function testReadBlock()
    {
        $block = $this->makeBlock();
        $this->json('GET', '/api/v1/blocks/'.$block->id);

        $this->assertApiResponse($block->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBlock()
    {
        $block = $this->makeBlock();
        $editedBlock = $this->fakeBlockData();

        $this->json('PUT', '/api/v1/blocks/'.$block->id, $editedBlock);

        $this->assertApiResponse($editedBlock);
    }

    /**
     * @test
     */
    public function testDeleteBlock()
    {
        $block = $this->makeBlock();
        $this->json('DELETE', '/api/v1/blocks/'.$block->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/blocks/'.$block->id);

        $this->assertResponseStatus(404);
    }
}
