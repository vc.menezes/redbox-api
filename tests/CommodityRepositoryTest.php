<?php

use App\Models\Commodity;
use App\Repositories\CommodityRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommodityRepositoryTest extends TestCase
{
    use MakeCommodityTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CommodityRepository
     */
    protected $commodityRepo;

    public function setUp()
    {
        parent::setUp();
        $this->commodityRepo = App::make(CommodityRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCommodity()
    {
        $commodity = $this->fakeCommodityData();
        $createdCommodity = $this->commodityRepo->create($commodity);
        $createdCommodity = $createdCommodity->toArray();
        $this->assertArrayHasKey('id', $createdCommodity);
        $this->assertNotNull($createdCommodity['id'], 'Created Commodity must have id specified');
        $this->assertNotNull(Commodity::find($createdCommodity['id']), 'Commodity with given id must be in DB');
        $this->assertModelData($commodity, $createdCommodity);
    }

    /**
     * @test read
     */
    public function testReadCommodity()
    {
        $commodity = $this->makeCommodity();
        $dbCommodity = $this->commodityRepo->find($commodity->id);
        $dbCommodity = $dbCommodity->toArray();
        $this->assertModelData($commodity->toArray(), $dbCommodity);
    }

    /**
     * @test update
     */
    public function testUpdateCommodity()
    {
        $commodity = $this->makeCommodity();
        $fakeCommodity = $this->fakeCommodityData();
        $updatedCommodity = $this->commodityRepo->update($fakeCommodity, $commodity->id);
        $this->assertModelData($fakeCommodity, $updatedCommodity->toArray());
        $dbCommodity = $this->commodityRepo->find($commodity->id);
        $this->assertModelData($fakeCommodity, $dbCommodity->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCommodity()
    {
        $commodity = $this->makeCommodity();
        $resp = $this->commodityRepo->delete($commodity->id);
        $this->assertTrue($resp);
        $this->assertNull(Commodity::find($commodity->id), 'Commodity should not exist in DB');
    }
}
