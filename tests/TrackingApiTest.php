<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TrackingApiTest extends TestCase
{
    use MakeTrackingTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTracking()
    {
        $tracking = $this->fakeTrackingData();
        $this->json('POST', '/api/v1/trackings', $tracking);

        $this->assertApiResponse($tracking);
    }

    /**
     * @test
     */
    public function testReadTracking()
    {
        $tracking = $this->makeTracking();
        $this->json('GET', '/api/v1/trackings/'.$tracking->id);

        $this->assertApiResponse($tracking->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTracking()
    {
        $tracking = $this->makeTracking();
        $editedTracking = $this->fakeTrackingData();

        $this->json('PUT', '/api/v1/trackings/'.$tracking->id, $editedTracking);

        $this->assertApiResponse($editedTracking);
    }

    /**
     * @test
     */
    public function testDeleteTracking()
    {
        $tracking = $this->makeTracking();
        $this->json('DELETE', '/api/v1/trackings/'.$tracking->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/trackings/'.$tracking->id);

        $this->assertResponseStatus(404);
    }
}
