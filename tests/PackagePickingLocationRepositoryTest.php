<?php

use App\Models\PackagePickingLocation;
use App\Repositories\PackagePickingLocationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackagePickingLocationRepositoryTest extends TestCase
{
    use MakePackagePickingLocationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PackagePickingLocationRepository
     */
    protected $packagePickingLocationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packagePickingLocationRepo = App::make(PackagePickingLocationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePackagePickingLocation()
    {
        $packagePickingLocation = $this->fakePackagePickingLocationData();
        $createdPackagePickingLocation = $this->packagePickingLocationRepo->create($packagePickingLocation);
        $createdPackagePickingLocation = $createdPackagePickingLocation->toArray();
        $this->assertArrayHasKey('id', $createdPackagePickingLocation);
        $this->assertNotNull($createdPackagePickingLocation['id'], 'Created PackagePickingLocation must have id specified');
        $this->assertNotNull(PackagePickingLocation::find($createdPackagePickingLocation['id']), 'PackagePickingLocation with given id must be in DB');
        $this->assertModelData($packagePickingLocation, $createdPackagePickingLocation);
    }

    /**
     * @test read
     */
    public function testReadPackagePickingLocation()
    {
        $packagePickingLocation = $this->makePackagePickingLocation();
        $dbPackagePickingLocation = $this->packagePickingLocationRepo->find($packagePickingLocation->id);
        $dbPackagePickingLocation = $dbPackagePickingLocation->toArray();
        $this->assertModelData($packagePickingLocation->toArray(), $dbPackagePickingLocation);
    }

    /**
     * @test update
     */
    public function testUpdatePackagePickingLocation()
    {
        $packagePickingLocation = $this->makePackagePickingLocation();
        $fakePackagePickingLocation = $this->fakePackagePickingLocationData();
        $updatedPackagePickingLocation = $this->packagePickingLocationRepo->update($fakePackagePickingLocation, $packagePickingLocation->id);
        $this->assertModelData($fakePackagePickingLocation, $updatedPackagePickingLocation->toArray());
        $dbPackagePickingLocation = $this->packagePickingLocationRepo->find($packagePickingLocation->id);
        $this->assertModelData($fakePackagePickingLocation, $dbPackagePickingLocation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePackagePickingLocation()
    {
        $packagePickingLocation = $this->makePackagePickingLocation();
        $resp = $this->packagePickingLocationRepo->delete($packagePickingLocation->id);
        $this->assertTrue($resp);
        $this->assertNull(PackagePickingLocation::find($packagePickingLocation->id), 'PackagePickingLocation should not exist in DB');
    }
}
