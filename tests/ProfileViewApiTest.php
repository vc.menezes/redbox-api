<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProfileViewApiTest extends TestCase
{
    use MakeProfileViewTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateProfileView()
    {
        $profileView = $this->fakeProfileViewData();
        $this->json('POST', '/api/v1/profileViews', $profileView);

        $this->assertApiResponse($profileView);
    }

    /**
     * @test
     */
    public function testReadProfileView()
    {
        $profileView = $this->makeProfileView();
        $this->json('GET', '/api/v1/profileViews/'.$profileView->id);

        $this->assertApiResponse($profileView->toArray());
    }

    /**
     * @test
     */
    public function testUpdateProfileView()
    {
        $profileView = $this->makeProfileView();
        $editedProfileView = $this->fakeProfileViewData();

        $this->json('PUT', '/api/v1/profileViews/'.$profileView->id, $editedProfileView);

        $this->assertApiResponse($editedProfileView);
    }

    /**
     * @test
     */
    public function testDeleteProfileView()
    {
        $profileView = $this->makeProfileView();
        $this->json('DELETE', '/api/v1/profileViews/'.$profileView->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/profileViews/'.$profileView->id);

        $this->assertResponseStatus(404);
    }
}
