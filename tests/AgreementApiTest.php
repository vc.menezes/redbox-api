<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AgreementApiTest extends TestCase
{
    use MakeAgreementTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAgreement()
    {
        $agreement = $this->fakeAgreementData();
        $this->json('POST', '/api/v1/agreements', $agreement);

        $this->assertApiResponse($agreement);
    }

    /**
     * @test
     */
    public function testReadAgreement()
    {
        $agreement = $this->makeAgreement();
        $this->json('GET', '/api/v1/agreements/'.$agreement->id);

        $this->assertApiResponse($agreement->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAgreement()
    {
        $agreement = $this->makeAgreement();
        $editedAgreement = $this->fakeAgreementData();

        $this->json('PUT', '/api/v1/agreements/'.$agreement->id, $editedAgreement);

        $this->assertApiResponse($editedAgreement);
    }

    /**
     * @test
     */
    public function testDeleteAgreement()
    {
        $agreement = $this->makeAgreement();
        $this->json('DELETE', '/api/v1/agreements/'.$agreement->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/agreements/'.$agreement->id);

        $this->assertResponseStatus(404);
    }
}
