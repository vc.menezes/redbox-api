<?php

use Faker\Factory as Faker;
use App\Models\Agreement;
use App\Repositories\AgreementRepository;

trait MakeAgreementTrait
{
    /**
     * Create fake instance of Agreement and save it in database
     *
     * @param array $agreementFields
     * @return Agreement
     */
    public function makeAgreement($agreementFields = [])
    {
        /** @var AgreementRepository $agreementRepo */
        $agreementRepo = App::make(AgreementRepository::class);
        $theme = $this->fakeAgreementData($agreementFields);
        return $agreementRepo->create($theme);
    }

    /**
     * Get fake instance of Agreement
     *
     * @param array $agreementFields
     * @return Agreement
     */
    public function fakeAgreement($agreementFields = [])
    {
        return new Agreement($this->fakeAgreementData($agreementFields));
    }

    /**
     * Get fake data of Agreement
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAgreementData($agreementFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'body' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $agreementFields);
    }
}
