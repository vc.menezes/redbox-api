<?php

use Faker\Factory as Faker;
use App\Models\PickingCharge;
use App\Repositories\PickingChargeRepository;

trait MakePickingChargeTrait
{
    /**
     * Create fake instance of PickingCharge and save it in database
     *
     * @param array $pickingChargeFields
     * @return PickingCharge
     */
    public function makePickingCharge($pickingChargeFields = [])
    {
        /** @var PickingChargeRepository $pickingChargeRepo */
        $pickingChargeRepo = App::make(PickingChargeRepository::class);
        $theme = $this->fakePickingChargeData($pickingChargeFields);
        return $pickingChargeRepo->create($theme);
    }

    /**
     * Get fake instance of PickingCharge
     *
     * @param array $pickingChargeFields
     * @return PickingCharge
     */
    public function fakePickingCharge($pickingChargeFields = [])
    {
        return new PickingCharge($this->fakePickingChargeData($pickingChargeFields));
    }

    /**
     * Get fake data of PickingCharge
     *
     * @param array $postFields
     * @return array
     */
    public function fakePickingChargeData($pickingChargeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'location_id' => $fake->randomDigitNotNull,
            'rate' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $pickingChargeFields);
    }
}
