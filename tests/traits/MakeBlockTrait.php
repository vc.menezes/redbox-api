<?php

use Faker\Factory as Faker;
use App\Models\Block;
use App\Repositories\BlockRepository;

trait MakeBlockTrait
{
    /**
     * Create fake instance of Block and save it in database
     *
     * @param array $blockFields
     * @return Block
     */
    public function makeBlock($blockFields = [])
    {
        /** @var BlockRepository $blockRepo */
        $blockRepo = App::make(BlockRepository::class);
        $theme = $this->fakeBlockData($blockFields);
        return $blockRepo->create($theme);
    }

    /**
     * Get fake instance of Block
     *
     * @param array $blockFields
     * @return Block
     */
    public function fakeBlock($blockFields = [])
    {
        return new Block($this->fakeBlockData($blockFields));
    }

    /**
     * Get fake data of Block
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBlockData($blockFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'parent_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'code' => $fake->word,
            'high' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $blockFields);
    }
}
