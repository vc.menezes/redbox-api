<?php

use Faker\Factory as Faker;
use App\Models\PackageDestination;
use App\Repositories\PackageDestinationRepository;

trait MakePackageDestinationTrait
{
    /**
     * Create fake instance of PackageDestination and save it in database
     *
     * @param array $packageDestinationFields
     * @return PackageDestination
     */
    public function makePackageDestination($packageDestinationFields = [])
    {
        /** @var PackageDestinationRepository $packageDestinationRepo */
        $packageDestinationRepo = App::make(PackageDestinationRepository::class);
        $theme = $this->fakePackageDestinationData($packageDestinationFields);
        return $packageDestinationRepo->create($theme);
    }

    /**
     * Get fake instance of PackageDestination
     *
     * @param array $packageDestinationFields
     * @return PackageDestination
     */
    public function fakePackageDestination($packageDestinationFields = [])
    {
        return new PackageDestination($this->fakePackageDestinationData($packageDestinationFields));
    }

    /**
     * Get fake data of PackageDestination
     *
     * @param array $postFields
     * @return array
     */
    public function fakePackageDestinationData($packageDestinationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'address' => $fake->word,
            'contact_number' => $fake->word,
            'location_id' => $fake->randomDigitNotNull,
            'package_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $packageDestinationFields);
    }
}
