<?php

use Faker\Factory as Faker;
use App\Models\ReceiptCheque;
use App\Repositories\ReceiptChequeRepository;

trait MakeReceiptChequeTrait
{
    /**
     * Create fake instance of ReceiptCheque and save it in database
     *
     * @param array $receiptChequeFields
     * @return ReceiptCheque
     */
    public function makeReceiptCheque($receiptChequeFields = [])
    {
        /** @var ReceiptChequeRepository $receiptChequeRepo */
        $receiptChequeRepo = App::make(ReceiptChequeRepository::class);
        $theme = $this->fakeReceiptChequeData($receiptChequeFields);
        return $receiptChequeRepo->create($theme);
    }

    /**
     * Get fake instance of ReceiptCheque
     *
     * @param array $receiptChequeFields
     * @return ReceiptCheque
     */
    public function fakeReceiptCheque($receiptChequeFields = [])
    {
        return new ReceiptCheque($this->fakeReceiptChequeData($receiptChequeFields));
    }

    /**
     * Get fake data of ReceiptCheque
     *
     * @param array $postFields
     * @return array
     */
    public function fakeReceiptChequeData($receiptChequeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'receipt_id' => $fake->randomDigitNotNull,
            'cheque_no' => $fake->word,
            'cheque_account_no' => $fake->word,
            'bank_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $receiptChequeFields);
    }
}
