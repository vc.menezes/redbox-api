<?php

use Faker\Factory as Faker;
use App\Models\Invoice;
use App\Repositories\InvoiceRepository;

trait MakeInvoiceTrait
{
    /**
     * Create fake instance of Invoice and save it in database
     *
     * @param array $invoiceFields
     * @return Invoice
     */
    public function makeInvoice($invoiceFields = [])
    {
        /** @var InvoiceRepository $invoiceRepo */
        $invoiceRepo = App::make(InvoiceRepository::class);
        $theme = $this->fakeInvoiceData($invoiceFields);
        return $invoiceRepo->create($theme);
    }

    /**
     * Get fake instance of Invoice
     *
     * @param array $invoiceFields
     * @return Invoice
     */
    public function fakeInvoice($invoiceFields = [])
    {
        return new Invoice($this->fakeInvoiceData($invoiceFields));
    }

    /**
     * Get fake data of Invoice
     *
     * @param array $postFields
     * @return array
     */
    public function fakeInvoiceData($invoiceFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'profile_id' => $fake->randomDigitNotNull,
            'serial_number' => $fake->word,
            'details' => $fake->word,
            'qty' => $fake->randomDigitNotNull,
            'rate' => $fake->word,
            'tax' => $fake->word,
            'total' => $fake->word,
            'data' => $fake->word,
            'type' => $fake->randomDigitNotNull,
            'status' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $invoiceFields);
    }
}
