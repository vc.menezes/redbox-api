<?php

use Faker\Factory as Faker;
use App\Models\CardType;
use App\Repositories\CardTypeRepository;

trait MakeCardTypeTrait
{
    /**
     * Create fake instance of CardType and save it in database
     *
     * @param array $cardTypeFields
     * @return CardType
     */
    public function makeCardType($cardTypeFields = [])
    {
        /** @var CardTypeRepository $cardTypeRepo */
        $cardTypeRepo = App::make(CardTypeRepository::class);
        $theme = $this->fakeCardTypeData($cardTypeFields);
        return $cardTypeRepo->create($theme);
    }

    /**
     * Get fake instance of CardType
     *
     * @param array $cardTypeFields
     * @return CardType
     */
    public function fakeCardType($cardTypeFields = [])
    {
        return new CardType($this->fakeCardTypeData($cardTypeFields));
    }

    /**
     * Get fake data of CardType
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCardTypeData($cardTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $cardTypeFields);
    }
}
