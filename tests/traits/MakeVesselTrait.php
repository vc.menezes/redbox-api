<?php

use Faker\Factory as Faker;
use App\Models\Vessel;
use App\Repositories\VesselRepository;

trait MakeVesselTrait
{
    /**
     * Create fake instance of Vessel and save it in database
     *
     * @param array $vesselFields
     * @return Vessel
     */
    public function makeVessel($vesselFields = [])
    {
        /** @var VesselRepository $vesselRepo */
        $vesselRepo = App::make(VesselRepository::class);
        $theme = $this->fakeVesselData($vesselFields);
        return $vesselRepo->create($theme);
    }

    /**
     * Get fake instance of Vessel
     *
     * @param array $vesselFields
     * @return Vessel
     */
    public function fakeVessel($vesselFields = [])
    {
        return new Vessel($this->fakeVesselData($vesselFields));
    }

    /**
     * Get fake data of Vessel
     *
     * @param array $postFields
     * @return array
     */
    public function fakeVesselData($vesselFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'registry_number' => $fake->word,
            'contact_person' => $fake->word,
            'contact_number' => $fake->word,
            'location_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $vesselFields);
    }
}
