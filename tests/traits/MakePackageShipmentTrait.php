<?php

use Faker\Factory as Faker;
use App\Models\PackageShipment;
use App\Repositories\PackageShipmentRepository;

trait MakePackageShipmentTrait
{
    /**
     * Create fake instance of PackageShipment and save it in database
     *
     * @param array $packageShipmentFields
     * @return PackageShipment
     */
    public function makePackageShipment($packageShipmentFields = [])
    {
        /** @var PackageShipmentRepository $packageShipmentRepo */
        $packageShipmentRepo = App::make(PackageShipmentRepository::class);
        $theme = $this->fakePackageShipmentData($packageShipmentFields);
        return $packageShipmentRepo->create($theme);
    }

    /**
     * Get fake instance of PackageShipment
     *
     * @param array $packageShipmentFields
     * @return PackageShipment
     */
    public function fakePackageShipment($packageShipmentFields = [])
    {
        return new PackageShipment($this->fakePackageShipmentData($packageShipmentFields));
    }

    /**
     * Get fake data of PackageShipment
     *
     * @param array $postFields
     * @return array
     */
    public function fakePackageShipmentData($packageShipmentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'vessel_id' => $fake->randomDigitNotNull,
            'package_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $packageShipmentFields);
    }
}
