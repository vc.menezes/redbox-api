<?php

use Faker\Factory as Faker;
use App\Models\PackageItem;
use App\Repositories\PackageItemRepository;

trait MakePackageItemTrait
{
    /**
     * Create fake instance of PackageItem and save it in database
     *
     * @param array $packageItemFields
     * @return PackageItem
     */
    public function makePackageItem($packageItemFields = [])
    {
        /** @var PackageItemRepository $packageItemRepo */
        $packageItemRepo = App::make(PackageItemRepository::class);
        $theme = $this->fakePackageItemData($packageItemFields);
        return $packageItemRepo->create($theme);
    }

    /**
     * Get fake instance of PackageItem
     *
     * @param array $packageItemFields
     * @return PackageItem
     */
    public function fakePackageItem($packageItemFields = [])
    {
        return new PackageItem($this->fakePackageItemData($packageItemFields));
    }

    /**
     * Get fake data of PackageItem
     *
     * @param array $postFields
     * @return array
     */
    public function fakePackageItemData($packageItemFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'qty' => $fake->randomDigitNotNull,
            'description' => $fake->word,
            'type' => $fake->word,
            'UOM' => $fake->word,
            'commodity_id' => $fake->randomDigitNotNull,
            'package_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $packageItemFields);
    }
}
