<?php

use Faker\Factory as Faker;
use App\Models\BulkOrderPackage;
use App\Repositories\BulkOrderPackageRepository;

trait MakeBulkOrderPackageTrait
{
    /**
     * Create fake instance of BulkOrderPackage and save it in database
     *
     * @param array $bulkOrderPackageFields
     * @return BulkOrderPackage
     */
    public function makeBulkOrderPackage($bulkOrderPackageFields = [])
    {
        /** @var BulkOrderPackageRepository $bulkOrderPackageRepo */
        $bulkOrderPackageRepo = App::make(BulkOrderPackageRepository::class);
        $theme = $this->fakeBulkOrderPackageData($bulkOrderPackageFields);
        return $bulkOrderPackageRepo->create($theme);
    }

    /**
     * Get fake instance of BulkOrderPackage
     *
     * @param array $bulkOrderPackageFields
     * @return BulkOrderPackage
     */
    public function fakeBulkOrderPackage($bulkOrderPackageFields = [])
    {
        return new BulkOrderPackage($this->fakeBulkOrderPackageData($bulkOrderPackageFields));
    }

    /**
     * Get fake data of BulkOrderPackage
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBulkOrderPackageData($bulkOrderPackageFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'bulk_order_id' => $fake->randomDigitNotNull,
            'package_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $bulkOrderPackageFields);
    }
}
