<?php

use Faker\Factory as Faker;
use App\Models\InvoiceReceipt;
use App\Repositories\InvoiceReceiptRepository;

trait MakeInvoiceReceiptTrait
{
    /**
     * Create fake instance of InvoiceReceipt and save it in database
     *
     * @param array $invoiceReceiptFields
     * @return InvoiceReceipt
     */
    public function makeInvoiceReceipt($invoiceReceiptFields = [])
    {
        /** @var InvoiceReceiptRepository $invoiceReceiptRepo */
        $invoiceReceiptRepo = App::make(InvoiceReceiptRepository::class);
        $theme = $this->fakeInvoiceReceiptData($invoiceReceiptFields);
        return $invoiceReceiptRepo->create($theme);
    }

    /**
     * Get fake instance of InvoiceReceipt
     *
     * @param array $invoiceReceiptFields
     * @return InvoiceReceipt
     */
    public function fakeInvoiceReceipt($invoiceReceiptFields = [])
    {
        return new InvoiceReceipt($this->fakeInvoiceReceiptData($invoiceReceiptFields));
    }

    /**
     * Get fake data of InvoiceReceipt
     *
     * @param array $postFields
     * @return array
     */
    public function fakeInvoiceReceiptData($invoiceReceiptFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'invoice_id' => $fake->randomDigitNotNull,
            'receipt_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $invoiceReceiptFields);
    }
}
