<?php

use Faker\Factory as Faker;
use App\Models\Berth;
use App\Repositories\BerthRepository;

trait MakeBerthTrait
{
    /**
     * Create fake instance of Berth and save it in database
     *
     * @param array $berthFields
     * @return Berth
     */
    public function makeBerth($berthFields = [])
    {
        /** @var BerthRepository $berthRepo */
        $berthRepo = App::make(BerthRepository::class);
        $theme = $this->fakeBerthData($berthFields);
        return $berthRepo->create($theme);
    }

    /**
     * Get fake instance of Berth
     *
     * @param array $berthFields
     * @return Berth
     */
    public function fakeBerth($berthFields = [])
    {
        return new Berth($this->fakeBerthData($berthFields));
    }

    /**
     * Get fake data of Berth
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBerthData($berthFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'location_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $berthFields);
    }
}
