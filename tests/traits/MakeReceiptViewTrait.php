<?php

use Faker\Factory as Faker;
use App\Models\ReceiptView;
use App\Repositories\ReceiptViewRepository;

trait MakeReceiptViewTrait
{
    /**
     * Create fake instance of ReceiptView and save it in database
     *
     * @param array $receiptViewFields
     * @return ReceiptView
     */
    public function makeReceiptView($receiptViewFields = [])
    {
        /** @var ReceiptViewRepository $receiptViewRepo */
        $receiptViewRepo = App::make(ReceiptViewRepository::class);
        $theme = $this->fakeReceiptViewData($receiptViewFields);
        return $receiptViewRepo->create($theme);
    }

    /**
     * Get fake instance of ReceiptView
     *
     * @param array $receiptViewFields
     * @return ReceiptView
     */
    public function fakeReceiptView($receiptViewFields = [])
    {
        return new ReceiptView($this->fakeReceiptViewData($receiptViewFields));
    }

    /**
     * Get fake data of ReceiptView
     *
     * @param array $postFields
     * @return array
     */
    public function fakeReceiptViewData($receiptViewFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $receiptViewFields);
    }
}
