<?php

use Faker\Factory as Faker;
use App\Models\BulkOrderView;
use App\Repositories\BulkOrderViewRepository;

trait MakeBulkOrderViewTrait
{
    /**
     * Create fake instance of BulkOrderView and save it in database
     *
     * @param array $bulkOrderViewFields
     * @return BulkOrderView
     */
    public function makeBulkOrderView($bulkOrderViewFields = [])
    {
        /** @var BulkOrderViewRepository $bulkOrderViewRepo */
        $bulkOrderViewRepo = App::make(BulkOrderViewRepository::class);
        $theme = $this->fakeBulkOrderViewData($bulkOrderViewFields);
        return $bulkOrderViewRepo->create($theme);
    }

    /**
     * Get fake instance of BulkOrderView
     *
     * @param array $bulkOrderViewFields
     * @return BulkOrderView
     */
    public function fakeBulkOrderView($bulkOrderViewFields = [])
    {
        return new BulkOrderView($this->fakeBulkOrderViewData($bulkOrderViewFields));
    }

    /**
     * Get fake data of BulkOrderView
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBulkOrderViewData($bulkOrderViewFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $bulkOrderViewFields);
    }
}
