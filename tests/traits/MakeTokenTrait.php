<?php

use Faker\Factory as Faker;
use App\Models\Token;
use App\Repositories\TokenRepository;

trait MakeTokenTrait
{
    /**
     * Create fake instance of Token and save it in database
     *
     * @param array $tokenFields
     * @return Token
     */
    public function makeToken($tokenFields = [])
    {
        /** @var TokenRepository $tokenRepo */
        $tokenRepo = App::make(TokenRepository::class);
        $theme = $this->fakeTokenData($tokenFields);
        return $tokenRepo->create($theme);
    }

    /**
     * Get fake instance of Token
     *
     * @param array $tokenFields
     * @return Token
     */
    public function fakeToken($tokenFields = [])
    {
        return new Token($this->fakeTokenData($tokenFields));
    }

    /**
     * Get fake data of Token
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTokenData($tokenFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'token' => $fake->word,
            'medium' => $fake->word,
            'user_id' => $fake->randomDigitNotNull,
            'expires_at' => $fake->word,
            'expired' => $fake->word,
            'payload' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $tokenFields);
    }
}
