<?php

use Faker\Factory as Faker;
use App\Models\Key;
use App\Repositories\KeyRepository;

trait MakeKeyTrait
{
    /**
     * Create fake instance of Key and save it in database
     *
     * @param array $keyFields
     * @return Key
     */
    public function makeKey($keyFields = [])
    {
        /** @var KeyRepository $keyRepo */
        $keyRepo = App::make(KeyRepository::class);
        $theme = $this->fakeKeyData($keyFields);
        return $keyRepo->create($theme);
    }

    /**
     * Get fake instance of Key
     *
     * @param array $keyFields
     * @return Key
     */
    public function fakeKey($keyFields = [])
    {
        return new Key($this->fakeKeyData($keyFields));
    }

    /**
     * Get fake data of Key
     *
     * @param array $postFields
     * @return array
     */
    public function fakeKeyData($keyFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'key_name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $keyFields);
    }
}
