<?php

use Faker\Factory as Faker;
use App\Models\ReceiptTranfer;
use App\Repositories\ReceiptTranferRepository;

trait MakeReceiptTranferTrait
{
    /**
     * Create fake instance of ReceiptTranfer and save it in database
     *
     * @param array $receiptTranferFields
     * @return ReceiptTranfer
     */
    public function makeReceiptTranfer($receiptTranferFields = [])
    {
        /** @var ReceiptTranferRepository $receiptTranferRepo */
        $receiptTranferRepo = App::make(ReceiptTranferRepository::class);
        $theme = $this->fakeReceiptTranferData($receiptTranferFields);
        return $receiptTranferRepo->create($theme);
    }

    /**
     * Get fake instance of ReceiptTranfer
     *
     * @param array $receiptTranferFields
     * @return ReceiptTranfer
     */
    public function fakeReceiptTranfer($receiptTranferFields = [])
    {
        return new ReceiptTranfer($this->fakeReceiptTranferData($receiptTranferFields));
    }

    /**
     * Get fake data of ReceiptTranfer
     *
     * @param array $postFields
     * @return array
     */
    public function fakeReceiptTranferData($receiptTranferFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'receipt_id' => $fake->randomDigitNotNull,
            'ref' => $fake->word,
            'from' => $fake->word,
            'amount' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $receiptTranferFields);
    }
}
