<?php

use Faker\Factory as Faker;
use App\Models\InvoiceItem;
use App\Repositories\InvoiceItemRepository;

trait MakeInvoiceItemTrait
{
    /**
     * Create fake instance of InvoiceItem and save it in database
     *
     * @param array $invoiceItemFields
     * @return InvoiceItem
     */
    public function makeInvoiceItem($invoiceItemFields = [])
    {
        /** @var InvoiceItemRepository $invoiceItemRepo */
        $invoiceItemRepo = App::make(InvoiceItemRepository::class);
        $theme = $this->fakeInvoiceItemData($invoiceItemFields);
        return $invoiceItemRepo->create($theme);
    }

    /**
     * Get fake instance of InvoiceItem
     *
     * @param array $invoiceItemFields
     * @return InvoiceItem
     */
    public function fakeInvoiceItem($invoiceItemFields = [])
    {
        return new InvoiceItem($this->fakeInvoiceItemData($invoiceItemFields));
    }

    /**
     * Get fake data of InvoiceItem
     *
     * @param array $postFields
     * @return array
     */
    public function fakeInvoiceItemData($invoiceItemFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'invoice_id' => $fake->randomDigitNotNull,
            'details' => $fake->word,
            'rate' => $fake->word,
            'UOM' => $fake->word,
            'qty' => $fake->word,
            'type' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $invoiceItemFields);
    }
}
