<?php

use Faker\Factory as Faker;
use App\Models\CommissionView;
use App\Repositories\CommissionViewRepository;

trait MakeCommissionViewTrait
{
    /**
     * Create fake instance of CommissionView and save it in database
     *
     * @param array $commissionViewFields
     * @return CommissionView
     */
    public function makeCommissionView($commissionViewFields = [])
    {
        /** @var CommissionViewRepository $commissionViewRepo */
        $commissionViewRepo = App::make(CommissionViewRepository::class);
        $theme = $this->fakeCommissionViewData($commissionViewFields);
        return $commissionViewRepo->create($theme);
    }

    /**
     * Get fake instance of CommissionView
     *
     * @param array $commissionViewFields
     * @return CommissionView
     */
    public function fakeCommissionView($commissionViewFields = [])
    {
        return new CommissionView($this->fakeCommissionViewData($commissionViewFields));
    }

    /**
     * Get fake data of CommissionView
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCommissionViewData($commissionViewFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $commissionViewFields);
    }
}
