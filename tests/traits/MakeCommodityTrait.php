<?php

use Faker\Factory as Faker;
use App\Models\Commodity;
use App\Repositories\CommodityRepository;

trait MakeCommodityTrait
{
    /**
     * Create fake instance of Commodity and save it in database
     *
     * @param array $commodityFields
     * @return Commodity
     */
    public function makeCommodity($commodityFields = [])
    {
        /** @var CommodityRepository $commodityRepo */
        $commodityRepo = App::make(CommodityRepository::class);
        $theme = $this->fakeCommodityData($commodityFields);
        return $commodityRepo->create($theme);
    }

    /**
     * Get fake instance of Commodity
     *
     * @param array $commodityFields
     * @return Commodity
     */
    public function fakeCommodity($commodityFields = [])
    {
        return new Commodity($this->fakeCommodityData($commodityFields));
    }

    /**
     * Get fake data of Commodity
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCommodityData($commodityFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'commodity_category_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $commodityFields);
    }
}
