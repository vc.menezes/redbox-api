<?php

use Faker\Factory as Faker;
use App\Models\PackagePickingLocation;
use App\Repositories\PackagePickingLocationRepository;

trait MakePackagePickingLocationTrait
{
    /**
     * Create fake instance of PackagePickingLocation and save it in database
     *
     * @param array $packagePickingLocationFields
     * @return PackagePickingLocation
     */
    public function makePackagePickingLocation($packagePickingLocationFields = [])
    {
        /** @var PackagePickingLocationRepository $packagePickingLocationRepo */
        $packagePickingLocationRepo = App::make(PackagePickingLocationRepository::class);
        $theme = $this->fakePackagePickingLocationData($packagePickingLocationFields);
        return $packagePickingLocationRepo->create($theme);
    }

    /**
     * Get fake instance of PackagePickingLocation
     *
     * @param array $packagePickingLocationFields
     * @return PackagePickingLocation
     */
    public function fakePackagePickingLocation($packagePickingLocationFields = [])
    {
        return new PackagePickingLocation($this->fakePackagePickingLocationData($packagePickingLocationFields));
    }

    /**
     * Get fake data of PackagePickingLocation
     *
     * @param array $postFields
     * @return array
     */
    public function fakePackagePickingLocationData($packagePickingLocationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'location_id' => $fake->randomDigitNotNull,
            'package_id' => $fake->randomDigitNotNull,
            'remarks' => $fake->word,
            'contact_number' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $packagePickingLocationFields);
    }
}
