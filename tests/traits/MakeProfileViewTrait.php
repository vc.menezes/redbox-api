<?php

use Faker\Factory as Faker;
use App\Models\ProfileView;
use App\Repositories\ProfileViewRepository;

trait MakeProfileViewTrait
{
    /**
     * Create fake instance of ProfileView and save it in database
     *
     * @param array $profileViewFields
     * @return ProfileView
     */
    public function makeProfileView($profileViewFields = [])
    {
        /** @var ProfileViewRepository $profileViewRepo */
        $profileViewRepo = App::make(ProfileViewRepository::class);
        $theme = $this->fakeProfileViewData($profileViewFields);
        return $profileViewRepo->create($theme);
    }

    /**
     * Get fake instance of ProfileView
     *
     * @param array $profileViewFields
     * @return ProfileView
     */
    public function fakeProfileView($profileViewFields = [])
    {
        return new ProfileView($this->fakeProfileViewData($profileViewFields));
    }

    /**
     * Get fake data of ProfileView
     *
     * @param array $postFields
     * @return array
     */
    public function fakeProfileViewData($profileViewFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $profileViewFields);
    }
}
