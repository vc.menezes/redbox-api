<?php

use Faker\Factory as Faker;
use App\Models\AgentPaymentPost;
use App\Repositories\AgentPaymentPostRepository;

trait MakeAgentPaymentPostTrait
{
    /**
     * Create fake instance of AgentPaymentPost and save it in database
     *
     * @param array $agentPaymentPostFields
     * @return AgentPaymentPost
     */
    public function makeAgentPaymentPost($agentPaymentPostFields = [])
    {
        /** @var AgentPaymentPostRepository $agentPaymentPostRepo */
        $agentPaymentPostRepo = App::make(AgentPaymentPostRepository::class);
        $theme = $this->fakeAgentPaymentPostData($agentPaymentPostFields);
        return $agentPaymentPostRepo->create($theme);
    }

    /**
     * Get fake instance of AgentPaymentPost
     *
     * @param array $agentPaymentPostFields
     * @return AgentPaymentPost
     */
    public function fakeAgentPaymentPost($agentPaymentPostFields = [])
    {
        return new AgentPaymentPost($this->fakeAgentPaymentPostData($agentPaymentPostFields));
    }

    /**
     * Get fake data of AgentPaymentPost
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAgentPaymentPostData($agentPaymentPostFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'profile_id' => $fake->randomDigitNotNull,
            'total' => $fake->word,
            'account_number' => $fake->word,
            'email' => $fake->word,
            'details' => $fake->word,
            'contact_number' => $fake->word,
            'identifier' => $fake->word,
            'weight' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $agentPaymentPostFields);
    }
}
