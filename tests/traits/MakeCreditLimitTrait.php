<?php

use Faker\Factory as Faker;
use App\Models\CreditLimit;
use App\Repositories\CreditLimitRepository;

trait MakeCreditLimitTrait
{
    /**
     * Create fake instance of CreditLimit and save it in database
     *
     * @param array $creditLimitFields
     * @return CreditLimit
     */
    public function makeCreditLimit($creditLimitFields = [])
    {
        /** @var CreditLimitRepository $creditLimitRepo */
        $creditLimitRepo = App::make(CreditLimitRepository::class);
        $theme = $this->fakeCreditLimitData($creditLimitFields);
        return $creditLimitRepo->create($theme);
    }

    /**
     * Get fake instance of CreditLimit
     *
     * @param array $creditLimitFields
     * @return CreditLimit
     */
    public function fakeCreditLimit($creditLimitFields = [])
    {
        return new CreditLimit($this->fakeCreditLimitData($creditLimitFields));
    }

    /**
     * Get fake data of CreditLimit
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCreditLimitData($creditLimitFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'profile_id' => $fake->randomDigitNotNull,
            'limit' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $creditLimitFields);
    }
}
