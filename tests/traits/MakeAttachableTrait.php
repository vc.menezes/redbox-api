<?php

use Faker\Factory as Faker;
use App\Models\Attachable;
use App\Repositories\AttachableRepository;

trait MakeAttachableTrait
{
    /**
     * Create fake instance of Attachable and save it in database
     *
     * @param array $attachableFields
     * @return Attachable
     */
    public function makeAttachable($attachableFields = [])
    {
        /** @var AttachableRepository $attachableRepo */
        $attachableRepo = App::make(AttachableRepository::class);
        $theme = $this->fakeAttachableData($attachableFields);
        return $attachableRepo->create($theme);
    }

    /**
     * Get fake instance of Attachable
     *
     * @param array $attachableFields
     * @return Attachable
     */
    public function fakeAttachable($attachableFields = [])
    {
        return new Attachable($this->fakeAttachableData($attachableFields));
    }

    /**
     * Get fake data of Attachable
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAttachableData($attachableFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'attachable' => $fake->word,
            'name' => $fake->word,
            'extension' => $fake->word,
            'type' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $attachableFields);
    }
}
