<?php

use Faker\Factory as Faker;
use App\Models\Receipt;
use App\Repositories\ReceiptRepository;

trait MakeReceiptTrait
{
    /**
     * Create fake instance of Receipt and save it in database
     *
     * @param array $receiptFields
     * @return Receipt
     */
    public function makeReceipt($receiptFields = [])
    {
        /** @var ReceiptRepository $receiptRepo */
        $receiptRepo = App::make(ReceiptRepository::class);
        $theme = $this->fakeReceiptData($receiptFields);
        return $receiptRepo->create($theme);
    }

    /**
     * Get fake instance of Receipt
     *
     * @param array $receiptFields
     * @return Receipt
     */
    public function fakeReceipt($receiptFields = [])
    {
        return new Receipt($this->fakeReceiptData($receiptFields));
    }

    /**
     * Get fake data of Receipt
     *
     * @param array $postFields
     * @return array
     */
    public function fakeReceiptData($receiptFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'customer_profile_id' => $fake->randomDigitNotNull,
            'created_by_profile_id' => $fake->randomDigitNotNull,
            'receipt_number' => $fake->word,
            'remarks' => $fake->word,
            'cash' => $fake->word,
            'cheque' => $fake->word,
            'card' => $fake->word,
            'transfer' => $fake->word,
            'status' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $receiptFields);
    }
}
