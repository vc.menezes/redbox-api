<?php

use Faker\Factory as Faker;
use App\Models\Sequence;
use App\Repositories\SequenceRepository;

trait MakeSequenceTrait
{
    /**
     * Create fake instance of Sequence and save it in database
     *
     * @param array $sequenceFields
     * @return Sequence
     */
    public function makeSequence($sequenceFields = [])
    {
        /** @var SequenceRepository $sequenceRepo */
        $sequenceRepo = App::make(SequenceRepository::class);
        $theme = $this->fakeSequenceData($sequenceFields);
        return $sequenceRepo->create($theme);
    }

    /**
     * Get fake instance of Sequence
     *
     * @param array $sequenceFields
     * @return Sequence
     */
    public function fakeSequence($sequenceFields = [])
    {
        return new Sequence($this->fakeSequenceData($sequenceFields));
    }

    /**
     * Get fake data of Sequence
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSequenceData($sequenceFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'data_type' => $fake->word,
            'current_number' => $fake->word,
            'prefix' => $fake->word,
            'post_fix' => $fake->word,
            'template' => $fake->word,
            'date' => $fake->word,
            'initial_number' => $fake->word,
            'reset_by' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $sequenceFields);
    }
}
