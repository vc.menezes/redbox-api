<?php

use Faker\Factory as Faker;
use App\Models\VesselBerth;
use App\Repositories\VesselBerthRepository;

trait MakeVesselBerthTrait
{
    /**
     * Create fake instance of VesselBerth and save it in database
     *
     * @param array $vesselBerthFields
     * @return VesselBerth
     */
    public function makeVesselBerth($vesselBerthFields = [])
    {
        /** @var VesselBerthRepository $vesselBerthRepo */
        $vesselBerthRepo = App::make(VesselBerthRepository::class);
        $theme = $this->fakeVesselBerthData($vesselBerthFields);
        return $vesselBerthRepo->create($theme);
    }

    /**
     * Get fake instance of VesselBerth
     *
     * @param array $vesselBerthFields
     * @return VesselBerth
     */
    public function fakeVesselBerth($vesselBerthFields = [])
    {
        return new VesselBerth($this->fakeVesselBerthData($vesselBerthFields));
    }

    /**
     * Get fake data of VesselBerth
     *
     * @param array $postFields
     * @return array
     */
    public function fakeVesselBerthData($vesselBerthFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'vessel_id' => $fake->randomDigitNotNull,
            'berth_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $vesselBerthFields);
    }
}
