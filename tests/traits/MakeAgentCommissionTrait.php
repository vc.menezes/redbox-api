<?php

use Faker\Factory as Faker;
use App\Models\AgentCommission;
use App\Repositories\AgentCommissionRepository;

trait MakeAgentCommissionTrait
{
    /**
     * Create fake instance of AgentCommission and save it in database
     *
     * @param array $agentCommissionFields
     * @return AgentCommission
     */
    public function makeAgentCommission($agentCommissionFields = [])
    {
        /** @var AgentCommissionRepository $agentCommissionRepo */
        $agentCommissionRepo = App::make(AgentCommissionRepository::class);
        $theme = $this->fakeAgentCommissionData($agentCommissionFields);
        return $agentCommissionRepo->create($theme);
    }

    /**
     * Get fake instance of AgentCommission
     *
     * @param array $agentCommissionFields
     * @return AgentCommission
     */
    public function fakeAgentCommission($agentCommissionFields = [])
    {
        return new AgentCommission($this->fakeAgentCommissionData($agentCommissionFields));
    }

    /**
     * Get fake data of AgentCommission
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAgentCommissionData($agentCommissionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'total_orders' => $fake->randomDigitNotNull,
            'agent_id' => $fake->randomDigitNotNull,
            'shipment_id' => $fake->randomDigitNotNull,
            'total_amount' => $fake->randomDigitNotNull,
            'percentage' => $fake->randomDigitNotNull,
            'agent_commission_total' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $agentCommissionFields);
    }
}
