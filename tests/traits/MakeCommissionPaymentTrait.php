<?php

use Faker\Factory as Faker;
use App\Models\CommissionPayment;
use App\Repositories\CommissionPaymentRepository;

trait MakeCommissionPaymentTrait
{
    /**
     * Create fake instance of CommissionPayment and save it in database
     *
     * @param array $commissionPaymentFields
     * @return CommissionPayment
     */
    public function makeCommissionPayment($commissionPaymentFields = [])
    {
        /** @var CommissionPaymentRepository $commissionPaymentRepo */
        $commissionPaymentRepo = App::make(CommissionPaymentRepository::class);
        $theme = $this->fakeCommissionPaymentData($commissionPaymentFields);
        return $commissionPaymentRepo->create($theme);
    }

    /**
     * Get fake instance of CommissionPayment
     *
     * @param array $commissionPaymentFields
     * @return CommissionPayment
     */
    public function fakeCommissionPayment($commissionPaymentFields = [])
    {
        return new CommissionPayment($this->fakeCommissionPaymentData($commissionPaymentFields));
    }

    /**
     * Get fake data of CommissionPayment
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCommissionPaymentData($commissionPaymentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'bank_account_id' => $fake->randomDigitNotNull,
            'reference_number' => $fake->word,
            'amount' => $fake->word,
            'date' => $fake->word,
            'remarks' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $commissionPaymentFields);
    }
}
