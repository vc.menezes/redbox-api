<?php

use Faker\Factory as Faker;
use App\Models\VesselRoute;
use App\Repositories\VesselRouteRepository;

trait MakeVesselRouteTrait
{
    /**
     * Create fake instance of VesselRoute and save it in database
     *
     * @param array $vesselRouteFields
     * @return VesselRoute
     */
    public function makeVesselRoute($vesselRouteFields = [])
    {
        /** @var VesselRouteRepository $vesselRouteRepo */
        $vesselRouteRepo = App::make(VesselRouteRepository::class);
        $theme = $this->fakeVesselRouteData($vesselRouteFields);
        return $vesselRouteRepo->create($theme);
    }

    /**
     * Get fake instance of VesselRoute
     *
     * @param array $vesselRouteFields
     * @return VesselRoute
     */
    public function fakeVesselRoute($vesselRouteFields = [])
    {
        return new VesselRoute($this->fakeVesselRouteData($vesselRouteFields));
    }

    /**
     * Get fake data of VesselRoute
     *
     * @param array $postFields
     * @return array
     */
    public function fakeVesselRouteData($vesselRouteFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'vessel_id' => $fake->randomDigitNotNull,
            'location_id' => $fake->randomDigitNotNull,
            'parent_id' => $fake->randomDigitNotNull,
            'contact_person' => $fake->word,
            'contact' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $vesselRouteFields);
    }
}
