<?php

use Faker\Factory as Faker;
use App\Models\Tracking;
use App\Repositories\TrackingRepository;

trait MakeTrackingTrait
{
    /**
     * Create fake instance of Tracking and save it in database
     *
     * @param array $trackingFields
     * @return Tracking
     */
    public function makeTracking($trackingFields = [])
    {
        /** @var TrackingRepository $trackingRepo */
        $trackingRepo = App::make(TrackingRepository::class);
        $theme = $this->fakeTrackingData($trackingFields);
        return $trackingRepo->create($theme);
    }

    /**
     * Get fake instance of Tracking
     *
     * @param array $trackingFields
     * @return Tracking
     */
    public function fakeTracking($trackingFields = [])
    {
        return new Tracking($this->fakeTrackingData($trackingFields));
    }

    /**
     * Get fake data of Tracking
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTrackingData($trackingFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'package_id' => $fake->randomDigitNotNull,
            'tracking_no' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $trackingFields);
    }
}
