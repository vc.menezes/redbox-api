<?php

use Faker\Factory as Faker;
use App\Models\TrackingLog;
use App\Repositories\TrackingLogRepository;

trait MakeTrackingLogTrait
{
    /**
     * Create fake instance of TrackingLog and save it in database
     *
     * @param array $trackingLogFields
     * @return TrackingLog
     */
    public function makeTrackingLog($trackingLogFields = [])
    {
        /** @var TrackingLogRepository $trackingLogRepo */
        $trackingLogRepo = App::make(TrackingLogRepository::class);
        $theme = $this->fakeTrackingLogData($trackingLogFields);
        return $trackingLogRepo->create($theme);
    }

    /**
     * Get fake instance of TrackingLog
     *
     * @param array $trackingLogFields
     * @return TrackingLog
     */
    public function fakeTrackingLog($trackingLogFields = [])
    {
        return new TrackingLog($this->fakeTrackingLogData($trackingLogFields));
    }

    /**
     * Get fake data of TrackingLog
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTrackingLogData($trackingLogFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'tracking_id' => $fake->randomDigitNotNull,
            'location_id' => $fake->randomDigitNotNull,
            'status_id' => $fake->randomDigitNotNull,
            'type' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $trackingLogFields);
    }
}
