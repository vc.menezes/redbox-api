<?php

use Faker\Factory as Faker;
use App\Models\Contactable;
use App\Repositories\ContactableRepository;

trait MakeContactableTrait
{
    /**
     * Create fake instance of Contactable and save it in database
     *
     * @param array $contactableFields
     * @return Contactable
     */
    public function makeContactable($contactableFields = [])
    {
        /** @var ContactableRepository $contactableRepo */
        $contactableRepo = App::make(ContactableRepository::class);
        $theme = $this->fakeContactableData($contactableFields);
        return $contactableRepo->create($theme);
    }

    /**
     * Get fake instance of Contactable
     *
     * @param array $contactableFields
     * @return Contactable
     */
    public function fakeContactable($contactableFields = [])
    {
        return new Contactable($this->fakeContactableData($contactableFields));
    }

    /**
     * Get fake data of Contactable
     *
     * @param array $postFields
     * @return array
     */
    public function fakeContactableData($contactableFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'value' => $fake->word,
            'key' => $fake->word,
            'contactable_id' => $fake->word,
            'contactable_type' => $fake->word,
            'type' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $contactableFields);
    }
}
