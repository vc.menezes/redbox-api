<?php

use Faker\Factory as Faker;
use App\Models\PackageHandover;
use App\Repositories\PackageHandoverRepository;

trait MakePackageHandoverTrait
{
    /**
     * Create fake instance of PackageHandover and save it in database
     *
     * @param array $packageHandoverFields
     * @return PackageHandover
     */
    public function makePackageHandover($packageHandoverFields = [])
    {
        /** @var PackageHandoverRepository $packageHandoverRepo */
        $packageHandoverRepo = App::make(PackageHandoverRepository::class);
        $theme = $this->fakePackageHandoverData($packageHandoverFields);
        return $packageHandoverRepo->create($theme);
    }

    /**
     * Get fake instance of PackageHandover
     *
     * @param array $packageHandoverFields
     * @return PackageHandover
     */
    public function fakePackageHandover($packageHandoverFields = [])
    {
        return new PackageHandover($this->fakePackageHandoverData($packageHandoverFields));
    }

    /**
     * Get fake data of PackageHandover
     *
     * @param array $postFields
     * @return array
     */
    public function fakePackageHandoverData($packageHandoverFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'contact' => $fake->word,
            'identifier' => $fake->word,
            'package_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $packageHandoverFields);
    }
}
