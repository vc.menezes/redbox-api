<?php

use Faker\Factory as Faker;
use App\Models\Payload;
use App\Repositories\PayloadRepository;

trait MakePayloadTrait
{
    /**
     * Create fake instance of Payload and save it in database
     *
     * @param array $payloadFields
     * @return Payload
     */
    public function makePayload($payloadFields = [])
    {
        /** @var PayloadRepository $payloadRepo */
        $payloadRepo = App::make(PayloadRepository::class);
        $theme = $this->fakePayloadData($payloadFields);
        return $payloadRepo->create($theme);
    }

    /**
     * Get fake instance of Payload
     *
     * @param array $payloadFields
     * @return Payload
     */
    public function fakePayload($payloadFields = [])
    {
        return new Payload($this->fakePayloadData($payloadFields));
    }

    /**
     * Get fake data of Payload
     *
     * @param array $postFields
     * @return array
     */
    public function fakePayloadData($payloadFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'data' => $fake->word,
            'payload' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $payloadFields);
    }
}
