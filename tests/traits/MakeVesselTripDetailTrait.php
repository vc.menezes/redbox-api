<?php

use Faker\Factory as Faker;
use App\Models\VesselTripDetail;
use App\Repositories\VesselTripDetailRepository;

trait MakeVesselTripDetailTrait
{
    /**
     * Create fake instance of VesselTripDetail and save it in database
     *
     * @param array $vesselTripDetailFields
     * @return VesselTripDetail
     */
    public function makeVesselTripDetail($vesselTripDetailFields = [])
    {
        /** @var VesselTripDetailRepository $vesselTripDetailRepo */
        $vesselTripDetailRepo = App::make(VesselTripDetailRepository::class);
        $theme = $this->fakeVesselTripDetailData($vesselTripDetailFields);
        return $vesselTripDetailRepo->create($theme);
    }

    /**
     * Get fake instance of VesselTripDetail
     *
     * @param array $vesselTripDetailFields
     * @return VesselTripDetail
     */
    public function fakeVesselTripDetail($vesselTripDetailFields = [])
    {
        return new VesselTripDetail($this->fakeVesselTripDetailData($vesselTripDetailFields));
    }

    /**
     * Get fake data of VesselTripDetail
     *
     * @param array $postFields
     * @return array
     */
    public function fakeVesselTripDetailData($vesselTripDetailFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'vessel_id' => $fake->randomDigitNotNull,
            'departure_at' => $fake->word,
            'eta' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $vesselTripDetailFields);
    }
}
