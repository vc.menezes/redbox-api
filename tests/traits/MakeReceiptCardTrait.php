<?php

use Faker\Factory as Faker;
use App\Models\ReceiptCard;
use App\Repositories\ReceiptCardRepository;

trait MakeReceiptCardTrait
{
    /**
     * Create fake instance of ReceiptCard and save it in database
     *
     * @param array $receiptCardFields
     * @return ReceiptCard
     */
    public function makeReceiptCard($receiptCardFields = [])
    {
        /** @var ReceiptCardRepository $receiptCardRepo */
        $receiptCardRepo = App::make(ReceiptCardRepository::class);
        $theme = $this->fakeReceiptCardData($receiptCardFields);
        return $receiptCardRepo->create($theme);
    }

    /**
     * Get fake instance of ReceiptCard
     *
     * @param array $receiptCardFields
     * @return ReceiptCard
     */
    public function fakeReceiptCard($receiptCardFields = [])
    {
        return new ReceiptCard($this->fakeReceiptCardData($receiptCardFields));
    }

    /**
     * Get fake data of ReceiptCard
     *
     * @param array $postFields
     * @return array
     */
    public function fakeReceiptCardData($receiptCardFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'receipt_id' => $fake->randomDigitNotNull,
            'card_type_id' => $fake->randomDigitNotNull,
            'transaction_number' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $receiptCardFields);
    }
}
