<?php

use Faker\Factory as Faker;
use App\Models\attributable;
use App\Repositories\attributableRepository;

trait MakeattributableTrait
{
    /**
     * Create fake instance of attributable and save it in database
     *
     * @param array $attributableFields
     * @return attributable
     */
    public function makeattributable($attributableFields = [])
    {
        /** @var attributableRepository $attributableRepo */
        $attributableRepo = App::make(attributableRepository::class);
        $theme = $this->fakeattributableData($attributableFields);
        return $attributableRepo->create($theme);
    }

    /**
     * Get fake instance of attributable
     *
     * @param array $attributableFields
     * @return attributable
     */
    public function fakeattributable($attributableFields = [])
    {
        return new attributable($this->fakeattributableData($attributableFields));
    }

    /**
     * Get fake data of attributable
     *
     * @param array $postFields
     * @return array
     */
    public function fakeattributableData($attributableFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'attributable' => $fake->word,
            'value' => $fake->word,
            'key_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $attributableFields);
    }
}
