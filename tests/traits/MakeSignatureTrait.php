<?php

use Faker\Factory as Faker;
use App\Models\Signature;
use App\Repositories\SignatureRepository;

trait MakeSignatureTrait
{
    /**
     * Create fake instance of Signature and save it in database
     *
     * @param array $signatureFields
     * @return Signature
     */
    public function makeSignature($signatureFields = [])
    {
        /** @var SignatureRepository $signatureRepo */
        $signatureRepo = App::make(SignatureRepository::class);
        $theme = $this->fakeSignatureData($signatureFields);
        return $signatureRepo->create($theme);
    }

    /**
     * Get fake instance of Signature
     *
     * @param array $signatureFields
     * @return Signature
     */
    public function fakeSignature($signatureFields = [])
    {
        return new Signature($this->fakeSignatureData($signatureFields));
    }

    /**
     * Get fake data of Signature
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSignatureData($signatureFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'package_id' => $fake->randomDigitNotNull,
            'profile_id' => $fake->randomDigitNotNull,
            'agreement_id' => $fake->randomDigitNotNull,
            'attacheble_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $signatureFields);
    }
}
