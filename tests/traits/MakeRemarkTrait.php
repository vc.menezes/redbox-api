<?php

use Faker\Factory as Faker;
use App\Models\Remark;
use App\Repositories\RemarkRepository;

trait MakeRemarkTrait
{
    /**
     * Create fake instance of Remark and save it in database
     *
     * @param array $remarkFields
     * @return Remark
     */
    public function makeRemark($remarkFields = [])
    {
        /** @var RemarkRepository $remarkRepo */
        $remarkRepo = App::make(RemarkRepository::class);
        $theme = $this->fakeRemarkData($remarkFields);
        return $remarkRepo->create($theme);
    }

    /**
     * Get fake instance of Remark
     *
     * @param array $remarkFields
     * @return Remark
     */
    public function fakeRemark($remarkFields = [])
    {
        return new Remark($this->fakeRemarkData($remarkFields));
    }

    /**
     * Get fake data of Remark
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRemarkData($remarkFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'remarkable' => $fake->word,
            'body' => $fake->text,
            'profile_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $remarkFields);
    }
}
