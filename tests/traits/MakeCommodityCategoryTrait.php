<?php

use Faker\Factory as Faker;
use App\Models\CommodityCategory;
use App\Repositories\CommodityCategoryRepository;

trait MakeCommodityCategoryTrait
{
    /**
     * Create fake instance of CommodityCategory and save it in database
     *
     * @param array $commodityCategoryFields
     * @return CommodityCategory
     */
    public function makeCommodityCategory($commodityCategoryFields = [])
    {
        /** @var CommodityCategoryRepository $commodityCategoryRepo */
        $commodityCategoryRepo = App::make(CommodityCategoryRepository::class);
        $theme = $this->fakeCommodityCategoryData($commodityCategoryFields);
        return $commodityCategoryRepo->create($theme);
    }

    /**
     * Get fake instance of CommodityCategory
     *
     * @param array $commodityCategoryFields
     * @return CommodityCategory
     */
    public function fakeCommodityCategory($commodityCategoryFields = [])
    {
        return new CommodityCategory($this->fakeCommodityCategoryData($commodityCategoryFields));
    }

    /**
     * Get fake data of CommodityCategory
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCommodityCategoryData($commodityCategoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $commodityCategoryFields);
    }
}
