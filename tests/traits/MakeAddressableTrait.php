<?php

use Faker\Factory as Faker;
use App\Models\Addressable;
use App\Repositories\AddressableRepository;

trait MakeAddressableTrait
{
    /**
     * Create fake instance of Addressable and save it in database
     *
     * @param array $addressableFields
     * @return Addressable
     */
    public function makeAddressable($addressableFields = [])
    {
        /** @var AddressableRepository $addressableRepo */
        $addressableRepo = App::make(AddressableRepository::class);
        $theme = $this->fakeAddressableData($addressableFields);
        return $addressableRepo->create($theme);
    }

    /**
     * Get fake instance of Addressable
     *
     * @param array $addressableFields
     * @return Addressable
     */
    public function fakeAddressable($addressableFields = [])
    {
        return new Addressable($this->fakeAddressableData($addressableFields));
    }

    /**
     * Get fake data of Addressable
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAddressableData($addressableFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'addressable' => $fake->word,
            'address_line' => $fake->word,
            'street' => $fake->word,
            'district' => $fake->word,
            'city' => $fake->word,
            'postal_code' => $fake->randomDigitNotNull,
            'country_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $addressableFields);
    }
}
