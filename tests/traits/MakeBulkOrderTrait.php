<?php

use Faker\Factory as Faker;
use App\Models\BulkOrder;
use App\Repositories\BulkOrderRepository;

trait MakeBulkOrderTrait
{
    /**
     * Create fake instance of BulkOrder and save it in database
     *
     * @param array $bulkOrderFields
     * @return BulkOrder
     */
    public function makeBulkOrder($bulkOrderFields = [])
    {
        /** @var BulkOrderRepository $bulkOrderRepo */
        $bulkOrderRepo = App::make(BulkOrderRepository::class);
        $theme = $this->fakeBulkOrderData($bulkOrderFields);
        return $bulkOrderRepo->create($theme);
    }

    /**
     * Get fake instance of BulkOrder
     *
     * @param array $bulkOrderFields
     * @return BulkOrder
     */
    public function fakeBulkOrder($bulkOrderFields = [])
    {
        return new BulkOrder($this->fakeBulkOrderData($bulkOrderFields));
    }

    /**
     * Get fake data of BulkOrder
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBulkOrderData($bulkOrderFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'description' => $fake->word,
            'weight' => $fake->word,
            'profile_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $bulkOrderFields);
    }
}
