<?php

use Faker\Factory as Faker;
use App\Models\StorageBlock;
use App\Repositories\StorageBlockRepository;

trait MakeStorageBlockTrait
{
    /**
     * Create fake instance of StorageBlock and save it in database
     *
     * @param array $storageBlockFields
     * @return StorageBlock
     */
    public function makeStorageBlock($storageBlockFields = [])
    {
        /** @var StorageBlockRepository $storageBlockRepo */
        $storageBlockRepo = App::make(StorageBlockRepository::class);
        $theme = $this->fakeStorageBlockData($storageBlockFields);
        return $storageBlockRepo->create($theme);
    }

    /**
     * Get fake instance of StorageBlock
     *
     * @param array $storageBlockFields
     * @return StorageBlock
     */
    public function fakeStorageBlock($storageBlockFields = [])
    {
        return new StorageBlock($this->fakeStorageBlockData($storageBlockFields));
    }

    /**
     * Get fake data of StorageBlock
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStorageBlockData($storageBlockFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'block_id' => $fake->randomDigitNotNull,
            'package_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $storageBlockFields);
    }
}
