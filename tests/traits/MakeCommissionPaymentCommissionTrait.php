<?php

use Faker\Factory as Faker;
use App\Models\CommissionPaymentCommission;
use App\Repositories\CommissionPaymentCommissionRepository;

trait MakeCommissionPaymentCommissionTrait
{
    /**
     * Create fake instance of CommissionPaymentCommission and save it in database
     *
     * @param array $commissionPaymentCommissionFields
     * @return CommissionPaymentCommission
     */
    public function makeCommissionPaymentCommission($commissionPaymentCommissionFields = [])
    {
        /** @var CommissionPaymentCommissionRepository $commissionPaymentCommissionRepo */
        $commissionPaymentCommissionRepo = App::make(CommissionPaymentCommissionRepository::class);
        $theme = $this->fakeCommissionPaymentCommissionData($commissionPaymentCommissionFields);
        return $commissionPaymentCommissionRepo->create($theme);
    }

    /**
     * Get fake instance of CommissionPaymentCommission
     *
     * @param array $commissionPaymentCommissionFields
     * @return CommissionPaymentCommission
     */
    public function fakeCommissionPaymentCommission($commissionPaymentCommissionFields = [])
    {
        return new CommissionPaymentCommission($this->fakeCommissionPaymentCommissionData($commissionPaymentCommissionFields));
    }

    /**
     * Get fake data of CommissionPaymentCommission
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCommissionPaymentCommissionData($commissionPaymentCommissionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'commission_id' => $fake->randomDigitNotNull,
            'commission_payment_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $commissionPaymentCommissionFields);
    }
}
