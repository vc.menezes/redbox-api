<?php

use Faker\Factory as Faker;
use App\Models\PackageRackView;
use App\Repositories\PackageRackViewRepository;

trait MakePackageRackViewTrait
{
    /**
     * Create fake instance of PackageRackView and save it in database
     *
     * @param array $packageRackViewFields
     * @return PackageRackView
     */
    public function makePackageRackView($packageRackViewFields = [])
    {
        /** @var PackageRackViewRepository $packageRackViewRepo */
        $packageRackViewRepo = App::make(PackageRackViewRepository::class);
        $theme = $this->fakePackageRackViewData($packageRackViewFields);
        return $packageRackViewRepo->create($theme);
    }

    /**
     * Get fake instance of PackageRackView
     *
     * @param array $packageRackViewFields
     * @return PackageRackView
     */
    public function fakePackageRackView($packageRackViewFields = [])
    {
        return new PackageRackView($this->fakePackageRackViewData($packageRackViewFields));
    }

    /**
     * Get fake data of PackageRackView
     *
     * @param array $postFields
     * @return array
     */
    public function fakePackageRackViewData($packageRackViewFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $packageRackViewFields);
    }
}
