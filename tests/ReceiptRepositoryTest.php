<?php

use App\Models\Receipt;
use App\Repositories\ReceiptRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReceiptRepositoryTest extends TestCase
{
    use MakeReceiptTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ReceiptRepository
     */
    protected $receiptRepo;

    public function setUp()
    {
        parent::setUp();
        $this->receiptRepo = App::make(ReceiptRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateReceipt()
    {
        $receipt = $this->fakeReceiptData();
        $createdReceipt = $this->receiptRepo->create($receipt);
        $createdReceipt = $createdReceipt->toArray();
        $this->assertArrayHasKey('id', $createdReceipt);
        $this->assertNotNull($createdReceipt['id'], 'Created Receipt must have id specified');
        $this->assertNotNull(Receipt::find($createdReceipt['id']), 'Receipt with given id must be in DB');
        $this->assertModelData($receipt, $createdReceipt);
    }

    /**
     * @test read
     */
    public function testReadReceipt()
    {
        $receipt = $this->makeReceipt();
        $dbReceipt = $this->receiptRepo->find($receipt->id);
        $dbReceipt = $dbReceipt->toArray();
        $this->assertModelData($receipt->toArray(), $dbReceipt);
    }

    /**
     * @test update
     */
    public function testUpdateReceipt()
    {
        $receipt = $this->makeReceipt();
        $fakeReceipt = $this->fakeReceiptData();
        $updatedReceipt = $this->receiptRepo->update($fakeReceipt, $receipt->id);
        $this->assertModelData($fakeReceipt, $updatedReceipt->toArray());
        $dbReceipt = $this->receiptRepo->find($receipt->id);
        $this->assertModelData($fakeReceipt, $dbReceipt->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteReceipt()
    {
        $receipt = $this->makeReceipt();
        $resp = $this->receiptRepo->delete($receipt->id);
        $this->assertTrue($resp);
        $this->assertNull(Receipt::find($receipt->id), 'Receipt should not exist in DB');
    }
}
