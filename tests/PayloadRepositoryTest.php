<?php

use App\Models\Payload;
use App\Repositories\PayloadRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PayloadRepositoryTest extends TestCase
{
    use MakePayloadTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PayloadRepository
     */
    protected $payloadRepo;

    public function setUp()
    {
        parent::setUp();
        $this->payloadRepo = App::make(PayloadRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePayload()
    {
        $payload = $this->fakePayloadData();
        $createdPayload = $this->payloadRepo->create($payload);
        $createdPayload = $createdPayload->toArray();
        $this->assertArrayHasKey('id', $createdPayload);
        $this->assertNotNull($createdPayload['id'], 'Created Payload must have id specified');
        $this->assertNotNull(Payload::find($createdPayload['id']), 'Payload with given id must be in DB');
        $this->assertModelData($payload, $createdPayload);
    }

    /**
     * @test read
     */
    public function testReadPayload()
    {
        $payload = $this->makePayload();
        $dbPayload = $this->payloadRepo->find($payload->id);
        $dbPayload = $dbPayload->toArray();
        $this->assertModelData($payload->toArray(), $dbPayload);
    }

    /**
     * @test update
     */
    public function testUpdatePayload()
    {
        $payload = $this->makePayload();
        $fakePayload = $this->fakePayloadData();
        $updatedPayload = $this->payloadRepo->update($fakePayload, $payload->id);
        $this->assertModelData($fakePayload, $updatedPayload->toArray());
        $dbPayload = $this->payloadRepo->find($payload->id);
        $this->assertModelData($fakePayload, $dbPayload->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePayload()
    {
        $payload = $this->makePayload();
        $resp = $this->payloadRepo->delete($payload->id);
        $this->assertTrue($resp);
        $this->assertNull(Payload::find($payload->id), 'Payload should not exist in DB');
    }
}
