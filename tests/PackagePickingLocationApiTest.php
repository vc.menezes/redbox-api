<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackagePickingLocationApiTest extends TestCase
{
    use MakePackagePickingLocationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePackagePickingLocation()
    {
        $packagePickingLocation = $this->fakePackagePickingLocationData();
        $this->json('POST', '/api/v1/packagePickingLocations', $packagePickingLocation);

        $this->assertApiResponse($packagePickingLocation);
    }

    /**
     * @test
     */
    public function testReadPackagePickingLocation()
    {
        $packagePickingLocation = $this->makePackagePickingLocation();
        $this->json('GET', '/api/v1/packagePickingLocations/'.$packagePickingLocation->id);

        $this->assertApiResponse($packagePickingLocation->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePackagePickingLocation()
    {
        $packagePickingLocation = $this->makePackagePickingLocation();
        $editedPackagePickingLocation = $this->fakePackagePickingLocationData();

        $this->json('PUT', '/api/v1/packagePickingLocations/'.$packagePickingLocation->id, $editedPackagePickingLocation);

        $this->assertApiResponse($editedPackagePickingLocation);
    }

    /**
     * @test
     */
    public function testDeletePackagePickingLocation()
    {
        $packagePickingLocation = $this->makePackagePickingLocation();
        $this->json('DELETE', '/api/v1/packagePickingLocations/'.$packagePickingLocation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packagePickingLocations/'.$packagePickingLocation->id);

        $this->assertResponseStatus(404);
    }
}
