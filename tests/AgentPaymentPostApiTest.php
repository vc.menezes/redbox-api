<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AgentPaymentPostApiTest extends TestCase
{
    use MakeAgentPaymentPostTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAgentPaymentPost()
    {
        $agentPaymentPost = $this->fakeAgentPaymentPostData();
        $this->json('POST', '/api/v1/agentPaymentPosts', $agentPaymentPost);

        $this->assertApiResponse($agentPaymentPost);
    }

    /**
     * @test
     */
    public function testReadAgentPaymentPost()
    {
        $agentPaymentPost = $this->makeAgentPaymentPost();
        $this->json('GET', '/api/v1/agentPaymentPosts/'.$agentPaymentPost->id);

        $this->assertApiResponse($agentPaymentPost->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAgentPaymentPost()
    {
        $agentPaymentPost = $this->makeAgentPaymentPost();
        $editedAgentPaymentPost = $this->fakeAgentPaymentPostData();

        $this->json('PUT', '/api/v1/agentPaymentPosts/'.$agentPaymentPost->id, $editedAgentPaymentPost);

        $this->assertApiResponse($editedAgentPaymentPost);
    }

    /**
     * @test
     */
    public function testDeleteAgentPaymentPost()
    {
        $agentPaymentPost = $this->makeAgentPaymentPost();
        $this->json('DELETE', '/api/v1/agentPaymentPosts/'.$agentPaymentPost->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/agentPaymentPosts/'.$agentPaymentPost->id);

        $this->assertResponseStatus(404);
    }
}
