<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VesselApiTest extends TestCase
{
    use MakeVesselTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateVessel()
    {
        $vessel = $this->fakeVesselData();
        $this->json('POST', '/api/v1/vessels', $vessel);

        $this->assertApiResponse($vessel);
    }

    /**
     * @test
     */
    public function testReadVessel()
    {
        $vessel = $this->makeVessel();
        $this->json('GET', '/api/v1/vessels/'.$vessel->id);

        $this->assertApiResponse($vessel->toArray());
    }

    /**
     * @test
     */
    public function testUpdateVessel()
    {
        $vessel = $this->makeVessel();
        $editedVessel = $this->fakeVesselData();

        $this->json('PUT', '/api/v1/vessels/'.$vessel->id, $editedVessel);

        $this->assertApiResponse($editedVessel);
    }

    /**
     * @test
     */
    public function testDeleteVessel()
    {
        $vessel = $this->makeVessel();
        $this->json('DELETE', '/api/v1/vessels/'.$vessel->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/vessels/'.$vessel->id);

        $this->assertResponseStatus(404);
    }
}
