<?php

use App\Models\VesselTripDetail;
use App\Repositories\VesselTripDetailRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VesselTripDetailRepositoryTest extends TestCase
{
    use MakeVesselTripDetailTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var VesselTripDetailRepository
     */
    protected $vesselTripDetailRepo;

    public function setUp()
    {
        parent::setUp();
        $this->vesselTripDetailRepo = App::make(VesselTripDetailRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateVesselTripDetail()
    {
        $vesselTripDetail = $this->fakeVesselTripDetailData();
        $createdVesselTripDetail = $this->vesselTripDetailRepo->create($vesselTripDetail);
        $createdVesselTripDetail = $createdVesselTripDetail->toArray();
        $this->assertArrayHasKey('id', $createdVesselTripDetail);
        $this->assertNotNull($createdVesselTripDetail['id'], 'Created VesselTripDetail must have id specified');
        $this->assertNotNull(VesselTripDetail::find($createdVesselTripDetail['id']), 'VesselTripDetail with given id must be in DB');
        $this->assertModelData($vesselTripDetail, $createdVesselTripDetail);
    }

    /**
     * @test read
     */
    public function testReadVesselTripDetail()
    {
        $vesselTripDetail = $this->makeVesselTripDetail();
        $dbVesselTripDetail = $this->vesselTripDetailRepo->find($vesselTripDetail->id);
        $dbVesselTripDetail = $dbVesselTripDetail->toArray();
        $this->assertModelData($vesselTripDetail->toArray(), $dbVesselTripDetail);
    }

    /**
     * @test update
     */
    public function testUpdateVesselTripDetail()
    {
        $vesselTripDetail = $this->makeVesselTripDetail();
        $fakeVesselTripDetail = $this->fakeVesselTripDetailData();
        $updatedVesselTripDetail = $this->vesselTripDetailRepo->update($fakeVesselTripDetail, $vesselTripDetail->id);
        $this->assertModelData($fakeVesselTripDetail, $updatedVesselTripDetail->toArray());
        $dbVesselTripDetail = $this->vesselTripDetailRepo->find($vesselTripDetail->id);
        $this->assertModelData($fakeVesselTripDetail, $dbVesselTripDetail->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteVesselTripDetail()
    {
        $vesselTripDetail = $this->makeVesselTripDetail();
        $resp = $this->vesselTripDetailRepo->delete($vesselTripDetail->id);
        $this->assertTrue($resp);
        $this->assertNull(VesselTripDetail::find($vesselTripDetail->id), 'VesselTripDetail should not exist in DB');
    }
}
