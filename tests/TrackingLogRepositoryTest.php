<?php

use App\Models\TrackingLog;
use App\Repositories\TrackingLogRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TrackingLogRepositoryTest extends TestCase
{
    use MakeTrackingLogTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TrackingLogRepository
     */
    protected $trackingLogRepo;

    public function setUp()
    {
        parent::setUp();
        $this->trackingLogRepo = App::make(TrackingLogRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTrackingLog()
    {
        $trackingLog = $this->fakeTrackingLogData();
        $createdTrackingLog = $this->trackingLogRepo->create($trackingLog);
        $createdTrackingLog = $createdTrackingLog->toArray();
        $this->assertArrayHasKey('id', $createdTrackingLog);
        $this->assertNotNull($createdTrackingLog['id'], 'Created TrackingLog must have id specified');
        $this->assertNotNull(TrackingLog::find($createdTrackingLog['id']), 'TrackingLog with given id must be in DB');
        $this->assertModelData($trackingLog, $createdTrackingLog);
    }

    /**
     * @test read
     */
    public function testReadTrackingLog()
    {
        $trackingLog = $this->makeTrackingLog();
        $dbTrackingLog = $this->trackingLogRepo->find($trackingLog->id);
        $dbTrackingLog = $dbTrackingLog->toArray();
        $this->assertModelData($trackingLog->toArray(), $dbTrackingLog);
    }

    /**
     * @test update
     */
    public function testUpdateTrackingLog()
    {
        $trackingLog = $this->makeTrackingLog();
        $fakeTrackingLog = $this->fakeTrackingLogData();
        $updatedTrackingLog = $this->trackingLogRepo->update($fakeTrackingLog, $trackingLog->id);
        $this->assertModelData($fakeTrackingLog, $updatedTrackingLog->toArray());
        $dbTrackingLog = $this->trackingLogRepo->find($trackingLog->id);
        $this->assertModelData($fakeTrackingLog, $dbTrackingLog->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTrackingLog()
    {
        $trackingLog = $this->makeTrackingLog();
        $resp = $this->trackingLogRepo->delete($trackingLog->id);
        $this->assertTrue($resp);
        $this->assertNull(TrackingLog::find($trackingLog->id), 'TrackingLog should not exist in DB');
    }
}
