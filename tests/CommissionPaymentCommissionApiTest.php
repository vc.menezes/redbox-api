<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommissionPaymentCommissionApiTest extends TestCase
{
    use MakeCommissionPaymentCommissionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCommissionPaymentCommission()
    {
        $commissionPaymentCommission = $this->fakeCommissionPaymentCommissionData();
        $this->json('POST', '/api/v1/commissionPaymentCommissions', $commissionPaymentCommission);

        $this->assertApiResponse($commissionPaymentCommission);
    }

    /**
     * @test
     */
    public function testReadCommissionPaymentCommission()
    {
        $commissionPaymentCommission = $this->makeCommissionPaymentCommission();
        $this->json('GET', '/api/v1/commissionPaymentCommissions/'.$commissionPaymentCommission->id);

        $this->assertApiResponse($commissionPaymentCommission->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCommissionPaymentCommission()
    {
        $commissionPaymentCommission = $this->makeCommissionPaymentCommission();
        $editedCommissionPaymentCommission = $this->fakeCommissionPaymentCommissionData();

        $this->json('PUT', '/api/v1/commissionPaymentCommissions/'.$commissionPaymentCommission->id, $editedCommissionPaymentCommission);

        $this->assertApiResponse($editedCommissionPaymentCommission);
    }

    /**
     * @test
     */
    public function testDeleteCommissionPaymentCommission()
    {
        $commissionPaymentCommission = $this->makeCommissionPaymentCommission();
        $this->json('DELETE', '/api/v1/commissionPaymentCommissions/'.$commissionPaymentCommission->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/commissionPaymentCommissions/'.$commissionPaymentCommission->id);

        $this->assertResponseStatus(404);
    }
}
