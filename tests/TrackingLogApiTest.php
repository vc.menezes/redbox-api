<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TrackingLogApiTest extends TestCase
{
    use MakeTrackingLogTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTrackingLog()
    {
        $trackingLog = $this->fakeTrackingLogData();
        $this->json('POST', '/api/v1/trackingLogs', $trackingLog);

        $this->assertApiResponse($trackingLog);
    }

    /**
     * @test
     */
    public function testReadTrackingLog()
    {
        $trackingLog = $this->makeTrackingLog();
        $this->json('GET', '/api/v1/trackingLogs/'.$trackingLog->id);

        $this->assertApiResponse($trackingLog->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTrackingLog()
    {
        $trackingLog = $this->makeTrackingLog();
        $editedTrackingLog = $this->fakeTrackingLogData();

        $this->json('PUT', '/api/v1/trackingLogs/'.$trackingLog->id, $editedTrackingLog);

        $this->assertApiResponse($editedTrackingLog);
    }

    /**
     * @test
     */
    public function testDeleteTrackingLog()
    {
        $trackingLog = $this->makeTrackingLog();
        $this->json('DELETE', '/api/v1/trackingLogs/'.$trackingLog->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/trackingLogs/'.$trackingLog->id);

        $this->assertResponseStatus(404);
    }
}
