<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommodityApiTest extends TestCase
{
    use MakeCommodityTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCommodity()
    {
        $commodity = $this->fakeCommodityData();
        $this->json('POST', '/api/v1/commodities', $commodity);

        $this->assertApiResponse($commodity);
    }

    /**
     * @test
     */
    public function testReadCommodity()
    {
        $commodity = $this->makeCommodity();
        $this->json('GET', '/api/v1/commodities/'.$commodity->id);

        $this->assertApiResponse($commodity->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCommodity()
    {
        $commodity = $this->makeCommodity();
        $editedCommodity = $this->fakeCommodityData();

        $this->json('PUT', '/api/v1/commodities/'.$commodity->id, $editedCommodity);

        $this->assertApiResponse($editedCommodity);
    }

    /**
     * @test
     */
    public function testDeleteCommodity()
    {
        $commodity = $this->makeCommodity();
        $this->json('DELETE', '/api/v1/commodities/'.$commodity->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/commodities/'.$commodity->id);

        $this->assertResponseStatus(404);
    }
}
