<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VesselRouteApiTest extends TestCase
{
    use MakeVesselRouteTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateVesselRoute()
    {
        $vesselRoute = $this->fakeVesselRouteData();
        $this->json('POST', '/api/v1/vesselRoutes', $vesselRoute);

        $this->assertApiResponse($vesselRoute);
    }

    /**
     * @test
     */
    public function testReadVesselRoute()
    {
        $vesselRoute = $this->makeVesselRoute();
        $this->json('GET', '/api/v1/vesselRoutes/'.$vesselRoute->id);

        $this->assertApiResponse($vesselRoute->toArray());
    }

    /**
     * @test
     */
    public function testUpdateVesselRoute()
    {
        $vesselRoute = $this->makeVesselRoute();
        $editedVesselRoute = $this->fakeVesselRouteData();

        $this->json('PUT', '/api/v1/vesselRoutes/'.$vesselRoute->id, $editedVesselRoute);

        $this->assertApiResponse($editedVesselRoute);
    }

    /**
     * @test
     */
    public function testDeleteVesselRoute()
    {
        $vesselRoute = $this->makeVesselRoute();
        $this->json('DELETE', '/api/v1/vesselRoutes/'.$vesselRoute->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/vesselRoutes/'.$vesselRoute->id);

        $this->assertResponseStatus(404);
    }
}
