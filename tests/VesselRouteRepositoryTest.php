<?php

use App\Models\VesselRoute;
use App\Repositories\VesselRouteRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VesselRouteRepositoryTest extends TestCase
{
    use MakeVesselRouteTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var VesselRouteRepository
     */
    protected $vesselRouteRepo;

    public function setUp()
    {
        parent::setUp();
        $this->vesselRouteRepo = App::make(VesselRouteRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateVesselRoute()
    {
        $vesselRoute = $this->fakeVesselRouteData();
        $createdVesselRoute = $this->vesselRouteRepo->create($vesselRoute);
        $createdVesselRoute = $createdVesselRoute->toArray();
        $this->assertArrayHasKey('id', $createdVesselRoute);
        $this->assertNotNull($createdVesselRoute['id'], 'Created VesselRoute must have id specified');
        $this->assertNotNull(VesselRoute::find($createdVesselRoute['id']), 'VesselRoute with given id must be in DB');
        $this->assertModelData($vesselRoute, $createdVesselRoute);
    }

    /**
     * @test read
     */
    public function testReadVesselRoute()
    {
        $vesselRoute = $this->makeVesselRoute();
        $dbVesselRoute = $this->vesselRouteRepo->find($vesselRoute->id);
        $dbVesselRoute = $dbVesselRoute->toArray();
        $this->assertModelData($vesselRoute->toArray(), $dbVesselRoute);
    }

    /**
     * @test update
     */
    public function testUpdateVesselRoute()
    {
        $vesselRoute = $this->makeVesselRoute();
        $fakeVesselRoute = $this->fakeVesselRouteData();
        $updatedVesselRoute = $this->vesselRouteRepo->update($fakeVesselRoute, $vesselRoute->id);
        $this->assertModelData($fakeVesselRoute, $updatedVesselRoute->toArray());
        $dbVesselRoute = $this->vesselRouteRepo->find($vesselRoute->id);
        $this->assertModelData($fakeVesselRoute, $dbVesselRoute->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteVesselRoute()
    {
        $vesselRoute = $this->makeVesselRoute();
        $resp = $this->vesselRouteRepo->delete($vesselRoute->id);
        $this->assertTrue($resp);
        $this->assertNull(VesselRoute::find($vesselRoute->id), 'VesselRoute should not exist in DB');
    }
}
