<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReceiptCardApiTest extends TestCase
{
    use MakeReceiptCardTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateReceiptCard()
    {
        $receiptCard = $this->fakeReceiptCardData();
        $this->json('POST', '/api/v1/receiptCards', $receiptCard);

        $this->assertApiResponse($receiptCard);
    }

    /**
     * @test
     */
    public function testReadReceiptCard()
    {
        $receiptCard = $this->makeReceiptCard();
        $this->json('GET', '/api/v1/receiptCards/'.$receiptCard->id);

        $this->assertApiResponse($receiptCard->toArray());
    }

    /**
     * @test
     */
    public function testUpdateReceiptCard()
    {
        $receiptCard = $this->makeReceiptCard();
        $editedReceiptCard = $this->fakeReceiptCardData();

        $this->json('PUT', '/api/v1/receiptCards/'.$receiptCard->id, $editedReceiptCard);

        $this->assertApiResponse($editedReceiptCard);
    }

    /**
     * @test
     */
    public function testDeleteReceiptCard()
    {
        $receiptCard = $this->makeReceiptCard();
        $this->json('DELETE', '/api/v1/receiptCards/'.$receiptCard->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/receiptCards/'.$receiptCard->id);

        $this->assertResponseStatus(404);
    }
}
