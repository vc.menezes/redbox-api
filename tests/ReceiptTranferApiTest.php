<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReceiptTranferApiTest extends TestCase
{
    use MakeReceiptTranferTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateReceiptTranfer()
    {
        $receiptTranfer = $this->fakeReceiptTranferData();
        $this->json('POST', '/api/v1/receiptTranfers', $receiptTranfer);

        $this->assertApiResponse($receiptTranfer);
    }

    /**
     * @test
     */
    public function testReadReceiptTranfer()
    {
        $receiptTranfer = $this->makeReceiptTranfer();
        $this->json('GET', '/api/v1/receiptTranfers/'.$receiptTranfer->id);

        $this->assertApiResponse($receiptTranfer->toArray());
    }

    /**
     * @test
     */
    public function testUpdateReceiptTranfer()
    {
        $receiptTranfer = $this->makeReceiptTranfer();
        $editedReceiptTranfer = $this->fakeReceiptTranferData();

        $this->json('PUT', '/api/v1/receiptTranfers/'.$receiptTranfer->id, $editedReceiptTranfer);

        $this->assertApiResponse($editedReceiptTranfer);
    }

    /**
     * @test
     */
    public function testDeleteReceiptTranfer()
    {
        $receiptTranfer = $this->makeReceiptTranfer();
        $this->json('DELETE', '/api/v1/receiptTranfers/'.$receiptTranfer->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/receiptTranfers/'.$receiptTranfer->id);

        $this->assertResponseStatus(404);
    }
}
