<?php

use App\Models\Remark;
use App\Repositories\RemarkRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RemarkRepositoryTest extends TestCase
{
    use MakeRemarkTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RemarkRepository
     */
    protected $remarkRepo;

    public function setUp()
    {
        parent::setUp();
        $this->remarkRepo = App::make(RemarkRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRemark()
    {
        $remark = $this->fakeRemarkData();
        $createdRemark = $this->remarkRepo->create($remark);
        $createdRemark = $createdRemark->toArray();
        $this->assertArrayHasKey('id', $createdRemark);
        $this->assertNotNull($createdRemark['id'], 'Created Remark must have id specified');
        $this->assertNotNull(Remark::find($createdRemark['id']), 'Remark with given id must be in DB');
        $this->assertModelData($remark, $createdRemark);
    }

    /**
     * @test read
     */
    public function testReadRemark()
    {
        $remark = $this->makeRemark();
        $dbRemark = $this->remarkRepo->find($remark->id);
        $dbRemark = $dbRemark->toArray();
        $this->assertModelData($remark->toArray(), $dbRemark);
    }

    /**
     * @test update
     */
    public function testUpdateRemark()
    {
        $remark = $this->makeRemark();
        $fakeRemark = $this->fakeRemarkData();
        $updatedRemark = $this->remarkRepo->update($fakeRemark, $remark->id);
        $this->assertModelData($fakeRemark, $updatedRemark->toArray());
        $dbRemark = $this->remarkRepo->find($remark->id);
        $this->assertModelData($fakeRemark, $dbRemark->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRemark()
    {
        $remark = $this->makeRemark();
        $resp = $this->remarkRepo->delete($remark->id);
        $this->assertTrue($resp);
        $this->assertNull(Remark::find($remark->id), 'Remark should not exist in DB');
    }
}
