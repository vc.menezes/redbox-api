<?php

use App\Models\InvoiceReceipt;
use App\Repositories\InvoiceReceiptRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InvoiceReceiptRepositoryTest extends TestCase
{
    use MakeInvoiceReceiptTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var InvoiceReceiptRepository
     */
    protected $invoiceReceiptRepo;

    public function setUp()
    {
        parent::setUp();
        $this->invoiceReceiptRepo = App::make(InvoiceReceiptRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateInvoiceReceipt()
    {
        $invoiceReceipt = $this->fakeInvoiceReceiptData();
        $createdInvoiceReceipt = $this->invoiceReceiptRepo->create($invoiceReceipt);
        $createdInvoiceReceipt = $createdInvoiceReceipt->toArray();
        $this->assertArrayHasKey('id', $createdInvoiceReceipt);
        $this->assertNotNull($createdInvoiceReceipt['id'], 'Created InvoiceReceipt must have id specified');
        $this->assertNotNull(InvoiceReceipt::find($createdInvoiceReceipt['id']), 'InvoiceReceipt with given id must be in DB');
        $this->assertModelData($invoiceReceipt, $createdInvoiceReceipt);
    }

    /**
     * @test read
     */
    public function testReadInvoiceReceipt()
    {
        $invoiceReceipt = $this->makeInvoiceReceipt();
        $dbInvoiceReceipt = $this->invoiceReceiptRepo->find($invoiceReceipt->id);
        $dbInvoiceReceipt = $dbInvoiceReceipt->toArray();
        $this->assertModelData($invoiceReceipt->toArray(), $dbInvoiceReceipt);
    }

    /**
     * @test update
     */
    public function testUpdateInvoiceReceipt()
    {
        $invoiceReceipt = $this->makeInvoiceReceipt();
        $fakeInvoiceReceipt = $this->fakeInvoiceReceiptData();
        $updatedInvoiceReceipt = $this->invoiceReceiptRepo->update($fakeInvoiceReceipt, $invoiceReceipt->id);
        $this->assertModelData($fakeInvoiceReceipt, $updatedInvoiceReceipt->toArray());
        $dbInvoiceReceipt = $this->invoiceReceiptRepo->find($invoiceReceipt->id);
        $this->assertModelData($fakeInvoiceReceipt, $dbInvoiceReceipt->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteInvoiceReceipt()
    {
        $invoiceReceipt = $this->makeInvoiceReceipt();
        $resp = $this->invoiceReceiptRepo->delete($invoiceReceipt->id);
        $this->assertTrue($resp);
        $this->assertNull(InvoiceReceipt::find($invoiceReceipt->id), 'InvoiceReceipt should not exist in DB');
    }
}
