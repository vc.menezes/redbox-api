<?php

use App\Models\ReceiptView;
use App\Repositories\ReceiptViewRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReceiptViewRepositoryTest extends TestCase
{
    use MakeReceiptViewTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ReceiptViewRepository
     */
    protected $receiptViewRepo;

    public function setUp()
    {
        parent::setUp();
        $this->receiptViewRepo = App::make(ReceiptViewRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateReceiptView()
    {
        $receiptView = $this->fakeReceiptViewData();
        $createdReceiptView = $this->receiptViewRepo->create($receiptView);
        $createdReceiptView = $createdReceiptView->toArray();
        $this->assertArrayHasKey('id', $createdReceiptView);
        $this->assertNotNull($createdReceiptView['id'], 'Created ReceiptView must have id specified');
        $this->assertNotNull(ReceiptView::find($createdReceiptView['id']), 'ReceiptView with given id must be in DB');
        $this->assertModelData($receiptView, $createdReceiptView);
    }

    /**
     * @test read
     */
    public function testReadReceiptView()
    {
        $receiptView = $this->makeReceiptView();
        $dbReceiptView = $this->receiptViewRepo->find($receiptView->id);
        $dbReceiptView = $dbReceiptView->toArray();
        $this->assertModelData($receiptView->toArray(), $dbReceiptView);
    }

    /**
     * @test update
     */
    public function testUpdateReceiptView()
    {
        $receiptView = $this->makeReceiptView();
        $fakeReceiptView = $this->fakeReceiptViewData();
        $updatedReceiptView = $this->receiptViewRepo->update($fakeReceiptView, $receiptView->id);
        $this->assertModelData($fakeReceiptView, $updatedReceiptView->toArray());
        $dbReceiptView = $this->receiptViewRepo->find($receiptView->id);
        $this->assertModelData($fakeReceiptView, $dbReceiptView->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteReceiptView()
    {
        $receiptView = $this->makeReceiptView();
        $resp = $this->receiptViewRepo->delete($receiptView->id);
        $this->assertTrue($resp);
        $this->assertNull(ReceiptView::find($receiptView->id), 'ReceiptView should not exist in DB');
    }
}
