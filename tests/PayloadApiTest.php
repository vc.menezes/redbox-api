<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PayloadApiTest extends TestCase
{
    use MakePayloadTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePayload()
    {
        $payload = $this->fakePayloadData();
        $this->json('POST', '/api/v1/payloads', $payload);

        $this->assertApiResponse($payload);
    }

    /**
     * @test
     */
    public function testReadPayload()
    {
        $payload = $this->makePayload();
        $this->json('GET', '/api/v1/payloads/'.$payload->id);

        $this->assertApiResponse($payload->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePayload()
    {
        $payload = $this->makePayload();
        $editedPayload = $this->fakePayloadData();

        $this->json('PUT', '/api/v1/payloads/'.$payload->id, $editedPayload);

        $this->assertApiResponse($editedPayload);
    }

    /**
     * @test
     */
    public function testDeletePayload()
    {
        $payload = $this->makePayload();
        $this->json('DELETE', '/api/v1/payloads/'.$payload->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/payloads/'.$payload->id);

        $this->assertResponseStatus(404);
    }
}
