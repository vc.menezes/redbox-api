<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class attributableApiTest extends TestCase
{
    use MakeattributableTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateattributable()
    {
        $attributable = $this->fakeattributableData();
        $this->json('POST', '/api/v1/attributables', $attributable);

        $this->assertApiResponse($attributable);
    }

    /**
     * @test
     */
    public function testReadattributable()
    {
        $attributable = $this->makeattributable();
        $this->json('GET', '/api/v1/attributables/'.$attributable->id);

        $this->assertApiResponse($attributable->toArray());
    }

    /**
     * @test
     */
    public function testUpdateattributable()
    {
        $attributable = $this->makeattributable();
        $editedattributable = $this->fakeattributableData();

        $this->json('PUT', '/api/v1/attributables/'.$attributable->id, $editedattributable);

        $this->assertApiResponse($editedattributable);
    }

    /**
     * @test
     */
    public function testDeleteattributable()
    {
        $attributable = $this->makeattributable();
        $this->json('DELETE', '/api/v1/attributables/'.$attributable->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/attributables/'.$attributable->id);

        $this->assertResponseStatus(404);
    }
}
