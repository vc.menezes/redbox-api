<?php

use App\Models\CardType;
use App\Repositories\CardTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CardTypeRepositoryTest extends TestCase
{
    use MakeCardTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CardTypeRepository
     */
    protected $cardTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->cardTypeRepo = App::make(CardTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCardType()
    {
        $cardType = $this->fakeCardTypeData();
        $createdCardType = $this->cardTypeRepo->create($cardType);
        $createdCardType = $createdCardType->toArray();
        $this->assertArrayHasKey('id', $createdCardType);
        $this->assertNotNull($createdCardType['id'], 'Created CardType must have id specified');
        $this->assertNotNull(CardType::find($createdCardType['id']), 'CardType with given id must be in DB');
        $this->assertModelData($cardType, $createdCardType);
    }

    /**
     * @test read
     */
    public function testReadCardType()
    {
        $cardType = $this->makeCardType();
        $dbCardType = $this->cardTypeRepo->find($cardType->id);
        $dbCardType = $dbCardType->toArray();
        $this->assertModelData($cardType->toArray(), $dbCardType);
    }

    /**
     * @test update
     */
    public function testUpdateCardType()
    {
        $cardType = $this->makeCardType();
        $fakeCardType = $this->fakeCardTypeData();
        $updatedCardType = $this->cardTypeRepo->update($fakeCardType, $cardType->id);
        $this->assertModelData($fakeCardType, $updatedCardType->toArray());
        $dbCardType = $this->cardTypeRepo->find($cardType->id);
        $this->assertModelData($fakeCardType, $dbCardType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCardType()
    {
        $cardType = $this->makeCardType();
        $resp = $this->cardTypeRepo->delete($cardType->id);
        $this->assertTrue($resp);
        $this->assertNull(CardType::find($cardType->id), 'CardType should not exist in DB');
    }
}
