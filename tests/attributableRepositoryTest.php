<?php

use App\Models\attributable;
use App\Repositories\attributableRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class attributableRepositoryTest extends TestCase
{
    use MakeattributableTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var attributableRepository
     */
    protected $attributableRepo;

    public function setUp()
    {
        parent::setUp();
        $this->attributableRepo = App::make(attributableRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateattributable()
    {
        $attributable = $this->fakeattributableData();
        $createdattributable = $this->attributableRepo->create($attributable);
        $createdattributable = $createdattributable->toArray();
        $this->assertArrayHasKey('id', $createdattributable);
        $this->assertNotNull($createdattributable['id'], 'Created attributable must have id specified');
        $this->assertNotNull(attributable::find($createdattributable['id']), 'attributable with given id must be in DB');
        $this->assertModelData($attributable, $createdattributable);
    }

    /**
     * @test read
     */
    public function testReadattributable()
    {
        $attributable = $this->makeattributable();
        $dbattributable = $this->attributableRepo->find($attributable->id);
        $dbattributable = $dbattributable->toArray();
        $this->assertModelData($attributable->toArray(), $dbattributable);
    }

    /**
     * @test update
     */
    public function testUpdateattributable()
    {
        $attributable = $this->makeattributable();
        $fakeattributable = $this->fakeattributableData();
        $updatedattributable = $this->attributableRepo->update($fakeattributable, $attributable->id);
        $this->assertModelData($fakeattributable, $updatedattributable->toArray());
        $dbattributable = $this->attributableRepo->find($attributable->id);
        $this->assertModelData($fakeattributable, $dbattributable->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteattributable()
    {
        $attributable = $this->makeattributable();
        $resp = $this->attributableRepo->delete($attributable->id);
        $this->assertTrue($resp);
        $this->assertNull(attributable::find($attributable->id), 'attributable should not exist in DB');
    }
}
