<?php

use App\Models\CommissionPayment;
use App\Repositories\CommissionPaymentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommissionPaymentRepositoryTest extends TestCase
{
    use MakeCommissionPaymentTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CommissionPaymentRepository
     */
    protected $commissionPaymentRepo;

    public function setUp()
    {
        parent::setUp();
        $this->commissionPaymentRepo = App::make(CommissionPaymentRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCommissionPayment()
    {
        $commissionPayment = $this->fakeCommissionPaymentData();
        $createdCommissionPayment = $this->commissionPaymentRepo->create($commissionPayment);
        $createdCommissionPayment = $createdCommissionPayment->toArray();
        $this->assertArrayHasKey('id', $createdCommissionPayment);
        $this->assertNotNull($createdCommissionPayment['id'], 'Created CommissionPayment must have id specified');
        $this->assertNotNull(CommissionPayment::find($createdCommissionPayment['id']), 'CommissionPayment with given id must be in DB');
        $this->assertModelData($commissionPayment, $createdCommissionPayment);
    }

    /**
     * @test read
     */
    public function testReadCommissionPayment()
    {
        $commissionPayment = $this->makeCommissionPayment();
        $dbCommissionPayment = $this->commissionPaymentRepo->find($commissionPayment->id);
        $dbCommissionPayment = $dbCommissionPayment->toArray();
        $this->assertModelData($commissionPayment->toArray(), $dbCommissionPayment);
    }

    /**
     * @test update
     */
    public function testUpdateCommissionPayment()
    {
        $commissionPayment = $this->makeCommissionPayment();
        $fakeCommissionPayment = $this->fakeCommissionPaymentData();
        $updatedCommissionPayment = $this->commissionPaymentRepo->update($fakeCommissionPayment, $commissionPayment->id);
        $this->assertModelData($fakeCommissionPayment, $updatedCommissionPayment->toArray());
        $dbCommissionPayment = $this->commissionPaymentRepo->find($commissionPayment->id);
        $this->assertModelData($fakeCommissionPayment, $dbCommissionPayment->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCommissionPayment()
    {
        $commissionPayment = $this->makeCommissionPayment();
        $resp = $this->commissionPaymentRepo->delete($commissionPayment->id);
        $this->assertTrue($resp);
        $this->assertNull(CommissionPayment::find($commissionPayment->id), 'CommissionPayment should not exist in DB');
    }
}
