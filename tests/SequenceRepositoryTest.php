<?php

use App\Models\Sequence;
use App\Repositories\SequenceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SequenceRepositoryTest extends TestCase
{
    use MakeSequenceTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SequenceRepository
     */
    protected $sequenceRepo;

    public function setUp()
    {
        parent::setUp();
        $this->sequenceRepo = App::make(SequenceRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSequence()
    {
        $sequence = $this->fakeSequenceData();
        $createdSequence = $this->sequenceRepo->create($sequence);
        $createdSequence = $createdSequence->toArray();
        $this->assertArrayHasKey('id', $createdSequence);
        $this->assertNotNull($createdSequence['id'], 'Created Sequence must have id specified');
        $this->assertNotNull(Sequence::find($createdSequence['id']), 'Sequence with given id must be in DB');
        $this->assertModelData($sequence, $createdSequence);
    }

    /**
     * @test read
     */
    public function testReadSequence()
    {
        $sequence = $this->makeSequence();
        $dbSequence = $this->sequenceRepo->find($sequence->id);
        $dbSequence = $dbSequence->toArray();
        $this->assertModelData($sequence->toArray(), $dbSequence);
    }

    /**
     * @test update
     */
    public function testUpdateSequence()
    {
        $sequence = $this->makeSequence();
        $fakeSequence = $this->fakeSequenceData();
        $updatedSequence = $this->sequenceRepo->update($fakeSequence, $sequence->id);
        $this->assertModelData($fakeSequence, $updatedSequence->toArray());
        $dbSequence = $this->sequenceRepo->find($sequence->id);
        $this->assertModelData($fakeSequence, $dbSequence->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSequence()
    {
        $sequence = $this->makeSequence();
        $resp = $this->sequenceRepo->delete($sequence->id);
        $this->assertTrue($resp);
        $this->assertNull(Sequence::find($sequence->id), 'Sequence should not exist in DB');
    }
}
