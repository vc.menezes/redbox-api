<?php

use App\Models\AgentCommission;
use App\Repositories\AgentCommissionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AgentCommissionRepositoryTest extends TestCase
{
    use MakeAgentCommissionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AgentCommissionRepository
     */
    protected $agentCommissionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->agentCommissionRepo = App::make(AgentCommissionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAgentCommission()
    {
        $agentCommission = $this->fakeAgentCommissionData();
        $createdAgentCommission = $this->agentCommissionRepo->create($agentCommission);
        $createdAgentCommission = $createdAgentCommission->toArray();
        $this->assertArrayHasKey('id', $createdAgentCommission);
        $this->assertNotNull($createdAgentCommission['id'], 'Created AgentCommission must have id specified');
        $this->assertNotNull(AgentCommission::find($createdAgentCommission['id']), 'AgentCommission with given id must be in DB');
        $this->assertModelData($agentCommission, $createdAgentCommission);
    }

    /**
     * @test read
     */
    public function testReadAgentCommission()
    {
        $agentCommission = $this->makeAgentCommission();
        $dbAgentCommission = $this->agentCommissionRepo->find($agentCommission->id);
        $dbAgentCommission = $dbAgentCommission->toArray();
        $this->assertModelData($agentCommission->toArray(), $dbAgentCommission);
    }

    /**
     * @test update
     */
    public function testUpdateAgentCommission()
    {
        $agentCommission = $this->makeAgentCommission();
        $fakeAgentCommission = $this->fakeAgentCommissionData();
        $updatedAgentCommission = $this->agentCommissionRepo->update($fakeAgentCommission, $agentCommission->id);
        $this->assertModelData($fakeAgentCommission, $updatedAgentCommission->toArray());
        $dbAgentCommission = $this->agentCommissionRepo->find($agentCommission->id);
        $this->assertModelData($fakeAgentCommission, $dbAgentCommission->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAgentCommission()
    {
        $agentCommission = $this->makeAgentCommission();
        $resp = $this->agentCommissionRepo->delete($agentCommission->id);
        $this->assertTrue($resp);
        $this->assertNull(AgentCommission::find($agentCommission->id), 'AgentCommission should not exist in DB');
    }
}
