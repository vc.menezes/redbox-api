<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReceiptApiTest extends TestCase
{
    use MakeReceiptTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateReceipt()
    {
        $receipt = $this->fakeReceiptData();
        $this->json('POST', '/api/v1/receipts', $receipt);

        $this->assertApiResponse($receipt);
    }

    /**
     * @test
     */
    public function testReadReceipt()
    {
        $receipt = $this->makeReceipt();
        $this->json('GET', '/api/v1/receipts/'.$receipt->id);

        $this->assertApiResponse($receipt->toArray());
    }

    /**
     * @test
     */
    public function testUpdateReceipt()
    {
        $receipt = $this->makeReceipt();
        $editedReceipt = $this->fakeReceiptData();

        $this->json('PUT', '/api/v1/receipts/'.$receipt->id, $editedReceipt);

        $this->assertApiResponse($editedReceipt);
    }

    /**
     * @test
     */
    public function testDeleteReceipt()
    {
        $receipt = $this->makeReceipt();
        $this->json('DELETE', '/api/v1/receipts/'.$receipt->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/receipts/'.$receipt->id);

        $this->assertResponseStatus(404);
    }
}
