<?php

use App\Models\BulkOrderPackage;
use App\Repositories\BulkOrderPackageRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BulkOrderPackageRepositoryTest extends TestCase
{
    use MakeBulkOrderPackageTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BulkOrderPackageRepository
     */
    protected $bulkOrderPackageRepo;

    public function setUp()
    {
        parent::setUp();
        $this->bulkOrderPackageRepo = App::make(BulkOrderPackageRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBulkOrderPackage()
    {
        $bulkOrderPackage = $this->fakeBulkOrderPackageData();
        $createdBulkOrderPackage = $this->bulkOrderPackageRepo->create($bulkOrderPackage);
        $createdBulkOrderPackage = $createdBulkOrderPackage->toArray();
        $this->assertArrayHasKey('id', $createdBulkOrderPackage);
        $this->assertNotNull($createdBulkOrderPackage['id'], 'Created BulkOrderPackage must have id specified');
        $this->assertNotNull(BulkOrderPackage::find($createdBulkOrderPackage['id']), 'BulkOrderPackage with given id must be in DB');
        $this->assertModelData($bulkOrderPackage, $createdBulkOrderPackage);
    }

    /**
     * @test read
     */
    public function testReadBulkOrderPackage()
    {
        $bulkOrderPackage = $this->makeBulkOrderPackage();
        $dbBulkOrderPackage = $this->bulkOrderPackageRepo->find($bulkOrderPackage->id);
        $dbBulkOrderPackage = $dbBulkOrderPackage->toArray();
        $this->assertModelData($bulkOrderPackage->toArray(), $dbBulkOrderPackage);
    }

    /**
     * @test update
     */
    public function testUpdateBulkOrderPackage()
    {
        $bulkOrderPackage = $this->makeBulkOrderPackage();
        $fakeBulkOrderPackage = $this->fakeBulkOrderPackageData();
        $updatedBulkOrderPackage = $this->bulkOrderPackageRepo->update($fakeBulkOrderPackage, $bulkOrderPackage->id);
        $this->assertModelData($fakeBulkOrderPackage, $updatedBulkOrderPackage->toArray());
        $dbBulkOrderPackage = $this->bulkOrderPackageRepo->find($bulkOrderPackage->id);
        $this->assertModelData($fakeBulkOrderPackage, $dbBulkOrderPackage->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBulkOrderPackage()
    {
        $bulkOrderPackage = $this->makeBulkOrderPackage();
        $resp = $this->bulkOrderPackageRepo->delete($bulkOrderPackage->id);
        $this->assertTrue($resp);
        $this->assertNull(BulkOrderPackage::find($bulkOrderPackage->id), 'BulkOrderPackage should not exist in DB');
    }
}
