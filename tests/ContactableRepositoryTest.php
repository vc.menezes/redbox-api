<?php

use App\Models\Contactable;
use App\Repositories\ContactableRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactableRepositoryTest extends TestCase
{
    use MakeContactableTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ContactableRepository
     */
    protected $contactableRepo;

    public function setUp()
    {
        parent::setUp();
        $this->contactableRepo = App::make(ContactableRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateContactable()
    {
        $contactable = $this->fakeContactableData();
        $createdContactable = $this->contactableRepo->create($contactable);
        $createdContactable = $createdContactable->toArray();
        $this->assertArrayHasKey('id', $createdContactable);
        $this->assertNotNull($createdContactable['id'], 'Created Contactable must have id specified');
        $this->assertNotNull(Contactable::find($createdContactable['id']), 'Contactable with given id must be in DB');
        $this->assertModelData($contactable, $createdContactable);
    }

    /**
     * @test read
     */
    public function testReadContactable()
    {
        $contactable = $this->makeContactable();
        $dbContactable = $this->contactableRepo->find($contactable->id);
        $dbContactable = $dbContactable->toArray();
        $this->assertModelData($contactable->toArray(), $dbContactable);
    }

    /**
     * @test update
     */
    public function testUpdateContactable()
    {
        $contactable = $this->makeContactable();
        $fakeContactable = $this->fakeContactableData();
        $updatedContactable = $this->contactableRepo->update($fakeContactable, $contactable->id);
        $this->assertModelData($fakeContactable, $updatedContactable->toArray());
        $dbContactable = $this->contactableRepo->find($contactable->id);
        $this->assertModelData($fakeContactable, $dbContactable->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteContactable()
    {
        $contactable = $this->makeContactable();
        $resp = $this->contactableRepo->delete($contactable->id);
        $this->assertTrue($resp);
        $this->assertNull(Contactable::find($contactable->id), 'Contactable should not exist in DB');
    }
}
