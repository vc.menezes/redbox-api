<?php

use App\Models\Key;
use App\Repositories\KeyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class KeyRepositoryTest extends TestCase
{
    use MakeKeyTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var KeyRepository
     */
    protected $keyRepo;

    public function setUp()
    {
        parent::setUp();
        $this->keyRepo = App::make(KeyRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateKey()
    {
        $key = $this->fakeKeyData();
        $createdKey = $this->keyRepo->create($key);
        $createdKey = $createdKey->toArray();
        $this->assertArrayHasKey('id', $createdKey);
        $this->assertNotNull($createdKey['id'], 'Created Key must have id specified');
        $this->assertNotNull(Key::find($createdKey['id']), 'Key with given id must be in DB');
        $this->assertModelData($key, $createdKey);
    }

    /**
     * @test read
     */
    public function testReadKey()
    {
        $key = $this->makeKey();
        $dbKey = $this->keyRepo->find($key->id);
        $dbKey = $dbKey->toArray();
        $this->assertModelData($key->toArray(), $dbKey);
    }

    /**
     * @test update
     */
    public function testUpdateKey()
    {
        $key = $this->makeKey();
        $fakeKey = $this->fakeKeyData();
        $updatedKey = $this->keyRepo->update($fakeKey, $key->id);
        $this->assertModelData($fakeKey, $updatedKey->toArray());
        $dbKey = $this->keyRepo->find($key->id);
        $this->assertModelData($fakeKey, $dbKey->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteKey()
    {
        $key = $this->makeKey();
        $resp = $this->keyRepo->delete($key->id);
        $this->assertTrue($resp);
        $this->assertNull(Key::find($key->id), 'Key should not exist in DB');
    }
}
