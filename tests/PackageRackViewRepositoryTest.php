<?php

use App\Models\PackageRackView;
use App\Repositories\PackageRackViewRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackageRackViewRepositoryTest extends TestCase
{
    use MakePackageRackViewTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PackageRackViewRepository
     */
    protected $packageRackViewRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packageRackViewRepo = App::make(PackageRackViewRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePackageRackView()
    {
        $packageRackView = $this->fakePackageRackViewData();
        $createdPackageRackView = $this->packageRackViewRepo->create($packageRackView);
        $createdPackageRackView = $createdPackageRackView->toArray();
        $this->assertArrayHasKey('id', $createdPackageRackView);
        $this->assertNotNull($createdPackageRackView['id'], 'Created PackageRackView must have id specified');
        $this->assertNotNull(PackageRackView::find($createdPackageRackView['id']), 'PackageRackView with given id must be in DB');
        $this->assertModelData($packageRackView, $createdPackageRackView);
    }

    /**
     * @test read
     */
    public function testReadPackageRackView()
    {
        $packageRackView = $this->makePackageRackView();
        $dbPackageRackView = $this->packageRackViewRepo->find($packageRackView->id);
        $dbPackageRackView = $dbPackageRackView->toArray();
        $this->assertModelData($packageRackView->toArray(), $dbPackageRackView);
    }

    /**
     * @test update
     */
    public function testUpdatePackageRackView()
    {
        $packageRackView = $this->makePackageRackView();
        $fakePackageRackView = $this->fakePackageRackViewData();
        $updatedPackageRackView = $this->packageRackViewRepo->update($fakePackageRackView, $packageRackView->id);
        $this->assertModelData($fakePackageRackView, $updatedPackageRackView->toArray());
        $dbPackageRackView = $this->packageRackViewRepo->find($packageRackView->id);
        $this->assertModelData($fakePackageRackView, $dbPackageRackView->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePackageRackView()
    {
        $packageRackView = $this->makePackageRackView();
        $resp = $this->packageRackViewRepo->delete($packageRackView->id);
        $this->assertTrue($resp);
        $this->assertNull(PackageRackView::find($packageRackView->id), 'PackageRackView should not exist in DB');
    }
}
