<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BulkOrderViewApiTest extends TestCase
{
    use MakeBulkOrderViewTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBulkOrderView()
    {
        $bulkOrderView = $this->fakeBulkOrderViewData();
        $this->json('POST', '/api/v1/bulkOrderViews', $bulkOrderView);

        $this->assertApiResponse($bulkOrderView);
    }

    /**
     * @test
     */
    public function testReadBulkOrderView()
    {
        $bulkOrderView = $this->makeBulkOrderView();
        $this->json('GET', '/api/v1/bulkOrderViews/'.$bulkOrderView->id);

        $this->assertApiResponse($bulkOrderView->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBulkOrderView()
    {
        $bulkOrderView = $this->makeBulkOrderView();
        $editedBulkOrderView = $this->fakeBulkOrderViewData();

        $this->json('PUT', '/api/v1/bulkOrderViews/'.$bulkOrderView->id, $editedBulkOrderView);

        $this->assertApiResponse($editedBulkOrderView);
    }

    /**
     * @test
     */
    public function testDeleteBulkOrderView()
    {
        $bulkOrderView = $this->makeBulkOrderView();
        $this->json('DELETE', '/api/v1/bulkOrderViews/'.$bulkOrderView->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/bulkOrderViews/'.$bulkOrderView->id);

        $this->assertResponseStatus(404);
    }
}
