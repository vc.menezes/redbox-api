<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PickingChargeApiTest extends TestCase
{
    use MakePickingChargeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePickingCharge()
    {
        $pickingCharge = $this->fakePickingChargeData();
        $this->json('POST', '/api/v1/pickingCharges', $pickingCharge);

        $this->assertApiResponse($pickingCharge);
    }

    /**
     * @test
     */
    public function testReadPickingCharge()
    {
        $pickingCharge = $this->makePickingCharge();
        $this->json('GET', '/api/v1/pickingCharges/'.$pickingCharge->id);

        $this->assertApiResponse($pickingCharge->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePickingCharge()
    {
        $pickingCharge = $this->makePickingCharge();
        $editedPickingCharge = $this->fakePickingChargeData();

        $this->json('PUT', '/api/v1/pickingCharges/'.$pickingCharge->id, $editedPickingCharge);

        $this->assertApiResponse($editedPickingCharge);
    }

    /**
     * @test
     */
    public function testDeletePickingCharge()
    {
        $pickingCharge = $this->makePickingCharge();
        $this->json('DELETE', '/api/v1/pickingCharges/'.$pickingCharge->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/pickingCharges/'.$pickingCharge->id);

        $this->assertResponseStatus(404);
    }
}
