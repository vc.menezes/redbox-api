<?php

use App\Models\Vessel;
use App\Repositories\VesselRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VesselRepositoryTest extends TestCase
{
    use MakeVesselTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var VesselRepository
     */
    protected $vesselRepo;

    public function setUp()
    {
        parent::setUp();
        $this->vesselRepo = App::make(VesselRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateVessel()
    {
        $vessel = $this->fakeVesselData();
        $createdVessel = $this->vesselRepo->create($vessel);
        $createdVessel = $createdVessel->toArray();
        $this->assertArrayHasKey('id', $createdVessel);
        $this->assertNotNull($createdVessel['id'], 'Created Vessel must have id specified');
        $this->assertNotNull(Vessel::find($createdVessel['id']), 'Vessel with given id must be in DB');
        $this->assertModelData($vessel, $createdVessel);
    }

    /**
     * @test read
     */
    public function testReadVessel()
    {
        $vessel = $this->makeVessel();
        $dbVessel = $this->vesselRepo->find($vessel->id);
        $dbVessel = $dbVessel->toArray();
        $this->assertModelData($vessel->toArray(), $dbVessel);
    }

    /**
     * @test update
     */
    public function testUpdateVessel()
    {
        $vessel = $this->makeVessel();
        $fakeVessel = $this->fakeVesselData();
        $updatedVessel = $this->vesselRepo->update($fakeVessel, $vessel->id);
        $this->assertModelData($fakeVessel, $updatedVessel->toArray());
        $dbVessel = $this->vesselRepo->find($vessel->id);
        $this->assertModelData($fakeVessel, $dbVessel->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteVessel()
    {
        $vessel = $this->makeVessel();
        $resp = $this->vesselRepo->delete($vessel->id);
        $this->assertTrue($resp);
        $this->assertNull(Vessel::find($vessel->id), 'Vessel should not exist in DB');
    }
}
