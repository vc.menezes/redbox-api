<?php

use App\Models\PackageDestination;
use App\Repositories\PackageDestinationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackageDestinationRepositoryTest extends TestCase
{
    use MakePackageDestinationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PackageDestinationRepository
     */
    protected $packageDestinationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packageDestinationRepo = App::make(PackageDestinationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePackageDestination()
    {
        $packageDestination = $this->fakePackageDestinationData();
        $createdPackageDestination = $this->packageDestinationRepo->create($packageDestination);
        $createdPackageDestination = $createdPackageDestination->toArray();
        $this->assertArrayHasKey('id', $createdPackageDestination);
        $this->assertNotNull($createdPackageDestination['id'], 'Created PackageDestination must have id specified');
        $this->assertNotNull(PackageDestination::find($createdPackageDestination['id']), 'PackageDestination with given id must be in DB');
        $this->assertModelData($packageDestination, $createdPackageDestination);
    }

    /**
     * @test read
     */
    public function testReadPackageDestination()
    {
        $packageDestination = $this->makePackageDestination();
        $dbPackageDestination = $this->packageDestinationRepo->find($packageDestination->id);
        $dbPackageDestination = $dbPackageDestination->toArray();
        $this->assertModelData($packageDestination->toArray(), $dbPackageDestination);
    }

    /**
     * @test update
     */
    public function testUpdatePackageDestination()
    {
        $packageDestination = $this->makePackageDestination();
        $fakePackageDestination = $this->fakePackageDestinationData();
        $updatedPackageDestination = $this->packageDestinationRepo->update($fakePackageDestination, $packageDestination->id);
        $this->assertModelData($fakePackageDestination, $updatedPackageDestination->toArray());
        $dbPackageDestination = $this->packageDestinationRepo->find($packageDestination->id);
        $this->assertModelData($fakePackageDestination, $dbPackageDestination->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePackageDestination()
    {
        $packageDestination = $this->makePackageDestination();
        $resp = $this->packageDestinationRepo->delete($packageDestination->id);
        $this->assertTrue($resp);
        $this->assertNull(PackageDestination::find($packageDestination->id), 'PackageDestination should not exist in DB');
    }
}
