<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CardTypeApiTest extends TestCase
{
    use MakeCardTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCardType()
    {
        $cardType = $this->fakeCardTypeData();
        $this->json('POST', '/api/v1/cardTypes', $cardType);

        $this->assertApiResponse($cardType);
    }

    /**
     * @test
     */
    public function testReadCardType()
    {
        $cardType = $this->makeCardType();
        $this->json('GET', '/api/v1/cardTypes/'.$cardType->id);

        $this->assertApiResponse($cardType->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCardType()
    {
        $cardType = $this->makeCardType();
        $editedCardType = $this->fakeCardTypeData();

        $this->json('PUT', '/api/v1/cardTypes/'.$cardType->id, $editedCardType);

        $this->assertApiResponse($editedCardType);
    }

    /**
     * @test
     */
    public function testDeleteCardType()
    {
        $cardType = $this->makeCardType();
        $this->json('DELETE', '/api/v1/cardTypes/'.$cardType->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/cardTypes/'.$cardType->id);

        $this->assertResponseStatus(404);
    }
}
