<?php

use App\Models\Attachable;
use App\Repositories\AttachableRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AttachableRepositoryTest extends TestCase
{
    use MakeAttachableTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AttachableRepository
     */
    protected $attachableRepo;

    public function setUp()
    {
        parent::setUp();
        $this->attachableRepo = App::make(AttachableRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAttachable()
    {
        $attachable = $this->fakeAttachableData();
        $createdAttachable = $this->attachableRepo->create($attachable);
        $createdAttachable = $createdAttachable->toArray();
        $this->assertArrayHasKey('id', $createdAttachable);
        $this->assertNotNull($createdAttachable['id'], 'Created Attachable must have id specified');
        $this->assertNotNull(Attachable::find($createdAttachable['id']), 'Attachable with given id must be in DB');
        $this->assertModelData($attachable, $createdAttachable);
    }

    /**
     * @test read
     */
    public function testReadAttachable()
    {
        $attachable = $this->makeAttachable();
        $dbAttachable = $this->attachableRepo->find($attachable->id);
        $dbAttachable = $dbAttachable->toArray();
        $this->assertModelData($attachable->toArray(), $dbAttachable);
    }

    /**
     * @test update
     */
    public function testUpdateAttachable()
    {
        $attachable = $this->makeAttachable();
        $fakeAttachable = $this->fakeAttachableData();
        $updatedAttachable = $this->attachableRepo->update($fakeAttachable, $attachable->id);
        $this->assertModelData($fakeAttachable, $updatedAttachable->toArray());
        $dbAttachable = $this->attachableRepo->find($attachable->id);
        $this->assertModelData($fakeAttachable, $dbAttachable->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAttachable()
    {
        $attachable = $this->makeAttachable();
        $resp = $this->attachableRepo->delete($attachable->id);
        $this->assertTrue($resp);
        $this->assertNull(Attachable::find($attachable->id), 'Attachable should not exist in DB');
    }
}
