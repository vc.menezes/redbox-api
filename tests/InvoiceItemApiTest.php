<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InvoiceItemApiTest extends TestCase
{
    use MakeInvoiceItemTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateInvoiceItem()
    {
        $invoiceItem = $this->fakeInvoiceItemData();
        $this->json('POST', '/api/v1/invoiceItems', $invoiceItem);

        $this->assertApiResponse($invoiceItem);
    }

    /**
     * @test
     */
    public function testReadInvoiceItem()
    {
        $invoiceItem = $this->makeInvoiceItem();
        $this->json('GET', '/api/v1/invoiceItems/'.$invoiceItem->id);

        $this->assertApiResponse($invoiceItem->toArray());
    }

    /**
     * @test
     */
    public function testUpdateInvoiceItem()
    {
        $invoiceItem = $this->makeInvoiceItem();
        $editedInvoiceItem = $this->fakeInvoiceItemData();

        $this->json('PUT', '/api/v1/invoiceItems/'.$invoiceItem->id, $editedInvoiceItem);

        $this->assertApiResponse($editedInvoiceItem);
    }

    /**
     * @test
     */
    public function testDeleteInvoiceItem()
    {
        $invoiceItem = $this->makeInvoiceItem();
        $this->json('DELETE', '/api/v1/invoiceItems/'.$invoiceItem->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/invoiceItems/'.$invoiceItem->id);

        $this->assertResponseStatus(404);
    }
}
