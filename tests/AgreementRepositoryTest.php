<?php

use App\Models\Agreement;
use App\Repositories\AgreementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AgreementRepositoryTest extends TestCase
{
    use MakeAgreementTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AgreementRepository
     */
    protected $agreementRepo;

    public function setUp()
    {
        parent::setUp();
        $this->agreementRepo = App::make(AgreementRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAgreement()
    {
        $agreement = $this->fakeAgreementData();
        $createdAgreement = $this->agreementRepo->create($agreement);
        $createdAgreement = $createdAgreement->toArray();
        $this->assertArrayHasKey('id', $createdAgreement);
        $this->assertNotNull($createdAgreement['id'], 'Created Agreement must have id specified');
        $this->assertNotNull(Agreement::find($createdAgreement['id']), 'Agreement with given id must be in DB');
        $this->assertModelData($agreement, $createdAgreement);
    }

    /**
     * @test read
     */
    public function testReadAgreement()
    {
        $agreement = $this->makeAgreement();
        $dbAgreement = $this->agreementRepo->find($agreement->id);
        $dbAgreement = $dbAgreement->toArray();
        $this->assertModelData($agreement->toArray(), $dbAgreement);
    }

    /**
     * @test update
     */
    public function testUpdateAgreement()
    {
        $agreement = $this->makeAgreement();
        $fakeAgreement = $this->fakeAgreementData();
        $updatedAgreement = $this->agreementRepo->update($fakeAgreement, $agreement->id);
        $this->assertModelData($fakeAgreement, $updatedAgreement->toArray());
        $dbAgreement = $this->agreementRepo->find($agreement->id);
        $this->assertModelData($fakeAgreement, $dbAgreement->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAgreement()
    {
        $agreement = $this->makeAgreement();
        $resp = $this->agreementRepo->delete($agreement->id);
        $this->assertTrue($resp);
        $this->assertNull(Agreement::find($agreement->id), 'Agreement should not exist in DB');
    }
}
