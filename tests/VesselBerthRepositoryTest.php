<?php

use App\Models\VesselBerth;
use App\Repositories\VesselBerthRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VesselBerthRepositoryTest extends TestCase
{
    use MakeVesselBerthTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var VesselBerthRepository
     */
    protected $vesselBerthRepo;

    public function setUp()
    {
        parent::setUp();
        $this->vesselBerthRepo = App::make(VesselBerthRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateVesselBerth()
    {
        $vesselBerth = $this->fakeVesselBerthData();
        $createdVesselBerth = $this->vesselBerthRepo->create($vesselBerth);
        $createdVesselBerth = $createdVesselBerth->toArray();
        $this->assertArrayHasKey('id', $createdVesselBerth);
        $this->assertNotNull($createdVesselBerth['id'], 'Created VesselBerth must have id specified');
        $this->assertNotNull(VesselBerth::find($createdVesselBerth['id']), 'VesselBerth with given id must be in DB');
        $this->assertModelData($vesselBerth, $createdVesselBerth);
    }

    /**
     * @test read
     */
    public function testReadVesselBerth()
    {
        $vesselBerth = $this->makeVesselBerth();
        $dbVesselBerth = $this->vesselBerthRepo->find($vesselBerth->id);
        $dbVesselBerth = $dbVesselBerth->toArray();
        $this->assertModelData($vesselBerth->toArray(), $dbVesselBerth);
    }

    /**
     * @test update
     */
    public function testUpdateVesselBerth()
    {
        $vesselBerth = $this->makeVesselBerth();
        $fakeVesselBerth = $this->fakeVesselBerthData();
        $updatedVesselBerth = $this->vesselBerthRepo->update($fakeVesselBerth, $vesselBerth->id);
        $this->assertModelData($fakeVesselBerth, $updatedVesselBerth->toArray());
        $dbVesselBerth = $this->vesselBerthRepo->find($vesselBerth->id);
        $this->assertModelData($fakeVesselBerth, $dbVesselBerth->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteVesselBerth()
    {
        $vesselBerth = $this->makeVesselBerth();
        $resp = $this->vesselBerthRepo->delete($vesselBerth->id);
        $this->assertTrue($resp);
        $this->assertNull(VesselBerth::find($vesselBerth->id), 'VesselBerth should not exist in DB');
    }
}
