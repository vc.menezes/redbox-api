<?php

use App\Models\PickingCharge;
use App\Repositories\PickingChargeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PickingChargeRepositoryTest extends TestCase
{
    use MakePickingChargeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PickingChargeRepository
     */
    protected $pickingChargeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->pickingChargeRepo = App::make(PickingChargeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePickingCharge()
    {
        $pickingCharge = $this->fakePickingChargeData();
        $createdPickingCharge = $this->pickingChargeRepo->create($pickingCharge);
        $createdPickingCharge = $createdPickingCharge->toArray();
        $this->assertArrayHasKey('id', $createdPickingCharge);
        $this->assertNotNull($createdPickingCharge['id'], 'Created PickingCharge must have id specified');
        $this->assertNotNull(PickingCharge::find($createdPickingCharge['id']), 'PickingCharge with given id must be in DB');
        $this->assertModelData($pickingCharge, $createdPickingCharge);
    }

    /**
     * @test read
     */
    public function testReadPickingCharge()
    {
        $pickingCharge = $this->makePickingCharge();
        $dbPickingCharge = $this->pickingChargeRepo->find($pickingCharge->id);
        $dbPickingCharge = $dbPickingCharge->toArray();
        $this->assertModelData($pickingCharge->toArray(), $dbPickingCharge);
    }

    /**
     * @test update
     */
    public function testUpdatePickingCharge()
    {
        $pickingCharge = $this->makePickingCharge();
        $fakePickingCharge = $this->fakePickingChargeData();
        $updatedPickingCharge = $this->pickingChargeRepo->update($fakePickingCharge, $pickingCharge->id);
        $this->assertModelData($fakePickingCharge, $updatedPickingCharge->toArray());
        $dbPickingCharge = $this->pickingChargeRepo->find($pickingCharge->id);
        $this->assertModelData($fakePickingCharge, $dbPickingCharge->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePickingCharge()
    {
        $pickingCharge = $this->makePickingCharge();
        $resp = $this->pickingChargeRepo->delete($pickingCharge->id);
        $this->assertTrue($resp);
        $this->assertNull(PickingCharge::find($pickingCharge->id), 'PickingCharge should not exist in DB');
    }
}
