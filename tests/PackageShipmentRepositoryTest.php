<?php

use App\Models\PackageShipment;
use App\Repositories\PackageShipmentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackageShipmentRepositoryTest extends TestCase
{
    use MakePackageShipmentTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PackageShipmentRepository
     */
    protected $packageShipmentRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packageShipmentRepo = App::make(PackageShipmentRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePackageShipment()
    {
        $packageShipment = $this->fakePackageShipmentData();
        $createdPackageShipment = $this->packageShipmentRepo->create($packageShipment);
        $createdPackageShipment = $createdPackageShipment->toArray();
        $this->assertArrayHasKey('id', $createdPackageShipment);
        $this->assertNotNull($createdPackageShipment['id'], 'Created PackageShipment must have id specified');
        $this->assertNotNull(PackageShipment::find($createdPackageShipment['id']), 'PackageShipment with given id must be in DB');
        $this->assertModelData($packageShipment, $createdPackageShipment);
    }

    /**
     * @test read
     */
    public function testReadPackageShipment()
    {
        $packageShipment = $this->makePackageShipment();
        $dbPackageShipment = $this->packageShipmentRepo->find($packageShipment->id);
        $dbPackageShipment = $dbPackageShipment->toArray();
        $this->assertModelData($packageShipment->toArray(), $dbPackageShipment);
    }

    /**
     * @test update
     */
    public function testUpdatePackageShipment()
    {
        $packageShipment = $this->makePackageShipment();
        $fakePackageShipment = $this->fakePackageShipmentData();
        $updatedPackageShipment = $this->packageShipmentRepo->update($fakePackageShipment, $packageShipment->id);
        $this->assertModelData($fakePackageShipment, $updatedPackageShipment->toArray());
        $dbPackageShipment = $this->packageShipmentRepo->find($packageShipment->id);
        $this->assertModelData($fakePackageShipment, $dbPackageShipment->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePackageShipment()
    {
        $packageShipment = $this->makePackageShipment();
        $resp = $this->packageShipmentRepo->delete($packageShipment->id);
        $this->assertTrue($resp);
        $this->assertNull(PackageShipment::find($packageShipment->id), 'PackageShipment should not exist in DB');
    }
}
