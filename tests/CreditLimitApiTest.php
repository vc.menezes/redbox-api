<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreditLimitApiTest extends TestCase
{
    use MakeCreditLimitTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCreditLimit()
    {
        $creditLimit = $this->fakeCreditLimitData();
        $this->json('POST', '/api/v1/creditLimits', $creditLimit);

        $this->assertApiResponse($creditLimit);
    }

    /**
     * @test
     */
    public function testReadCreditLimit()
    {
        $creditLimit = $this->makeCreditLimit();
        $this->json('GET', '/api/v1/creditLimits/'.$creditLimit->id);

        $this->assertApiResponse($creditLimit->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCreditLimit()
    {
        $creditLimit = $this->makeCreditLimit();
        $editedCreditLimit = $this->fakeCreditLimitData();

        $this->json('PUT', '/api/v1/creditLimits/'.$creditLimit->id, $editedCreditLimit);

        $this->assertApiResponse($editedCreditLimit);
    }

    /**
     * @test
     */
    public function testDeleteCreditLimit()
    {
        $creditLimit = $this->makeCreditLimit();
        $this->json('DELETE', '/api/v1/creditLimits/'.$creditLimit->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/creditLimits/'.$creditLimit->id);

        $this->assertResponseStatus(404);
    }
}
