<?php

use App\Models\AgentPaymentPost;
use App\Repositories\AgentPaymentPostRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AgentPaymentPostRepositoryTest extends TestCase
{
    use MakeAgentPaymentPostTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AgentPaymentPostRepository
     */
    protected $agentPaymentPostRepo;

    public function setUp()
    {
        parent::setUp();
        $this->agentPaymentPostRepo = App::make(AgentPaymentPostRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAgentPaymentPost()
    {
        $agentPaymentPost = $this->fakeAgentPaymentPostData();
        $createdAgentPaymentPost = $this->agentPaymentPostRepo->create($agentPaymentPost);
        $createdAgentPaymentPost = $createdAgentPaymentPost->toArray();
        $this->assertArrayHasKey('id', $createdAgentPaymentPost);
        $this->assertNotNull($createdAgentPaymentPost['id'], 'Created AgentPaymentPost must have id specified');
        $this->assertNotNull(AgentPaymentPost::find($createdAgentPaymentPost['id']), 'AgentPaymentPost with given id must be in DB');
        $this->assertModelData($agentPaymentPost, $createdAgentPaymentPost);
    }

    /**
     * @test read
     */
    public function testReadAgentPaymentPost()
    {
        $agentPaymentPost = $this->makeAgentPaymentPost();
        $dbAgentPaymentPost = $this->agentPaymentPostRepo->find($agentPaymentPost->id);
        $dbAgentPaymentPost = $dbAgentPaymentPost->toArray();
        $this->assertModelData($agentPaymentPost->toArray(), $dbAgentPaymentPost);
    }

    /**
     * @test update
     */
    public function testUpdateAgentPaymentPost()
    {
        $agentPaymentPost = $this->makeAgentPaymentPost();
        $fakeAgentPaymentPost = $this->fakeAgentPaymentPostData();
        $updatedAgentPaymentPost = $this->agentPaymentPostRepo->update($fakeAgentPaymentPost, $agentPaymentPost->id);
        $this->assertModelData($fakeAgentPaymentPost, $updatedAgentPaymentPost->toArray());
        $dbAgentPaymentPost = $this->agentPaymentPostRepo->find($agentPaymentPost->id);
        $this->assertModelData($fakeAgentPaymentPost, $dbAgentPaymentPost->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAgentPaymentPost()
    {
        $agentPaymentPost = $this->makeAgentPaymentPost();
        $resp = $this->agentPaymentPostRepo->delete($agentPaymentPost->id);
        $this->assertTrue($resp);
        $this->assertNull(AgentPaymentPost::find($agentPaymentPost->id), 'AgentPaymentPost should not exist in DB');
    }
}
