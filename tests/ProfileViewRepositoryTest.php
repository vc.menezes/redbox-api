<?php

use App\Models\ProfileView;
use App\Repositories\ProfileViewRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProfileViewRepositoryTest extends TestCase
{
    use MakeProfileViewTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProfileViewRepository
     */
    protected $profileViewRepo;

    public function setUp()
    {
        parent::setUp();
        $this->profileViewRepo = App::make(ProfileViewRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateProfileView()
    {
        $profileView = $this->fakeProfileViewData();
        $createdProfileView = $this->profileViewRepo->create($profileView);
        $createdProfileView = $createdProfileView->toArray();
        $this->assertArrayHasKey('id', $createdProfileView);
        $this->assertNotNull($createdProfileView['id'], 'Created ProfileView must have id specified');
        $this->assertNotNull(ProfileView::find($createdProfileView['id']), 'ProfileView with given id must be in DB');
        $this->assertModelData($profileView, $createdProfileView);
    }

    /**
     * @test read
     */
    public function testReadProfileView()
    {
        $profileView = $this->makeProfileView();
        $dbProfileView = $this->profileViewRepo->find($profileView->id);
        $dbProfileView = $dbProfileView->toArray();
        $this->assertModelData($profileView->toArray(), $dbProfileView);
    }

    /**
     * @test update
     */
    public function testUpdateProfileView()
    {
        $profileView = $this->makeProfileView();
        $fakeProfileView = $this->fakeProfileViewData();
        $updatedProfileView = $this->profileViewRepo->update($fakeProfileView, $profileView->id);
        $this->assertModelData($fakeProfileView, $updatedProfileView->toArray());
        $dbProfileView = $this->profileViewRepo->find($profileView->id);
        $this->assertModelData($fakeProfileView, $dbProfileView->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteProfileView()
    {
        $profileView = $this->makeProfileView();
        $resp = $this->profileViewRepo->delete($profileView->id);
        $this->assertTrue($resp);
        $this->assertNull(ProfileView::find($profileView->id), 'ProfileView should not exist in DB');
    }
}
