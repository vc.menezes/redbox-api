<?php

use App\Models\CommodityCategory;
use App\Repositories\CommodityCategoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommodityCategoryRepositoryTest extends TestCase
{
    use MakeCommodityCategoryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CommodityCategoryRepository
     */
    protected $commodityCategoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->commodityCategoryRepo = App::make(CommodityCategoryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCommodityCategory()
    {
        $commodityCategory = $this->fakeCommodityCategoryData();
        $createdCommodityCategory = $this->commodityCategoryRepo->create($commodityCategory);
        $createdCommodityCategory = $createdCommodityCategory->toArray();
        $this->assertArrayHasKey('id', $createdCommodityCategory);
        $this->assertNotNull($createdCommodityCategory['id'], 'Created CommodityCategory must have id specified');
        $this->assertNotNull(CommodityCategory::find($createdCommodityCategory['id']), 'CommodityCategory with given id must be in DB');
        $this->assertModelData($commodityCategory, $createdCommodityCategory);
    }

    /**
     * @test read
     */
    public function testReadCommodityCategory()
    {
        $commodityCategory = $this->makeCommodityCategory();
        $dbCommodityCategory = $this->commodityCategoryRepo->find($commodityCategory->id);
        $dbCommodityCategory = $dbCommodityCategory->toArray();
        $this->assertModelData($commodityCategory->toArray(), $dbCommodityCategory);
    }

    /**
     * @test update
     */
    public function testUpdateCommodityCategory()
    {
        $commodityCategory = $this->makeCommodityCategory();
        $fakeCommodityCategory = $this->fakeCommodityCategoryData();
        $updatedCommodityCategory = $this->commodityCategoryRepo->update($fakeCommodityCategory, $commodityCategory->id);
        $this->assertModelData($fakeCommodityCategory, $updatedCommodityCategory->toArray());
        $dbCommodityCategory = $this->commodityCategoryRepo->find($commodityCategory->id);
        $this->assertModelData($fakeCommodityCategory, $dbCommodityCategory->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCommodityCategory()
    {
        $commodityCategory = $this->makeCommodityCategory();
        $resp = $this->commodityCategoryRepo->delete($commodityCategory->id);
        $this->assertTrue($resp);
        $this->assertNull(CommodityCategory::find($commodityCategory->id), 'CommodityCategory should not exist in DB');
    }
}
