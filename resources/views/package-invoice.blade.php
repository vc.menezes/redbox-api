<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <style>

        .invoice-box {
            padding: 30px;
            font-family: 'Times New Roman', 'Arial', sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
            font-size: 20px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
            font-size: 20px;
            padding: 10px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
            font-size: 20px;
            padding: 16px 10px;
        }

        .total {
            text-align: center
        }

    </style>
</head>

<body>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="7">
                <table>
                    <tr>
                        <td class="title">
                            <img src="./images/logo.png" style="width:200px">
                        </td>

                        <td>
                            Invoice #: {{ $data->serial_number}}<br>
                            Created: {{  \Carbon\Carbon::parse($data->created_at)->toFormattedDateString() }}<br>
                            Order #: {{ $data->package->code }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="8">
                <table>
                    <tr>
                        <td>
                            RB International Pvt. Ltd<br>
                            info@redbox.mv<br>
                            +960 3011887<br>
                            H. Whitevilla, Abadhahfehimagu, <br>
                            Male, Maldives
                        </td>

                        <td>
                            {{ $data->profile->name}}<br>
                            {{ $data->profile->email}}<br>
                            {{ $data->profile->contact_number}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="heading">
            <td colspan="2">
                Description
            </td>

            <td style="text-align:left">
                code
            </td>

            <td style="text-align:left">
                UOM
            </td>
            <td>
                Qty
            </td>
            <td>
                Rate
            </td>

            <td  style="text-align:right">
                Tax
            </td>
            <td class="total"  style="text-align:right">
                Total
            </td>
        </tr>
        @foreach($data->items as $item)
            @if($item->type == 'charge')
            <tr class="item">
                <td colspan="2">
                    {{ $item->details }}
                </td>

                <td style="text-align:left">
                    @if($item->charge_type != 'picking')
                    {{ $item->item_code }}
                    @endif
                </td>

                <td style="text-align:left">
                    {{ $item->UOM }}
                </td>
                <td>
                    {{ $item->qty }}
                </td>
                <td>
                    {{ $item->rate }}
                </td>

                <td  style="text-align:right">
                    {{ Redbox::get_tax($item->rate) }}
                </td>
                <td class="total"  style="text-align:right">
                    {{ Redbox::get_tax($item->rate) + $item->rate}}
                </td>
            </tr>
            @endif
        @endforeach

        <tr class="item">
            <td colspan="6"></td>
            <td><b>Sub Total</b></td>
            <td style="text-align:right"><b>{{($data->total - $data->tax)}}</b></td>
        </tr>

        <tr class="item">
            <td colspan="6"></td>
            <td><b>GST</b></td>
            <td style="text-align:right"><b>{{$data->tax}}</b></td>
        </tr>

        <tr class="item">
            <td colspan="6"></td>
            <td><b>Total</b></td>
            <td style="text-align:right"><b>{{$data->total}}</b></td>
        </tr>

    </table>
    <p>Please settle the due amount to the following bank account:</p>
    <small>
    Account Name: RB International Pvt Ltd <br>
    <b>Bank Details<b> <br>
    <b>Bank Name:</b>  BANK OF MALDIVES PLC SWIFT <br>
     <b>Code:</b>    MALBMVMV Bank Address
     <b>BML BUILDING,</b>  11 BODUTHAKURUFAANU MAGU <br>
      <b> Bank Branch:</b> Main Branch <br>
    Important: All bank charges will be borne by the customer.
    </small>
    <h3>Thank you for your business.</h3>
</div>
</html>
