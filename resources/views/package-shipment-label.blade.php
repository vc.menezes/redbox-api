<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 25px;
            font-family: 'Times New Roman', 'Arial', sans-serif;
        }
        
        
        th,td {
            padding:10px;
            
        }
        th {
            text-align:left;
        }
        
        table {
            /* width: 100%; */
        } 
        
        .logo{
            display: block;
            width: 200px;
        }

	</style>
</head>
<body>

    <table>
        <tr>
            <td colspan= "2" style="height: 70px">
                <img class="logo" src="./images/logo.png">
            </td>

            <td colspan= "2">
                SHIPMENT #: {{ $data->code}}
                <br/>
                NO. BOX #: {{ $data->reference_no ? $data->reference_no : 1}}
            </td>
        </tr>
        <tr>
            <td colspan= "2">
                <div style="padding-left: 30px">
                    {!! DNS2D::getBarcodeHTML($data->barcode, "QRCODE", 7, 7) !!}
                </div>
            </td>
                
            <td colspan= "2" style="text-align:center;">
                <div style="padding:0 10px">
                    {!! DNS1D::getBarcodeHTML($data->barcode, "C128", 5, 120) !!}
                </div>
                {{-- {{ $data->barcode }} --}}
            </td>
        </tr>	
        
        <tr>
            <td colspan= "2"> 
                <b>ORIGIN</b>
                <br>
                MLE 
            </td>
            <td colspan= "2"> 
                <b>DESTINATION</b>
                <br>
                {{ $data->destination ?  $data->destination->name .' / '.  $data->destination->prefix : '-'}}
            </td>
        </tr>
    
        <tr>
            <td colspan="2">
                <b>FROM</b>
            </td>
            <td colspan="2">
                <b>Redbox</b><br>
                Male', Maldives<br/>
                +960 3011887
            </td> 
        </tr>	

        <tr>
            <td colspan="2">
                <b>TO</b>
            </td>
            <td colspan="2">
                <b>{{ $data->address_name }}</b><br>
                @if($data->address)
                {{ $data->address->address_line }}<br/>
                @endif
                {{ $data->destination->name }}<br>
                {{ $data->destinations_contact_number }}
                {{ $contacts ? ', '.$contacts : '' }}
            </td>
        </tr>	
    </table>
    <p style="color: grey; font-size: 16px">RB International Pvt. Ltd | info@redbox.mv | +960 3011887 | H. Whitevilla, Abadhahfehimagu, Male, Maldives </p>
</body>
</html>