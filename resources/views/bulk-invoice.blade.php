<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	
	<style>
	
	.invoice-box{
		padding:30px;
		font-family: 'Times New Roman', 'Arial', sans-serif;
		color:#555;
	}
	
	.invoice-box table{
		width:100%
	}
		
	.invoice-box table tr td:nth-child(2){
		/* text-align:left; */
		font-size: 20px
	}
	
	.invoice-box table tr.information table td{
		padding-bottom:40px;
        font-size:20px;
	}
	
	.invoice-box table tr.heading td{
		background:#ccc;
		border-bottom:1px solid #000;
		border-top:1px solid #000;
		font-weight:bold;
        font-size: 20px;
        padding: 10px;
		/* text-align: left; */
	}

	.item:nth-child(odd) {background: #eee}
	
	
	.invoice-box table tr.item td{
		border-bottom:1px solid #eee;
        font-size: 20px;
        padding: 10px;
	}

	.item:last-child > td{
		border-bottom: 1px solid black!important;
	}
	
	.total {
		text-align: center;
	}

	.title {
		vertical-align: top;
		font-size:20px;
	}

	.paid {
		font-size: 40px;
		color: red;
		text-align: center;
	}
	.t-r {
		text-align: right;
	}
	.total {
		text-align: right;
	}

	.border td {
		border-right: 1px solid black;
	}
	.qty {
		border-left: 1px solid black;
	}
	.total-table {
		font-size: 20px;
		text-align: right;
	}
	.note {
		font-size: 20px
	}
	.txt-cap {
		text-transform: capitalize;
	}
	.paid:after
	{
		content:"PAID";
		position:relative;
		z-index:1;
		font-family:Arial,sans-serif;
		font-size:60px;
		color:#c00;
		background:#fff;
		border:solid 4px #c00;
		padding:5px;
		border-radius:5px;
		zoom:1;
		filter:alpha(opacity=20);
		opacity:0.5;
		text-shadow: 0 0 2px #c00;
		box-shadow: 0 0 2px #c00;
	}
	
	</style>
</head>

<body>
	<div class="invoice-box">
		<table cellpadding="0" cellspacing="0">
			{{-- <tr>
				<td colspan="8" class="paid">
					PAID
				</td>
			</tr> --}}
			<tr class="top">
				<td colspan="8">
					<table style="margin-bottom:50px">
						<tr>
							<td class="title" colspan="6">
								<img src="./images/logo.png" style="width:200px"> <br>
								<b>REDBOX</b><br>
								C1140/2015 <br>
								RB International Pvt. Ltd<br>
								H. Whitevilla, Abadhahfehimagu, Male, Maldives <br>
								TIN:1061799GST501 <br><br>
								Email:info@rbinternation.mv<br>
								Tel:3011887, Fax:3011899<br>
							</td>
							
							<td style="width: 330px; vertical-align: top">
								<table>
									<tr>
										<td colspan="2" style="font-size: 40px; font-weight: bold; padding-bottom: 10px">TAX INVOICE</td>
									</tr>
									<tr>
										<td>Date:</td>
										<td>{{ \Carbon\Carbon::parse($data->created_at)->toFormattedDateString() }}</td>
									</tr>
									<tr>
										<td>Invoice No.:</td>
										<td>{{ $data->serial_number}}</td>
									</tr>
									@if($data->charge_type == 'bulkorder')
									<tr>
										<td>Due Date:</td>
										<td>{{ \Carbon\Carbon::parse($data->created_at)->addDays(30)->toFormattedDateString() }}</td>
									</tr>
									@endif
									{{-- <tr>
										<td style="width: 130px; vertical-align: top;">Sales Person:</td>
										<td>{{ $user->name }}</td>
									</tr> --}}
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr class="information">
				<td colspan="8">
					<table>
						
						<tr>
							<td>
								<b>Bill To: </b><br>
								{{ $data->profile_view->name}}<br>
								@if($data->profile_view->identifier && $data->profile_view->type == 'organization')
									REG NO.: {{ $data->profile_view->identifier }}<br>
								@endif
								@if($data->profile_view->tin)
									TIN: {{ $data->profile_view->tin }}<br>
								@endif
								@if($data->profile_view->location_name)
									{{ $data->profile_view->location_name }} <br>
								@endif
								@if(mb_substr($data->profile_view->identifier, 0, 1, 'utf-8') == 'A' || mb_substr($data->profile_view->identifier, 0, 1, 'utf-8') == 'a')
									{{ $data->profile_view->identifier }}
								@else
									TIN#: {{ $data->profile_view->identifier }}
								@endif

							</td>
							
							{{-- <td style="width: 330px">
								<b>Ship To: </b><br>
								DHL<br>
								H. Hollywood<br>
								Sosunmagu, Male'<br>
								Maldives
							</td> --}}
						</tr>
					</table>
				</td>
			</tr>
			
			<tr class="heading border">
				<td class="qty">
					Qty
				</td>
				<td>
					Tracking (AWB)
				</td>
				<td colspan="4">
					Description
				</td>
				
				<td class="t-r">
					Unit Price
				</td>
				
				<td class="total">
					Total
				</td>
			</tr>
			
			@foreach ($data->items as $item)
				@if($item->type == 'charge')
					<tr class="item border">
						<td class="qty">
							{{ $item->qty }}
						</td>
						<td>
							@if($item->charge_type != 'picking')
							{{ $item->item_code }}
							@endif
						</td>
						<td colspan="4">
							{{ $item->details }}
						</td>
						<td class="t-r">
							{{ $item->rate }}
						</td>
						<td class="total">
							{{ $item->rate }}
						</td>
					</tr>
				@endif
			@endforeach
			
		</table>
		<table class="total-table">
			<tr>
				<td colspan="6"></td>
				<td>Subtotal</td>
				<td style="width:150px">{{ $data->total - $data->tax}}</td>
			</tr>
			<tr>
				<td colspan="6"></td>
				<td>GST</td>
				<td style="width:150px">{{ $data->tax}}</td>
			</tr>
			<tr>
				<td colspan="6"></td>
				<td><b>TOTAL</b></td>
				<td style="width:150px"><b>{{$data->total}}</b></td>
			</tr>
			
			{{-- <tr>
				<td colspan="6"></td>
				<td><b>PAID</b></td>
				<td style="width:150px"><b>{{$data->total}}</b></td>
			</tr> --}}
		</table>

		@if($data->status == 0)
			<div class="paid"></div>
		@endif
		@if($data->charge_type == 'bulkorder')
		<table class="note">
			<tr>
				<td style="padding-bottom:10px; padding-top: 50px">
					Please settle the due amount to the following bank account:
				</td>
			</tr>
			<tr>
				<td style="padding-bottom:40px">
					Account Name: RB International Pvt Ltd
				</td>
			</tr>
			<tr>
				<td>
					Bank Details:
				</td>
			</tr>
			<tr>
				<td>
					Bank Name: BANK OF MALDIVES PLC
				</td>
			</tr>
			<tr>
				<td>
					SWIFT Code: MALBMVMV
				</td>
			</tr>
			<tr>
				<td>
					Bank Address: BML BUILDING, 11 BODUTHAKURUFAANU MAGU
				</td>
			</tr>
			<tr>
				<td style="padding-bottom:30px">
					Bank Branch: Main Branch
				</td>
			</tr>
			<tr>
				<td style="padding-bottom:10px">
				 	<b>Important: All bank charges will be borne by the customer.</b>
				</td>
			</tr>
			<tr>
				<td>
					<b>Thank you for your business.</b>
				</td>
			</tr>
		</table>
		@endif
	</div>
</html>
