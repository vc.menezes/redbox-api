<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	
	<style>
	
	.invoice-box{
		padding:30px;
		font-family: 'Times New Roman', 'Arial', sans-serif;
		color:#555;
	}
	
	.invoice-box table{
		width:100%
	}
	td {
		padding: 10px;
		vertical-align: top;
		font-size: 17px;
	}
	h1 {
		margin: 0;
	}
	.pb {
		padding-bottom: 20px;
	}
	.header {
		background: red;
		font-weight: bold;
		color: #000;
		padding: 8px;
    	border: 1px solid black;
	}
	.sb {
		border: 1px solid #eee
	}
	.sbb {
		border-bottom: 1px solid #eee
	}
	.sbl {
		border-left: 1px solid #eee
	}
	.sbr {
		border-right: 1px solid #eee
	}
	.sbt {
		border-top: 1px solid #eee
	}
	.txc {
		text-align: center
	}
	.bgbr {
		background-color: #ffc1a8;
		font-size: 13px;
	}
	</style>
</head>

<body>
	<div class="invoice-box">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td style="width: 500px">
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td style="width:120px">
								<img style="width:120px" src="./images/rbl.png" alt="">
							</td>
							<td style="padding-bottom: 19px">
								<b>RB INTERNATIONAL <br/> PRIVATE LIMITED.</b><br>
								<small>H. White Villa,</small><br>
								<small>Abadhahfehimagu,</small><br>
								<small>Male’, Maldives,</small><br>
								<small>Tel: +960 3011887</small><br>
								<small>Email: info@rbinternational.mv</small>
								<br>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="header">
								1. PAYMENT DETAILS
							</td>
						</tr>
						<tr>
							<td class="sbr sbl" colspan="2">
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td><input type="checkbox" checked>Cash</td>
										<td><input type="checkbox">Invoice</td>
										<td><input type="checkbox">COD</td>
										<td><input type="checkbox">Credit Card</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td colspan="2" class="sbt"></td></tr>
						<tr>
							<td colspan="2" class="header">
								2. FROM (SENDER)
							</td>
						</tr>
						<tr>
							<td class="sb" colspan="2">
								Sender Company: <b>{{ $data->profile->name}}</b>
							</td>
						</tr>
						<tr>
							<td class="sbl sbr" colspan="2">
								Name: <b>{{ $data->profile->name}}</b>
							</td>
						</tr>
						<tr>
							<td class="sb" colspan="2">
								Address: <b>{{ count($data->profile->address) ?  $data->profile->address[0]->address_line : '' }}</b>
							</td>
						</tr>
						<tr>
							<td class="sbr sbl">
								Post/Zip Code <br/> <b>{{ count($data->profile->address) ?  $data->profile->address[0]->postal_code : '' }}</b>
							</td>
							<td class="sbr">
								Contact Number <br/> <b>{{ $data->profile->contact_number}}</b>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="sbt"></td>
						</tr>
						<tr>
							<td colspan="2" class="header">
								3. TO (RECEIVER)
							</td>
						</tr>
						<tr>
							<td class="sb" colspan="2">
								Recipient Company: 
							</td>
						</tr>
						<tr>
							<td class="sbl sbr" colspan="2">
								Name: <b>{{ $data->address_name }}</b>
							</td>
						</tr>
						<tr>
							<td class="sb" colspan="2">
								Address: <b>{{ $data->destinations_address }} / {{ $data->destination_island}}</b>
							</td>
						</tr>
						<tr>
							<td class="sbb sbr sbl">
								Post/Zip Code <br/> <b></b>
							</td>
							<td class="sbb sbr">
								Contact Number <br/> <b>{{ $data->destinations_contact_number }}</b>
							</td>
						</tr>
					</table>
				</td>
				<td style="width: 500px">
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td colspan="2" class="txc" style="padding: 0">
								<b>SHIPMENT WAYBILL</b> <br><br>
								<div style="padding-left: 45px">
									{!! DNS1D::getBarcodeHTML($data->barcode, "C128", 4, 70); !!}
								</div>
								
								<p style="margin: 0">{{ $data->reference_no ? $data->reference_no : $data->tracking_no }}</p>
								<br><br>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="header">
								4. SHIPMENT DETAILS
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding: 0">
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td class="sbl sbb bgbr">Total No. of Packages</td>
										<td class="sbl sbb bgbr">Total Weight in kg</td>
										<td class="sbl sbr sbb txc bgbr">
											Dimensions in cm <br>
											<table cellpadding="0" cellspacing="0" style="font-size: 10px">
												<tr>
													<td style="font-size: 12px">Pcs</td>
													<td style="font-size: 12px">Length</td>
													<td style="font-size: 12px">Width</td>
													<td style="font-size: 12px">Height</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td class="sbl sbb">{{ count($data->items) }}</td>
										<td class="sbl sbb">{{ $data->weight }}kg</td>
										<td class="sbl sbr sbb"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td colspan="2" class="header">
								5. FULL DESCRIPTION OF CONTENTS
							</td>
						</tr>
						<tr>
							<td colspan="2" class="sbr sbl">
								{{ $data->contents }}
							</td>
						</tr>
						<tr>
							<td colspan="2" class="sbt"></td>
						</tr>
						<tr>
							<td colspan="2" class="header">
								6. DUTIABLE SHIPMENTS ONLY(CUSTOMS REQUIREMENT) <br>
								<small>Attach the original and two copies of a Commercial Invoice</small>
							</td>
						</tr>
						<tr>
							<td class="sb"> <small>Shipper's VAT/GST Number</small><br><br><br></td>
							<td class="sbb sbr"><small>Receiver's VAT/GST Number</small><br><br><br></td>
						</tr>
						<tr>
							<td class="sbl sbr"><small>Declared Value for Customs (as an commercial Invoice)</small><br><br><br></td>
							<td class="sbr txc">
								<small>Destination Duties/Taxes</small>
								<table>
									<tr>
										<td><input type="checkbox">Receiver</td>
										<td><input type="checkbox">Shipper</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="sbt"></td>
						</tr>
						<tr>
							<td colspan="2" class="header">
								7. SHIPPER'S AGREEMENT(SIGNATURE REQUIRED)
							</td>
						</tr>
						<tr>
							<td class="sbl sbr" colspan="2">
								<small style="font-size: 14px">
								I/We agree that RB INTERNATIONAL's Terms and conditions are all the terms of the contract between me /us and RB INTERNATIONAL and (1) such Terms & Conditions Warsaw Convention limits and
								/or excludes RB INTERNATIONAL's and, where applicable, the liability for loss,damage or delay and (2) this shipment does not contain cash or dangerous goods.
								</small>
							</td>
						</tr>
						<tr>
							<td class="sbl sbr sbb">Signature <br><br><br> </td>
							<td class="sbr sbb ">Date/Time</td>
						</tr>
					</table>
				</td>
				<td style="width: 500px">
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td colspan="2">
								<img width="330px" src="./images/logo.png" alt="">
								<br>
								<small>Track this shipment from REDBOX website redbox.mv</small>
								<br><br><br>
							</td>
						</tr>
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0">
									<tr class="header">
										<td>ORIGIN CODE</td>
									</tr>
									<tr>
										<td class="sb txc">
											<h1>MLE</h1>
										</td>
									</tr>
								</table>
							</td>
							<td>
								<table cellpadding="0" cellspacing="0">
									<tr class="header">
										<td>DESTINATION CODE</td>
									</tr>
									<tr>
										<td class="sb txc">
											<h1>{{ $data->destination->prefix ? $data->destination->prefix : $data->destination->name }}</h1>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td colspan="2" class="header">
								8. SERVICES
							</td>
						</tr>
						<tr>
							<td class="sb">Type</td>
							<td class="sbr sbb" style="text-transform: capitalize">{{ $data->transport_medium }}</td>
						</tr>
						<tr>
							<td class="sbr sbl sbb">Others</td>
							<td class="sbr sbb">Domestic Delivery</td>
						</tr>
						<tr>
							<td class="sbl sbr sbb">Insurance</td>
							<td class="sbr sbb"></td>
						</tr>
						<tr>
							<td class="sbl sbr">TOTAL</td>
							<td class="sbr">{{ $data->invoice ? $data->invoice->total : '' }}</td>
						</tr>
						<tr>
							<td colspan="2" class="sbt"></td>
						</tr>
						<tr>
							<td colspan="2" class="header">
								9. PICK UP DETAILS
							</td>
						</tr>
						<tr>
							<td class="sbl sbr sbb">Picked Up By</td>
							<td class="sbr sbb">{{$data->pickup ? $data->pickup->profile->name : ''}}</td>
						</tr>
						<tr>
							<td class="sbl sbr">Date/Time</td>
							<td class="sbr">{{ $data->pickup ? \Carbon\Carbon::parse($data->pickup->created_at)->format('d M Y, H:m A') : '' }}</td>
						</tr>
						<tr><td colspan="2" class="sbt"></td></tr>
						<tr>
							<td colspan="2" class="header">
								10. DELIVERY DETAILS
							</td>
						</tr>
						<tr>
							<td class="sbl sbr sbb">Received By</td>
							<td class="sbr sbb"></td>
						</tr>
						<tr>
							<td class="sbl sbr sbb">Date/Time</td>
							<td class="sbr sbb"></td>
						</tr>
						<tr>
							<td class="sbl sbr sbb">Signature</td>
							<td class="sbr sbb"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</html>
