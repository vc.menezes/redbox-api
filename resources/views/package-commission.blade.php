<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	
	<style>
	
	.invoice-box{
		padding:30px;
		font-family: 'Times New Roman', 'Arial', sans-serif;
		color:#555;
	}
	
	.invoice-box table{
		width:100%
	}
		
	.invoice-box table tr td:nth-child(2){
		text-align:right;
	}
	
	.invoice-box table tr.top table td{
		padding-bottom:20px;
	}

	
	.invoice-box table tr.information table td{
		padding-bottom:40px;
        font-size:20px;
	}
	
	.invoice-box table tr.heading td{
		background:#eee;
		border-bottom:1px solid #ddd;
		font-weight:bold;
        font-size: 20px;
        padding: 10px;
	}
	
	
	.invoice-box table tr.item td{
		border-bottom:1px solid #eee;
        font-size: 20px;
        padding: 16px 10px;
	}
	
	
	</style>
</head>

<body>
	<div class="invoice-box">
		<table cellpadding="0" cellspacing="0">
			<tr class="top">
				<td colspan="7">
					<table>\
						<tr>
							<td class="title">
								<img src="./images/logo.png" style="width:200px">
							</td>
							
							<td>
								<b>COMMISSION</b><br>
								Created: {{  \Carbon\Carbon::parse($data->created_at)->toFormattedDateString() }}<br>
								{{-- Order #: {{ $data->package->code }} --}}
							</td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr class="information">
				<td colspan="8">
					<table>
						<tr>
							<td>
                                RB International Pvt. Ltd<br>
								info@redbox.mv<br>
								+960 3011887<br>
								H. Whitevilla, Abadhahfehimagu, <br>
								Male, Maldives
							</td>
							
							<td>
								{{ $data->profile->name}}<br>
								{{ $data->profile->email}}<br>
								{{ $data->profile->contact_number}}
							</td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr class="heading">
				<td>
					Code
				</td>
				<td style="text-align: left" colspan="5">
					Description
				</td>
				
				<td style="text-align: right" class="total">
					Amount
				</td>
			</tr>
			@foreach ($data->details as $item)
				<tr class="item">
					<td>
						{{ $item->package_view->reference_no ? $item->package_view->reference_no : $item->package_view->tracking_no }}
					</td>
					<td style="text-align: left" colspan="5">
						{{ $item->package_view->address_name }} ({{ $item->package_view->destinations_contact_number }}) /
						{{ $item->package_view->destinations_address }}  - {{ $item->package_view->weight }} kg
					</td>
					
					<td style="text-align: right" class="total">
						{{ $item->total }}
					</td>
				</tr>
			@endforeach
			<tr class="heading">
				<td colspan="6">
					TOTAL
				</td>
				
				<td style="text-align: right" class="total">
					{{ $data->agent_commission_total }}
				</td>
			</tr>
			
		</table>
	</div>
</html>
