<!DOCTYPE html>
<html style="width:100%;font-family:arial,'helvetica neue',helvetica,sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="telephone=no" name="format-detection">
    <title>conatct</title>

    <style>
        @media only screen and (max-width:600px) {
            a,
            ol li,
            p,
            ul li {
                font-size: 16px!important
            }
            h1 {
                font-size: 30px!important;
                text-align: center
            }
            h2 {
                font-size: 26px!important;
                text-align: center
            }
            h3 {
                font-size: 20px!important;
                text-align: center
            }
            h1 a {
                font-size: 30px!important
            }
            h2 a {
                font-size: 26px!important
            }
            h3 a {
                font-size: 20px!important
            }
            .es-menu td a {
                font-size: 16px!important
            }
            .es-header-body a,
            .es-header-body ol li,
            .es-header-body p,
            .es-header-body ul li {
                font-size: 16px!important
            }
            .es-footer-body a,
            .es-footer-body ol li,
            .es-footer-body p,
            .es-footer-body ul li {
                font-size: 16px!important
            }
            .es-infoblock a,
            .es-infoblock ol li,
            .es-infoblock p,
            .es-infoblock ul li {
                font-size: 12px!important
            }
            [class=gmail-fix] {
                display: none!important
            }
            .es-m-txt-c,
            .es-m-txt-c h1,
            .es-m-txt-c h2,
            .es-m-txt-c h3 {
                text-align: center!important
            }
            .es-m-txt-r,
            .es-m-txt-r h1,
            .es-m-txt-r h2,
            .es-m-txt-r h3 {
                text-align: right!important
            }
            .es-m-txt-l,
            .es-m-txt-l h1,
            .es-m-txt-l h2,
            .es-m-txt-l h3 {
                text-align: left!important
            }
            .es-m-txt-c a img,
            .es-m-txt-l a img,
            .es-m-txt-r a img {
                display: inline!important
            }
            .es-button-border {
                display: block!important
            }
            .es-button {
                font-size: 20px!important;
                display: block!important;
                border-width: 10px 0 10px 0!important
            }
            .es-btn-fw {
                border-width: 10px 0!important;
                text-align: center!important
            }
            .es-adaptive table,
            .es-btn-fw,
            .es-btn-fw-brdr,
            .es-left,
            .es-right {
                width: 100%!important
            }
            .es-content,
            .es-content table,
            .es-footer,
            .es-footer table,
            .es-header,
            .es-header table {
                width: 100%!important;
                max-width: 600px!important
            }
            .es-adapt-td {
                display: block!important;
                width: 100%!important
            }
            .adapt-img {
                width: 100%!important;
                height: auto!important
            }
            .es-m-p0 {
                padding: 0!important
            }
            .es-m-p0r {
                padding-right: 0!important
            }
            .es-m-p0l {
                padding-left: 0!important
            }
            .es-m-p0t {
                padding-top: 0!important
            }
            .es-m-p0b {
                padding-bottom: 0!important
            }
            .es-m-p20b {
                padding-bottom: 20px!important
            }
            .es-hidden {
                display: none!important
            }
            .esd-block-html table,
            table.es-table-not-adapt {
                width: auto!important
            }
            table.es-social {
                display: inline-block!important
            }
            table.es-social td {
                display: inline-block!important
            }
        }
    </style>
    <style>
        #outlook a {
            padding: 0
        }
        
        .ExternalClass {
            width: 100%
        }
        
        .ExternalClass,
        .ExternalClass div,
        .ExternalClass font,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass td {
            line-height: 100%
        }
        
        .es-button {
            mso-style-priority: 100!important;
            text-decoration: none!important
        }
        
        a[x-apple-data-detectors] {
            color: inherit!important;
            text-decoration: none!important;
            font-size: inherit!important;
            font-family: inherit!important;
            font-weight: inherit!important;
            line-height: inherit!important;
        }
        
        
    </style>
</head>

<body style="width:100%;font-family:arial,'helvetica neue',helvetica,sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">
    <div class="es-wrapper-color" style="background-color:#fff">
        <!--[if gte mso 9]><v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t"><v:fill type="tile" src="" color="#ffffff"></v:fill></v:background><![endif]-->
        <table cellpadding="0" cellspacing="0" class="es-wrapper" width="100%" style="mso-table-lspace:0;mso-table-rspace:0;border-collapse:collapse;border-spacing:0;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top">
            <tbody>
                <tr style="border-collapse:collapse">
                    <td valign="top" style="padding:0;Margin:0">
                        <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0;mso-table-rspace:0;border-collapse:collapse;border-spacing:0;table-layout:fixed!important;width:100%">
                            <tbody>
                                <tr style="border-collapse:collapse">
                                    <td align="center" style="padding:0;Margin:0">
                                        <table bgcolor="#f2f2f2" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0;mso-table-rspace:0;border-collapse:collapse;border-spacing:0;background-color:#f2f2f2">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td align="left" style="padding:20px;Margin:0">
                                                        <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0;mso-table-rspace:0;border-collapse:collapse;border-spacing:0">
                                                            <tbody>
                                                                <tr style="border-collapse:collapse">
                                                                    <td width="560" align="center" valign="top" style="padding:0;Margin:0">
                                                                        <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0;mso-table-rspace:0;border-collapse:collapse;border-spacing:0">
                                                                            <tbody>
                                                                                <tr style="border-collapse:collapse">
                                                                                    <td align="left" style="padding:0;Margin:0;padding-bottom:15px">
                                                                                        <h2 style="Margin:0;line-height:120%;mso-line-height-rule:exactly;font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:24px;font-style:normal;font-weight:400;color:#333">Contact Us - via Website</h2></td>
                                                                                </tr>
                                                                                <tr style="border-collapse:collapse">
                                                                                    <td align="left" style="padding:0;Margin:0">
                                                                                        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:#333">Name: {{ $input["name"] }}
                                                                                            <br>
                                                                                        </p>
                                                                                        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:#333">Email:  {{ $input["email"] }}
                                                                                            <br>
                                                                                        </p>
                                                                                        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:#333">Message:</p>
                                                                                        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:#333">{{ $input["message"] }}</p>
                                                                                        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:#333">
                                                                                            <br>
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>