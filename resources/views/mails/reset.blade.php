<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Password Reset</title>
</head>

<body style="background: rgb(255, 255, 255); margin:0">
    <table style="border-collapse: collapse; height: 100%; margin: 0; padding: 0; width: 100%">
        <tbody>
            <tr>
                <td style="height: 100%; margin: 0; padding: 0; width: 100%">
                    <table style="border-collapse: collapse; margin: 0 auto">
                        <tbody>
                            <tr>
                                <td style="background: rgb(255, 255, 255) none no-repeat center/ cover;background-color: rgb(255, 255, 255);background-image: none;background-repeat: no-repeat;background-position: center;border-top: 0;border-bottom: 0;padding-top: 82px;padding-bottom: 30px;">
                                    <table style="border-collapse: collapse; max-width: 400px !important; margin: 0 auto">
                                        <tbody>
                                            <tr>
                                                <td style="background-image: none; background-repeat: no-repeat; background-position: center; border-top: 0; border-bottom: 0; padding-top: 0px; padding-bottom: 0px">
                                                    <table style="min-width: 100%; border-collapse: collapse">
                                                        <tbody>
                                                            <tr>
                                                                <td style="padding: 0px">
                                                                    <table style="min-width: 100%; border-collapse: collapse">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align: center"> <img alt="logo" style="max-width: 125px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;" src="{{ asset('/images/logo.png') }}"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="background: rgb(255, 255, 255) none no-repeat center/ cover;background-color: rgb(255, 255, 255);background-image: none;background-repeat: no-repeat;background-position: center;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0px;">
                                    <table style="border-collapse: collapse; max-width: 400px !important">
                                        <tbody>
                                            <tr>
                                                <td style="background-image: none; background-repeat: no-repeat; background-position: center; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 0">
                                                    <table style="min-width: 100%; border-collapse: collapse">
                                                        <tbody>
                                                            <tr>
                                                                <td style="padding-top: 9px">
                                                                    <table style="max-width: 100%; min-width: 100%; border-collapse: collapse">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 0px; color: rgb(128, 128, 128); font-family: Helvetica; font-size: 16px; line-height: 150%; text-align: left">
                                                                                    <p style="text-align: left; margin: 10px 0; padding: 0; color: rgb(128, 128, 128); font-family: Helvetica; font-size: 16px; line-height: 150%"> <span style="font-family: helvetica neue, helvetica, arial, verdana, sans-serif">Hi {{ $user->name }}</span></p>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="padding-top: 0; padding-right: 18px; padding-left: 0px; color: rgb(128, 128, 128); font-family: Helvetica; font-size: 16px; line-height: 150%; text-align: left">
                                                                                    <p style="text-align: left; margin:0; padding: 0; color: rgb(128, 128, 128); font-family: Helvetica; font-size: 16px; line-height: 150%"> <span style="font-family: helvetica neue, helvetica, arial, verdana, sans-serif"> You have requested to reset the password of the following account. </span></p>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table style="min-width: 100%; border-collapse: collapse; margin-top:20px">
                                                        <tbody>
                                                            <tr>
                                                                <td style="padding-top: 9px">
                                                                    <table style="max-width: 100%; min-width: 100%; border-collapse: collapse; background-color: #eee">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="padding-top: 0; padding-right: 18px; padding-bottom: 0px; padding-top: 15px; padding-left: 18px; color: rgb(128, 128, 128); font-family: Helvetica; font-size: 16px; line-height: 150%; text-align: left">
                                                                                    <p style="text-align: left; margin: 0; padding: 0; color: rgb(128, 128, 128); font-family: Helvetica; font-size: 14px; line-height: 150%"> <span style="font-family: helvetica neue, helvetica, arial, verdana, sans-serif">Name</span></p>
                                                                                </td>
                                                                                <td style="padding-top: 0; padding-right: 18px; padding-bottom: 0px; padding-left: 18px; padding-top: 15px; color: rgb(128, 128, 128); font-family: Helvetica; font-size: 16px; line-height: 150%; text-align: left">
                                                                                    <p style="text-align: left; margin: 0; padding: 0; color: rgb(128, 128, 128); font-family: Helvetica; font-size: 14px; line-height: 150%"> <span style="font-family: helvetica neue, helvetica, arial, verdana, sans-serif">{{ $user->name }}</span></p>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="padding-top: 0; padding-right: 18px; padding-bottom: 10px; padding-left: 18px; color: rgb(128, 128, 128); font-family: Helvetica; font-size: 16px; line-height: 150%; text-align: left">
                                                                                    <p style="text-align: left; margin: 0; padding: 0; color: rgb(128, 128, 128); font-family: Helvetica; font-size: 14px; line-height: 150%"> <span style="font-family: helvetica neue, helvetica, arial, verdana, sans-serif">Email</span></p>
                                                                                </td>
                                                                                <td style="padding-top: 0; padding-right: 18px; padding-bottom: 10px; padding-left: 18px; color: rgb(128, 128, 128); font-family: Helvetica; font-size: 16px; line-height: 150%; text-align: left">
                                                                                    <p style="text-align: left; margin: 0; padding: 0; color: rgb(128, 128, 128); font-family: Helvetica; font-size: 14px; line-height: 150%"> <span style="font-family: helvetica neue, helvetica, arial, verdana, sans-serif">{{ $user->email }}</span></p>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table style="min-width: 100%; border-collapse: collapse; table-layout: fixed !important">
                                                        <tbody>
                                                            <tr>
                                                                <td style="min-width: 100%; padding: 9px 18px 0px">
                                                                    <table style="min-width: 100%; border-collapse: collapse">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style=""> <span></span></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table style="min-width: 100%; border-collapse: collapse; table-layout: fixed !important">
                                                        <tbody>
                                                            <tr>
                                                                <td style="min-width: 100%; padding: 18px 18px 0px">
                                                                    <table style="min-width: 100%; border-collapse: collapse">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style=""> <span></span></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table style="min-width: 100%; border-collapse: collapse">
                                                        <tbody>
                                                            <tr>
                                                                <td style="padding-top: 9px">
                                                                    <table style="max-width: 100%; min-width: 100%; border-collapse: collapse">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;color:rgb(128,128,128);text-align:center"> <a href="{{ env('SITE_URL').'/reset-password?token='.$token}}" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;border-radius:3px;color:#fff;display:inline-block;text-decoration:none;background-color:#000000;border-top:10px solid #000000;border-right:18px solid #000000;border-bottom:10px solid #000000;border-left:18px solid #000000" target="_blank">Reset Password</a></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table style="min-width: 100%; border-collapse: collapse">
                                                        <tbody>
                                                            <tr>
                                                                <td style="padding-top: 9px">
                                                                    <table style="max-width: 100%; min-width: 100%; border-collapse: collapse">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 0px; color: rgb(128, 128, 128); font-family: Helvetica; font-size: 16px; line-height: 150%; text-align: left">
                                                                                    <p style="text-align: center; margin: 10px 0; padding: 0; color: rgb(128, 128, 128); font-family: Helvetica; font-size: 16px; line-height: 150%"> <span style="font-family: helvetica neue, helvetica, arial, verdana, sans-serif"> If you don't want to reset your password or if you didn't request this change, you can ignore this email. No one else can change your password with this link. </span></p>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-top: 0; border-bottom: 0; padding-top: 25px; padding-bottom: 15px">
                                    <table style="border-collapse: collapse; max-width: 400px !important; border-top: 1px solid #b8b8b8">
                                        <tbody>
                                            <tr>
                                                <td style="background-image: none; background-repeat: no-repeat; background-position: center; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 0">
                                                    <table style="min-width: 100%; border-collapse: collapse">
                                                        <tbody>
                                                            <tr>
                                                                <td style="padding-top: 9px">
                                                                    <table style="max-width: 100%; min-width: 100%; border-collapse: collapse">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="padding: 0px 18px 9px; color: rgb(177, 177, 177); font-family: Helvetica; font-size: 12px; line-height: 150%; text-align: center"> <span style="font-family: helvetica neue, helvetica, arial, verdana, sans-serif"> <span style="font-size: 12px"> <b>{{env('WEBSITE')}}</b> <br> <small>CONNECTING PEOPLE. IMPROVING LIVES.</small> <br> <br> <small>Phone: {{env('PHONE')}} | Fax: {{env('FAX')}} | Email: {{env('EMAIL')}}</small> </span> </span>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>