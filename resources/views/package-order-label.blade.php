<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 25px;
            font-family: 'Times New Roman', 'Arial', sans-serif;
        }
        
        
        th,td {
            padding:10px;
            
        }
        th {
            text-align:left;
        }
        
        table {
            /* width: 100%; */
        } 
        
        .logo{
            display: block;
            width: 200px;

        }
        .text-cap {
            text-transform: capitalize;
        }
	</style>
</head>
<body>
    <p style="color: grey; font-size: 18px">To view tracking details visit our website www.redbox.mv or use our mobile app</p>
    <table>
        <tr>
            <td colspan= "2" style="height: 70px">
                <img class="logo" src="./images/logo.png">
            </td>

            <td colspan= "2">
                TRACKING #: <b>{{ $data->reference_no ? $data->reference_no : $data->tracking_no}}</b>
                <br/>
                {{-- REFERENCE: {{ $data->reference_no ? $data->reference_no : '-' }} --}}
            </td>
        </tr>
        <tr>
            <td colspan= "2">
                <div style="padding-left: 60px">
                    {!! DNS2D::getBarcodeHTML($data->barcode, "QRCODE", 7, 7) !!}
                </div>
            </td>
                
            <td colspan= "2" style="text-align:center;">
                <div style="padding:0 10px">
                    {!! DNS1D::getBarcodeHTML($data->barcode, "C128", 5, 120) !!}
                </div>
                {{-- {{ $data->barcode }} --}}
            </td>
        </tr>	
        
        <tr>
            <td rowspan="3">
                <b> PACKAGE <br/> DETAILS</b>
            </td>
            <td> 
                ORIGIN:
                <br>
                <b>MLE</b> 
            </td>
            <td> 
                DESTINATION:
                <br>
                <b>{{ $data->destination ?  $data->destination->prefix : $data->destination->name }}</b>
            </td>

            <td class="text-cap">
                MEDIUM:
                <br>
                <b>{{ $data->transport_medium }}</b>
            </td>
        </tr>
        <tr>
            <td> 
                WEIGHT:
                <br>
                <b>{{ $data->weight}} </b>
            </td>
            <td> 
                QTY: <br>
                <b>{{ count($data->items) }} PCS</b>
            </td>

            <td> 
                {{ $data->pickup ? 'PICKUP DATE' : 'DATE' }}
                <br>
                <b> {{ $data->pickup ? \Carbon\Carbon::parse($data->pickup->created_at)->format('d-m-Y H:mA') : \Carbon\Carbon::parse($data->created_at)->format('d-m-Y H:mA') }} </b>
            </td>
        </tr>	
        
        <tr>
            <td colspan="3"> 
                CONTENT:<br>
                <b>{{ str_limit($data->contents, 25)}}</b>
            </td>
        </tr>	
    
        <tr>
            <td>
                <b>FROM</b>
            </td>
            <td colspan="2">
                <b>{{ $data->profile->name }}</b>
                @if($data->picking_location)
                    <br> {{ $data->picking_location->address ?  $data->picking_location->address->address_line : $data->picking_location->location->name }}  
                @else
                    <br>{{ $data->address ? $data->address->address_line : ''}}
                @endif
                @if($data->profile->location_name)
                <br>
                {{ $data->profile->location_name}}<br>
                @endif
            </td> 
            <td>
                CONTACT:<br>
                <b>{{ $data->profile->contact_number}}</b>
            </td>
        
        </tr>	

        <tr>
            <td>
                <b>TO</b>
            </td>
            <td colspan="2">
                <b>{{ $data->address_name }}</b><br>
                {{ $data->destinations_address }}<br>
                {{ $data->destination_island }}<br>
            </td>
            <td>
                CONTACT:
                <br/>
                <b>{{ $data->destinations_contact_number }}</b>
            </td>
        </tr>	
    </table>
    <p style="color: grey; font-size: 16px">RB International Pvt. Ltd | info@redbox.mv | +960 3011887 | H. Whitevilla, Abadhahfehimagu, Male, Maldives </p>

</body>
</html>