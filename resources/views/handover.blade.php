<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	
	<style>
	
	.invoice-box{
		padding:30px;
		font-family: 'Times New Roman', 'Arial', sans-serif;
		color:#555;
	}
	
	.invoice-box table{
		width:100%
	}
    .border {
        border-collapse: collapse;
    }

    .border  td {
        border: 1px solid black;
    }
	td {
		padding: 10px;
		vertical-align: top;
		font-size: 17px;
	}
	/* .border {
        border: 1px solid #000;
    } */
	</style>
</head>

<body>
	<div class="invoice-box">
        <h3>SHIPMENTS HANDOVER SHEET</h3>
        <hr>
		<table cellpadding="0" cellspacing="0">
			<tr>
                <td> <b>FROM: </b> {{ $data->name }} </td>
                <td style="text-align: right"> <b>DATE: </b> {{ \Carbon\Carbon::parse($data->created_at)->format('d M Y') }} </td>
            </tr>
			<tr>
                <td> <b>TO: </b> Redbox </td>
                <td style="text-align: right"> <b>BULK ORDER NO.: {{ $data->id }} </b></td>
            </tr>
        </table>
        <hr>
        <table class="border">
            <tr style="background-color: #eee; font-weight: bold">
                <td>#</td>
                <td>TRACKING NUMBER</td>
                <td>CUSTOMER NAME</td>
                <td>CONTACT</td>
                <td>DESTINATION</td>
                <td>CONTENT</td>
                <td>WEIGHT</td>
            </tr>
            {{ $id = 1 }}
            @foreach ($data->packages as $order)
                <tr>
                <td>{{$id}}</td>
                    <td>{{ $order->reference_no ? $order->reference_no : $order->tracking_no  }}</td>
                    <td>{{ $order->address_name }}</td>
                    <td>{{ $order->destinations_contact_number }}</td>
                    <td>{{ $order->destination_island }}</td>
                    <td>
                        @foreach ($order->items as $item)
                            {{ count($order->items) == 1 ? $item->commodity->name : $item->commodity->name. ', ' }}
                        @endforeach
                    </td>
                    <td>{{ $order->weight }} KG</td>
                </tr>
                {{ $id ++ }}
            @endforeach
        </table>
        <hr>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td> <b>CREATED BY: </b></td>
                <td style="text-align: right; padding-right: 200px""> <b>RECEIVED BY:</b> </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td> <b>SIGN: </b></td>
                <td style="text-align: right; padding-right: 200px"> <b>SIGN: </b> </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td> <b>DATE: </b> {{ \Carbon\Carbon::parse($data->created_at)->format('d M Y') }} </td>
                <td style="text-align: right; padding-right: 200px"> <b>DATE: </b> </td>
            </tr>
        </table>
	</div>
</html>
