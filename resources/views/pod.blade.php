<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <style>

        .invoice-box {
            padding: 30px;
            font-family: 'Times New Roman', 'Arial', sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%
        }

        td {
            padding: 5px;

        }

        .pb {
            padding-bottom: 20px;
        }
    </style>
</head>

<body>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img src="./images/logo.png" style="width:200px;  padding-bottom: 30px;">
            </td>
            
            <td style="font-size: 40px; font-weight: bold; padding-bottom: 40px; text-align: right;"> Proof of Delivery</td>
        </tr>
        <tr>
            <td colspan="2">Dear Customer,</td>
        </tr>
        <tr>
            <td class="pb" colspan="2">This serves as proof of delivery for the shipment listed below:</td>
        </tr>
        <tr>
            <td><b>Tracking Number:</b></td>
            <td>{{ $data->reference_no ? $data->reference_no : $data->tracking_no }}</td>
        </tr>
        @if($data->storage)
        <tr>
            <td style="vertical-align: top"><b>Location:</b></td>
            <td>{{ $data->storage->block->code }}</td>
        </tr>
        @endif
        <tr>
            <td><b>Shipment Reference:</b></td>
            <td>{{ $data->parent ?  $data->parent->code : '-' }}</td>
        </tr>
        <tr>
            <td><b>Service:</b></td>
            <td>Domestic Delivery</td>
        </tr>
        <tr>
            <td><b>Weight:</b></td>
            <td>{{ $data->weight }}kg</td>
        </tr>
        <tr>
            <td><b>Shipped or Billed on:</b></td>
            <td>{{ $data->invoice ? \Carbon\Carbon::parse($data->invoice->created_at)->format('d M Y, H:m A') : '-' }}</td>
        </tr>
        <tr>
            <td><b>Delivered on:</b></td>
            <td>{{$data->delivered ? \Carbon\Carbon::parse($data->delivered->created_at)->format('d M Y, H:m A') : '-'}}</td>
        </tr>
        <tr>
            <td style="vertical-align: top"><b>Shipped by:</b></td>
            <td>
                {{ $data->profile->name }}<br>
                {{ $data->profile->contact_number }} <br>
                @foreach($data->profile->address as $item)
                    {{ $item->address_line }} <br>
                    {{ $item->street}}<br>
                @endforeach
            </td>

        </tr>
        <tr>
            <td style="vertical-align: top"><b>Delivered to:</b></td>
            <td>
                {{ $data->address_name }}<br>
                {{ $data->destinations_address }} <br>
                {{ $data->destination_island}}<br>
                {{ $data->destinations_contact_number }}
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top"><b>Received by:</b></td>
            {{-- @if($data->handOver) --}}
                <td>
                    Name: {{ $data->handOver ? $data->handOver->name : '' }}<br>
                    ID Card: {{ $data->handOver ? $data->handOver->identifier: '' }} <br>
                    Contact No.: {{ $data->handOver ? $data->handOver->contact: '' }} <br>
                    Received Date: {{ $data->handOver ? \Carbon\Carbon::parse($data->handOver->created_at)->format('d M Y, H:m A') : '' }}
                </td>
            {{-- @endIf --}}
        </tr>
        <tr>
            <td><b>Signature:</b></td>
            <td style="vertical-align: top" class="pb">
                @if($data->sign)
                    <img width="200" src="{{ env('SITE_URL').'/api/open/attachables/blaalbalaa/'.$data->sign->id }}"
                         alt="logo">
                @else
                    <br><br><br><br>
                @endif
            </td>
        </tr>
        <tr>
            <td class="pb">Thank you for choosing us to serve you.</td>
        </tr>
        <tr>
            <td>Sincerely,</td>
        </tr>
        <tr>
            <td style="padding-bottom: 30px">REDBOX</td>
        </tr>
    </table>
    {!! DNS1D::getBarcodeHTML($data->barcode, "C128", 4, 70); !!}
    <p style="color: #555; font-size: 18px; margin-bottom:0; margin-top: 10px">Tracking results provided by
        redbox.mv</p>
    <hr>
    <p style="color: #555; font-size: 18px; margin:0">RB International Pvt. Ltd | info@redbox.mv | +960 3011887 | H.
        Whitevilla, Abadhahfehimagu, Male, Maldives</p>
</div>
</html>
