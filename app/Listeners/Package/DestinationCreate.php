<?php

namespace App\Listeners\Package;

use App\Events\OrderCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DestinationCreate
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  object $event
	 * @return void
	 */
	public function handle(OrderCreated $event)
	{
		if ($event->request->destination) {

			$event->package->package_destination()->create($event->request->destination);

		}
	}
}
