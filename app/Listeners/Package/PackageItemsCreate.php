<?php

namespace App\Listeners\Package;

use App\Events\OrderCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class PackageItemsCreate
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  object $event
	 * @return void
	 */
	public function handle(OrderCreated $event)
	{
		if ($event->request->items) {
			$event->package->items()->createMany($event->request->items);
		}

	}
}
