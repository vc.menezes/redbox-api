<?php

namespace App\Listeners\Package;

use App\Events\OrderCreated;
use App\Models\StorageBlock;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class StorageBlockCreate
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  object $event
	 * @return void
	 */
	public function handle(OrderCreated $event)
	{
		if ($event->request->block_id) {
			StorageBlock::create(['block_id' => $event->request->block_id, 'package_id' => $event->package->id]);
		}
	}
}
