<?php

namespace App\Listeners\Package;

use App\Events\OrderCreated;
use App\Models\Package;
use App\Modules\Finance\Invoice;
use App\Modules\Finance\Shipping;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class CreateInvoice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(OrderCreated $event)
    {
			$input['weight'] = $event->package->weight;
			$input['operator'] = '=';

			$medium = $event->package->package_destination->medium;
			if ($medium != 'land') {
				$input['medium'] = $medium;
			}

			if ($event->package->weight < 1) {
				$input['weight'] = ceil($event->package->weight);
				$input['operator'] = '<';
			}

			$package = Package::find($event->package->id);


			$shipping = new Shipping($input, $package);
			$invoice = new Invoice($shipping);
			$invoice = $invoice->create();

			if ($invoice->error) {
				Log::error($invoice->error->getmessage());
			}
    }
}
