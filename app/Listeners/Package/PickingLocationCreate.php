<?php

namespace App\Listeners\Package;

use App\Events\OrderCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PickingLocationCreate
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  OrderCreated $event
	 * @return void
	 */
	public function handle(OrderCreated $event)
	{
		if ($event->request->pickingLocation) {
			$event->package->pickingLocation()->create($event->request->pickingLocation);
		}
	}
}
