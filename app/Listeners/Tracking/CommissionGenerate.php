<?php

namespace App\Listeners\Tracking;

use App\Events\TrackingLogUpdated;
use App\Models\Setting;
use App\Modules\Commission\Commission;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommissionGenerate
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  object $event
	 * @return void
	 */
	public function handle(TrackingLogUpdated $event)
	{
		if ($this->isCommissionable($event)) {
				$commission = new Commission($event->tracking_log->tracking->package);
				$commission->raiseAgentCommission()->raiseCommission();

			}
	}

	/**
	 * @param TrackingLogUpdated $event
	 * @param $COMMISSION_GENERATE_STATUS
	 * @return bool
	 */
	public function isCommissionable(TrackingLogUpdated $event): bool
	{
		$COMMISSION_GENERATE_STATUS = Setting::where('key', 'COMMISSION_GENERATE_STATUS')->first();
		return $COMMISSION_GENERATE_STATUS->value == $event->tracking_log->status_id && $this->IsShipment($event);
	}

	/**
	 * @param TrackingLogUpdated $event
	 * @return bool
	 */
	public function IsShipment(TrackingLogUpdated $event): bool
	{
		return $event->tracking_log->tracking->package->type == 'shipment';
	}


}
