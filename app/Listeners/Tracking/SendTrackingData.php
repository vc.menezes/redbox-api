<?php

namespace App\Listeners\Tracking;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SendTrackingData
{
	public $client;
	public $url;
	public $headers;
	public $base_url;

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->setClient();
		$this->setBaseUrl();
		$this->setUrl();
		$this->setHeaders();
	}

	/**
	 *
	 */
	public function setClient(): void
	{
		$this->client = new \GuzzleHttp\Client();



	}

	/**
	 *
	 */
	public function setUrl(): void
	{
		$this->url = '/integrations/delivery/presto/update_delivery.json';
	}

	/**
	 *
	 */
	public function setHeaders(): void
	{
		$this->headers = [
			'Content-Type'=>'application/json',
			'METHOD'=>'POST'
		];
	}

	/**
	 *
	 */
	public function setBaseUrl(): void
	{
		$this->base_url = 'http://prestostaging.ooredoo.mv';
	}

	/**
	 * Handle the event.
	 *
	 * @param  object $event
	 * @return void
	 */
	public function handle($event)
	{
//		$data['tracking'] = $event->tracking_log->tracking->toArray();
//		$data['package'] = $event->tracking_log->tracking->package->toArray();
//		$data['package']['profile'] = $event->tracking_log->profile->toArray();
//		$data['tracking']['tracking_log'] = $event->tracking_log->tracking->toArray();
//		$data['package']['package_destination'] = $event->tracking_log->tracking->package->package_destination->toArray();
//
//
//		$request_url = $this->base_url.$this->url;
//		$request = $this->client->post($request_url,['form_params'=>$data,'header'=>$this->headers]);
//    $response = $request->send();
//
//		Log::info('tracking updated',$response);
	}
}
