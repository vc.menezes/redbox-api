<?php

namespace App\Providers;

use App\Helpers\RedBox;
use Illuminate\Support\ServiceProvider;

class RedboxServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('redbox', function () {
            return new RedBox();
        });
    }
}
