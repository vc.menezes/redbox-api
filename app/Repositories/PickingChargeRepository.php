<?php

namespace App\Repositories;

use App\Models\PickingCharge;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PickingChargeRepository
 * @package App\Repositories
 * @version October 21, 2018, 3:40 am UTC
 *
 * @method PickingCharge findWithoutFail($id, $columns = ['*'])
 * @method PickingCharge find($id, $columns = ['*'])
 * @method PickingCharge first($columns = ['*'])
*/
class PickingChargeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PickingCharge::class;
    }
}
