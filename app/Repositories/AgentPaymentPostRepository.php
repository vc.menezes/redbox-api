<?php

namespace App\Repositories;

use App\Models\AgentPaymentPost;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AgentPaymentPostRepository
 * @package App\Repositories
 * @version March 4, 2019, 5:50 am UTC
 *
 * @method AgentPaymentPost findWithoutFail($id, $columns = ['*'])
 * @method AgentPaymentPost find($id, $columns = ['*'])
 * @method AgentPaymentPost first($columns = ['*'])
*/
class AgentPaymentPostRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AgentPaymentPost::class;
    }
}
