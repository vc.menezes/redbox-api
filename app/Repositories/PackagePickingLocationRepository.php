<?php

namespace App\Repositories;

use App\Models\PackagePickingLocation;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PackagePickingLocationRepository
 * @package App\Repositories
 * @version July 22, 2018, 4:49 am UTC
 *
 * @method PackagePickingLocation findWithoutFail($id, $columns = ['*'])
 * @method PackagePickingLocation find($id, $columns = ['*'])
 * @method PackagePickingLocation first($columns = ['*'])
*/
class PackagePickingLocationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PackagePickingLocation::class;
    }
}
