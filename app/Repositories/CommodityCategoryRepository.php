<?php

namespace App\Repositories;

use App\Models\CommodityCategory;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CommodityCategoryRepository
 * @package App\Repositories
 * @version July 22, 2018, 5:16 am UTC
 *
 * @method CommodityCategory findWithoutFail($id, $columns = ['*'])
 * @method CommodityCategory find($id, $columns = ['*'])
 * @method CommodityCategory first($columns = ['*'])
*/
class CommodityCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CommodityCategory::class;
    }
}
