<?php

namespace App\Repositories;

use App\Models\Sequence;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SequenceRepository
 * @package App\Repositories
 * @version July 21, 2018, 7:14 am UTC
 *
 * @method Sequence findWithoutFail($id, $columns = ['*'])
 * @method Sequence find($id, $columns = ['*'])
 * @method Sequence first($columns = ['*'])
*/
class SequenceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sequence::class;
    }
}
