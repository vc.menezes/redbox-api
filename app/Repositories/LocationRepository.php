<?php

namespace App\Repositories;

use App\Models\Location;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class LocationRepository
 * @package App\Repositories
 * @version July 18, 2018, 6:32 am UTC
 *
 * @method Location findWithoutFail($id, $columns = ['*'])
 * @method Location find($id, $columns = ['*'])
 * @method Location first($columns = ['*'])
 */
class LocationRepository extends BaseRepository
{
    /**
     * @var array
     */

    public $test = [];
    protected $fieldSearchable = [
        'name' => 'like'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Location::class;
    }



}
