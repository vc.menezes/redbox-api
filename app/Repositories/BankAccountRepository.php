<?php

namespace App\Repositories;

use App\Models\BankAccount;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BankAccountRepository
 * @package App\Repositories
 * @version July 18, 2018, 8:14 am UTC
 *
 * @method BankAccount findWithoutFail($id, $columns = ['*'])
 * @method BankAccount find($id, $columns = ['*'])
 * @method BankAccount first($columns = ['*'])
*/
class BankAccountRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'account_no',
        'active',
        'currency'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BankAccount::class;
    }
}
