<?php

namespace App\Repositories;

use App\Models\Berth;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BerthRepository
 * @package App\Repositories
 * @version July 25, 2018, 4:44 am UTC
 *
 * @method Berth findWithoutFail($id, $columns = ['*'])
 * @method Berth find($id, $columns = ['*'])
 * @method Berth first($columns = ['*'])
*/
class BerthRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Berth::class;
    }
}
