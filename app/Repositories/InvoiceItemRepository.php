<?php

namespace App\Repositories;

use App\Models\InvoiceItem;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class InvoiceItemRepository
 * @package App\Repositories
 * @version October 18, 2018, 8:34 am UTC
 *
 * @method InvoiceItem findWithoutFail($id, $columns = ['*'])
 * @method InvoiceItem find($id, $columns = ['*'])
 * @method InvoiceItem first($columns = ['*'])
*/
class InvoiceItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InvoiceItem::class;
    }
}
