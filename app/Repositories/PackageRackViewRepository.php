<?php

namespace App\Repositories;

use App\Views\PackageRackView;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PackageRackViewRepository
 * @package App\Repositories
 * @version October 15, 2018, 9:38 am UTC
 *
 * @method PackageRackView findWithoutFail($id, $columns = ['*'])
 * @method PackageRackView find($id, $columns = ['*'])
 * @method PackageRackView first($columns = ['*'])
*/
class PackageRackViewRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'reference_no' => 'like',
        'tracking_no' => 'like',
        'code' => 'like',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PackageRackView::class;
    }
}
