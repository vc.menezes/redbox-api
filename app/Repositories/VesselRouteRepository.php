<?php

namespace App\Repositories;

use App\Models\VesselRoute;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VesselRouteRepository
 * @package App\Repositories
 * @version July 25, 2018, 4:57 am UTC
 *
 * @method VesselRoute findWithoutFail($id, $columns = ['*'])
 * @method VesselRoute find($id, $columns = ['*'])
 * @method VesselRoute first($columns = ['*'])
*/
class VesselRouteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VesselRoute::class;
    }
}
