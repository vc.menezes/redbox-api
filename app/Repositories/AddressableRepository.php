<?php

namespace App\Repositories;

use App\Models\Addressable;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AddressableRepository
 * @package App\Repositories
 * @version August 2, 2018, 4:47 am UTC
 *
 * @method Addressable findWithoutFail($id, $columns = ['*'])
 * @method Addressable find($id, $columns = ['*'])
 * @method Addressable first($columns = ['*'])
*/
class AddressableRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Addressable::class;
    }
}
