<?php

namespace App\Repositories;

use App\Models\Payload;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PayloadRepository
 * @package App\Repositories
 * @version July 24, 2018, 6:56 pm UTC
 *
 * @method Payload findWithoutFail($id, $columns = ['*'])
 * @method Payload find($id, $columns = ['*'])
 * @method Payload first($columns = ['*'])
*/
class PayloadRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'data'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Payload::class;
    }
}
