<?php

namespace App\Repositories;

use App\Models\Signature;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SignatureRepository
 * @package App\Repositories
 * @version July 24, 2018, 8:49 am UTC
 *
 * @method Signature findWithoutFail($id, $columns = ['*'])
 * @method Signature find($id, $columns = ['*'])
 * @method Signature first($columns = ['*'])
*/
class SignatureRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Signature::class;
    }
}
