<?php

namespace App\Repositories;

use App\Views\ProfileView;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AgentProfileRepository
 * @package App\Repositories
 * @version September 15, 2018, 11:41 am UTC
 *
 * @method AgentProfile findWithoutFail($id, $columns = ['*'])
 * @method AgentProfile find($id, $columns = ['*'])
 * @method AgentProfile first($columns = ['*'])
*/
class AgentProfileRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name' => 'like',
        'identifier',
        'contact_number' => 'like',
        'location_name' => 'like'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProfileView::class;
    }
}
