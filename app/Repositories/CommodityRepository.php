<?php

namespace App\Repositories;

use App\Models\Commodity;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CommodityRepository
 * @package App\Repositories
 * @version July 22, 2018, 6:16 am UTC
 *
 * @method Commodity findWithoutFail($id, $columns = ['*'])
 * @method Commodity find($id, $columns = ['*'])
 * @method Commodity first($columns = ['*'])
*/
class CommodityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name' => 'like'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Commodity::class;
    }
}
