<?php

namespace App\Repositories;

use App\Models\AgentCommission;
use App\Models\Invoice;
use App\Models\Package;
use App\Models\Setting;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AgentCommissionRepository
 * @package App\Repositories
 * @version September 27, 2018, 5:03 am UTC
 *
 * @method AgentCommission findWithoutFail($id, $columns = ['*'])
 * @method AgentCommission find($id, $columns = ['*'])
 * @method AgentCommission first($columns = ['*'])
 */
class AgentCommissionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AgentCommission::class;
    }


}
