<?php

namespace App\Repositories;

use App\Models\InvoiceReceipt;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class InvoiceReceiptRepository
 * @package App\Repositories
 * @version July 24, 2018, 6:53 pm UTC
 *
 * @method InvoiceReceipt findWithoutFail($id, $columns = ['*'])
 * @method InvoiceReceipt find($id, $columns = ['*'])
 * @method InvoiceReceipt first($columns = ['*'])
*/
class InvoiceReceiptRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InvoiceReceipt::class;
    }
}
