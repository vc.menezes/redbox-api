<?php

namespace App\Repositories;

use App\Models\attributable;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class attributableRepository
 * @package App\Repositories
 * @version July 25, 2018, 4:35 am UTC
 *
 * @method attributable findWithoutFail($id, $columns = ['*'])
 * @method attributable find($id, $columns = ['*'])
 * @method attributable first($columns = ['*'])
*/
class attributableRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'attributable'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return attributable::class;
    }
}
