<?php

namespace App\Repositories;

use App\Models\Attachable;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AttachableRepository
 * @package App\Repositories
 * @version July 25, 2018, 4:28 am UTC
 *
 * @method Attachable findWithoutFail($id, $columns = ['*'])
 * @method Attachable find($id, $columns = ['*'])
 * @method Attachable first($columns = ['*'])
*/
class AttachableRepository extends BaseRepository
{
    private $image_ext = ['jpg', 'jpeg', 'png', 'gif'];
    private $audio_ext = ['mp3', 'ogg', 'mpga'];
    private $video_ext = ['mp4', 'mpeg'];
    private $document_ext = ['doc', 'docx', 'pdf', 'odt'];
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'attachable'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Attachable::class;
    }


    public function attachFiles($files, $request)
    {
        

        $input['attachable_id'] = $request->attachable_id;
        $input['attachable_type'] = 'App\\Models\\'.$request->attachable_type;
        $input['type'] = 'file';

        foreach ($files as $file) {
            $input['extension'] = $file->getClientOriginalExtension();
            $input['name'] = time().'-'.$file->getClientOriginalName();
            $input['mime_type'] = $this->getType($input['extension']);
            
            $this->create($input);

            $file->move(storage_path(). '/app/file/', $input['name']);  
            // \File::put(storage_path(). '/app/file/' . $input['name'], $file);
        }

        
    }

    /**
     * deleteFile file from disk and database
     * @param  String  $filename 
     * @return boolean      True if success, otherwise - false
     */
    public function deleteFile($filename)
    {
        // dd($filename);
        if (\Storage::disk('local')->exists($filename)) {
            if (\Storage::disk('local')->delete( $filename )) {
                return true;
            }
        }

        return false;
    }

    private function getType($ext)
    {
        if (in_array($ext, $this->image_ext)) {
            return 'image';
        }

        if (in_array($ext, $this->audio_ext)) {
            return 'audio';
        }

        if (in_array($ext, $this->video_ext)) {
            return 'video';
        }

        if (in_array($ext, $this->document_ext)) {
            return 'document';
        }
    }
}
