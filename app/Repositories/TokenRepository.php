<?php

namespace App\Repositories;

use App\Models\Token;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TokenRepository
 * @package App\Repositories
 * @version July 20, 2018, 8:31 am UTC
 *
 * @method Token findWithoutFail($id, $columns = ['*'])
 * @method Token find($id, $columns = ['*'])
 * @method Token first($columns = ['*'])
*/
class TokenRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Token::class;
    }

    public function generate(){

    }
}
