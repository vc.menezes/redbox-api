<?php

namespace App\Repositories;

use App\Views\CommissionReportView;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CommisionReportViewRepository
 * @package App\Repositories
 * @version February 24, 2019, 3:34 pm UTC
 *
 * @method CommisionReportView findWithoutFail($id, $columns = ['*'])
 * @method CommisionReportView find($id, $columns = ['*'])
 * @method CommisionReportView first($columns = ['*'])
*/
class CommissionReportViewRepository extends BaseRepository
{

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CommissionReportView::class;
    }
}
