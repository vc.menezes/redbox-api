<?php

namespace App\Repositories;

use App\Views\PackageView;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PackageViewRepository
 * @package App\Repositories
 * @version September 16, 2018, 5:25 am UTC
 *
 * @method PackageView findWithoutFail($id, $columns = ['*'])
 * @method PackageView find($id, $columns = ['*'])
 * @method PackageView first($columns = ['*'])
*/
class PackageViewRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tracking_no' => 'like',
        'reference_no' => 'like',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PackageView::class;
    }
}
