<?php

namespace App\Repositories;

use App\Models\Tracking;
use App\Models\StorageBlock;
use InfyOm\Generator\Common\BaseRepository;
use App\Jobs\GiveSequence;
use App\Notifications\TrackingUpdated;
use App\Jobs\SendShipmentSms;
/**
 * Class TrackingRepository
 * @package App\Repositories
 * @version July 24, 2018, 8:53 am UTC
 *
 * @method Tracking findWithoutFail($id, $columns = ['*'])
 * @method Tracking find($id, $columns = ['*'])
 * @method Tracking first($columns = ['*'])
 */
class TrackingRepository extends BaseRepository
{
    use \App\Traits\Jwt;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tracking_no'
    ];



    /**
     * Configure the Model
     **/
    public function model()
    {
        return Tracking::class;
    }

    public function createNewTracking(\App\Models\Package $package, $request = null, $status = 1, $location_id = null, $description = null, $eta = null)
    {

        if($package->type == 'shipment') {
            $arr = [
                'tracking_no' => '-',
                'estimated_delivery_date' => $eta,
                'status' => 1
            ];
        } else {
            $arr = [
                'tracking_no' => '-',
                'estimated_delivery_date' => \Carbon\Carbon::now()->addWeek(),
                'status' => 1
            ];
        }
        //crate tracking for the new created package
        $tracking = $package->tracking()->create($arr);

        //give tracking number to the tracking
        GiveSequence::dispatch($tracking, 'tracking_no', 'tracking_number');

        //create tracking log for the new tracking
        $tracking->tracking_log()->create([
            'status_id' => $status,
            'location_id' => $location_id,
            'description' => $description,
            'profile_id' => $this->getProfileId()
        ]);

        //check package type , if shipment
        if ($package->type == 'shipment') {
            $this->linkToParent($package, $request, $status, $location_id, $description);
        }

        if($package->type == 'shipment' && $eta) {
            $this->updateETA($package, $eta);
        }


    }

    public function updateETA($package, $eta) {
        foreach($package->child as $order) {
            $order->tracking->update([
                'estimated_delivery_date' => $eta
            ]);
        }
    }

    // params []
    public function linkToParent($package, $request, $status_id, $location_id = null, $description = null)
    {
        //find all child package
        $childs = \App\Models\Package::whereIn('id', $request->order_id)->get();

        //linking to parent
        foreach ($childs as $order) {

            $order->parent_id = $package->id;
            $order->save();

            $this->createTrackingLog($order,$status_id, $location_id, $description);
        }

    }

    public function updateChildTrackingLogs(\App\Models\Package $package, $status_id, $location_id = null, $description = null)
    {
        $this->createTrackingLog($package, $status_id, $location_id, $description);

        if($package->type =='shipment' && $status_id == 5) {
            $this->createTrackingLog($package, 6, $location_id, $description);
        }

        //send sms to agent if package status == shipped from male and package == shipment
        if($package->type =='shipment' && $status_id == 4) {
            
            //send sms with shipment details to agent
            SendShipmentSms::dispatch($package->profile->contact_number, $package);
            
        }

        foreach ($package->child as $order) {
            $this->createTrackingLog($order, $status_id, $location_id, $description);
        }
    }


    public function createTrackingLog(\App\Models\Package $package, $status_id, $location_id = null, $description = null)
    {
        $log = $package->tracking->tracking_log()->create([
            'status_id' => $status_id,
            'location_id' => $location_id,
            'description' => $description,
            'profile_id' => $this->getProfileId()
        ]);

        //remove package from block if status = shipped from male or delivered
        if($status_id == 4) {
            StorageBlock::where('package_id', $package->id)->delete();
        }

        if($status_id == 6) {
            $this->updateParent($log);
        }
    }

    public function updateTrackingLog(\App\Models\Package $package, $log, $status_id, $location_id = null, $description = null)
    {
        // dd($status_id);
        $log = $package->tracking->tracking_log->update([
            'status_id' => $status_id,
            'location_id' => $location_id,
            'description' => $description,
            'profile_id' => $this->getProfileId()
        ], $log->id);

        if($status_id == 6) {
            $this->updateParent($log);
        }
    }

    public function updateParent($trackingLog)
    {
        $tracking = \App\Models\Tracking::find($trackingLog->tracking_id);
        $tracking->status = 0;
        $tracking->save();
    }


}
