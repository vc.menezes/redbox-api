<?php

namespace App\Repositories;

use App\Models\Remark;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RemarkRepository
 * @package App\Repositories
 * @version October 14, 2018, 10:08 am UTC
 *
 * @method Remark findWithoutFail($id, $columns = ['*'])
 * @method Remark find($id, $columns = ['*'])
 * @method Remark first($columns = ['*'])
*/
class RemarkRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'remarkable'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Remark::class;
    }
}
