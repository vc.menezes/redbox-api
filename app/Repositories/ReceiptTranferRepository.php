<?php

namespace App\Repositories;

use App\Models\ReceiptTranfer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReceiptTranferRepository
 * @package App\Repositories
 * @version August 19, 2018, 6:15 am UTC
 *
 * @method ReceiptTranfer findWithoutFail($id, $columns = ['*'])
 * @method ReceiptTranfer find($id, $columns = ['*'])
 * @method ReceiptTranfer first($columns = ['*'])
*/
class ReceiptTranferRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReceiptTranfer::class;
    }
}
