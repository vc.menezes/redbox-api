<?php

namespace App\Repositories;

use App\Models\Agreement;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AgreementRepository
 * @package App\Repositories
 * @version July 24, 2018, 8:43 am UTC
 *
 * @method Agreement findWithoutFail($id, $columns = ['*'])
 * @method Agreement find($id, $columns = ['*'])
 * @method Agreement first($columns = ['*'])
*/
class AgreementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Agreement::class;
    }
}
