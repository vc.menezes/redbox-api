<?php

namespace App\Repositories;

use App\Models\VesselTripDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VesselTripDetailRepository
 * @package App\Repositories
 * @version September 23, 2018, 5:38 am UTC
 *
 * @method VesselTripDetail findWithoutFail($id, $columns = ['*'])
 * @method VesselTripDetail find($id, $columns = ['*'])
 * @method VesselTripDetail first($columns = ['*'])
*/
class VesselTripDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VesselTripDetail::class;
    }
}
