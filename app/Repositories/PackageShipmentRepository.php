<?php

namespace App\Repositories;

use App\Models\PackageShipment;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PackageShipmentRepository
 * @package App\Repositories
 * @version September 9, 2018, 9:49 am UTC
 *
 * @method PackageShipment findWithoutFail($id, $columns = ['*'])
 * @method PackageShipment find($id, $columns = ['*'])
 * @method PackageShipment first($columns = ['*'])
*/
class PackageShipmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PackageShipment::class;
    }
}
