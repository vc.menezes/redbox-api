<?php

namespace App\Repositories;

use App\Models\ReceiptCard;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReceiptCardRepository
 * @package App\Repositories
 * @version August 19, 2018, 5:04 am UTC
 *
 * @method ReceiptCard findWithoutFail($id, $columns = ['*'])
 * @method ReceiptCard find($id, $columns = ['*'])
 * @method ReceiptCard first($columns = ['*'])
*/
class ReceiptCardRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReceiptCard::class;
    }
}
