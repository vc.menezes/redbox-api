<?php

namespace App\Repositories;

use App\Views\ProfileView;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CustomerRepository
 * @package App\Repositories
 * @version September 15, 2018, 5:49 am UTC
 *
 * @method Customer findWithoutFail($id, $columns = ['*'])
 * @method Customer find($id, $columns = ['*'])
 * @method Customer first($columns = ['*'])
*/
class CustomerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name' => 'like',
        'identifier',
        'contact_number' => 'like'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProfileView::class;
    }
}
