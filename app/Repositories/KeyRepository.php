<?php

namespace App\Repositories;

use App\Models\Key;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class KeyRepository
 * @package App\Repositories
 * @version July 25, 2018, 4:31 am UTC
 *
 * @method Key findWithoutFail($id, $columns = ['*'])
 * @method Key find($id, $columns = ['*'])
 * @method Key first($columns = ['*'])
*/
class KeyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'key_name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Key::class;
    }
}
