<?php

namespace App\Repositories;

use App\Views\ReceiptView;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReceiptViewRepository
 * @package App\Repositories
 * @version October 2, 2018, 9:57 am UTC
 *
 * @method ReceiptView findWithoutFail($id, $columns = ['*'])
 * @method ReceiptView find($id, $columns = ['*'])
 * @method ReceiptView first($columns = ['*'])
*/
class ReceiptViewRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'receipt_number',
        'contact_number'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReceiptView::class;
    }
}
