<?php

namespace App\Repositories;

use App\Models\Invoice;
use App\Models\Package;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class InvoiceRepository
 * @package App\Repositories
 * @version July 24, 2018, 10:06 am UTC
 *
 * @method Invoice findWithoutFail($id, $columns = ['*'])
 * @method Invoice find($id, $columns = ['*'])
 * @method Invoice first($columns = ['*'])
*/
class InvoiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'serial_number' => 'like',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Invoice::class;
    }

    public function sum($invoice_ids){

        return Invoice::whereIn('id',$invoice_ids)->sum('total');
    }

    public function pay($invoice_ids){
       $invoices =  Invoice::whereIn('id',$invoice_ids)->get();
    }



}
