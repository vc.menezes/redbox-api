<?php

namespace App\Repositories;

use App\Views\ProfileView;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OrganizationViewRepository
 * @package App\Repositories
 * @version September 16, 2018, 7:11 am UTC
 *
 * @method OrganizationView findWithoutFail($id, $columns = ['*'])
 * @method OrganizationView find($id, $columns = ['*'])
 * @method OrganizationView first($columns = ['*'])
*/
class OrganizationViewRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tin',
        'identifier',
        'name' => 'like'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProfileView::class;
    }
}
