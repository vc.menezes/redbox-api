<?php

namespace App\Repositories;

use App\Models\Receipt;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReceiptRepository
 * @package App\Repositories
 * @version July 24, 2018, 6:53 pm UTC
 *
 * @method Receipt findWithoutFail($id, $columns = ['*'])
 * @method Receipt find($id, $columns = ['*'])
 * @method Receipt first($columns = ['*'])
*/
class ReceiptRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'receipt_number',
        'remarks',
        'cash',
        'cheque',
        'card',
        'transfer',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Receipt::class;
    }

    public function cancel(Receipt $receipt){
        $receipt->invoice_receipts();
    }
}
