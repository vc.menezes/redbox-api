<?php

namespace App\Repositories;

use App\Models\TrackingLog;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TrackingLogRepository
 * @package App\Repositories
 * @version July 24, 2018, 9:51 am UTC
 *
 * @method TrackingLog findWithoutFail($id, $columns = ['*'])
 * @method TrackingLog find($id, $columns = ['*'])
 * @method TrackingLog first($columns = ['*'])
*/
class TrackingLogRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TrackingLog::class;
    }

    public function updateParent(TrackingLog $trackingLog)
    {
        $tracking = \App\Models\Tracking::find($trackingLog->tracking_id);
        $tracking->status = 0;
        $tracking->save();
    }
}
