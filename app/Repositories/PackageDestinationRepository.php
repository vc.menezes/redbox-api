<?php

namespace App\Repositories;

use App\Models\PackageDestination;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PackageDestinationRepository
 * @package App\Repositories
 * @version July 22, 2018, 5:12 am UTC
 *
 * @method PackageDestination findWithoutFail($id, $columns = ['*'])
 * @method PackageDestination find($id, $columns = ['*'])
 * @method PackageDestination first($columns = ['*'])
*/
class PackageDestinationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PackageDestination::class;
    }
}
