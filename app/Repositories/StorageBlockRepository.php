<?php

namespace App\Repositories;

use App\Models\StorageBlock;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StorageBlockRepository
 * @package App\Repositories
 * @version July 22, 2018, 4:59 am UTC
 *
 * @method StorageBlock findWithoutFail($id, $columns = ['*'])
 * @method StorageBlock find($id, $columns = ['*'])
 * @method StorageBlock first($columns = ['*'])
*/
class StorageBlockRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StorageBlock::class;
    }
}
