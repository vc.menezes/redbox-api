<?php

namespace App\Repositories;

use App\Models\Agent;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AgentRepository
 * @package App\Repositories
 * @version July 18, 2018, 7:48 am UTC
 *
 * @method Agent findWithoutFail($id, $columns = ['*'])
 * @method Agent find($id, $columns = ['*'])
 * @method Agent first($columns = ['*'])
*/
class AgentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
        'name' => 'like',
        'contact_number' => 'like'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Agent::class;
    }

    public function unique()
    {
        $this->applyCriteria();

        return $this->model->groupBy('profile_id')->select(['profile_id']);
    }
}
