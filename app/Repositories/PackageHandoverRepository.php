<?php

namespace App\Repositories;

use App\Models\PackageHandover;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PackageHandoverRepository
 * @package App\Repositories
 * @version September 9, 2018, 9:52 am UTC
 *
 * @method PackageHandover findWithoutFail($id, $columns = ['*'])
 * @method PackageHandover find($id, $columns = ['*'])
 * @method PackageHandover first($columns = ['*'])
*/
class PackageHandoverRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PackageHandover::class;
    }
}
