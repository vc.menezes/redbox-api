<?php

namespace App\Repositories;

use App\Views\CommissionView;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CommissionViewRepository
 * @package App\Repositories
 * @version October 3, 2018, 9:46 am UTC
 *
 * @method CommissionView findWithoutFail($id, $columns = ['*'])
 * @method CommissionView find($id, $columns = ['*'])
 * @method CommissionView first($columns = ['*'])
*/
class CommissionViewRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CommissionView::class;
    }
}
