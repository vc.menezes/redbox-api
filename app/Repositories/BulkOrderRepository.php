<?php

namespace App\Repositories;

use App\Models\BulkOrder;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BulkOrderRepository
 * @package App\Repositories
 * @version October 10, 2018, 2:12 am UTC
 *
 * @method BulkOrder findWithoutFail($id, $columns = ['*'])
 * @method BulkOrder find($id, $columns = ['*'])
 * @method BulkOrder first($columns = ['*'])
*/
class BulkOrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'description',
        'weight'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BulkOrder::class;
    }
}
