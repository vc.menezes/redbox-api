<?php

namespace App\Repositories;

use App\Models\CommissionPayment;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CommissionPaymentRepository
 * @package App\Repositories
 * @version October 7, 2018, 6:12 pm UTC
 *
 * @method CommissionPayment findWithoutFail($id, $columns = ['*'])
 * @method CommissionPayment find($id, $columns = ['*'])
 * @method CommissionPayment first($columns = ['*'])
*/
class CommissionPaymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CommissionPayment::class;
    }
}
