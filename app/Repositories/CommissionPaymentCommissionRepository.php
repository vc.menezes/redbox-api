<?php

namespace App\Repositories;

use App\Models\CommissionPaymentCommission;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CommissionPaymentCommissionRepository
 * @package App\Repositories
 * @version October 7, 2018, 6:18 pm UTC
 *
 * @method CommissionPaymentCommission findWithoutFail($id, $columns = ['*'])
 * @method CommissionPaymentCommission find($id, $columns = ['*'])
 * @method CommissionPaymentCommission first($columns = ['*'])
*/
class CommissionPaymentCommissionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CommissionPaymentCommission::class;
    }
}
