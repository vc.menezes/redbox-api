<?php

namespace App\Repositories;

use App\Models\PackageItem;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PackageItemRepository
 * @package App\Repositories
 * @version July 22, 2018, 7:03 am UTC
 *
 * @method PackageItem findWithoutFail($id, $columns = ['*'])
 * @method PackageItem find($id, $columns = ['*'])
 * @method PackageItem first($columns = ['*'])
*/
class PackageItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PackageItem::class;
    }
}
