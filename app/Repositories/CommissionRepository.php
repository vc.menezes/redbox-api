<?php

namespace App\Repositories;

use App\Models\AgentCommission;
use App\Models\Commission;
use App\Models\Invoice;
use App\Models\Package;
use App\Models\Setting;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CommissionRepository
 * @package App\Repositories
 * @version September 15, 2018, 12:53 pm UTC
 *
 * @method Commission findWithoutFail($id, $columns = ['*'])
 * @method Commission find($id, $columns = ['*'])
 * @method Commission first($columns = ['*'])
 */
class CommissionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'total',
        'details',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Commission::class;
    }




}
