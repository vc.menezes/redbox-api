<?php

namespace App\Repositories;

use App\Models\Commission;
use App\Models\Invoice;
use App\Models\Package;
use App\Views\PackageView;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PackageRepository
 * @package App\Repositories
 * @version July 21, 2018, 6:48 am UTC
 *
 * @method Package findWithoutFail($id, $columns = ['*'])
 * @method Package find($id, $columns = ['*'])
 * @method Package first($columns = ['*'])
 */
class PackageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'barcode' => 'like',
        'reference_no' => 'like',
        'code' => 'like',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Package::class;
    }

    public function create_shipment($request)
    {
        $request['type'] = 'shipment';
        $this->model()->create($request);
    }

    public function list_commission($id)
    {
        //get shipments
        //get all ids from child package
//        $ids = Package::where('parent_id',$id)->pluck('id');
        //get all commission by those ids
        return Commission::where('package_id', $id)->get();
    }

    public function delete_package(Package $package)
    {

        $invoice = Invoice::whereDataId($package->id)->package()->whereIn('status', [0, 1, 2])->first();
        if ($invoice) {
            return 'Before Cancelling the shipment please resolved invoice from finance';
        }

        Package::whereParentId($package->id)->update(['parent_id' => null]);


        $package->package_destination()->delete();
        $package->picking_location()->delete();
        $package->items()->delete();
//        $packge->tracking()->tracking_log()->delete();
        $package->tracking()->delete();
        $package->StorageBlock()->delete();
        $package->shipment()->delete();
        $package->commission()->delete();
        $package->delete();

        return 'Shipment cancelled successfully';

    }

    public function after_pay($invoice)
    {
        if ($invoice->data_type == 'App\\Models\\Package') {
            $package = Package::find($invoice->data_id);
            $package->payment_status = 'paid';
            $package->save();
        }
    }

    public function OnPayReverse(Invoice $invoice){
        if ($invoice->data_type == 'App\\Models\\Package') {
            $package = Package::find($invoice->data_id);
            $package->payment_status = 'invoiced';
            $package->save();
        }
    }

    public function cancelOrder(Package $package)
    {
        $package->package_destination()->delete();
        $package->picking_location()->delete();
        $package->items()->delete();
        $package->tracking->tracking_log()->delete();
        $package->tracking()->delete();
        $package->bulkOrderPackage()->delete();
        $package->delete();
    }

}
