<?php

namespace App\Repositories;

use App\Models\Vessel;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VesselRepository
 * @package App\Repositories
 * @version July 25, 2018, 4:46 am UTC
 *
 * @method Vessel findWithoutFail($id, $columns = ['*'])
 * @method Vessel find($id, $columns = ['*'])
 * @method Vessel first($columns = ['*'])
*/
class VesselRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'=>'like'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Vessel::class;
    }


}
