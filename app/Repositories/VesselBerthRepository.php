<?php

namespace App\Repositories;

use App\Models\VesselBerth;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VesselBerthRepository
 * @package App\Repositories
 * @version July 25, 2018, 4:49 am UTC
 *
 * @method VesselBerth findWithoutFail($id, $columns = ['*'])
 * @method VesselBerth find($id, $columns = ['*'])
 * @method VesselBerth first($columns = ['*'])
*/
class VesselBerthRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VesselBerth::class;
    }
}
