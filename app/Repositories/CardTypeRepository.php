<?php

namespace App\Repositories;

use App\Models\CardType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CardTypeRepository
 * @package App\Repositories
 * @version August 18, 2018, 9:48 am UTC
 *
 * @method CardType findWithoutFail($id, $columns = ['*'])
 * @method CardType find($id, $columns = ['*'])
 * @method CardType first($columns = ['*'])
*/
class CardTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CardType::class;
    }
}
