<?php

namespace App\Repositories;

use App\Models\Rate;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RateRepository
 * @package App\Repositories
 * @version August 15, 2018, 8:27 am UTC
 *
 * @method Rate findWithoutFail($id, $columns = ['*'])
 * @method Rate find($id, $columns = ['*'])
 * @method Rate first($columns = ['*'])
*/
class RateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Rate::class;
    }
}
