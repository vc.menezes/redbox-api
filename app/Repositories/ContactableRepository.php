<?php

namespace App\Repositories;

use App\Models\Contactable;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ContactableRepository
 * @package App\Repositories
 * @version October 29, 2018, 6:35 am UTC
 *
 * @method Contactable findWithoutFail($id, $columns = ['*'])
 * @method Contactable find($id, $columns = ['*'])
 * @method Contactable first($columns = ['*'])
*/
class ContactableRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'value',
        'key',
        'contactable_id',
        'contactable_type',
        'type'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Contactable::class;
    }
}
