<?php

namespace App\Repositories;

use App\Views\PackageView;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OrderRepository
 * @package App\Repositories
 * @version September 11, 2018, 11:53 am UTC
 *
 * @method Order findWithoutFail($id, $columns = ['*'])
 * @method Order find($id, $columns = ['*'])
 * @method Order first($columns = ['*'])
*/
class OrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tracking_no' => 'like',
        'destinations_contact_number' => 'like',
        'address_name' => 'like',
        'destination_island' => 'like',
        'destinations_address' => 'like',
        'description' => 'like',
        'reference_no' => 'like',
        'code' => 'like',
        'barcode'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PackageView::class;
    }
}
