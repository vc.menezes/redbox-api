<?php

namespace App\Repositories;

use App\Models\CreditLimit;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CreditLimitRepository
 * @package App\Repositories
 * @version August 16, 2018, 6:57 am UTC
 *
 * @method CreditLimit findWithoutFail($id, $columns = ['*'])
 * @method CreditLimit find($id, $columns = ['*'])
 * @method CreditLimit first($columns = ['*'])
*/
class CreditLimitRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CreditLimit::class;
    }
}
