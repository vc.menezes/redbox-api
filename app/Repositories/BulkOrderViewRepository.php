<?php

namespace App\Repositories;

use App\Views\BulkOrderView;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BulkOrderViewRepository
 * @package App\Repositories
 * @version October 15, 2018, 10:16 am UTC
 *
 * @method BulkOrderView findWithoutFail($id, $columns = ['*'])
 * @method BulkOrderView find($id, $columns = ['*'])
 * @method BulkOrderView first($columns = ['*'])
*/
class BulkOrderViewRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name' => 'like',
        'description' => 'like',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BulkOrderView::class;
    }
}
