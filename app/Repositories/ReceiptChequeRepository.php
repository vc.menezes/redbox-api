<?php

namespace App\Repositories;

use App\Models\ReceiptCheque;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReceiptChequeRepository
 * @package App\Repositories
 * @version August 19, 2018, 5:07 am UTC
 *
 * @method ReceiptCheque findWithoutFail($id, $columns = ['*'])
 * @method ReceiptCheque find($id, $columns = ['*'])
 * @method ReceiptCheque first($columns = ['*'])
*/
class ReceiptChequeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReceiptCheque::class;
    }
}
