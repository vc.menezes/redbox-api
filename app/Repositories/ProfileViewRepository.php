<?php

namespace App\Repositories;

use App\Views\ProfileView;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProfileViewRepository
 * @package App\Repositories
 * @version September 12, 2018, 7:40 am UTC
 *
 * @method ProfileView findWithoutFail($id, $columns = ['*'])
 * @method ProfileView find($id, $columns = ['*'])
 * @method ProfileView first($columns = ['*'])
*/
class ProfileViewRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'=>'like',
        'identifier',
        'contact_number' => 'like'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProfileView::class;
    }
}
