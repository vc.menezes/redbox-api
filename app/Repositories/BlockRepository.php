<?php

namespace App\Repositories;

use App\Models\Block;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BlockRepository
 * @package App\Repositories
 * @version July 22, 2018, 4:54 am UTC
 *
 * @method Block findWithoutFail($id, $columns = ['*'])
 * @method Block find($id, $columns = ['*'])
 * @method Block first($columns = ['*'])
*/
class BlockRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Block::class;
    }
}
