<?php

namespace App\Repositories;

use App\Models\BulkOrderPackage;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BulkOrderPackageRepository
 * @package App\Repositories
 * @version October 10, 2018, 2:14 am UTC
 *
 * @method BulkOrderPackage findWithoutFail($id, $columns = ['*'])
 * @method BulkOrderPackage find($id, $columns = ['*'])
 * @method BulkOrderPackage first($columns = ['*'])
*/
class BulkOrderPackageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BulkOrderPackage::class;
    }
}
