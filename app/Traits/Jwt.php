<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 18/08/2018
 * Time: 11:49 AM
 */

namespace App\Traits;
use App\Models\Profile;

trait Jwt
{

    public function getProfileId() {
        $user = $this->getUser();
        if($user){
            return $user->profile_id;
        }

        return null;
        
    }


    public function getUser()
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            return null;
            // return $this->sendError('Token Expired');
        }
        return $user;
    }
}