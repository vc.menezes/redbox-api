<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 18/08/2018
 * Time: 11:49 AM
 */

namespace App\Traits;

use App\Http\Requests\API\CreateRemarkAPIRequest;
use App\Models\Profile;
use App\Models\Remark as Model;
trait Remark
{
    use Jwt;

    public function add_remarks($model ,$body)
    {
        $remarks = new Model();
        $remarks->profile_id = $this->getProfileId();
        $remarks->remarkable_type = get_class($model);
        $remarks->remarkable_id = $model->id;
        $remarks->body = $body;
        if($remarks->save()){
            $model->remark = $remarks;
        }
        return $model;
    }

    public function require_remarks($model,$request){
        $request->validate([
            'remarks' => ['required', 'string']
        ]);
       $this->add_remarks($model ,$request->remarks);
    }


}