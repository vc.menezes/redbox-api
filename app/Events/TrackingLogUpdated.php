<?php

namespace App\Events;


use App\Models\TrackingLog;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;


class TrackingLogUpdated
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public  $tracking_log ;

	/**
	 * Create a new event instance.
	 * @param $tracking_log
	 * @return void
	 */
	public function __construct(TrackingLog $tracking_log)
	{
		$this->tracking_log = $tracking_log;

	}

	/**
	 * Get the channels the event should broadcast on.
	 *
	 * @return \Illuminate\Broadcasting\Channel|array
	 */
	public function broadcastOn()
	{
		return new PrivateChannel('channel-name');
	}
}
