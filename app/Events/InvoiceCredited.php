<?php

namespace App\Events;

use App\Models\BulkOrder;
use App\Models\Invoice;
use App\Models\Package;
use App\Models\PackagePickingLocation;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class InvoiceCredited
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $model;
    public $invoice;
    public $error;

    public function __construct($invoice)
    {

        $this->invoice = $invoice;

        $ClassNameArray = explode('\\', $invoice->data_type);

        $ClassName = array_pop($ClassNameArray);

        $this->error = $this->$ClassName();


    }



    public function BulkOrder()
    {
        $this->model = BulkOrder::find($this->invoice->data_id);
        $this->model->payment_status = 'cancelled';
        return $this->model->save();
    }

    public function Package()
    {
        $this->model = Package::find($this->invoice->data_id);
        $this->model->payment_status = 'cancelled';
        return $this->model->save();
    }

    public function PackagePickingLocation()
    {
        $this->model = PackagePickingLocation::find($this->invoice->data_id);
        $this->model->payment_status = 'cancelled';
        return $this->model->save();
    }
}
