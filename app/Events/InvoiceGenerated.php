<?php

namespace App\Events;

use App\Models\BulkOrder;
use App\Models\Invoice;
use App\Models\Package;
use App\Models\PackagePickingLocation;
use App\Modules\Finance\InvoiceInterface;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class InvoiceGenerated implements InvoiceInterface
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public $model;
    public $invoice;
    public $error;

    public function __construct($invoice)
    {


        $this->invoice = $invoice;

        $ClassNameArray = explode('\\', $invoice->data_type);

        $ClassName = array_pop($ClassNameArray);

        $this->error = $this->$ClassName();


    }

    public function BulkOrder()
    {
        $this->model = BulkOrder::find($this->invoice->data_id);
        $this->model->payment_status = 'invoiced';

       if($this->model->save()){
            foreach ($this->model->packages_model as $package){
                $package->payment_status = 'invoiced';
                $package->save();
            }
       }
       return true;
    }

    public function Package()
    {
        $this->model = Package::find($this->invoice->data_id);
        $this->model->payment_status = 'invoiced';
       return $this->model->save();
    }

    public function PackagePickingLocation()
    {
        $this->model = PackagePickingLocation::find($this->invoice->data_id);
        $this->model->payment_status = 'invoiced';
        return $this->model->save();
    }


}
