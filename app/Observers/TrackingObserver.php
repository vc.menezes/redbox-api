<?php

namespace App\Observers;

use App\Jobs\SendSms;
use App\Models\Tracking;
use App\Notifications\TrackingUpdated;

class TrackingObserver
{
    /**
     * Handle the tracking "created" event.
     *
     * @param  \App\Tracking  $tracking
     * @return void
     */
    public function created(Tracking $tracking)
    {
        //
    }

    /**
     * Handle the tracking "updated" event.
     *
     * @param  \App\Tracking  $tracking
     * @return void
     */
    public function updated(Tracking $tracking)
    {
       if($tracking->status==4) {
           \Notification::send( new TrackingUpdated($tracking));
           \Log::critical('messageasdfasdfasd');
       }
    }

    /**
     * Handle the tracking "deleted" event.
     *
     * @param  \App\Tracking  $tracking
     * @return void
     */
    public function deleted(Tracking $tracking)
    {
        //
    }

    /**
     * Handle the tracking "restored" event.
     *
     * @param  \App\Tracking  $tracking
     * @return void
     */
    public function restored(Tracking $tracking)
    {
        //
    }

    /**
     * Handle the tracking "force deleted" event.
     *
     * @param  \App\Tracking  $tracking
     * @return void
     */
    public function forceDeleted(Tracking $tracking)
    {
        //
    }
}
