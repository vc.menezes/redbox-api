<?php

namespace App\Jobs;

use App\Models\Invoice;
use App\Models\Package;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GiveItemCode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $invoice;

    public function __construct($id)
    {
        $this->invoice = Invoice::find($id);;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->invoice->charge_type == 'shipping') {
            $model = Package::find($this->invoice->data_id);
            foreach ($this->invoice->items as $item) {

                $item->item_code = $model->reference_no ? $model->reference_no : $model->tracking->tracking_no;

                $item->save();
            }
        }
    }
}
