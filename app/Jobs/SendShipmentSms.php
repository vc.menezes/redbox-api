<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Notifications\TrackingUpdated;
use Notification;

class SendShipmentSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $contact_number;
    public $package;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct($contact_number, $package)
    {
        $this->contact_number = $contact_number;
        $this->package = $package;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Notification::route('nexmo', $this->contact_number)->notify(new TrackingUpdated($this->package));
        } catch(\Exception $e) {
            // dd($e);
        }
    }
}
