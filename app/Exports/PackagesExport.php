<?php

namespace App\Exports;

use App\Models\Package;
use App\Models\Status;
use App\Views\PackageView;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Zend\Diactoros\Request;


class PackagesExport implements FromCollection, WithHeadings
{

    public $input;
    public $from_date;
    public $to_date;
    public $status;
    public $transport_medium;

    public function __construct($input)
    {
        $status_ids = Status::pluck('id')->toArray();
        $this->input = $input;
        $this->status = $input->status  ? explode(',', $input->status) : $status_ids;
        $this->transport_medium = $input->transport_medium ? explode(',', $input->transport_medium) : ['sea', 'air', 'land'];


        $this->set_default_dates($input);

    }

    public $filed_name = [
        "barcode",
        "reference_no",
        "description",
        "code",
        "weight",
        "payment_status",
        "destinations_contact_number",
        "atoll_code",
        "destination_island",
        "destinations_address",
        "address_name",
        "transport_medium",
        "shipped_vessel_name",
        "shipped_vessel_contact_number",
        "tracking_no",
        "tracking_status",
        "estimated_delivery_date",
        "created_at",
    ];

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        if(is_null($this->input->status)){
           $this->status = Status::pluck('id')->toArray();
        }

        if($this->input->status==0){
            $this->status = [0];
        }

        if ( $this->input->location_id && $this->input->location_id!='undefined' && !is_null($this->input->location_id)) {
            return PackageView::whereBetween('created_at', [$this->from_date, $this->to_date])
                ->whereIn('transport_medium', $this->transport_medium)
                ->whereIn('tracking_status', $this->status)
                ->where('destinations_location_id', $this->input->location_id)
                ->get($this->filed_name);
        }

        return PackageView::whereBetween('created_at', [$this->from_date, $this->to_date])
            ->whereIn('transport_medium', $this->transport_medium)
            ->whereIn('tracking_status', $this->status)
            ->get($this->filed_name);

    }

    public function headings(): array
    {
        return array_map(function ($str) {
            return ucwords(str_replace("_", " ", $str));
        }, $this->filed_name);

    }

    public function set_default_dates($input)
    {

        $start = Carbon::now()->startOfMonth();


        $end = Carbon::now()->addMonths(1)->setTime(23, 59, 59);

        if (!$input->start) {
            $this->from_date = $start;
        } else {
            $this->from_date = Carbon::parse($input->start);
        }

        if (!$input->end) {
            $this->to_date = $end;
        } else {
            $this->to_date = Carbon::parse($input->end);
        }

    }


}
