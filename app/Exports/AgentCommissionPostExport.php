<?php

namespace App\Exports;

use App\Models\Invoice;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AgentCommissionPostExport implements FromCollection, WithHeadings
{
	public $input;


	public function __construct()
	{
		$this->input = null;

	}

	public $filed_name = [
		"id",
		"name",
		"email",
		"account_number",
		"contact_number",
		"identifier",
		"date",
		"weight",
		"total",
	];

	public function headings(): array
	{
		return array_map(function ($str) {
			return ucwords(str_replace("_", " ", $str));
		}, $this->filed_name);

	}

	/**
	 * @return \Illuminate\Support\Collection
	 */
	public function collection()
	{


		return \DB::table('agent_payment_posts')
			->join('profiles', 'agent_payment_posts.profile_id', '=', 'profiles.id')
			->select(
				'agent_payment_posts.id',
				'profiles.name',
				'agent_payment_posts.email',
				'agent_payment_posts.account_number',
				'agent_payment_posts.contact_number',
				'agent_payment_posts.identifier',
				'agent_payment_posts.date',
				'agent_payment_posts.weight',
				'agent_payment_posts.total',
			)
			->whereNull('agent_payment_posts.deleted_at')
			->where('agent_payment_posts.status', 1)
			->get();

	}


}
