<?php

namespace App\Exports;

use App\Models\Invoice;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SalesExport implements FromCollection, WithHeadings
{
    public $input;
    public $from_date;
    public $to_date;
    public $status;

    public function __construct($input)
    {
        $this->input = $input;
        $this->status = explode(',', $input->status);;



        $this->set_default_dates($input);

    }

    public function headings(): array
    {
        return [
            'Date', 'Total', 'Tax'
        ];

    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {

        return Invoice::select(\DB::raw('DATE(created_at) as date'), \DB::raw('sum(total) as total'), \DB::raw('sum(tax) as tax'))
            ->groupBy('date')
            ->whereIn('status',$this->status)
            ->whereBetween('created_at', [$this->from_date, $this->to_date])
            ->get();
    }

    public function set_default_dates($input)
    {
        $start = Carbon::now()->startOfMonth();


        $end = Carbon::now()->setTime(23, 59, 59);

        if (!$input->from_date) {
            $this->from_date = $start;
        } else {
            $this->from_date = Carbon::parse($input->from_date);
        }

        if (!$input->to_date) {
            $this->to_date = $end;
        } else {
            $this->to_date = Carbon::parse($input->to_date);
        }

    }
}
