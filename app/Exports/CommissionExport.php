<?php

namespace App\Exports;

use App\Views\CommissionReportView;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CommissionExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public $profile_id;
    public $status_array;
    public $status;

    public function __construct($profile_id = null, $status = '1')
    {
        $this->profile_id = $profile_id;
        $this->status = $status;
        $this->status_array = explode(',', $this->status);

    }

    public $filed_name = [
        "id",
        "total",
        "details",
        "identifier",
        "name",
        "contact_number",
        "email",
        "barcode",
        "code",
        "description",
        "weight",
        "weight",
        "account_no",
        "created_at",
    ];


    public function collection()
    {
        if (is_null($this->profile_id)) {
           return CommissionReportView::whereIn('status', $this->status_array)
                ->get($this->filed_name);
        } else {
           return CommissionReportView::whereIn('status', $this->status_array)
                ->where('profile_id', $this->profile_id)
                ->get($this->filed_name);
        }

    }

    public function headings(): array
    {
        return array_map(function ($str) {
            return ucwords(str_replace("_", " ", $str));
        }, $this->filed_name);

    }
}
