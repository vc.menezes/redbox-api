<?php

namespace App\Exports;

use App\Criteria\Commission\PendingCriteria;
use App\Criteria\PendingCommisionViewCriteriaCriteria;
use App\Repositories\CommissionReportViewRepository;
use App\Views\CommissionReportView;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AgentCommissionExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public $request;
    public $reportViewRepository;


    public function __construct(Request $request,CommissionReportViewRepository $reportViewRepository)
    {
        $this->$request = $request;
        $this->reportViewRepository = $reportViewRepository;

    }

    public $filed_name = [
			  "name",
        "total",
        "profile_id",
        "account_no",
        "email",
        "details",
        "identifier",
        "contact_number",
        "weight",
        "type",
        "created_at"
    ];


    public function collection()
    {
			$this->reportViewRepository->pushCriteria(new PendingCommisionViewCriteriaCriteria());
			return $this->reportViewRepository->get();

    }

    public function headings(): array
    {
        return array_map(function ($str) {
            return ucwords(str_replace("_", " ", $str));
        }, $this->filed_name);

    }
}
