<?php

namespace App\Resolvers;

use Illuminate\Support\Facades\Request;
use \OwenIt\Auditing\Contracts\UserResolver as Resolver;
use JWTAuth;
use App\User;

class UserResolver implements Resolver
{
    public static function resolve() {

        try {
            if(JWTAuth::getToken()) {
                $user = User::find(JWTAuth::parseToken()->toUser()->id);
                if($user) {
                    return $user;
                }
            }

            return null;

        } catch (JWTException $e) {
            return null;
        } catch (Exception $e) {
            return null;
        }
    }
}