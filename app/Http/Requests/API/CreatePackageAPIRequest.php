<?php

namespace App\Http\Requests\API;

use App\Models\Package;
use App\Rules\ShippingWeightRule;
use InfyOm\Generator\Request\APIRequest;

class CreatePackageAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->type=='order') {
            $this->validate([
                'weight' => ['required', new ShippingWeightRule($this->destination['medium'])],
            ]);
        }

        return Package::$rules;
    }
}
