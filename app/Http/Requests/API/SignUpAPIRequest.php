<?php

namespace App\Http\Requests\API;

use App\Rules\UserProfile\MobileNumber;
use App\Rules\UserProfile\UserType;
use App\User;
use InfyOm\Generator\Request\APIRequest;

class SignUpAPIRequest extends APIRequest
{
//    this is default validation rules for persona and organizaion
    public $default_rules = [
        'name'=> 'required:string',
        // 'address_id'=> 'required:integer',
        'email'=> 'required|unique:users|max:255',
        'contact_number'=> 'required|unique:profiles|max:10',
        'type'=> 'required',
        'password'=> 'required|confirmed|min:6',
        // 'location_id'=> 'required',
        'user_type'=> 'required',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $this->validate([
            'contact_number' => ['required', 'string', new MobileNumber()],
            'type' => ['required', 'string', new UserType()]
        ]);

        $type = $this->type;
        return $this->$type();
    }



    private function person(){
        $person = [
            'id_card_no'=>'required:string|max:20|min:6',
        ];
        return array_merge($person,$this->default_rules);
    }
    private function organization(){
        $organization = [
            // 'tin'=>'required:string',
            'registry_number'=>'required:string|max:20|min:4',
        ];
        return array_merge($organization,$this->default_rules);
    }
}
