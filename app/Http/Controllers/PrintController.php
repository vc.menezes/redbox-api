<?php

namespace App\Http\Controllers;

use App\Exports\SalesExport;
use App\Models\StorageBlock;
use Illuminate\Http\Request;
use App\Views\PackageView;
use App\Views\BulkOrderView;
use App\Models\Invoice;
use App\Traits\Jwt;

class PrintController extends Controller
{

    use \App\Traits\Jwt;
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    //@param $package id
    public function label($id)
    {
        $str = '';
        $data  = PackageView::with(['picking_location', 'profile', 'items.commodity'])->findOrFail($id);
        $data->pickup = \App\Models\TrackingLog::with('profile')->where(['tracking_id' => $data->tracking_id, 'status_id' => 2])->first();
        $data->address = \App\Models\Addressable::where('profile_id', $data->profile_id)->first();
     


        // return $data;
        if($data->type == 'order') {
            
            foreach($data->items as $item) {
                $str = $str.$item->commodity->name.', ';
            }
            $data->contents = $str;
            $pdf = \PDF::loadView('package-order-label', compact('data'));
            return $pdf->stream($data->code.'-order-lable.pdf');
            
        }



        // return $data;

        $contacts = \App\Models\Contactable::where(['contactable_id' => $data->profile_id, 'key' => 'mobile'])->pluck('value');

        $contacts = implode(", ", $contacts->toArray());
        $pdf = \PDF::loadView('package-shipment-label', compact('data', 'contacts'));
        return $pdf->stream($data->code.'-shipment-lable.pdf');
    }


    //@param $package id
    public function invoice($id)
    {
        $data = Invoice::with(['items', 'profile_view'])->where(['data_id' => $id, 'data_type' => 'App\\Models\\Package'])->orderBy('id', 'desc')->first();

        $pdf = \PDF::loadView('bulk-invoice', compact('data'));
        return $pdf->stream($id.'-invoice.pdf');
    }

    //@param $bulk order_id id
    public function bulk($id)
    {
        $data = Invoice::with(['items', 'profile_view'])->where(['data_id' => $id, 'data_type' => 'App\\Models\\BulkOrder'])->first();

        $pdf = \PDF::loadView('bulk-invoice', compact('data'));
        return $pdf->stream($id.'-invoice.pdf');
    }

    //@param $package id
    public function commission($id)
    {
        $data = \App\Models\AgentCommission::with(['details.package_view', 'profile'])->where('shipment_id', $id)->first();
        $pdf = \PDF::loadView('package-commission', compact('data'));
        return $pdf->stream($data->id.'-commission.pdf');
    }

    //@param $id
    public function receipt($id)
    {
        $data = \App\Models\Receipt::with(['invoices','profile', 'issuedBy'])->findOrFail($id);
        
        $pdf = \PDF::loadView('receipt', compact('data'));
        return $pdf->stream($data->receipt_number.'-receipt.pdf');
    }

    //@param package $id
    public function proofOfDelivery($id)
    {
        $data = \App\Views\PackageView::with(['profile.address', 'destination', 'parent', 'handOver'])->findOrFail($id);

        $data->storage = StorageBlock::wherePackageId($id)->with(['block'])->orderBy('id','desc')->first();

        $data->invoice = \App\Models\Invoice::where(['data_id' => $data->id, 'data_type' => 'App\\Models\\Package'])->first();
        $data->delivered = \App\Models\TrackingLog::where(['tracking_id' => $data->tracking_id, 'status_id' => 6])->first();
        $data->sign = null;
        if($data->signature) {
            $data->sign = \App\Models\Attachable::where(['attachable_id' => $data->signature->id, 'attachable_type' => 'App\\Models\\Signature'])->first();
        }
//        return $data;

        // return view('pod', compact('data'));

        $pdf = \PDF::loadView('pod', compact('data'));
        return $pdf->stream($id.'-pod.pdf');
    }


    //@param package $id
    public function wayBill($id)
    {
        $str = '';
        $data = \App\Views\PackageView::with(['profile.address', 'items.commodity', 'destination'])->findOrFail($id);
        

        if($data->type == 'order') {
            foreach($data->items as $item) {
                $str = $str.$item->commodity->name.', ';
            }
            $data->contents = $str;
        }

        $data->invoice = \App\Models\Invoice::where(['data_id' => $data->id, 'data_type' => 'App\\Models\\Package'])->first();
        $data->pickup = \App\Models\TrackingLog::with('profile')->where(['tracking_id' => $data->tracking_id, 'status_id' => 2])->first();


        $pdf = \PDF::loadView('waybill', compact('data'))->setPaper('a4', 'landscape');
        return $pdf->stream($id.'-waybill.pdf');
    }

    public function daily_sales_download(Request $request){
        \Excel::download(new SalesExport($request), 'sales.xlsx');
//        return (new SalesExport($request))->download('invoices.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }

    //bulk orders handerover sheet
    public function handOver($id) {
        // $user = $this->getUser();
        $data = BulkOrderView::with(['packages.profile', 'packages.items.commodity'])->find($id);
        // return view('handover', compact('data'));


        $pdf = \PDF::loadView('handover', compact('data'))->setPaper('a4', 'landscape');;
        return $pdf->stream($id.'-orders-handover.pdf');
    }
}
