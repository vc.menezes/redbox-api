<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBulkOrderAPIRequest;
use App\Http\Requests\API\UpdateBulkOrderAPIRequest;
use App\Models\BulkOrder;
use App\Modules\Finance\BulkOrderCharge;
use App\Modules\Finance\Invoice;
use App\Repositories\BulkOrderPackageRepository;
use App\Repositories\BulkOrderRepository;
use App\Repositories\PackageRepository;
use App\Repositories\TrackingRepository;
use App\Traits\Jwt;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Jobs\GiveSequence;

/**
 * Class BulkOrderController
 * @package App\Http\Controllers\API
 */
class BulkOrderAPIController extends AppBaseController
{
    /** @var  BulkOrderRepository */
    private $bulkOrderRepository;
    private $packageRepository;
    private $bulkOrderPackageRepository;
    private $trackingRepository;
    use Jwt;


    public function __construct(
        BulkOrderRepository $bulkOrderRepo,
        PackageRepository $packageRepo ,
        BulkOrderPackageRepository $bulkOrderPackageRepository,
        TrackingRepository $trackingRepo
)
    {
        $this->bulkOrderRepository = $bulkOrderRepo;
        $this->packageRepository = $packageRepo;
        $this->bulkOrderPackageRepository = $bulkOrderPackageRepository;
        $this->trackingRepository = $trackingRepo;
    }

    /**
     * Display a listing of the BulkOrder.
     * GET|HEAD /bulkOrders
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->bulkOrderRepository->pushCriteria(new RequestCriteria($request));
        $this->bulkOrderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $bulkOrders = $this->bulkOrderRepository->all();

        return $this->sendResponse($bulkOrders->toArray(), 'Bulk Orders retrieved successfully');
    }

    /**
     * Store a newly created BulkOrder in storage.
     * POST /bulkOrders
     *
     * @param CreateBulkOrderAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBulkOrderAPIRequest $request)
    {

        $orders = collect($request->orders);



        $user = $this->getUser();
        $profile_id = $request->profile_id ? $request->profile_id : $user->profile_id;
        $input = $request->except('orders');
        $input['profile_id'] = $profile_id;
        $input['weight'] = $orders->sum('weight');


        $bulkOrders = $this->bulkOrderRepository->create($input);


        foreach ($request->orders as $order) {
            $package = [
                'barcode' => '-',
                'code' => '-',
                'type' => 'order',
                'flag' => 'bulk',
                'profile_id' => $profile_id,
                'weight' => $order['weight'],
                'reference_no' => $order['reference_no'],
                'description' => $order['description'],
            ];

            $packages = $this->packageRepository->create($package);

            GiveSequence::dispatch($packages, 'barcode', 'order_barcode');

            GiveSequence::dispatch($packages, 'code', 'order_serial');

            $this->trackingRepository->createNewTracking($packages, null, 8, 455);

            $this->bulkOrderPackageRepository->create(['bulk_order_id'=>$bulkOrders->id,'package_id'=>$packages->id]);

            if ($order['block_id']) {
                \App\Models\StorageBlock::create(['block_id' => $order['block_id'], 'package_id' => $packages->id]);
            }

            if ($order['package_items'])
                $packages->items()->createMany([$order['package_items']]);

            if ($order['package_destinations'])
                $packages->package_destination()->create($order['package_destinations']);
        }

        return $this->sendResponse($bulkOrders->toArray(), 'Bulk Order saved successfully');
    }

    /**
     * Display the specified BulkOrder.
     * GET|HEAD /bulkOrders/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BulkOrder $bulkOrder */
        $bulkOrder = $this->bulkOrderRepository->with(['packages.status'])->findWithoutFail($id);

        if (empty($bulkOrder)) {
            return $this->sendError('Bulk Order not found');
        }

        return $this->sendResponse($bulkOrder->toArray(), 'Bulk Order retrieved successfully');
    }

    /**
     * Update the specified BulkOrder in storage.
     * PUT/PATCH /bulkOrders/{id}
     *
     * @param  int $id
     * @param UpdateBulkOrderAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBulkOrderAPIRequest $request)
    {
        $input = $request->all();

        /** @var BulkOrder $bulkOrder */
        $bulkOrder = $this->bulkOrderRepository->findWithoutFail($id);

        if (empty($bulkOrder)) {
            return $this->sendError('Bulk Order not found');
        }

        $bulkOrder = $this->bulkOrderRepository->update($input, $id);

        return $this->sendResponse($bulkOrder->toArray(), 'BulkOrder updated successfully');
    }

    /**
     * Remove the specified BulkOrder from storage.
     * DELETE /bulkOrders/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BulkOrder $bulkOrder */
        $bulkOrder = $this->bulkOrderRepository->findWithoutFail($id);

        if (empty($bulkOrder)) {
            return $this->sendError('Bulk Order not found');
        }

        $bulkOrder->delete();

        return $this->sendResponse($id, 'Bulk Order deleted successfully');
    }

    public function raise_invoice($id){
        $bulk_order = $this->bulkOrderRepository->find($id);
        $bulk_order_charge = new BulkOrderCharge($bulk_order,true);
        $invoice = new Invoice($bulk_order_charge);
        $response = $invoice->create();
        return $this->sendResponse($response, 'bulk order response');
    }


}
