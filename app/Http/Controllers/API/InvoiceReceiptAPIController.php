<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateInvoiceReceiptAPIRequest;
use App\Http\Requests\API\UpdateInvoiceReceiptAPIRequest;
use App\Models\InvoiceReceipt;
use App\Repositories\InvoiceReceiptRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class InvoiceReceiptController
 * @package App\Http\Controllers\API
 */

class InvoiceReceiptAPIController extends AppBaseController
{
    /** @var  InvoiceReceiptRepository */
    private $invoiceReceiptRepository;

    public function __construct(InvoiceReceiptRepository $invoiceReceiptRepo)
    {
        $this->invoiceReceiptRepository = $invoiceReceiptRepo;
    }

    /**
     * Display a listing of the InvoiceReceipt.
     * GET|HEAD /invoiceReceipts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->invoiceReceiptRepository->pushCriteria(new RequestCriteria($request));
        $this->invoiceReceiptRepository->pushCriteria(new LimitOffsetCriteria($request));
        $invoiceReceipts = $this->invoiceReceiptRepository->all();

        return $this->sendResponse($invoiceReceipts->toArray(), 'Invoice Receipts retrieved successfully');
    }

    /**
     * Store a newly created InvoiceReceipt in storage.
     * POST /invoiceReceipts
     *
     * @param CreateInvoiceReceiptAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInvoiceReceiptAPIRequest $request)
    {
        $input = $request->all();

        $invoiceReceipts = $this->invoiceReceiptRepository->create($input);

        return $this->sendResponse($invoiceReceipts->toArray(), 'Invoice Receipt saved successfully');
    }

    /**
     * Display the specified InvoiceReceipt.
     * GET|HEAD /invoiceReceipts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var InvoiceReceipt $invoiceReceipt */
        $invoiceReceipt = $this->invoiceReceiptRepository->findWithoutFail($id);

        if (empty($invoiceReceipt)) {
            return $this->sendError('Invoice Receipt not found');
        }

        return $this->sendResponse($invoiceReceipt->toArray(), 'Invoice Receipt retrieved successfully');
    }

    /**
     * Update the specified InvoiceReceipt in storage.
     * PUT/PATCH /invoiceReceipts/{id}
     *
     * @param  int $id
     * @param UpdateInvoiceReceiptAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInvoiceReceiptAPIRequest $request)
    {
        $input = $request->all();

        /** @var InvoiceReceipt $invoiceReceipt */
        $invoiceReceipt = $this->invoiceReceiptRepository->findWithoutFail($id);

        if (empty($invoiceReceipt)) {
            return $this->sendError('Invoice Receipt not found');
        }

        $invoiceReceipt = $this->invoiceReceiptRepository->update($input, $id);

        return $this->sendResponse($invoiceReceipt->toArray(), 'InvoiceReceipt updated successfully');
    }

    /**
     * Remove the specified InvoiceReceipt from storage.
     * DELETE /invoiceReceipts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var InvoiceReceipt $invoiceReceipt */
        $invoiceReceipt = $this->invoiceReceiptRepository->findWithoutFail($id);

        if (empty($invoiceReceipt)) {
            return $this->sendError('Invoice Receipt not found');
        }

        $invoiceReceipt->delete();

        return $this->sendResponse($id, 'Invoice Receipt deleted successfully');
    }
}
