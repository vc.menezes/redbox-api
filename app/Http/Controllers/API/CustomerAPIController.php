<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerAPIRequest;
use App\Http\Requests\API\UpdateCustomerAPIRequest;
use App\Models\Customer;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\FilterColumnsCriteria;
use Response;

/**
 * Class CustomerController
 * @package App\Http\Controllers\API
 */

class CustomerAPIController extends AppBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepository = $customerRepo;
    }

    /**
     * Display a listing of the Customer.
     * GET|HEAD /customers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->customerRepository->pushCriteria(new FilterColumnsCriteria(['user_type' => 'customer']));
        $this->customerRepository->pushCriteria(new RequestCriteria($request));
        $this->customerRepository->pushCriteria(new LimitOffsetCriteria($request));
        $customers = $this->customerRepository->paginate($request->pageSize);

        return $this->sendResponse($customers->toArray(), 'Customers retrieved successfully');
    }


    public function show($id)
    {
        /** @var Addressable $addressable */
        $customer = $this->customerRepository->with(['location', 'person'])->findWithoutFail($id);

        if (empty($customer)) {
            return $this->sendError('Addressable not found');
        }

        return $this->sendResponse($customer->toArray(), 'Customer retrieved successfully');
    }
    
}
