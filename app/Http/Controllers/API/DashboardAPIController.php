<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAddressableAPIRequest;
use App\Http\Requests\API\UpdateAddressableAPIRequest;
use App\Models\Agent;
use App\Models\Commission;
use App\Models\Invoice;
use App\Models\Status;
use App\Models\VesselRoute;
use App\Repositories\AddressableRepository;
use App\Traits\Jwt;
use App\User;
use App\Views\PackageView;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AddressableController
 * @package App\Http\Controllers\API
 */

class DashboardAPIController extends AppBaseController
{
    use Jwt;
    public function packages(){
        $data = PackageView::select('last_tracking_status_id as tracking_status', \DB::raw('count(*) as count'))
            ->groupBy('last_tracking_status_id')
            ->get();

        foreach ($data as $item) {
            if ($item->tracking_status == 0) {
                $blackStatus = new \stdClass();
                $blackStatus->id = 0;
                $blackStatus->name = "Completed";

                $item->status_name = $blackStatus;
            } else {
                $item->status_name = Status::find($item->tracking_status);
            }

        }
        return $this->sendResponse($data, 'Packages Statistics');
    }

    public function sales_over_view(){
       $total_paid = Invoice::whereStatus(0)->sum('total') + Invoice::whereStatus(0)->sum('tax');
       $total_pending = Invoice::whereIn('status',[1,2])->sum('total') + Invoice::whereIn('status',[1,2])->sum('tax');

       $data = [
           ['type'=>'Paid','details'=>'Total Payment Received','amount'=>round($total_paid,2)],
           ['type'=>'Pending','details'=>'Not Paid Yet','amount'=>round($total_pending,2)]
       ];

        return $this->sendResponse($data, 'sales_over_view Statistics');
    }

    public function users_count(){
        $data = User::select('user_type', \DB::raw('count(*) as count'))
            ->groupBy('user_type')
            ->get();
        foreach ($data as $item){
            $item->user_type = ucwords(str_replace('_', ' ', $item->user_type));
        }
        return $this->sendResponse($data, 'users_count Statistics');
    }

    public function packages_agent(){

        $profile_id = $this->getProfileId();
        $data = PackageView::select('last_tracking_status_id as tracking_status', \DB::raw('count(*) as count'))
            ->groupBy('last_tracking_status_id')
            ->where('profile_id',$profile_id)
            ->get();

        foreach ($data as $item) {
            if ($item->tracking_status == 0) {
                $blackStatus = new \stdClass();
                $blackStatus->id = 0;
                $blackStatus->name = "Completed";

                $item->status_name = $blackStatus;
            } else {
                $item->status_name = Status::find($item->tracking_status);
            }

        }
        return $this->sendResponse($data, 'Packages Statistics');
    }

    public function agent_commission(){
        $profile_id = $this->getProfileId();
        $not_resovled =Commission::whereProfile($profile_id)->status(2)->sum('total');
        $resovled = Commission::whereProfile($profile_id)->status(1)->sum('total');

        $data = [
            ['type'=>'not resolved','amount'=>round($not_resovled,2)],
            ['type'=>'resolved','amount'=>round($resovled,2)]
        ];

        return $this->sendResponse($data, 'sales_over_view Statistics');
    }

    public function agent_location(){
        $profile_id = $this->getProfileId();

        $data = Agent::whereProfileId($profile_id)->with('location')->get();

         return $this->sendResponse($data, 'agent_location');
    }

    public function agent_routes(){
        $profile_id = $this->getProfileId();

        $location_id = Agent::whereProfileId($profile_id)->pluck('location_id')->toArray();


        $data = VesselRoute::whereIn('location_id',$location_id)->with(['vessel','location'])->get();

        return $this->sendResponse($data, 'agent_location');
    }
}
