<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePackageItemAPIRequest;
use App\Http\Requests\API\UpdatePackageItemAPIRequest;
use App\Models\PackageItem;
use App\Repositories\CommodityRepository;
use App\Repositories\PackageItemRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PackageItemController
 * @package App\Http\Controllers\API
 */

class PackageItemAPIController extends AppBaseController
{
    /** @var  PackageItemRepository */
    private $packageItemRepository;
    private $comdoityRepository;

    public function __construct(PackageItemRepository $packageItemRepo,CommodityRepository $commodityRepository)
    {
        $this->packageItemRepository = $packageItemRepo;
        $this->comdoityRepository = $commodityRepository;
    }

    /**
     * Display a listing of the PackageItem.
     * GET|HEAD /packageItems
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->packageItemRepository->pushCriteria(new RequestCriteria($request));
        $this->packageItemRepository->pushCriteria(new LimitOffsetCriteria($request));
        $packageItems = $this->packageItemRepository->all();

        return $this->sendResponse($packageItems->toArray(), 'Package Items retrieved successfully');
    }

    /**
     * Store a newly created PackageItem in storage.
     * POST /packageItems
     *
     * @param CreatePackageItemAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePackageItemAPIRequest $request)
    {
        $input = $request->except('commodity');

        if(!is_array($request->commodity)){
            $commodity = [
                'name'=> $request->commodity
            ];

            $new_commodity =$this->comdoityRepository->create($commodity);
            $input['commodity_id'] = $new_commodity->id;
        }



        $packageItems = $this->packageItemRepository->create($input);

        return $this->sendResponse($packageItems->toArray(), 'Package Item saved successfully');
    }

    /**
     * Display the specified PackageItem.
     * GET|HEAD /packageItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PackageItem $packageItem */
        $packageItem = $this->packageItemRepository->findWithoutFail($id);

        if (empty($packageItem)) {
            return $this->sendError('Package Item not found');
        }

        return $this->sendResponse($packageItem->toArray(), 'Package Item retrieved successfully');
    }

    /**
     * Update the specified PackageItem in storage.
     * PUT/PATCH /packageItems/{id}
     *
     * @param  int $id
     * @param UpdatePackageItemAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePackageItemAPIRequest $request)
    {
        $input = $request->all();

        /** @var PackageItem $packageItem */
        $packageItem = $this->packageItemRepository->findWithoutFail($id);

        if (empty($packageItem)) {
            return $this->sendError('Package Item not found');
        }

        $packageItem = $this->packageItemRepository->update($input, $id);

        return $this->sendResponse($packageItem->toArray(), 'PackageItem updated successfully');
    }

    /**
     * Remove the specified PackageItem from storage.
     * DELETE /packageItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PackageItem $packageItem */
        $packageItem = $this->packageItemRepository->findWithoutFail($id);

        if (empty($packageItem)) {
            return $this->sendError('Package Item not found');
        }

        $packageItem->delete();

        return $this->sendResponse($id, 'Package Item deleted successfully');
    }
}
