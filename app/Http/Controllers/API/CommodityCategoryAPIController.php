<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCommodityCategoryAPIRequest;
use App\Http\Requests\API\UpdateCommodityCategoryAPIRequest;
use App\Models\CommodityCategory;
use App\Repositories\CommodityCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CommodityCategoryController
 * @package App\Http\Controllers\API
 */

class CommodityCategoryAPIController extends AppBaseController
{
    /** @var  CommodityCategoryRepository */
    private $commodityCategoryRepository;

    public function __construct(CommodityCategoryRepository $commodityCategoryRepo)
    {
        $this->commodityCategoryRepository = $commodityCategoryRepo;
    }

    /**
     * Display a listing of the CommodityCategory.
     * GET|HEAD /commodityCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->commodityCategoryRepository->pushCriteria(new RequestCriteria($request));
        $this->commodityCategoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $commodityCategories = $this->commodityCategoryRepository->paginate($request->pageSize);

        return $this->sendResponse($commodityCategories->toArray(), 'Commodity Categories retrieved successfully');
    }

    /**
     * Store a newly created CommodityCategory in storage.
     * POST /commodityCategories
     *
     * @param CreateCommodityCategoryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCommodityCategoryAPIRequest $request)
    {
        $input = $request->all();

        $commodityCategories = $this->commodityCategoryRepository->create($input);

        return $this->sendResponse($commodityCategories->toArray(), 'Commodity Category saved successfully');
    }

    /**
     * Display the specified CommodityCategory.
     * GET|HEAD /commodityCategories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CommodityCategory $commodityCategory */
        $commodityCategory = $this->commodityCategoryRepository->findWithoutFail($id);

        if (empty($commodityCategory)) {
            return $this->sendError('Commodity Category not found');
        }

        return $this->sendResponse($commodityCategory->toArray(), 'Commodity Category retrieved successfully');
    }

    /**
     * Update the specified CommodityCategory in storage.
     * PUT/PATCH /commodityCategories/{id}
     *
     * @param  int $id
     * @param UpdateCommodityCategoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCommodityCategoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var CommodityCategory $commodityCategory */
        $commodityCategory = $this->commodityCategoryRepository->findWithoutFail($id);

        if (empty($commodityCategory)) {
            return $this->sendError('Commodity Category not found');
        }

        $commodityCategory = $this->commodityCategoryRepository->update($input, $id);

        return $this->sendResponse($commodityCategory->toArray(), 'CommodityCategory updated successfully');
    }

    /**
     * Remove the specified CommodityCategory from storage.
     * DELETE /commodityCategories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CommodityCategory $commodityCategory */
        $commodityCategory = $this->commodityCategoryRepository->findWithoutFail($id);

        if (empty($commodityCategory)) {
            return $this->sendError('Commodity Category not found');
        }

        $commodityCategory->delete();

        return $this->sendResponse($id, 'Commodity Category deleted successfully');
    }
}
