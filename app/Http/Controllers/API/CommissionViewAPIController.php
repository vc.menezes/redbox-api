<?php

namespace App\Http\Controllers\API;

use App\Criteria\BetweenTwoDateCriteria;
use App\Criteria\FilterColumnsCriteria;
use App\Http\Requests\API\CreateCommissionViewAPIRequest;
use App\Http\Requests\API\UpdateCommissionViewAPIRequest;
use App\Views\CommissionView;
use App\Repositories\CommissionViewRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CommissionViewController
 * @package App\Http\Controllers\API
 */

class CommissionViewAPIController extends AppBaseController
{
    /** @var  CommissionViewRepository */
    private $commissionViewRepository;

    public function __construct(CommissionViewRepository $commissionViewRepo)
    {
        $this->commissionViewRepository = $commissionViewRepo;
    }

    /**
     * Display a listing of the CommissionView.
     * GET|HEAD /commissionViews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {


        if ($request->from_date && $request->to_date) {
            $this->commissionViewRepository->pushCriteria(new BetweenTwoDateCriteria($request->from_date, $request->to_date));
        }

        if($request->profile_id){
            $this->commissionViewRepository->pushCriteria(new FilterColumnsCriteria(['profile_id'=>$request->profile_id]));
        }

        $this->commissionViewRepository->pushCriteria(new RequestCriteria($request));
        $this->commissionViewRepository->pushCriteria(new LimitOffsetCriteria($request));
        $commissionViews = $this->commissionViewRepository->paginate($request->pageSize);

        return $this->sendResponse($commissionViews->toArray(), 'Commission Views retrieved successfully');
    }

    /**
     * Store a newly created CommissionView in storage.
     * POST /commissionViews
     *
     * @param CreateCommissionViewAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCommissionViewAPIRequest $request)
    {
        $input = $request->all();

        $commissionViews = $this->commissionViewRepository->create($input);

        return $this->sendResponse($commissionViews->toArray(), 'Commission View saved successfully');
    }

    /**
     * Display the specified CommissionView.
     * GET|HEAD /commissionViews/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CommissionView $commissionView */
        $commissionView = $this->commissionViewRepository->findWithoutFail($id);

        if (empty($commissionView)) {
            return $this->sendError('Commission View not found');
        }

        return $this->sendResponse($commissionView->toArray(), 'Commission View retrieved successfully');
    }

    /**
     * Update the specified CommissionView in storage.
     * PUT/PATCH /commissionViews/{id}
     *
     * @param  int $id
     * @param UpdateCommissionViewAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCommissionViewAPIRequest $request)
    {
        $input = $request->all();

        /** @var CommissionView $commissionView */
        $commissionView = $this->commissionViewRepository->findWithoutFail($id);

        if (empty($commissionView)) {
            return $this->sendError('Commission View not found');
        }

        $commissionView = $this->commissionViewRepository->update($input, $id);

        return $this->sendResponse($commissionView->toArray(), 'CommissionView updated successfully');
    }

    /**
     * Remove the specified CommissionView from storage.
     * DELETE /commissionViews/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CommissionView $commissionView */
        $commissionView = $this->commissionViewRepository->findWithoutFail($id);

        if (empty($commissionView)) {
            return $this->sendError('Commission View not found');
        }

        $commissionView->delete();

        return $this->sendResponse($id, 'Commission View deleted successfully');
    }
}
