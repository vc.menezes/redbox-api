<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateReceiptCardAPIRequest;
use App\Http\Requests\API\UpdateReceiptCardAPIRequest;
use App\Models\ReceiptCard;
use App\Repositories\ReceiptCardRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ReceiptCardController
 * @package App\Http\Controllers\API
 */

class ReceiptCardAPIController extends AppBaseController
{
    /** @var  ReceiptCardRepository */
    private $receiptCardRepository;

    public function __construct(ReceiptCardRepository $receiptCardRepo)
    {
        $this->receiptCardRepository = $receiptCardRepo;
    }

    /**
     * Display a listing of the ReceiptCard.
     * GET|HEAD /receiptCards
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->receiptCardRepository->pushCriteria(new RequestCriteria($request));
        $this->receiptCardRepository->pushCriteria(new LimitOffsetCriteria($request));
        $receiptCards = $this->receiptCardRepository->all();

        return $this->sendResponse($receiptCards->toArray(), 'Receipt Cards retrieved successfully');
    }

    /**
     * Store a newly created ReceiptCard in storage.
     * POST /receiptCards
     *
     * @param CreateReceiptCardAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateReceiptCardAPIRequest $request)
    {
        $input = $request->all();

        $receiptCards = $this->receiptCardRepository->create($input);

        return $this->sendResponse($receiptCards->toArray(), 'Receipt Card saved successfully');
    }

    /**
     * Display the specified ReceiptCard.
     * GET|HEAD /receiptCards/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ReceiptCard $receiptCard */
        $receiptCard = $this->receiptCardRepository->findWithoutFail($id);

        if (empty($receiptCard)) {
            return $this->sendError('Receipt Card not found');
        }

        return $this->sendResponse($receiptCard->toArray(), 'Receipt Card retrieved successfully');
    }

    /**
     * Update the specified ReceiptCard in storage.
     * PUT/PATCH /receiptCards/{id}
     *
     * @param  int $id
     * @param UpdateReceiptCardAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReceiptCardAPIRequest $request)
    {
        $input = $request->all();

        /** @var ReceiptCard $receiptCard */
        $receiptCard = $this->receiptCardRepository->findWithoutFail($id);

        if (empty($receiptCard)) {
            return $this->sendError('Receipt Card not found');
        }

        $receiptCard = $this->receiptCardRepository->update($input, $id);

        return $this->sendResponse($receiptCard->toArray(), 'ReceiptCard updated successfully');
    }

    /**
     * Remove the specified ReceiptCard from storage.
     * DELETE /receiptCards/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ReceiptCard $receiptCard */
        $receiptCard = $this->receiptCardRepository->findWithoutFail($id);

        if (empty($receiptCard)) {
            return $this->sendError('Receipt Card not found');
        }

        $receiptCard->delete();

        return $this->sendResponse($id, 'Receipt Card deleted successfully');
    }
}
