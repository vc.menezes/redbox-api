<?php

namespace App\Http\Controllers\API;

use App\Events\InvoicePaid;
use App\Exports\SalesExport;
use App\Http\Requests\API\CreateInvoiceAPIRequest;
use App\Http\Requests\API\UpdateInvoiceAPIRequest;
use App\Jobs\GiveSequence;
use App\Models\Invoice;
use App\Criteria\FilterColumnsCriteria;
use App\Criteria\OrWhereCriteria;
use App\Criteria\WhereInCriteria;
use App\Modules\Finance\Reverse\ReverseInvoice;
use App\Repositories\InvoiceReceiptRepository;
use App\Repositories\InvoiceRepository;
use App\Repositories\PackageRepository;
use App\Repositories\ReceiptRepository;
use App\Traits\Jwt;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class InvoiceController
 * @package App\Http\Controllers\API
 */
class InvoiceAPIController extends AppBaseController
{
    /** @var  InvoiceRepository */
    private $invoiceRepository;
    private $receiptRepository;
    private $invoiceReceiptRepository;
    private $packageRepository;
    use Jwt;

    public function __construct(PackageRepository $packageRepo, InvoiceRepository $invoiceRepo, ReceiptRepository $receiptRepo, InvoiceReceiptRepository $invoiceReceiptRepo)
    {
        $this->invoiceRepository = $invoiceRepo;
        $this->receiptRepository = $receiptRepo;
        $this->invoiceReceiptRepository = $invoiceReceiptRepo;
        $this->packageRepository = $packageRepo;
    }

    /**
     * Display a listing of the Invoice.
     * GET|HEAD /invoices
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        
        $this->invoiceRepository->pushCriteria(new WhereInCriteria('data_type', ['App\\Models\\BulkOrder','data_type' => 'App\\Models\\Package'] ));

        if ($request->profile_id) {
            $this->invoiceRepository->pushCriteria(new FilterColumnsCriteria(['profile_id' => $request->profile_id]));
        }

        if ($request->status != null) {
            $this->invoiceRepository->pushCriteria(new FilterColumnsCriteria(['status' => $request->status]));
        }

        $this->invoiceRepository->pushCriteria(new RequestCriteria($request));
        $this->invoiceRepository->pushCriteria(new LimitOffsetCriteria($request));

        $invoices = $this->invoiceRepository->with(['profile'])->orderBy('id', 'desc')->paginate($request->pageSize);

        return $this->sendResponse($invoices->toArray(), 'Invoices retrieved successfully');
    }

    /**
     * Store a newly created Invoice in storage.
     * POST /invoices
     *
     * @param CreateInvoiceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInvoiceAPIRequest $request)
    {
        $input = $request->all();

        $invoices = $this->invoiceRepository->create($input);

        return $this->sendResponse($invoices->toArray(), 'Invoice saved successfully');
    }

    /**
     * Display the specified Invoice.
     * GET|HEAD /invoices/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Invoice $invoice */
        $invoice = $this->invoiceRepository->with('receipts')->findWithoutFail($id);

        if (empty($invoice)) {
            return $this->sendError('Invoice not found');
        }

        return $this->sendResponse($invoice->toArray(), 'Invoice retrieved successfully');
    }

    /**
     * Update the specified Invoice in storage.
     * PUT/PATCH /invoices/{id}
     *
     * @param  int $id
     * @param UpdateInvoiceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInvoiceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Invoice $invoice */
        $invoice = $this->invoiceRepository->findWithoutFail($id);

        if (empty($invoice)) {
            return $this->sendError('Invoice not found');
        }

        $invoice = $this->invoiceRepository->update($input, $id);

        return $this->sendResponse($invoice->toArray(), 'Invoice updated successfully');
    }

    /**
     * Remove the specified Invoice from storage.
     * DELETE /invoices/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Invoice $invoice */
        $invoice = $this->invoiceRepository->findWithoutFail($id);

        if (empty($invoice)) {
            return $this->sendError('Invoice not found');
        }

        $invoice->delete();

        return $this->sendResponse($id, 'Invoice deleted successfully');
    }

    /**
     * Display the all invoices belongs to package.
     * GET|HEAD /invoices/package/{package_id}
     *
     * @param  int $package_id
     *
     * @return Response
     */

    public function getPackageInvoice($package_id)
    {
        $condition = ['data_id' => $package_id, 'data_type' => 'App\\Models\\Package'];

        $invoice = $this->invoiceRepository->findWhere($condition)->all();

        if (empty($invoice)) {
            return $this->sendResponse([], 'No invoice');
        }
        return $this->sendResponse($invoice, 'Invoices');
    }

    /**
     * Display the all invoices belongs to profile_id.
     * GET|HEAD /invoices/profile/{profile_id}
     *
     * @param  int $profile_id
     *
     * @return Response
     */
    public function getProfileInvoice($profile_id = null, Request $request)
    {
        $id = $profile_id ? $profile_id : (\JWTAuth::parseToken()->authenticate())->profile_id;
        $user = \JWTAuth::parseToken()->authenticate();
        $this->invoiceRepository->pushCriteria(new WhereInCriteria('data_type', ['App\\Models\\BulkOrder','data_type' => 'App\\Models\\Package'] ));
        $this->invoiceRepository->pushCriteria(new FilterColumnsCriteria(['profile_id' => $id]));
        $this->invoiceRepository->pushCriteria(new RequestCriteria($request));
        $invoice = $this->invoiceRepository->orderBy('id', 'desc')->paginate($request->pageSize);

        return $this->sendResponse($invoice, 'Invoice retrieved successfully');
    }

    public function sum_selected(Request $request)
    {
        return $this->invoiceRepository->sum($request->data);
    }

    public function cancel(Request $request, $id)
    {
        $invoice = $this->invoiceRepository->find($id);
        $re = new ReverseInvoice($invoice, $request->remarks ? $request->remarks : 'i want cancel invoice');
        return $this->sendResponse($invoice, $re->error_message);

    }

    public function pay(Request $request)
    {

        $invoices = Invoice::whereIn('id', $request->invoice_ids)->whereIn('status', [2, 3])->get();

        if (!count($invoices)) {
            return $this->sendResponse($invoices->toArray(), 'Invalid Invoice');
        }

        $profile_id = array_column($invoices->toArray(), 'profile_id');

        if (count(array_unique($profile_id)) > 1) {

            return $this->sendResponse($invoices->toArray(), 'all invoice should depend to one customer');
        };


        //get inputs
        $input = $request->except('invoice_ids');
        $input['created_by_profile_id'] = $this->getProfileId();
        $input['receipt_number'] = '-';
        $input['status'] = 1;
        $input['customer_profile_id'] = $profile_id[0];

        //get invoice profile_id

        //create receipt
        $receipt = $this->receiptRepository->create($input);

        GiveSequence::dispatch($receipt, 'receipt_number', 'receipt_number');

        foreach ($invoices as $invoice) {
            $this->invoiceReceiptRepository->create(['invoice_id' => $invoice->id, 'receipt_id' => $receipt->id]);
            $invoice->status = 0;
            $invoice->save();
            event(new InvoicePaid($invoice));
        }

        return $this->sendResponse($receipt, 'Receipt created successfully');

    }

    public function daily_sales(Request $request)
    {
        $new = new SalesExport($request);
        return $new->collection();
    }

    public function daily_sales_download(Request $request)
    {
//        $new = new SalesExport($request);
//        return $new->collection();

        return \Excel::download(new SalesExport($request), 'sales.xlsx');

    }

    function yearly_sales()
    {
        $now = Carbon::now();
        $year = $now->year;
        $data = Invoice::select(\DB::raw('YEAR(created_at) as year'), \DB::raw('sum(total) as total'), \DB::raw('sum(tax) as tax'))
            ->groupBy('year')
            ->whereIn('status', [0])
            ->get();

        foreach ($data as $item){
            $item->current = false;
            if($item->year==$year){
                $item->current = true;
            }
        }

        return $this->sendResponse($data, 'Yearly Sales Successfully');
    }

    public function monthly_sales(Request $request){

        if($request->year){

            $year = $request->year;
        }else{
            $now = Carbon::now();
            $year = $now->year;
        }

        $data = Invoice::select(\DB::raw('MONTH(created_at) as month'), \DB::raw('sum(total) as total'), \DB::raw('sum(tax) as tax'))
            ->groupBy('month')
            ->whereYear('created_at',$year)
            ->whereIn('status', [0])
            ->get();

        foreach ($data as $item){
            $item->month_name = \Redbox::MonthName($item->month);
            $item->total =round($item->total,2);
            $item->tax =round($item->tax,2);
        }
        return $this->sendResponse($data, 'monthly sales  Successfully');
    }
}
