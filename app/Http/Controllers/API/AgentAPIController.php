<?php

namespace App\Http\Controllers\API;

use App\Criteria\ByMediumCriteria;
use App\Criteria\NotYetShippedCriteria;
use App\Http\Requests\API\CreateAgentAPIRequest;
use App\Http\Requests\API\UpdateAgentAPIRequest;
use App\Models\Agent;
use App\Models\Commission;
use App\Models\Package;
use App\Repositories\AgentRepository;
use App\Repositories\OrderRepository;
use App\Repositories\PackageRepository;
use App\Repositories\ProfileRepository;
use App\Traits\Jwt;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AgentController
 * @package App\Http\Controllers\API
 */

class AgentAPIController extends AppBaseController
{
    /** @var  AgentRepository */
    private $agentRepository;
    private $packageRepository;
    private $orderRepository;
    private $profileRepository;
    use Jwt;
    public function __construct(AgentRepository $agentRepo,
                                PackageRepository $packageRepo,
                                OrderRepository $orderRepo,
                                ProfileRepository $profileRepository
    )
    {
        $this->agentRepository = $agentRepo;
        $this->packageRepository = $packageRepo;
        $this->orderRepository = $orderRepo;
        $this->profileRepository = $profileRepository;
    }

    /**
     * Display a listing of the Agent.
     * GET|HEAD /agents
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->agentRepository->pushCriteria(new RequestCriteria($request));
        $this->agentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $agents = $this->agentRepository->all();

        return $this->sendResponse($agents->toArray(), 'Agents retrieved successfully');
    }

    /**
     * Store a newly created Agent in storage.
     * POST /agents
     *
     * @param CreateAgentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAgentAPIRequest $request)
    {
        $input = $request->only('profile_id', 'location_id');
        $islands = $this->agentRepository->findWhere(['profile_id' => $request->profile_id, 'location_id' => $request->location_id])->all();

        if (!empty($islands)) {
            return $this->sendResponse([], 'Island already assigned');
        }

        $agents = $this->agentRepository->create($input);

        return $this->sendResponse($agents->toArray(), 'Agent saved successfully');
    }

    /**
     * Display the specified Agent.
     * GET|HEAD /agents/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Agent $agent */
        $agent = $this->agentRepository->findWithoutFail($id);

        if (empty($agent)) {
            return $this->sendError('Agent not found');
        }

        return $this->sendResponse($agent->toArray(), 'Agent retrieved successfully');
    }

    /**
     * Update the specified Agent in storage.
     * PUT/PATCH /agents/{id}
     *
     * @param  int $id
     * @param UpdateAgentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAgentAPIRequest $request)
    {
        $input = $request->all();

        /** @var Agent $agent */
        $agent = $this->agentRepository->findWithoutFail($id);

        if (empty($agent)) {
            return $this->sendError('Agent not found');
        }

        $agent = $this->agentRepository->update($input, $id);

        return $this->sendResponse($agent->toArray(), 'Agent updated successfully');
    }

    /**
     * Remove the specified Agent from storage.
     * DELETE /agents/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Agent $agent */
        $agent = $this->agentRepository->findWithoutFail($id);

        if (empty($agent)) {
            return $this->sendError('Agent not found');
        }

        $agent->delete();

        return $this->sendResponse($id, 'Agent deleted successfully');
    }

    public function package($parent_id=null){
        $user = $this->getUser();

        return Package::whereProfileId($user->profile_id)->get();
    }

    public function commissions($agent_id){

        $result = Commission::
        select(
            \DB::raw('MONTH(created_at) month'),
            \DB::raw('sum(total) total'),
            \DB::raw('max(profile_id) profile_id')
        )->groupBy('month')
            ->with('profile')
            ->whereStatus(1)
            ->whereProfileId($agent_id)
            ->whereNull('deleted_at')->get();

        foreach ($result as $item){
            $item->commissions = \App\Models\Commission::whereProfileId($item->profile_id)->whereMonth('created_at',$item->month)->whereStatus(1)->get();
            $item->month_name = date("F", mktime(0, 0, 0, $item->month, 1));
        }
        return $this->sendResponse($result, 'show agent pending commission successfully');
    }

    public function not_ship_packages_agent(Request $request){
//        $this->orderRepository->pushCriteria(new ByMediumCriteria($request->medium));
        $this->orderRepository->pushCriteria(new NotYetShippedCriteria());
        $order = $this->orderRepository->get()->toArray();



        $destinations_location_id = array_column($order,'destinations_location_id');

        if ($request->package_id) {
            $package = Package::find($request->package_id);
            $location_id = [$package->profile->location_id];
            $destinations_location_id = array_merge($destinations_location_id, $location_id);
        }





        //get all agents which belongs to vessel routes
        $agents = Agent::groupBy('profile_id')->select(['profile_id'])
            ->with('profile')
            ->whereIn('location_id', $destinations_location_id)->get();


        return $this->sendResponse($agents, 'Not shipped packages agents');
    }
}
