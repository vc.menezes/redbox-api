<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateContactUsAPIRequestAPIRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Mail\ContactUs;
use Mail;

/**
 * Class ContactUsAPIControllerAPIController
 * @package App\Http\Controllers\API
 */

class ContactUsAPIControllerAPIController extends AppBaseController
{


    /**
     * Contac us message mail to prodesigners mail 
     * PUT/PATCH /jobAPIControllers/{id}
     *
     * @param  int $id
     * @param CreateContactUsAPIRequestAPIRequest $request
     *
     * @return Response
     */
    public function sendContactUsMessage(CreateContactUsAPIRequestAPIRequest $request)
    {
        $input = $request->all();      

        Mail::queue(new ContactUs($input));

        return response([ 'message' => 'Thank you for reaching us!'], 200);
    }
}
