<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRateAPIRequest;
use App\Http\Requests\API\UpdateRateAPIRequest;
use App\Models\Rate;
use App\Modules\Finance\Shipping;
use App\Repositories\RateRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RateController
 * @package App\Http\Controllers\API
 */

class RateAPIController extends AppBaseController
{
    /** @var  RateRepository */
    private $rateRepository;

    public function __construct(RateRepository $rateRepo)
    {
        $this->rateRepository = $rateRepo;
    }

    /**
     * Display a listing of the Rate.
     * GET|HEAD /rates
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->rateRepository->pushCriteria(new RequestCriteria($request));
        $this->rateRepository->pushCriteria(new LimitOffsetCriteria($request));
        $rates = $this->rateRepository->with(['from','to'])->orderBy('id', 'desc')->paginate($request->pageSize);

        return $this->sendResponse($rates->toArray(), 'Rates retrieved successfully');
    }

    /**
     * Store a newly created Rate in storage.
     * POST /rates
     *
     * @param CreateRateAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRateAPIRequest $request)
    {
        $input = $request->except('from','to');

        $rates = $this->rateRepository->create($input);

        return $this->sendResponse($rates->toArray(), 'Rate saved successfully');
    }

    /**
     * Display the specified Rate.
     * GET|HEAD /rates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Rate $rate */
        $rate = $this->rateRepository->findWithoutFail($id);

        if (empty($rate)) {
            return $this->sendError('Rate not found');
        }

        return $this->sendResponse($rate->toArray(), 'Rate retrieved successfully');
    }

    /**
     * Update the specified Rate in storage.
     * PUT/PATCH /rates/{id}
     *
     * @param  int $id
     * @param UpdateRateAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRateAPIRequest $request)
    {
        $input = $request->except('from', 'to');

        /** @var Rate $rate */
        $rate = $this->rateRepository->findWithoutFail($id);

        if (empty($rate)) {
            return $this->sendError('Rate not found');
        }

        $rate = $this->rateRepository->update($input, $id);

        return $this->sendResponse($rate->toArray(), 'Rate updated successfully');
    }

    /**
     * Remove the specified Rate from storage.
     * DELETE /rates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Rate $rate */
        $rate = $this->rateRepository->findWithoutFail($id);

        if (empty($rate)) {
            return $this->sendError('Rate not found');
        }

        $rate->delete();

        return $this->sendResponse($id, 'Rate deleted successfully');
    }

    /**
     * @param Request $request
     * weight  :  required [100]
     * medium  :  required [sea or air]
     * type    :  required [local or international]
     * from_id :  [location_id]:nullable
     * to_id   :  [location_id]:nullable
     * @return mixed
     */
    public function calculate(Request $request){
        $input = $request->all();

        $input['weight'] = floor($request->weight);
        $input['operator'] = '=';

        if($request->weight<1){
            $input['weight'] = ceil($request->weight);
            $input['operator'] = '<';
        }



        if($request->type) {
            $input['type'] = $request->type;
        }

        if($request->medium) {
            $input['medium'] = $request->medium;
        }

        if($request->from_id) {
            $input['from_id'] = $request->from_id;
        }

        if($request->to_id) {
            $input['to_id'] = $request->to_id;
        }



        $rate = $this->rateRepository->findWhere($input)->first();



        // $rate->rate = $rate->rate *  \App\Models\Setting::where("key", "GST")->first()1.06;

        if(empty($rate)) {
            return $this->sendResponse(0, 'Not calculated');
        }

        $calulated= round(($rate->rate * 1.06),2);

        return $this->sendResponse($calulated, 'Rate');

    }

    /**
     * @param Request $request
     * GEt all from and to location locations 
     * @return mixed
     */
    public function locationRates(Request $request) {

        $input = $request->only('medium', 'type');
        // return $input;
        $locations = \App\Models\Rate::select('from_id', 'to_id')->where($input)->groupBy(['from_id', 'to_id'])->get();
        
        $fromIds = array_pluck($locations, 'from_id');
        $toIds = array_pluck($locations, 'to_id');

        $data['from'] = \App\Models\Location::whereIn('id', $fromIds)->get();
        $data['to'] = \App\Models\Location::whereIn('id', $toIds)->get();

        return $this->sendResponse($data, 'Location received');
    }


    public function getMedium(Request $request) {
        if(!$request->type) {
           return $this->sendError('Rate not found');
        }
        $mediums =  \App\Models\Rate::select('medium')->whereType($request->type)->groupBy(['medium'])->get();
        return $this->sendResponse($mediums, 'Mediums');
    }
}
