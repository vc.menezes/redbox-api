<?php

namespace App\Http\Controllers\API;

use App\Criteria\FilterColumnsCriteria;
use App\Exports\AgentCommissionExport;
use App\Exports\AgentCommissionPostExport;
use App\Http\Requests\API\CreateAgentPaymentPostAPIRequest;
use App\Http\Requests\API\UpdateAgentPaymentPostAPIRequest;
use App\Models\AgentPaymentPost;
use App\Modules\Commission\PendingCommission;
use App\Repositories\AgentPaymentPostRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AgentPaymentPostController
 * @package App\Http\Controllers\API
 */
class AgentPaymentPostAPIController extends AppBaseController
{
	/** @var  AgentPaymentPostRepository */
	private $agentPaymentPostRepository;

	public function __construct(AgentPaymentPostRepository $agentPaymentPostRepo)
	{
		$this->agentPaymentPostRepository = $agentPaymentPostRepo;
	}

	/**
	 * Display a listing of the AgentPaymentPost.
	 * GET|HEAD /agentPaymentPosts
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{


		$this->agentPaymentPostRepository->pushCriteria(new RequestCriteria($request));
		$this->agentPaymentPostRepository->pushCriteria(new LimitOffsetCriteria($request));
		$this->agentPaymentPostRepository->pushCriteria(new FilterColumnsCriteria(['status'=>1]));
		$agentPaymentPosts = $this->agentPaymentPostRepository->with('profile')->get();

		if($request->download){
			return \Excel::download(new AgentCommissionPostExport, 'agent_commission_post_export.xlsx');
		}

		return $this->sendResponse($agentPaymentPosts->toArray(), 'Agent Payment Posts retrieved successfully');
	}

	/**
	 * Store a newly created AgentPaymentPost in storage.
	 * POST /agentPaymentPosts
	 *
	 * @param CreateAgentPaymentPostAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$input = [];
		$pendingCommission = new PendingCommission();
		$data_by_month = $pendingCommission->get_by_month($request->month);

		if (!count($data_by_month)) {
			return $this->sendError('No Pending Commission in this month');
		}

		$today = Carbon::now();
		$today->month = $request->month;
		$today->year = $request->year;

		$thisMonth  =  new Carbon('first day of this month');

		if($today->greaterThan($thisMonth)){
			return $this->sendError('Cannot pay Commission for running month');
		}

		foreach ($data_by_month as $item) {
			if (!is_null($item->bank_account)) {
				$input['profile_id'] = $item->profile_id;
				$input['total'] = $item->total;
				$input['account_number'] = $item->bank_account->account_no;
				$input['email'] = $item->profile->email;
				$input['identifier'] = $item->profile->identifier;
				$input['details'] = 'commission';
				$input['contact_number'] = $item->profile->contact_number;
				$input['weight'] = 0;
				$input['date'] = $today->toDateString();

				$AgentPaymentPost = AgentPaymentPost::whereMonth('date', '=', $request->month)->whereProfileId($item->profile_id)->first();
				if (!$AgentPaymentPost) {
					$agentPaymentPosts = $this->agentPaymentPostRepository->create($input);
				}
			}

		}

		return $this->sendResponse([$input], 'Agent Payment Post saved successfully');
	}

	/**
	 * Display the specified AgentPaymentPost.
	 * GET|HEAD /agentPaymentPosts/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var AgentPaymentPost $agentPaymentPost */
		$agentPaymentPost = $this->agentPaymentPostRepository->findWithoutFail($id);

		if (empty($agentPaymentPost)) {
			return $this->sendError('Agent Payment Post not found');
		}

		return $this->sendResponse($agentPaymentPost->toArray(), 'Agent Payment Post retrieved successfully');
	}

	/**
	 * Update the specified AgentPaymentPost in storage.
	 * PUT/PATCH /agentPaymentPosts/{id}
	 *
	 * @param  int $id
	 * @param UpdateAgentPaymentPostAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateAgentPaymentPostAPIRequest $request)
	{
		$input = $request->all();

		/** @var AgentPaymentPost $agentPaymentPost */
		$agentPaymentPost = $this->agentPaymentPostRepository->findWithoutFail($id);

		if (empty($agentPaymentPost)) {
			return $this->sendError('Agent Payment Post not found');
		}

		$agentPaymentPost = $this->agentPaymentPostRepository->update($input, $id);

		return $this->sendResponse($agentPaymentPost->toArray(), 'AgentPaymentPost updated successfully');
	}

	/**
	 * Remove the specified AgentPaymentPost from storage.
	 * DELETE /agentPaymentPosts/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var AgentPaymentPost $agentPaymentPost */
		$agentPaymentPost = $this->agentPaymentPostRepository->findWithoutFail($id);

		if (empty($agentPaymentPost)) {
			return $this->sendError('Agent Payment Post not found');
		}

		$agentPaymentPost->delete();

		return $this->sendResponse($id, 'Agent Payment Post deleted successfully');
	}

	public function download(){
//		return \Excel::download(new \App\Exports\PackagesExport($request), 'Packages.xlsx');
		$new = new AgentCommissionPostExport();
		return $new->collection();
	}
}
