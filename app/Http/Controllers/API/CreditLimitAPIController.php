<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCreditLimitAPIRequest;
use App\Http\Requests\API\UpdateCreditLimitAPIRequest;
use App\Models\CreditLimit;
use App\Repositories\CreditLimitRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CreditLimitController
 * @package App\Http\Controllers\API
 */

class CreditLimitAPIController extends AppBaseController
{
    /** @var  CreditLimitRepository */
    private $creditLimitRepository;

    public function __construct(CreditLimitRepository $creditLimitRepo)
    {
        $this->creditLimitRepository = $creditLimitRepo;
    }

    /**
     * Display a listing of the CreditLimit.
     * GET|HEAD /creditLimits
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->creditLimitRepository->pushCriteria(new RequestCriteria($request));
        $this->creditLimitRepository->pushCriteria(new LimitOffsetCriteria($request));
        $creditLimits = $this->creditLimitRepository->all();

        return $this->sendResponse($creditLimits->toArray(), 'Credit Limits retrieved successfully');
    }

    /**
     * Store a newly created CreditLimit in storage.
     * POST /creditLimits
     *
     * @param CreateCreditLimitAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCreditLimitAPIRequest $request)
    {
        $input = $request->all();

        $creditLimits = $this->creditLimitRepository->create($input);

        return $this->sendResponse($creditLimits->toArray(), 'Credit Limit saved successfully');
    }

    /**
     * Display the specified CreditLimit.
     * GET|HEAD /creditLimits/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CreditLimit $creditLimit */
        $creditLimit = $this->creditLimitRepository->findWithoutFail($id);

        if (empty($creditLimit)) {
            return $this->sendError('Credit Limit not found');
        }

        return $this->sendResponse($creditLimit->toArray(), 'Credit Limit retrieved successfully');
    }

    /**
     * Update the specified CreditLimit in storage.
     * PUT/PATCH /creditLimits/{id}
     *
     * @param  int $id
     * @param UpdateCreditLimitAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCreditLimitAPIRequest $request)
    {
        $input = $request->all();

        /** @var CreditLimit $creditLimit */
        $creditLimit = $this->creditLimitRepository->findWithoutFail($id);

        if (empty($creditLimit)) {
            return $this->sendError('Credit Limit not found');
        }

        $creditLimit = $this->creditLimitRepository->update($input, $id);

        return $this->sendResponse($creditLimit->toArray(), 'CreditLimit updated successfully');
    }

    /**
     * Remove the specified CreditLimit from storage.
     * DELETE /creditLimits/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CreditLimit $creditLimit */
        $creditLimit = $this->creditLimitRepository->findWithoutFail($id);

        if (empty($creditLimit)) {
            return $this->sendError('Credit Limit not found');
        }

        $creditLimit->delete();

        return $this->sendResponse($id, 'Credit Limit deleted successfully');
    }
}
