<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePackageShipmentAPIRequest;
use App\Http\Requests\API\UpdatePackageShipmentAPIRequest;
use App\Models\PackageShipment;
use App\Repositories\PackageShipmentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PackageShipmentController
 * @package App\Http\Controllers\API
 */

class PackageShipmentAPIController extends AppBaseController
{
    /** @var  PackageShipmentRepository */
    private $packageShipmentRepository;

    public function __construct(PackageShipmentRepository $packageShipmentRepo)
    {
        $this->packageShipmentRepository = $packageShipmentRepo;
    }

    /**
     * Display a listing of the PackageShipment.
     * GET|HEAD /packageShipments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->packageShipmentRepository->pushCriteria(new RequestCriteria($request));
        $this->packageShipmentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $packageShipments = $this->packageShipmentRepository->all();

        return $this->sendResponse($packageShipments->toArray(), 'Package Shipments retrieved successfully');
    }

    /**
     * Store a newly created PackageShipment in storage.
     * POST /packageShipments
     *
     * @param CreatePackageShipmentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePackageShipmentAPIRequest $request)
    {
        $input = $request->all();

        $packageShipments = $this->packageShipmentRepository->create($input);

        return $this->sendResponse($packageShipments->toArray(), 'Package Shipment saved successfully');
    }

    /**
     * Display the specified PackageShipment.
     * GET|HEAD /packageShipments/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PackageShipment $packageShipment */
        $packageShipment = $this->packageShipmentRepository->findWithoutFail($id);

        if (empty($packageShipment)) {
            return $this->sendError('Package Shipment not found');
        }

        return $this->sendResponse($packageShipment->toArray(), 'Package Shipment retrieved successfully');
    }

    /**
     * Update the specified PackageShipment in storage.
     * PUT/PATCH /packageShipments/{id}
     *
     * @param  int $id
     * @param UpdatePackageShipmentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePackageShipmentAPIRequest $request)
    {
        $input = $request->all();

        /** @var PackageShipment $packageShipment */
        $packageShipment = $this->packageShipmentRepository->findWithoutFail($id);

        if (empty($packageShipment)) {
            return $this->sendError('Package Shipment not found');
        }

        $packageShipment = $this->packageShipmentRepository->update($input, $id);

        return $this->sendResponse($packageShipment->toArray(), 'PackageShipment updated successfully');
    }

    /**
     * Remove the specified PackageShipment from storage.
     * DELETE /packageShipments/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PackageShipment $packageShipment */
        $packageShipment = $this->packageShipmentRepository->findWithoutFail($id);

        if (empty($packageShipment)) {
            return $this->sendError('Package Shipment not found');
        }

        $packageShipment->delete();

        return $this->sendResponse($id, 'Package Shipment deleted successfully');
    }
}
