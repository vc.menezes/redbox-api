<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateKeyAPIRequest;
use App\Http\Requests\API\UpdateKeyAPIRequest;
use App\Models\Key;
use App\Repositories\KeyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class KeyController
 * @package App\Http\Controllers\API
 */

class KeyAPIController extends AppBaseController
{
    /** @var  KeyRepository */
    private $keyRepository;

    public function __construct(KeyRepository $keyRepo)
    {
        $this->keyRepository = $keyRepo;
    }

    /**
     * Display a listing of the Key.
     * GET|HEAD /keys
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->keyRepository->pushCriteria(new RequestCriteria($request));
        $this->keyRepository->pushCriteria(new LimitOffsetCriteria($request));
        $keys = $this->keyRepository->all();

        return $this->sendResponse($keys->toArray(), 'Keys retrieved successfully');
    }

    /**
     * Store a newly created Key in storage.
     * POST /keys
     *
     * @param CreateKeyAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateKeyAPIRequest $request)
    {
        $input = $request->all();

        $keys = $this->keyRepository->create($input);

        return $this->sendResponse($keys->toArray(), 'Key saved successfully');
    }

    /**
     * Display the specified Key.
     * GET|HEAD /keys/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Key $key */
        $key = $this->keyRepository->findWithoutFail($id);

        if (empty($key)) {
            return $this->sendError('Key not found');
        }

        return $this->sendResponse($key->toArray(), 'Key retrieved successfully');
    }

    /**
     * Update the specified Key in storage.
     * PUT/PATCH /keys/{id}
     *
     * @param  int $id
     * @param UpdateKeyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKeyAPIRequest $request)
    {
        $input = $request->all();

        /** @var Key $key */
        $key = $this->keyRepository->findWithoutFail($id);

        if (empty($key)) {
            return $this->sendError('Key not found');
        }

        $key = $this->keyRepository->update($input, $id);

        return $this->sendResponse($key->toArray(), 'Key updated successfully');
    }

    /**
     * Remove the specified Key from storage.
     * DELETE /keys/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Key $key */
        $key = $this->keyRepository->findWithoutFail($id);

        if (empty($key)) {
            return $this->sendError('Key not found');
        }

        $key->delete();

        return $this->sendResponse($id, 'Key deleted successfully');
    }
}
