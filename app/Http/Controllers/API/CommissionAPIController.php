<?php

namespace App\Http\Controllers\API;

use App\Criteria\BetweenTwoDateCriteria;
use App\Criteria\Commission\PendingCriteria;
use App\Criteria\FilterColumnsCriteria;
use App\Exports\AgentCommissionExport;
use App\Exports\CommissionExport;
use App\Http\Requests\API\CreateCommissionAPIRequest;
use App\Http\Requests\API\UpdateCommissionAPIRequest;
use App\Models\Commission;
use App\Models\AgentCommission;
use App\Repositories\CommissionReportViewRepository;
use App\Repositories\CommissionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CommissionController
 * @package App\Http\Controllers\API
 */
class CommissionAPIController extends AppBaseController
{
    /** @var  CommissionRepository */
    private $commissionRepository;

    public function __construct(CommissionRepository $commissionRepo)
    {
        $this->commissionRepository = $commissionRepo;
    }

    /**
     * Display a listing of the Commission.
     * GET|HEAD /commissions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {


        if ($request->from_date && $request->to_date) {
            $this->commissionRepository->pushCriteria(new BetweenTwoDateCriteria($request->from_date, $request->to_date));
        }

        if ($request->profile_id) {
            $this->commissionRepository->pushCriteria(new FilterColumnsCriteria(['profile_id' => $request->profile_id]));
        }

        $this->commissionRepository->pushCriteria(new RequestCriteria($request));
        $this->commissionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $commissions = $this->commissionRepository->all();

        return $this->sendResponse($commissions->toArray(), 'Commissions retrieved successfully');
    }

    /**
     * Store a newly created Commission in storage.
     * POST /commissions
     *
     * @param CreateCommissionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCommissionAPIRequest $request)
    {
        $input = $request->all();

        $commissions = $this->commissionRepository->create($input);

        return $this->sendResponse($commissions->toArray(), 'Commission saved successfully');
    }

    /**
     * Display the specified Commission.
     * GET|HEAD /commissions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Commission $commission */
        $commission = $this->commissionRepository->findWithoutFail($id);

        if (empty($commission)) {
            return $this->sendError('Commission not found');
        }

        return $this->sendResponse($commission->toArray(), 'Commission retrieved successfully');
    }

    /**
     * Update the specified Commission in storage.
     * PUT/PATCH /commissions/{id}
     *
     * @param  int $id
     * @param UpdateCommissionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCommissionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Commission $commission */
        $commission = $this->commissionRepository->findWithoutFail($id);

        if (empty($commission)) {
            return $this->sendError('Commission not found');
        }

        $commission = $this->commissionRepository->update($input, $id);

        return $this->sendResponse($commission->toArray(), 'Commission updated successfully');
    }

    /**
     * Remove the specified Commission from storage.
     * DELETE /commissions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Commission $commission */
        $commission = $this->commissionRepository->findWithoutFail($id);

        if($commission->status == 1)

        if ($commission && count($commission->commission_payment_commissions)) {
            return $this->sendError('Commission is alread in paid status, delete commission before this');
        }

        if (empty($commission)) {
            return $this->sendError('Commission not found');
        }

        if($commission->status == 2) {
            return $this->sendError('Commission is alread in paid status');
        }
        if($commission->status == 1) {
            $total = AgentCommission::find($commission->agent_commission_id);
           
            if($total->total_orders > 1) {
                $total->total_orders -= 1;
                $total_amount = $commission->total * (100/$total->percentage);
                $total->total_amount -= $total_amount;
                $total->agent_commission_total = round(($total_amount * ($total->percentage / 100)), 2);

                // return $total;
                $total->save();
            } else {
                $total->delete();
            }
            $commission->delete();
        }


        return $this->sendResponse($id, 'Commission deleted successfully');
    }

    public function show_by_agent(Request $request)
    {

        $this->commissionRepository->pushCriteria(new PendingCriteria());
        if ($request->profile_id) {
            $this->commissionRepository->pushCriteria(new FilterColumnsCriteria(['profile_id' => $request->profile_id]));
        }
        $pending = $this->commissionRepository->with(['profile.bank_account'])->get();

        return $this->sendResponse($pending, 'Pending commissions');
    }

    public function pending_commission(Request $request)
    {
        $profile_id = $request->profile_id ? $request->profile_id : null;

        $status = $request->status ? $request->status : '1';

        $new = new  CommissionExport($profile_id,$status);

        return $this->sendResponse($new->collection(), 'Pending commissions');

    }

    public function download_pending_commission(Request $request)
    {
        $profile_id = $request->profile_id ? $request->profile_id : null;

        $status = $request->status ? $request->status : '1';

        return \Excel::download(new \App\Exports\CommissionExport($profile_id,$status), 'pending_commission.xlsx');

    }


	public function download_by_agent(Request $request,CommissionReportViewRepository $reportViewRepository)
	{

		return \Excel::download(new AgentCommissionExport($request,$reportViewRepository), 'pending_commission.xlsx');
	}

}
