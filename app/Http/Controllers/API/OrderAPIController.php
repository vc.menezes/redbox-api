<?php

namespace App\Http\Controllers\API;

use App\Models\Agent;
use App\Repositories\OrderRepository;
use App\Views\PackageView;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\FilterColumnsCriteria;
use Response;

/**
 * Class OrderController
 * @package App\Http\Controllers\API
 */
class OrderAPIController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepo)
    {
        $this->orderRepository = $orderRepo;
    }

    /**
     * Display a listing of the Order.
     * GET|HEAD /orders
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->orderRepository->pushCriteria(new RequestCriteria($request));
        $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->orderRepository->pushCriteria(new FilterColumnsCriteria(['type' => 'order']));

        if ($request->atoll_code) {
            $this->orderRepository->pushCriteria(new FilterColumnsCriteria(['atoll_code' => $request->atoll_code]));
        }

        if ($request->destinations_location_id) {
            $this->orderRepository->pushCriteria(new FilterColumnsCriteria(['destinations_location_id' => $request->destinations_location_id]));
        }

        if ($request->last_tracking_status_id) {
            $this->orderRepository->pushCriteria(new FilterColumnsCriteria(['last_tracking_status_id' => $request->last_tracking_status_id]));
        }

        if ($request->payment_status) {
            $this->orderRepository->pushCriteria(new FilterColumnsCriteria(['payment_status' => $request->payment_status]));
        }

        $orders = $this->orderRepository->with(['status', 'profile'])->orderBy('id', 'desc')->paginate($request->pageSize);

        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }


    public function show($id)
    {
        /** @var Order $order */
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        return $this->sendResponse($order->toArray(), 'Order retrieved successfully');
    }

//    public function by_location($location_id){
//        $this->orderRepository->findWhere([''])
//    }




}
