<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAddressableAPIRequest;
use App\Http\Requests\API\UpdateAddressableAPIRequest;
use App\Models\Addressable;
use App\Repositories\AddressableRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AddressableController
 * @package App\Http\Controllers\API
 */

class AddressableAPIController extends AppBaseController
{
    /** @var  AddressableRepository */
    private $addressableRepository;

    public function __construct(AddressableRepository $addressableRepo)
    {
        $this->addressableRepository = $addressableRepo;
    }

    /**
     * Display a listing of the Addressable.
     * GET|HEAD /addressables
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->addressableRepository->pushCriteria(new RequestCriteria($request));
        $this->addressableRepository->pushCriteria(new LimitOffsetCriteria($request));
        $addressables = $this->addressableRepository->all();

        return $this->sendResponse($addressables->toArray(), 'Address retrieved successfully');
    }

    /**
     * Store a newly created Addressable in storage.
     * POST /addressables
     *
     * @param CreateAddressableAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAddressableAPIRequest $request)
    {
        $input = $request->only('address_line', 'street', 'district', 'postal_code', 'country_id', 'type', 'profile_id', 'location_id');

        $addressables = $this->addressableRepository->create($input);
        $addressable = $this->addressableRepository->with(['location', 'country'])->findWithoutFail($addressables->id);

        return $this->sendResponse($addressable->toArray(), 'Address saved successfully');
    }

    /**
     * Display the specified Addressable.
     * GET|HEAD /addressables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Addressable $addressable */
        $addressable = $this->addressableRepository->with(['location', 'country'])->findWithoutFail($id);

        if (empty($addressable)) {
            return $this->sendError('Addressable not found');
        }

        return $this->sendResponse($addressable->toArray(), 'Address retrieved successfully');
    }

    /**
     * Update the specified Addressable in storage.
     * PUT/PATCH /addressables/{id}
     *
     * @param  int $id
     * @param UpdateAddressableAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAddressableAPIRequest $request)
    {
        $input = $request->only('address_line', 'street', 'district', 'postal_code', 'country_id', 'type', 'profile_id', 'location_id');

        /** @var Addressable $addressable */
        $addressable = $this->addressableRepository->findWithoutFail($id);

        if (empty($addressable)) {
            return $this->sendError('Addressable not found');
        }

        $addressable = $this->addressableRepository->with(['location', 'country'])->update($input, $id);

        return $this->sendResponse($addressable->toArray(), 'Address updated successfully');
    }

    /**
     * Remove the specified Addressable from storage.
     * DELETE /addressables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Addressable $addressable */
        $addressable = $this->addressableRepository->findWithoutFail($id);

        if (empty($addressable)) {
            return $this->sendError('Addressable not found');
        }

        $addressable->delete();

        return $this->sendResponse($id, 'Address deleted successfully');
    }

    public function profileAddress(Request $request)
    {
        if($request->profile_id) {
            $addressable = $this->addressableRepository->with(['location', 'country'])->findByField('profile_id', $request->profile_id)->all();
            if (empty($addressable)) {
                return $this->sendResponse([], 'Selected Customer has no recoded address');
            }
            return $this->sendResponse($addressable, 'Addresses found');
        }
        return $this->sendResponse([], 'Error');
    }
}
