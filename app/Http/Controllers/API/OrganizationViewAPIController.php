<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrganizationViewAPIRequest;
use App\Http\Requests\API\UpdateOrganizationViewAPIRequest;
use App\Models\OrganizationView;
use App\Repositories\OrganizationViewRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\FilterColumnsCriteria;
use Response;

/**
 * Class OrganizationViewController
 * @package App\Http\Controllers\API
 */

class OrganizationViewAPIController extends AppBaseController
{
    /** @var  OrganizationViewRepository */
    private $organizationViewRepository;

    public function __construct(OrganizationViewRepository $organizationViewRepo)
    {
        $this->organizationViewRepository = $organizationViewRepo;
    }

    /**
     * Display a listing of the OrganizationView.
     * GET|HEAD /organizationViews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->organizationViewRepository->pushCriteria(new FilterColumnsCriteria(['user_type' => 'commercial_customer']));
        $this->organizationViewRepository->pushCriteria(new RequestCriteria($request));
        $this->organizationViewRepository->pushCriteria(new LimitOffsetCriteria($request));
        $organizationViews = $this->organizationViewRepository->paginate($request->pageSize);

        return $this->sendResponse($organizationViews->toArray(), 'Organization Views retrieved successfully');
    }


    public function show($id)
    {
        /** @var Addressable $addressable */
        $org = $this->organizationViewRepository->with(['location', 'organization'])->findWithoutFail($id);

        if (empty($org)) {
            return $this->sendError('Organization not found');
        }

        return $this->sendResponse($org->toArray(), 'Organization retrieved successfully');
    }
    

}