<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSequenceAPIRequest;
use App\Http\Requests\API\UpdateSequenceAPIRequest;
use App\Models\Sequence;
use App\Repositories\SequenceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SequenceController
 * @package App\Http\Controllers\API
 */

class SequenceAPIController extends AppBaseController
{
    /** @var  SequenceRepository */
    private $sequenceRepository;

    public function __construct(SequenceRepository $sequenceRepo)
    {
        $this->sequenceRepository = $sequenceRepo;
    }

    /**
     * Display a listing of the Sequence.
     * GET|HEAD /sequences
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sequenceRepository->pushCriteria(new RequestCriteria($request));
        $this->sequenceRepository->pushCriteria(new LimitOffsetCriteria($request));
        $sequences = $this->sequenceRepository->paginate();

        return $this->sendResponse($sequences->toArray(), 'Sequences retrieved successfully');
    }

    /**
     * Store a newly created Sequence in storage.
     * POST /sequences
     *
     * @param CreateSequenceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSequenceAPIRequest $request)
    {
        $input = $request->all();

        $sequences = $this->sequenceRepository->create($input);

        return $this->sendResponse($sequences->toArray(), 'Sequence saved successfully');
    }

    /**
     * Display the specified Sequence.
     * GET|HEAD /sequences/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Sequence $sequence */
        $sequence = $this->sequenceRepository->findWithoutFail($id);

        if (empty($sequence)) {
            return $this->sendError('Sequence not found');
        }

        return $this->sendResponse($sequence->toArray(), 'Sequence retrieved successfully');
    }

    /**
     * Update the specified Sequence in storage.
     * PUT/PATCH /sequences/{id}
     *
     * @param  int $id
     * @param UpdateSequenceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSequenceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Sequence $sequence */
        $sequence = $this->sequenceRepository->findWithoutFail($id);

        if (empty($sequence)) {
            return $this->sendError('Sequence not found');
        }

        $sequence = $this->sequenceRepository->update($input, $id);

        return $this->sendResponse($sequence->toArray(), 'Sequence updated successfully');
    }

    /**
     * Remove the specified Sequence from storage.
     * DELETE /sequences/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Sequence $sequence */
        $sequence = $this->sequenceRepository->findWithoutFail($id);

        if (empty($sequence)) {
            return $this->sendError('Sequence not found');
        }

        $sequence->delete();

        return $this->sendResponse($id, 'Sequence deleted successfully');
    }
}
