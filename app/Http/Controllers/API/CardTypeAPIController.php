<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCardTypeAPIRequest;
use App\Http\Requests\API\UpdateCardTypeAPIRequest;
use App\Models\CardType;
use App\Repositories\CardTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CardTypeController
 * @package App\Http\Controllers\API
 */

class CardTypeAPIController extends AppBaseController
{
    /** @var  CardTypeRepository */
    private $cardTypeRepository;

    public function __construct(CardTypeRepository $cardTypeRepo)
    {
        $this->cardTypeRepository = $cardTypeRepo;
    }

    /**
     * Display a listing of the CardType.
     * GET|HEAD /cardTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->cardTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->cardTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $cardTypes = $this->cardTypeRepository->paginate($request->pageSize);

        return $this->sendResponse($cardTypes->toArray(), 'Card Types retrieved successfully');
    }

    /**
     * Store a newly created CardType in storage.
     * POST /cardTypes
     *
     * @param CreateCardTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCardTypeAPIRequest $request)
    {
        $input = $request->all();

        $cardTypes = $this->cardTypeRepository->create($input);

        return $this->sendResponse($cardTypes->toArray(), 'Card Type saved successfully');
    }

    /**
     * Display the specified CardType.
     * GET|HEAD /cardTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CardType $cardType */
        $cardType = $this->cardTypeRepository->findWithoutFail($id);

        if (empty($cardType)) {
            return $this->sendError('Card Type not found');
        }

        return $this->sendResponse($cardType->toArray(), 'Card Type retrieved successfully');
    }

    /**
     * Update the specified CardType in storage.
     * PUT/PATCH /cardTypes/{id}
     *
     * @param  int $id
     * @param UpdateCardTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCardTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var CardType $cardType */
        $cardType = $this->cardTypeRepository->findWithoutFail($id);

        if (empty($cardType)) {
            return $this->sendError('Card Type not found');
        }

        $cardType = $this->cardTypeRepository->update($input, $id);

        return $this->sendResponse($cardType->toArray(), 'CardType updated successfully');
    }

    /**
     * Remove the specified CardType from storage.
     * DELETE /cardTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CardType $cardType */
        $cardType = $this->cardTypeRepository->findWithoutFail($id);

        if (empty($cardType)) {
            return $this->sendError('Card Type not found');
        }

        $cardType->delete();

        return $this->sendResponse($id, 'Card Type deleted successfully');
    }
}
