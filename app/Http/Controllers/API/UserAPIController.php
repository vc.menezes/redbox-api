<?php

namespace App\Http\Controllers\API;

use App\Criteria\FindValidTokenCriteria;
use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\SignUpAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Http\Requests\API\PasswordForgetRequest;
use App\Http\Requests\API\ChangePasswordRequest;
use App\Http\Requests\API\UpdateUserProfileAPIRequest;
use App\Models\Package;
use App\Notifications\Registered;
use App\Repositories\OrganizationRepository;
use App\Repositories\PersonRepository;
use App\Repositories\ProfileRepository;
use App\Repositories\TokenRepository;
use App\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Mail\PasswordReset;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */
class UserAPIController extends AppBaseController
{
	/** @var  UserRepository */
	private $userRepository;
	private $profileRepository;
	private $personRepository;
	private $organizationRepository;
	private $tokenRepository;

	use SendsPasswordResetEmails;


	public function __construct(
		UserRepository $userRepo,
		ProfileRepository $profileRepo,
		PersonRepository $personRepo,
		OrganizationRepository $organizationRepo,
		TokenRepository $tokenRepo
	)
	{
		$this->userRepository = $userRepo;
		$this->profileRepository = $profileRepo;
		$this->personRepository = $personRepo;
		$this->organizationRepository = $organizationRepo;
		$this->tokenRepository = $tokenRepo;
	}

	/**
	 * Display a listing of the User.
	 * GET|HEAD /users
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->userRepository->pushCriteria(new RequestCriteria($request));

		if ($request->all) {
			$users = $this->userRepository->all();
		} else {
			$users = $this->userRepository->paginate($request->pageSize);
		}

		return $this->sendResponse($users->toArray(), 'Users retrieved successfully');
	}

	/**
	 * Store a newly created User in storage.
	 * POST /users
	 *
	 * @param CreateUserAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateUserAPIRequest $request)
	{
		$input = $request->all();

		$users = $this->userRepository->create($input);

		return $this->sendResponse($users->toArray(), 'User saved successfully');
	}

	/**
	 * Display the specified User.
	 * GET|HEAD /users/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var User $user */

		$user = $this->userRepository->findWithoutFail($id);

		if (empty($user)) {
			return $this->sendError('User not found');
		}

		return $this->sendResponse($user->toArray(), 'User retrieved successfully');
	}

	/**
	 * Update the specified User in storage.
	 * PUT/PATCH /users/{id}
	 *
	 * @param  int $id
	 * @param UpdateUserAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateUserAPIRequest $request)
	{
		$input = $request->all();

		/** @var User $user */
		$user = $this->userRepository->findWithoutFail($id);

		if (empty($user)) {
			return $this->sendError('User not found');
		}

		$user = $this->userRepository->update($input, $id);

		return $this->sendResponse($user->toArray(), 'User updated successfully');
	}

	/**
	 * Remove the specified User from storage.
	 * DELETE /users/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var User $user */
		$user = $this->userRepository->findWithoutFail($id);

		if (empty($user)) {
			return $this->sendError('User not found');
		}
		$user_data_exist = Package::whereProfileId($user->profile_id)->count();
		if ($user_data_exist) {
			return $this->sendError('cannot delete user, which has data');
		}

		$user->delete();

		return $this->sendResponse($id, 'User deleted successfully');
	}


	public function register(SignUpAPIRequest $request)
	{

		$message = 'User created successfully';

		/**
		 * preparing inputs for creating profile
		 */
		$profile_input = $request->only('name', 'address_id', 'email', 'contact_number', 'type', 'location_id');
//         $profile_input['identifier'] = $request->id_card_no | $request->registry_number;
//        $profile_input['identifier'] = $request->id_card_no;

		if ($request->id_card_no) {
			// dd($request->id_card_no);
			$profile_input['identifier'] = $request->id_card_no;
		} else {
			$profile_input['identifier'] = $request->registry_number;
		}


		/**
		 * New Profile Created
		 */
		$new_profile = $this->profileRepository->create($profile_input);

		/**
		 * checking the profile created or not
		 */
		if (empty($new_profile)) {
			return $this->sendResponse($new_profile, 'Cannot Create Profile');
		}

		/**
		 * creating and extending profile with organization
		 */
		if ($new_profile->type == 'organization') {
			$message = 'Organization created successfully';
			$organization_input = $request->only('contact_person', 'tin');
			$organization_input['registry_number'] = $new_profile->identifier;
			$organization_input['profile_id'] = $new_profile->id;
			$new_profile->organization = $this->organizationRepository->create($organization_input);

		}

		/**
		 * creating and extending profile with person
		 */
		if ($new_profile->type == 'person') {
			$person_input = $request->only('date_of_birth');
			$person_input['id_card_no'] = $new_profile->identifier;
			$person_input['profile_id'] = $new_profile->id;
			$new_profile->person = $this->personRepository->create($person_input);
		}
		/**
		 * and finally creating user
		 */
		$user_input = $request->only('email', 'password', 'user_type');
		$user_input['profile_id'] = $new_profile->id;
		$user_input['name'] = $new_profile->name;
		$user_input['status'] = $request->admin ? 1 : 0;
		$user_input['password'] = \Hash::make($request->password);
		$new_user = $this->userRepository->create($user_input);

		if (empty($new_user)) {
			return $this->sendResponse($new_profile, 'Cannot Create Users');
		}

		/**
		 * sending sms if user is customer
		 */
		if ($new_user->user_type == 'customer' && !$request->admin) {
			$new_profile->notify(new Registered($new_user));
		}

		/**
		 *
		 * TODO
		 * need to add Push notification and email Notification here
		 */
		$data['user'] = $new_user;
		$data['profile'] = $new_profile;
		if ($new_user->user_type == 'customer') {
			$new_user->attachRole(\App\Models\Role::firstOrCreate(['name' => 'customer']));
			$data['token'] = \JWTAuth::fromUser($new_user);
			return $this->sendResponse($data, 'Sign up successfully. Please verify your account');
		}
		return $this->sendResponse($data, $message);

		//send verification
	}


	public function update_profile(UpdateUserProfileAPIRequest $request, $id)
	{


		/** @var User $user */
		$user = \App\User::where('profile_id', $id)->first();


		/**
		 * preparing inputs for creating profile
		 */
		$profile_input = $request->only('name', 'address_id', 'email', 'contact_number', 'type', 'location_id');
		// $profile_input['identifier'] = $request->id_card_no | $request->registry_number;
		if ($request->id_card_no) {
			// dd($request->id_card_no);
			$profile_input['identifier'] = $request->id_card_no;
		} else {
			$profile_input['identifier'] = $request->registry_number;
		}


		/**
		 * Update Profile
		 */
		$profile = $this->profileRepository->with(['location'])->update($profile_input, $user->profile_id);

		/**
		 * creating and extending profile with organization
		 */
		if ($profile->type == 'organization') {

			$organization = $this->organizationRepository->findByField('profile_id', $profile->id)->first();

			$organization_input = $request->only('contact_person', 'tin');
			$organization_input['registry_number'] = $profile->identifier;
			$organization_input['profile_id'] = $profile->id;
			if ($organization) {
				$profile->organization = $this->organizationRepository->update($organization_input, $organization->id);
			}


		}


		/**
		 * creating and extending profile with person
		 */
		if ($profile->type == 'person') {

			$person = $this->personRepository->findByField('profile_id', $profile->id)->first();

			$person_input = $request->only('date_of_birth');
			$person_input['id_card_no'] = $profile->identifier;
			$person_input['profile_id'] = $profile->id;


			$profile->person = $this->personRepository->update($person_input, $person->id);
		}
		/**
		 * and finally creating user
		 */
		$user_input = $request->only('email', 'user_type', 'status');
		$user_input['profile_id'] = $profile->id;
		$user_input['name'] = $profile->name;
		// $user_input['status'] = 1;
		// $user_input['password'] = \Hash::make($request->password);
		$new_user = $this->userRepository->update($user_input, $user->id);

		$data['user'] = $new_user;
		$data['profile'] = $profile;

		return $this->sendResponse($data, 'Profile Updated');


	}

	public function authenticate(Request $request)
	{
		// grab credentials from the request
		$credentials = $request->only('email', 'password');

		try {
			// attempt to verify the credentials and create a token for the user
			if (!$token = \JWTAuth::attempt($credentials)) {
				// return response()->json(['error' => 'invalid_credentials'], 401);
				return $this->sendError('Invalid Credentials', 401); // please follow same convention, otherwise this make trouble on mobile
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			// return response()->json(['error' => 'could_not_create_token'], 500);
			return $this->sendError('could_not_create_token', 500); // please follow same convention, otherwise this make trouble on mobile
		}

		return $this->sendResponse(['token' => $token, 'user' => auth()->user()], 'Authenticated');
	}

	public function verify(Request $request)
	{
		/**
		 * getting valid token for use the token
		 */
		$token = $this->tokenRepository->pushCriteria(new FindValidTokenCriteria($request))->first();

		/**
		 * checking the query validity
		 */
		if (!$token) {
			return $this->sendError('Invalid Token or Token is expired');
		}

		/**
		 * update user status after valid token
		 */
		$user = $this->userRepository->update(['status' => 1], $token->user_id);

		/**
		 * set token expired after usage of it
		 */
		$this->tokenRepository->update(['expired' => 1], $token->id);


		return $this->sendResponse($user, 'User Verified successfully');

	}

	// TODO adam
	public function resend_token()
	{

		$user = \JWTAuth::parseToken()->authenticate();
		// return $user;
		$profile = $this->profileRepository->find($user->profile_id);

		$profile->notify(new Registered($user));

		return $this->sendResponse([], 'New token sent');
	}

	/**
	 * mail to user with password reset link url
	 * POST /users/password/forget
	 *
	 * @param  string $email
	 *
	 * @return Response
	 */
	public function passwordForget(PasswordForgetRequest $request)
	{

		$user = $this->userRepository->findByField('email', $request->email)->first();
		if (!$user) {
			return $this->sendError('We can\'t find a user with that e-mail address.');
		}
		$token = $this->broker()->createToken($user);
		\Mail::to($user->email)->send(new PasswordReset($token, $user));
		return $this->sendResponse(['sent' => true], "Password reset link has been sent to your email address");

	}

	/**
	 * If user is authenticate then give user profile
	 * POST /users/password/forget
	 *
	 * @param  string $email
	 *
	 * @return Response
	 */
	public function profile(Request $request)
	{
		try {
			$data['user'] = \JWTAuth::parseToken()->authenticate();
			$menu = new \App\Modules\Acl\Menu();
			$data['menu'] = $menu->allowed();
		} catch (\Exception $e) {
			return $this->sendError('Token Expired', 401);
		}

		return $this->sendResponse($data, 'User retreived successfully');


	}

	/**
	 * Change password
	 * POST /users/password/change
	 *
	 * @param  string $old_password , password, pass_confirmation
	 *
	 * @return Response
	 */
	public function passwordChange(ChangePasswordRequest $request)
	{

		try {
			$user = \App\User::find(\JWTAuth::parseToken()->toUser()->id);

		} catch (\JWTException $e) {
			return $this->sendError('could_not_create_token', 500);
		}

		$credentials['password'] = $request->old_password;
		$credentials['email'] = $user->email;

		try {
			// attempt to verify the credentials and create a token for the user
			if (!$token = \JWTAuth::attempt($credentials)) {
				return $this->sendError('The old password is wrong');
			}
		} catch (\JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return $this->sendError('could_not_create_token', 500);
		}

		$user->password = bcrypt($request->password);

		if ($user->save()) {

			return $this->sendResponse($user->toArray(), 'Password change successfully');
		}
	}

	public function updatePassword($id, Request $request)
	{
		$user = $this->userRepository->findWithoutFail($id);
		if (!$user) {
			return $this->sendError('User not found');
		}
		$user->password = bcrypt($request->password);

		if ($user->save()) {
			return $this->sendResponse($user->toArray(), 'Password change successfully');
		}
	}


	// laratrust ACL
	public function get_user_roles($id)
	{
		$roles = \App\Models\Role::all();
		foreach ($roles as $role) {
			if (\DB::table('role_user')->whereUserId($id)->whereRoleId($role->id)->count()) {
				$role->selected = true;
			} else {
				$role->selected = false;
			}
		}
		return $this->sendResponse($roles->toArray(), 'User updated successfully');
	}

	public function role_toggle(Request $request)
	{
		$role = \App\Models\Role::Find($request->role_id);
		$user = \App\User::find($request->user_id);
		if ($user->hasRole($role->name)) {
			$user->detachRole($role);
			return $this->sendResponse(['selected' => false], 'User updated successfully');
		} else {
			$user->attachRole($role);
			return $this->sendResponse(['selected' => true], 'User updated successfully');
		}
	}

	public function permissions($id)
	{
		$permissions = \App\Models\Permission::all();
		foreach ($permissions as $permission) {
			if (\DB::table('permission_user')->whereUserId($id)->wherePermissionId($permission->id)->count()) {
				$permission->selected = true;
			} else {
				$permission->selected = false;
			}
		}
		return $this->sendResponse($permissions->toArray(), 'permissions retrieved successfully !');
	}

	public function permissions_toggle(Request $request)
	{
		$user = \App\User::find($request->user_id);
		$permission = \App\Models\Permission::find($request->permission_id);

		if ($user->hasPermission($permission->name)) {
			$user->detachPermission($permission);
			return $this->sendResponse(['selected' => false], 'User updated successfully');
		} else {
			$user->attachPermission($permission);
			return $this->sendResponse(['selected' => true], 'User updated successfully');
		}
	}

	public function refreshToken()
	{
		try {
			$data['token'] = \JWTAuth::refresh(\JWTAuth::getToken());
			\JWTAuth::setToken($data['token']);
			$data['user'] = \JWTAuth::authenticate($data['token']);
			return $this->sendResponse($data, 'Token');
		} catch (\JWTException $e) {
			return $this->sendError($e->message);
		}

	}
}
