<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAgreementAPIRequest;
use App\Http\Requests\API\UpdateAgreementAPIRequest;
use App\Models\Agreement;
use App\Repositories\AgreementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AgreementController
 * @package App\Http\Controllers\API
 */

class AgreementAPIController extends AppBaseController
{
    /** @var  AgreementRepository */
    private $agreementRepository;

    public function __construct(AgreementRepository $agreementRepo)
    {
        $this->agreementRepository = $agreementRepo;
    }

    /**
     * Display a listing of the Agreement.
     * GET|HEAD /agreements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->agreementRepository->pushCriteria(new RequestCriteria($request));
        $this->agreementRepository->pushCriteria(new LimitOffsetCriteria($request));
        $agreements = $this->agreementRepository->paginate();

        return $this->sendResponse($agreements->toArray(), 'Agreements retrieved successfully');
    }

    /**
     * Store a newly created Agreement in storage.
     * POST /agreements
     *
     * @param CreateAgreementAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAgreementAPIRequest $request)
    {
        $input = $request->all();

        $agreements = $this->agreementRepository->create($input);

        return $this->sendResponse($agreements->toArray(), 'Agreement saved successfully');
    }

    /**
     * Display the specified Agreement.
     * GET|HEAD /agreements/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Agreement $agreement */
        $agreement = $this->agreementRepository->findWithoutFail($id);

        if (empty($agreement)) {
            return $this->sendError('Agreement not found');
        }

        return $this->sendResponse($agreement->toArray(), 'Agreement retrieved successfully');
    }

    /**
     * Update the specified Agreement in storage.
     * PUT/PATCH /agreements/{id}
     *
     * @param  int $id
     * @param UpdateAgreementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAgreementAPIRequest $request)
    {
        $input = $request->all();

        /** @var Agreement $agreement */
        $agreement = $this->agreementRepository->findWithoutFail($id);

        if (empty($agreement)) {
            return $this->sendError('Agreement not found');
        }

        $agreement = $this->agreementRepository->update($input, $id);

        return $this->sendResponse($agreement->toArray(), 'Agreement updated successfully');
    }

    /**
     * Remove the specified Agreement from storage.
     * DELETE /agreements/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Agreement $agreement */
        $agreement = $this->agreementRepository->findWithoutFail($id);

        if (empty($agreement)) {
            return $this->sendError('Agreement not found');
        }

        $agreement->delete();

        return $this->sendResponse($id, 'Agreement deleted successfully');
    }
}
