<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateattributableAPIRequest;
use App\Http\Requests\API\UpdateattributableAPIRequest;
use App\Models\attributable;
use App\Repositories\attributableRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class attributableController
 * @package App\Http\Controllers\API
 */

class attributableAPIController extends AppBaseController
{
    /** @var  attributableRepository */
    private $attributableRepository;

    public function __construct(attributableRepository $attributableRepo)
    {
        $this->attributableRepository = $attributableRepo;
    }

    /**
     * Display a listing of the attributable.
     * GET|HEAD /attributables
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->attributableRepository->pushCriteria(new RequestCriteria($request));
        $this->attributableRepository->pushCriteria(new LimitOffsetCriteria($request));
        $attributables = $this->attributableRepository->all();

        return $this->sendResponse($attributables->toArray(), 'Attributables retrieved successfully');
    }

    /**
     * Store a newly created attributable in storage.
     * POST /attributables
     *
     * @param CreateattributableAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateattributableAPIRequest $request)
    {
        $input = $request->all();

        $attributables = $this->attributableRepository->create($input);

        return $this->sendResponse($attributables->toArray(), 'Attributable saved successfully');
    }

    /**
     * Display the specified attributable.
     * GET|HEAD /attributables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var attributable $attributable */
        $attributable = $this->attributableRepository->findWithoutFail($id);

        if (empty($attributable)) {
            return $this->sendError('Attributable not found');
        }

        return $this->sendResponse($attributable->toArray(), 'Attributable retrieved successfully');
    }

    /**
     * Update the specified attributable in storage.
     * PUT/PATCH /attributables/{id}
     *
     * @param  int $id
     * @param UpdateattributableAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateattributableAPIRequest $request)
    {
        $input = $request->all();

        /** @var attributable $attributable */
        $attributable = $this->attributableRepository->findWithoutFail($id);

        if (empty($attributable)) {
            return $this->sendError('Attributable not found');
        }

        $attributable = $this->attributableRepository->update($input, $id);

        return $this->sendResponse($attributable->toArray(), 'attributable updated successfully');
    }

    /**
     * Remove the specified attributable from storage.
     * DELETE /attributables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var attributable $attributable */
        $attributable = $this->attributableRepository->findWithoutFail($id);

        if (empty($attributable)) {
            return $this->sendError('Attributable not found');
        }

        $attributable->delete();

        return $this->sendResponse($id, 'Attributable deleted successfully');
    }
}
