<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBulkOrderViewAPIRequest;
use App\Http\Requests\API\UpdateBulkOrderViewAPIRequest;
use App\Views\BulkOrderView;
use App\Repositories\BulkOrderViewRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BulkOrderViewController
 * @package App\Http\Controllers\API
 */

class BulkOrderViewAPIController extends AppBaseController
{
    /** @var  BulkOrderViewRepository */
    private $bulkOrderViewRepository;

    public function __construct(BulkOrderViewRepository $bulkOrderViewRepo)
    {
        $this->bulkOrderViewRepository = $bulkOrderViewRepo;
    }

    /**
     * Display a listing of the BulkOrderView.
     * GET|HEAD /bulkOrderViews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->bulkOrderViewRepository->pushCriteria(new RequestCriteria($request));
        $this->bulkOrderViewRepository->pushCriteria(new LimitOffsetCriteria($request));
        $bulkOrderViews = $this->bulkOrderViewRepository->orderBy('id', 'desc')->paginate($request->pageSize);

        return $this->sendResponse($bulkOrderViews->toArray(), 'Bulk Order Views retrieved successfully');
    }

    /**
     * Store a newly created BulkOrderView in storage.
     * POST /bulkOrderViews
     *
     * @param CreateBulkOrderViewAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBulkOrderViewAPIRequest $request)
    {
        $input = $request->all();

        $bulkOrderViews = $this->bulkOrderViewRepository->create($input);

        return $this->sendResponse($bulkOrderViews->toArray(), 'Bulk Order View saved successfully');
    }

    /**
     * Display the specified BulkOrderView.
     * GET|HEAD /bulkOrderViews/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BulkOrderView $bulkOrderView */
        $bulkOrderView = $this->bulkOrderViewRepository->with(['packages.status'])->findWithoutFail($id);

        if (empty($bulkOrderView)) {
            return $this->sendError('Bulk Order View not found');
        }

        return $this->sendResponse($bulkOrderView->toArray(), 'Bulk Order View retrieved successfully');
    }

    /**
     * Update the specified BulkOrderView in storage.
     * PUT/PATCH /bulkOrderViews/{id}
     *
     * @param  int $id
     * @param UpdateBulkOrderViewAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBulkOrderViewAPIRequest $request)
    {
        $input = $request->all();

        /** @var BulkOrderView $bulkOrderView */
        $bulkOrderView = $this->bulkOrderViewRepository->findWithoutFail($id);

        if (empty($bulkOrderView)) {
            return $this->sendError('Bulk Order View not found');
        }

        $bulkOrderView = $this->bulkOrderViewRepository->update($input, $id);

        return $this->sendResponse($bulkOrderView->toArray(), 'BulkOrderView updated successfully');
    }

    /**
     * Remove the specified BulkOrderView from storage.
     * DELETE /bulkOrderViews/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BulkOrderView $bulkOrderView */
        $bulkOrderView = $this->bulkOrderViewRepository->findWithoutFail($id);

        if (empty($bulkOrderView)) {
            return $this->sendError('Bulk Order View not found');
        }

        $bulkOrderView->delete();

        return $this->sendResponse($id, 'Bulk Order View deleted successfully');
    }
}
