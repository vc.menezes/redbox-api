<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateReceiptViewAPIRequest;
use App\Http\Requests\API\UpdateReceiptViewAPIRequest;
use App\Views\ReceiptView;
use App\Repositories\ReceiptViewRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ReceiptViewController
 * @package App\Http\Controllers\API
 */

class ReceiptViewAPIController extends AppBaseController
{
    /** @var  ReceiptViewRepository */
    private $receiptViewRepository;

    public function __construct(ReceiptViewRepository $receiptViewRepo)
    {
        $this->receiptViewRepository = $receiptViewRepo;
    }

    /**
     * Display a listing of the ReceiptView.
     * GET|HEAD /receiptViews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->receiptViewRepository->pushCriteria(new RequestCriteria($request));
        $this->receiptViewRepository->pushCriteria(new LimitOffsetCriteria($request));
        $receiptViews = $this->receiptViewRepository->orderBy('id', 'desc')->paginate($request->pageSize);

        return $this->sendResponse($receiptViews->toArray(), 'Receipt Views retrieved successfully');
    }

    /**
     * Store a newly created ReceiptView in storage.
     * POST /receiptViews
     *
     * @param CreateReceiptViewAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateReceiptViewAPIRequest $request)
    {
        $input = $request->all();

        $receiptViews = $this->receiptViewRepository->create($input);

        return $this->sendResponse($receiptViews->toArray(), 'Receipt View saved successfully');
    }

    /**
     * Display the specified ReceiptView.
     * GET|HEAD /receiptViews/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ReceiptView $receiptView */
        $receiptView = $this->receiptViewRepository->findWithoutFail($id);

        if (empty($receiptView)) {
            return $this->sendError('Receipt View not found');
        }

        return $this->sendResponse($receiptView->toArray(), 'Receipt View retrieved successfully');
    }

    /**
     * Update the specified ReceiptView in storage.
     * PUT/PATCH /receiptViews/{id}
     *
     * @param  int $id
     * @param UpdateReceiptViewAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReceiptViewAPIRequest $request)
    {
        $input = $request->all();

        /** @var ReceiptView $receiptView */
        $receiptView = $this->receiptViewRepository->findWithoutFail($id);

        if (empty($receiptView)) {
            return $this->sendError('Receipt View not found');
        }

        $receiptView = $this->receiptViewRepository->update($input, $id);

        return $this->sendResponse($receiptView->toArray(), 'ReceiptView updated successfully');
    }

    /**
     * Remove the specified ReceiptView from storage.
     * DELETE /receiptViews/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ReceiptView $receiptView */
        $receiptView = $this->receiptViewRepository->findWithoutFail($id);

        if (empty($receiptView)) {
            return $this->sendError('Receipt View not found');
        }

        $receiptView->delete();

        return $this->sendResponse($id, 'Receipt View deleted successfully');
    }
}
