<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateReceiptChequeAPIRequest;
use App\Http\Requests\API\UpdateReceiptChequeAPIRequest;
use App\Models\ReceiptCheque;
use App\Repositories\ReceiptChequeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ReceiptChequeController
 * @package App\Http\Controllers\API
 */

class ReceiptChequeAPIController extends AppBaseController
{
    /** @var  ReceiptChequeRepository */
    private $receiptChequeRepository;

    public function __construct(ReceiptChequeRepository $receiptChequeRepo)
    {
        $this->receiptChequeRepository = $receiptChequeRepo;
    }

    /**
     * Display a listing of the ReceiptCheque.
     * GET|HEAD /receiptCheques
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->receiptChequeRepository->pushCriteria(new RequestCriteria($request));
        $this->receiptChequeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $receiptCheques = $this->receiptChequeRepository->all();

        return $this->sendResponse($receiptCheques->toArray(), 'Receipt Cheques retrieved successfully');
    }

    /**
     * Store a newly created ReceiptCheque in storage.
     * POST /receiptCheques
     *
     * @param CreateReceiptChequeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateReceiptChequeAPIRequest $request)
    {
        $input = $request->all();

        $receiptCheques = $this->receiptChequeRepository->create($input);

        return $this->sendResponse($receiptCheques->toArray(), 'Receipt Cheque saved successfully');
    }

    /**
     * Display the specified ReceiptCheque.
     * GET|HEAD /receiptCheques/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ReceiptCheque $receiptCheque */
        $receiptCheque = $this->receiptChequeRepository->findWithoutFail($id);

        if (empty($receiptCheque)) {
            return $this->sendError('Receipt Cheque not found');
        }

        return $this->sendResponse($receiptCheque->toArray(), 'Receipt Cheque retrieved successfully');
    }

    /**
     * Update the specified ReceiptCheque in storage.
     * PUT/PATCH /receiptCheques/{id}
     *
     * @param  int $id
     * @param UpdateReceiptChequeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReceiptChequeAPIRequest $request)
    {
        $input = $request->all();

        /** @var ReceiptCheque $receiptCheque */
        $receiptCheque = $this->receiptChequeRepository->findWithoutFail($id);

        if (empty($receiptCheque)) {
            return $this->sendError('Receipt Cheque not found');
        }

        $receiptCheque = $this->receiptChequeRepository->update($input, $id);

        return $this->sendResponse($receiptCheque->toArray(), 'ReceiptCheque updated successfully');
    }

    /**
     * Remove the specified ReceiptCheque from storage.
     * DELETE /receiptCheques/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ReceiptCheque $receiptCheque */
        $receiptCheque = $this->receiptChequeRepository->findWithoutFail($id);

        if (empty($receiptCheque)) {
            return $this->sendError('Receipt Cheque not found');
        }

        $receiptCheque->delete();

        return $this->sendResponse($id, 'Receipt Cheque deleted successfully');
    }
}
