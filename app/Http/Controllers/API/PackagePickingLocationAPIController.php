<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePackagePickingLocationAPIRequest;
use App\Http\Requests\API\UpdatePackagePickingLocationAPIRequest;
use App\Models\PackagePickingLocation;
use App\Repositories\PackagePickingLocationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PackagePickingLocationController
 * @package App\Http\Controllers\API
 */

class PackagePickingLocationAPIController extends AppBaseController
{
    /** @var  PackagePickingLocationRepository */
    private $packagePickingLocationRepository;

    public function __construct(PackagePickingLocationRepository $packagePickingLocationRepo)
    {
        $this->packagePickingLocationRepository = $packagePickingLocationRepo;
    }

    /**
     * Display a listing of the PackagePickingLocation.
     * GET|HEAD /packagePickingLocations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->packagePickingLocationRepository->pushCriteria(new RequestCriteria($request));
        $this->packagePickingLocationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $packagePickingLocations = $this->packagePickingLocationRepository->all();

        return $this->sendResponse($packagePickingLocations->toArray(), 'Package Picking Locations retrieved successfully');
    }

    /**
     * Store a newly created PackagePickingLocation in storage.
     * POST /packagePickingLocations
     *
     * @param CreatePackagePickingLocationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePackagePickingLocationAPIRequest $request)
    {
        $input = $request->all();

        $packagePickingLocations = $this->packagePickingLocationRepository->create($input);

        return $this->sendResponse($packagePickingLocations->toArray(), 'Package Picking Location saved successfully');
    }

    /**
     * Display the specified PackagePickingLocation.
     * GET|HEAD /packagePickingLocations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PackagePickingLocation $packagePickingLocation */
        $packagePickingLocation = $this->packagePickingLocationRepository->findWithoutFail($id);

        if (empty($packagePickingLocation)) {
            return $this->sendError('Package Picking Location not found');
        }

        return $this->sendResponse($packagePickingLocation->toArray(), 'Package Picking Location retrieved successfully');
    }

    /**
     * Update the specified PackagePickingLocation in storage.
     * PUT/PATCH /packagePickingLocations/{id}
     *
     * @param  int $id
     * @param UpdatePackagePickingLocationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePackagePickingLocationAPIRequest $request)
    {
        $input = $request->all();

        /** @var PackagePickingLocation $packagePickingLocation */
        $packagePickingLocation = $this->packagePickingLocationRepository->findWithoutFail($id);

        if (empty($packagePickingLocation)) {
            return $this->sendError('Package Picking Location not found');
        }

        $packagePickingLocation = $this->packagePickingLocationRepository->update($input, $id);

        return $this->sendResponse($packagePickingLocation->toArray(), 'PackagePickingLocation updated successfully');
    }

    /**
     * Remove the specified PackagePickingLocation from storage.
     * DELETE /packagePickingLocations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PackagePickingLocation $packagePickingLocation */
        $packagePickingLocation = $this->packagePickingLocationRepository->findWithoutFail($id);

        if (empty($packagePickingLocation)) {
            return $this->sendError('Package Picking Location not found');
        }

        $packagePickingLocation->delete();

        return $this->sendResponse($id, 'Package Picking Location deleted successfully');
    }
}
