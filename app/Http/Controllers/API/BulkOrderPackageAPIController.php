<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBulkOrderPackageAPIRequest;
use App\Http\Requests\API\UpdateBulkOrderPackageAPIRequest;
use App\Models\BulkOrderPackage;
use App\Repositories\BulkOrderPackageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BulkOrderPackageController
 * @package App\Http\Controllers\API
 */

class BulkOrderPackageAPIController extends AppBaseController
{
    /** @var  BulkOrderPackageRepository */
    private $bulkOrderPackageRepository;

    public function __construct(BulkOrderPackageRepository $bulkOrderPackageRepo)
    {
        $this->bulkOrderPackageRepository = $bulkOrderPackageRepo;
    }

    /**
     * Display a listing of the BulkOrderPackage.
     * GET|HEAD /bulkOrderPackages
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->bulkOrderPackageRepository->pushCriteria(new RequestCriteria($request));
        $this->bulkOrderPackageRepository->pushCriteria(new LimitOffsetCriteria($request));
        $bulkOrderPackages = $this->bulkOrderPackageRepository->all();

        return $this->sendResponse($bulkOrderPackages->toArray(), 'Bulk Order Packages retrieved successfully');
    }

    /**
     * Store a newly created BulkOrderPackage in storage.
     * POST /bulkOrderPackages
     *
     * @param CreateBulkOrderPackageAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBulkOrderPackageAPIRequest $request)
    {
        $input = $request->all();

        $bulkOrderPackages = $this->bulkOrderPackageRepository->create($input);

        return $this->sendResponse($bulkOrderPackages->toArray(), 'Bulk Order Package saved successfully');
    }

    /**
     * Display the specified BulkOrderPackage.
     * GET|HEAD /bulkOrderPackages/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BulkOrderPackage $bulkOrderPackage */
        $bulkOrderPackage = $this->bulkOrderPackageRepository->findWithoutFail($id);

        if (empty($bulkOrderPackage)) {
            return $this->sendError('Bulk Order Package not found');
        }

        return $this->sendResponse($bulkOrderPackage->toArray(), 'Bulk Order Package retrieved successfully');
    }

    /**
     * Update the specified BulkOrderPackage in storage.
     * PUT/PATCH /bulkOrderPackages/{id}
     *
     * @param  int $id
     * @param UpdateBulkOrderPackageAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBulkOrderPackageAPIRequest $request)
    {
        $input = $request->all();

        /** @var BulkOrderPackage $bulkOrderPackage */
        $bulkOrderPackage = $this->bulkOrderPackageRepository->findWithoutFail($id);

        if (empty($bulkOrderPackage)) {
            return $this->sendError('Bulk Order Package not found');
        }

        $bulkOrderPackage = $this->bulkOrderPackageRepository->update($input, $id);

        return $this->sendResponse($bulkOrderPackage->toArray(), 'BulkOrderPackage updated successfully');
    }

    /**
     * Remove the specified BulkOrderPackage from storage.
     * DELETE /bulkOrderPackages/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BulkOrderPackage $bulkOrderPackage */
        $bulkOrderPackage = $this->bulkOrderPackageRepository->findWithoutFail($id);

        if (empty($bulkOrderPackage)) {
            return $this->sendError('Bulk Order Package not found');
        }

        $bulkOrderPackage->delete();

        return $this->sendResponse($id, 'Bulk Order Package deleted successfully');
    }
}
