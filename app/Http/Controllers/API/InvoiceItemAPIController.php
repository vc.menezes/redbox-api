<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateInvoiceItemAPIRequest;
use App\Http\Requests\API\UpdateInvoiceItemAPIRequest;
use App\Models\InvoiceItem;
use App\Repositories\InvoiceItemRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class InvoiceItemController
 * @package App\Http\Controllers\API
 */

class InvoiceItemAPIController extends AppBaseController
{
    /** @var  InvoiceItemRepository */
    private $invoiceItemRepository;

    public function __construct(InvoiceItemRepository $invoiceItemRepo)
    {
        $this->invoiceItemRepository = $invoiceItemRepo;
    }

    /**
     * Display a listing of the InvoiceItem.
     * GET|HEAD /invoiceItems
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->invoiceItemRepository->pushCriteria(new RequestCriteria($request));
        $this->invoiceItemRepository->pushCriteria(new LimitOffsetCriteria($request));
        $invoiceItems = $this->invoiceItemRepository->all();

        return $this->sendResponse($invoiceItems->toArray(), 'Invoice Items retrieved successfully');
    }

    /**
     * Store a newly created InvoiceItem in storage.
     * POST /invoiceItems
     *
     * @param CreateInvoiceItemAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInvoiceItemAPIRequest $request)
    {
        $input = $request->all();

        $invoiceItems = $this->invoiceItemRepository->create($input);

        return $this->sendResponse($invoiceItems->toArray(), 'Invoice Item saved successfully');
    }

    /**
     * Display the specified InvoiceItem.
     * GET|HEAD /invoiceItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var InvoiceItem $invoiceItem */
        $invoiceItem = $this->invoiceItemRepository->findWithoutFail($id);

        if (empty($invoiceItem)) {
            return $this->sendError('Invoice Item not found');
        }

        return $this->sendResponse($invoiceItem->toArray(), 'Invoice Item retrieved successfully');
    }

    /**
     * Update the specified InvoiceItem in storage.
     * PUT/PATCH /invoiceItems/{id}
     *
     * @param  int $id
     * @param UpdateInvoiceItemAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInvoiceItemAPIRequest $request)
    {
        $input = $request->all();

        /** @var InvoiceItem $invoiceItem */
        $invoiceItem = $this->invoiceItemRepository->findWithoutFail($id);

        if (empty($invoiceItem)) {
            return $this->sendError('Invoice Item not found');
        }

        $invoiceItem = $this->invoiceItemRepository->update($input, $id);

        return $this->sendResponse($invoiceItem->toArray(), 'InvoiceItem updated successfully');
    }

    /**
     * Remove the specified InvoiceItem from storage.
     * DELETE /invoiceItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var InvoiceItem $invoiceItem */
        $invoiceItem = $this->invoiceItemRepository->findWithoutFail($id);

        if (empty($invoiceItem)) {
            return $this->sendError('Invoice Item not found');
        }

        $invoiceItem->delete();

        return $this->sendResponse($id, 'Invoice Item deleted successfully');
    }
}
