<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePackageDestinationAPIRequest;
use App\Http\Requests\API\UpdatePackageDestinationAPIRequest;
use App\Models\PackageDestination;
use App\Repositories\PackageDestinationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PackageDestinationController
 * @package App\Http\Controllers\API
 */

class PackageDestinationAPIController extends AppBaseController
{
    /** @var  PackageDestinationRepository */
    private $packageDestinationRepository;

    public function __construct(PackageDestinationRepository $packageDestinationRepo)
    {
        $this->packageDestinationRepository = $packageDestinationRepo;
    }

    /**
     * Display a listing of the PackageDestination.
     * GET|HEAD /packageDestinations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->packageDestinationRepository->pushCriteria(new RequestCriteria($request));
        $this->packageDestinationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $packageDestinations = $this->packageDestinationRepository->all();

        return $this->sendResponse($packageDestinations->toArray(), 'Package Destinations retrieved successfully');
    }

    /**
     * Store a newly created PackageDestination in storage.
     * POST /packageDestinations
     *
     * @param CreatePackageDestinationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePackageDestinationAPIRequest $request)
    {
        $input = $request->all();

        $packageDestinations = $this->packageDestinationRepository->create($input);

        return $this->sendResponse($packageDestinations->toArray(), 'Package Destination saved successfully');
    }

    /**
     * Display the specified PackageDestination.
     * GET|HEAD /packageDestinations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PackageDestination $packageDestination */
        $packageDestination = $this->packageDestinationRepository->findWithoutFail($id);

        if (empty($packageDestination)) {
            return $this->sendError('Package Destination not found');
        }

        return $this->sendResponse($packageDestination->toArray(), 'Package Destination retrieved successfully');
    }

    /**
     * Update the specified PackageDestination in storage.
     * PUT/PATCH /packageDestinations/{id}
     *
     * @param  int $id
     * @param UpdatePackageDestinationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePackageDestinationAPIRequest $request)
    {
        $input = $request->all();

        /** @var PackageDestination $packageDestination */
        $packageDestination = $this->packageDestinationRepository->findWithoutFail($id);

        if (empty($packageDestination)) {
            return $this->sendError('Package Destination not found');
        }

        $packageDestination = $this->packageDestinationRepository->update($input, $id);

        return $this->sendResponse($packageDestination->toArray(), 'PackageDestination updated successfully');
    }

    /**
     * Remove the specified PackageDestination from storage.
     * DELETE /packageDestinations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PackageDestination $packageDestination */
        $packageDestination = $this->packageDestinationRepository->findWithoutFail($id);

        if (empty($packageDestination)) {
            return $this->sendError('Package Destination not found');
        }

        $packageDestination->delete();

        return $this->sendResponse($id, 'Package Destination deleted successfully');
    }

    public function list_unique(Request $request){
        $filter_by='';
        if (is_numeric($request->search)) { $filter_by='contact_number'; } else { $filter_by='name';}
        $result = PackageDestination::
        select(\DB::raw('
       count(*) as user_count,
        contact_number,
        max(name) as name,
        max(address) as address,
        max(location_id) as location_id,
        max(medium) as medium
       '))->groupby('contact_number')
            ->where($filter_by, 'like', '%'.$request->search.'%')
            ->with('location')
            ->get();

        return $this->sendResponse($result, 'list unique destination');
    }
}
