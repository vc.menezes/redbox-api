<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAgentCommissionAPIRequest;
use App\Http\Requests\API\UpdateAgentCommissionAPIRequest;
use App\Models\AgentCommission;
use App\Repositories\AgentCommissionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\FilterColumnsCriteria;
use Response;

/**
 * Class AgentCommissionController
 * @package App\Http\Controllers\API
 */

class AgentCommissionAPIController extends AppBaseController
{
    /** @var  AgentCommissionRepository */
    private $agentCommissionRepository;

    public function __construct(AgentCommissionRepository $agentCommissionRepo)
    {
        $this->agentCommissionRepository = $agentCommissionRepo;
    }

    /**
     * Display a listing of the AgentCommission.
     * GET|HEAD /agentCommissions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->agentCommissionRepository->pushCriteria(new RequestCriteria($request));
        $this->agentCommissionRepository->pushCriteria(new LimitOffsetCriteria($request));
        if($request->profile_id){
            $this->agentCommissionRepository->pushCriteria(new FilterColumnsCriteria(['agent_id'=>$request->profile_id]));
        }
        $agentCommissions = $this->agentCommissionRepository->with(['package', 'profile'])->orderBy('id', 'desc')->paginate($request->pageSize);

        return $this->sendResponse($agentCommissions->toArray(), 'Agent Commissions retrieved successfully');
    }

    /**
     * Store a newly created AgentCommission in storage.
     * POST /agentCommissions
     *
     * @param CreateAgentCommissionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAgentCommissionAPIRequest $request)
    {
        $input = $request->all();

        $agentCommissions = $this->agentCommissionRepository->create($input);

        return $this->sendResponse($agentCommissions->toArray(), 'Agent Commission saved successfully');
    }

    /**
     * Display the specified AgentCommission.
     * GET|HEAD /agentCommissions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var AgentCommission $agentCommission */
        $agentCommission = $this->agentCommissionRepository->findWithoutFail($id);

        if (empty($agentCommission)) {
            return $this->sendError('Agent Commission not found');
        }

        return $this->sendResponse($agentCommission->toArray(), 'Agent Commission retrieved successfully');
    }

    /**
     * Update the specified AgentCommission in storage.
     * PUT/PATCH /agentCommissions/{id}
     *
     * @param  int $id
     * @param UpdateAgentCommissionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAgentCommissionAPIRequest $request)
    {
        $input = $request->all();

        /** @var AgentCommission $agentCommission */
        $agentCommission = $this->agentCommissionRepository->findWithoutFail($id);

        if (empty($agentCommission)) {
            return $this->sendError('Agent Commission not found');
        }

        $agentCommission = $this->agentCommissionRepository->update($input, $id);

        return $this->sendResponse($agentCommission->toArray(), 'AgentCommission updated successfully');
    }

    /**
     * Remove the specified AgentCommission from storage.
     * DELETE /agentCommissions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var AgentCommission $agentCommission */
        $agentCommission = $this->agentCommissionRepository->findWithoutFail($id);

        if (empty($agentCommission)) {
            return $this->sendError('Agent Commission not found');
        }

        $agentCommission->delete();

        return $this->sendResponse($id, 'Agent Commission deleted successfully');
    }
}
