<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateReceiptTranferAPIRequest;
use App\Http\Requests\API\UpdateReceiptTranferAPIRequest;
use App\Models\ReceiptTranfer;
use App\Repositories\ReceiptTranferRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ReceiptTranferController
 * @package App\Http\Controllers\API
 */

class ReceiptTranferAPIController extends AppBaseController
{
    /** @var  ReceiptTranferRepository */
    private $receiptTranferRepository;

    public function __construct(ReceiptTranferRepository $receiptTranferRepo)
    {
        $this->receiptTranferRepository = $receiptTranferRepo;
    }

    /**
     * Display a listing of the ReceiptTranfer.
     * GET|HEAD /receiptTranfers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->receiptTranferRepository->pushCriteria(new RequestCriteria($request));
        $this->receiptTranferRepository->pushCriteria(new LimitOffsetCriteria($request));
        $receiptTranfers = $this->receiptTranferRepository->all();

        return $this->sendResponse($receiptTranfers->toArray(), 'Receipt Tranfers retrieved successfully');
    }

    /**
     * Store a newly created ReceiptTranfer in storage.
     * POST /receiptTranfers
     *
     * @param CreateReceiptTranferAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateReceiptTranferAPIRequest $request)
    {
        $input = $request->all();

        $receiptTranfers = $this->receiptTranferRepository->create($input);

        return $this->sendResponse($receiptTranfers->toArray(), 'Receipt Tranfer saved successfully');
    }

    /**
     * Display the specified ReceiptTranfer.
     * GET|HEAD /receiptTranfers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ReceiptTranfer $receiptTranfer */
        $receiptTranfer = $this->receiptTranferRepository->findWithoutFail($id);

        if (empty($receiptTranfer)) {
            return $this->sendError('Receipt Tranfer not found');
        }

        return $this->sendResponse($receiptTranfer->toArray(), 'Receipt Tranfer retrieved successfully');
    }

    /**
     * Update the specified ReceiptTranfer in storage.
     * PUT/PATCH /receiptTranfers/{id}
     *
     * @param  int $id
     * @param UpdateReceiptTranferAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReceiptTranferAPIRequest $request)
    {
        $input = $request->all();

        /** @var ReceiptTranfer $receiptTranfer */
        $receiptTranfer = $this->receiptTranferRepository->findWithoutFail($id);

        if (empty($receiptTranfer)) {
            return $this->sendError('Receipt Tranfer not found');
        }

        $receiptTranfer = $this->receiptTranferRepository->update($input, $id);

        return $this->sendResponse($receiptTranfer->toArray(), 'ReceiptTranfer updated successfully');
    }

    /**
     * Remove the specified ReceiptTranfer from storage.
     * DELETE /receiptTranfers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ReceiptTranfer $receiptTranfer */
        $receiptTranfer = $this->receiptTranferRepository->findWithoutFail($id);

        if (empty($receiptTranfer)) {
            return $this->sendError('Receipt Tranfer not found');
        }

        $receiptTranfer->delete();

        return $this->sendResponse($id, 'Receipt Tranfer deleted successfully');
    }
}
