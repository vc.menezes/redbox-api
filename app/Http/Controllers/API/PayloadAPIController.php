<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePayloadAPIRequest;
use App\Http\Requests\API\UpdatePayloadAPIRequest;
use App\Models\Payload;
use App\Repositories\PayloadRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PayloadController
 * @package App\Http\Controllers\API
 */

class PayloadAPIController extends AppBaseController
{
    /** @var  PayloadRepository */
    private $payloadRepository;

    public function __construct(PayloadRepository $payloadRepo)
    {
        $this->payloadRepository = $payloadRepo;
    }

    /**
     * Display a listing of the Payload.
     * GET|HEAD /payloads
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->payloadRepository->pushCriteria(new RequestCriteria($request));
        $this->payloadRepository->pushCriteria(new LimitOffsetCriteria($request));
        $payloads = $this->payloadRepository->all();

        return $this->sendResponse($payloads->toArray(), 'Payloads retrieved successfully');
    }

    /**
     * Store a newly created Payload in storage.
     * POST /payloads
     *
     * @param CreatePayloadAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePayloadAPIRequest $request)
    {
        $input = $request->all();

        $payloads = $this->payloadRepository->create($input);

        return $this->sendResponse($payloads->toArray(), 'Payload saved successfully');
    }

    /**
     * Display the specified Payload.
     * GET|HEAD /payloads/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Payload $payload */
        $payload = $this->payloadRepository->findWithoutFail($id);

        if (empty($payload)) {
            return $this->sendError('Payload not found');
        }

        return $this->sendResponse($payload->toArray(), 'Payload retrieved successfully');
    }

    /**
     * Update the specified Payload in storage.
     * PUT/PATCH /payloads/{id}
     *
     * @param  int $id
     * @param UpdatePayloadAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePayloadAPIRequest $request)
    {
        $input = $request->all();

        /** @var Payload $payload */
        $payload = $this->payloadRepository->findWithoutFail($id);

        if (empty($payload)) {
            return $this->sendError('Payload not found');
        }

        $payload = $this->payloadRepository->update($input, $id);

        return $this->sendResponse($payload->toArray(), 'Payload updated successfully');
    }

    /**
     * Remove the specified Payload from storage.
     * DELETE /payloads/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Payload $payload */
        $payload = $this->payloadRepository->findWithoutFail($id);

        if (empty($payload)) {
            return $this->sendError('Payload not found');
        }

        $payload->delete();

        return $this->sendResponse($id, 'Payload deleted successfully');
    }
}
