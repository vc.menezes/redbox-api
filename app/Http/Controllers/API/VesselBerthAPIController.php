<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVesselBerthAPIRequest;
use App\Http\Requests\API\UpdateVesselBerthAPIRequest;
use App\Models\VesselBerth;
use App\Repositories\VesselBerthRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class VesselBerthController
 * @package App\Http\Controllers\API
 */

class VesselBerthAPIController extends AppBaseController
{
    /** @var  VesselBerthRepository */
    private $vesselBerthRepository;

    public function __construct(VesselBerthRepository $vesselBerthRepo)
    {
        $this->vesselBerthRepository = $vesselBerthRepo;
    }

    /**
     * Display a listing of the VesselBerth.
     * GET|HEAD /vesselBerths
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->vesselBerthRepository->pushCriteria(new RequestCriteria($request));
        $this->vesselBerthRepository->pushCriteria(new LimitOffsetCriteria($request));
        $vesselBerths = $this->vesselBerthRepository->all();

        return $this->sendResponse($vesselBerths->toArray(), 'Vessel Berths retrieved successfully');
    }

    /**
     * Store a newly created VesselBerth in storage.
     * POST /vesselBerths
     *
     * @param CreateVesselBerthAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVesselBerthAPIRequest $request)
    {
        $input = $request->all();

        $vesselBerths = $this->vesselBerthRepository->create($input);

        return $this->sendResponse($vesselBerths->toArray(), 'Vessel Berth saved successfully');
    }

    /**
     * Display the specified VesselBerth.
     * GET|HEAD /vesselBerths/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var VesselBerth $vesselBerth */
        $vesselBerth = $this->vesselBerthRepository->findWithoutFail($id);

        if (empty($vesselBerth)) {
            return $this->sendError('Vessel Berth not found');
        }

        return $this->sendResponse($vesselBerth->toArray(), 'Vessel Berth retrieved successfully');
    }

    /**
     * Update the specified VesselBerth in storage.
     * PUT/PATCH /vesselBerths/{id}
     *
     * @param  int $id
     * @param UpdateVesselBerthAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVesselBerthAPIRequest $request)
    {
        $input = $request->all();

        /** @var VesselBerth $vesselBerth */
        $vesselBerth = $this->vesselBerthRepository->findWithoutFail($id);

        if (empty($vesselBerth)) {
            return $this->sendError('Vessel Berth not found');
        }

        $vesselBerth = $this->vesselBerthRepository->update($input, $id);

        return $this->sendResponse($vesselBerth->toArray(), 'VesselBerth updated successfully');
    }

    /**
     * Remove the specified VesselBerth from storage.
     * DELETE /vesselBerths/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var VesselBerth $vesselBerth */
        $vesselBerth = $this->vesselBerthRepository->findWithoutFail($id);

        if (empty($vesselBerth)) {
            return $this->sendError('Vessel Berth not found');
        }

        $vesselBerth->delete();

        return $this->sendResponse($id, 'Vessel Berth deleted successfully');
    }
}
