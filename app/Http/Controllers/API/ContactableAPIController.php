<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateContactableAPIRequest;
use App\Http\Requests\API\UpdateContactableAPIRequest;
use App\Models\Contactable;
use App\Repositories\ContactableRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ContactableController
 * @package App\Http\Controllers\API
 */

class ContactableAPIController extends AppBaseController
{
    /** @var  ContactableRepository */
    private $contactableRepository;

    public function __construct(ContactableRepository $contactableRepo)
    {
        $this->contactableRepository = $contactableRepo;
    }

    /**
     * Display a listing of the Contactable.
     * GET|HEAD /contactables
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->contactableRepository->pushCriteria(new RequestCriteria($request));
        $this->contactableRepository->pushCriteria(new LimitOffsetCriteria($request));
        $contactables = $this->contactableRepository->all();

        return $this->sendResponse($contactables->toArray(), 'Contactables retrieved successfully');
    }

    /**
     * Store a newly created Contactable in storage.
     * POST /contactables
     *
     * @param CreateContactableAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateContactableAPIRequest $request)
    {
        $input = $request->all();
        $input['contactable_type'] = 'App\\Models\\'.$input['contactable_type'];

        $contactables = $this->contactableRepository->create($input);

        return $this->sendResponse($contactables->toArray(), 'Contactable saved successfully');
    }

    /**
     * Display the specified Contactable.
     * GET|HEAD /contactables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Contactable $contactable */
        $contactable = $this->contactableRepository->findWithoutFail($id);

        if (empty($contactable)) {
            return $this->sendError('Contactable not found');
        }

        return $this->sendResponse($contactable->toArray(), 'Contactable retrieved successfully');
    }

    /**
     * Update the specified Contactable in storage.
     * PUT/PATCH /contactables/{id}
     *
     * @param  int $id
     * @param UpdateContactableAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateContactableAPIRequest $request)
    {
        $input = $request->all();

        // $input['contactable_type'] = 'App\\Models\\'.$input['contactable_type'];

        /** @var Contactable $contactable */
        $contactable = $this->contactableRepository->findWithoutFail($id);

        if (empty($contactable)) {
            return $this->sendError('Contactable not found');
        }

        $contactable = $this->contactableRepository->update($input, $id);

        return $this->sendResponse($contactable->toArray(), 'Contactable updated successfully');
    }

    /**
     * Remove the specified Contactable from storage.
     * DELETE /contactables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Contactable $contactable */
        $contactable = $this->contactableRepository->findWithoutFail($id);

        if (empty($contactable)) {
            return $this->sendError('Contactable not found');
        }

        $contactable->delete();

        return $this->sendResponse($id, 'Contactable deleted successfully');
    }

    public function profileContact($id)
    {
        if($id) {
            $contacts = $this->contactableRepository->findWhere(['contactable_id', $request->id, 'contactable_type' => 'App\\Models\\Profile'])->all();
            if (empty($contacts)) {
                return $this->sendResponse([], 'Selected Customer has no recoded address');
            }
            return $this->sendResponse($contacts, 'Addresses found');
        }
        return $this->sendResponse([], 'Error');
    }
}
