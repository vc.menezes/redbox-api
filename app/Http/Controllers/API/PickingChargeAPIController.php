<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePickingChargeAPIRequest;
use App\Http\Requests\API\UpdatePickingChargeAPIRequest;
use App\Models\PickingCharge;
use App\Repositories\PickingChargeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PickingChargeController
 * @package App\Http\Controllers\API
 */

class PickingChargeAPIController extends AppBaseController
{
    /** @var  PickingChargeRepository */
    private $pickingChargeRepository;

    public function __construct(PickingChargeRepository $pickingChargeRepo)
    {
        $this->pickingChargeRepository = $pickingChargeRepo;
    }

    /**
     * Display a listing of the PickingCharge.
     * GET|HEAD /pickingCharges
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pickingChargeRepository->pushCriteria(new RequestCriteria($request));
        $this->pickingChargeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $pickingCharges = $this->pickingChargeRepository->with('location')->paginate($request->pageSize);

        return $this->sendResponse($pickingCharges->toArray(), 'Picking Charges retrieved successfully');
    }

    /**
     * Store a newly created PickingCharge in storage.
     * POST /pickingCharges
     *
     * @param CreatePickingChargeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePickingChargeAPIRequest $request)
    {
        $input = $request->except('location');

        $pickingCharges = $this->pickingChargeRepository->create($input);

        return $this->sendResponse($pickingCharges->toArray(), 'Picking Charge saved successfully');
    }

    /**
     * Display the specified PickingCharge.
     * GET|HEAD /pickingCharges/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PickingCharge $pickingCharge */
        $pickingCharge = $this->pickingChargeRepository->findWithoutFail($id);

        if (empty($pickingCharge)) {
            return $this->sendError('Picking Charge not found');
        }

        return $this->sendResponse($pickingCharge->toArray(), 'Picking Charge retrieved successfully');
    }

    /**
     * Update the specified PickingCharge in storage.
     * PUT/PATCH /pickingCharges/{id}
     *
     * @param  int $id
     * @param UpdatePickingChargeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePickingChargeAPIRequest $request)
    {
        $input = $request->except('location');

        /** @var PickingCharge $pickingCharge */
        $pickingCharge = $this->pickingChargeRepository->findWithoutFail($id);

        if (empty($pickingCharge)) {
            return $this->sendError('Picking Charge not found');
        }

        $pickingCharge = $this->pickingChargeRepository->update($input, $id);

        return $this->sendResponse($pickingCharge->toArray(), 'PickingCharge updated successfully');
    }

    /**
     * Remove the specified PickingCharge from storage.
     * DELETE /pickingCharges/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PickingCharge $pickingCharge */
        $pickingCharge = $this->pickingChargeRepository->findWithoutFail($id);

        if (empty($pickingCharge)) {
            return $this->sendError('Picking Charge not found');
        }

        $pickingCharge->delete();

        return $this->sendResponse($id, 'Picking Charge deleted successfully');
    }
}
