<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVesselTripDetailAPIRequest;
use App\Http\Requests\API\UpdateVesselTripDetailAPIRequest;
use App\Models\VesselTripDetail;
use App\Repositories\VesselTripDetailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class VesselTripDetailController
 * @package App\Http\Controllers\API
 */

class VesselTripDetailAPIController extends AppBaseController
{
    /** @var  VesselTripDetailRepository */
    private $vesselTripDetailRepository;

    public function __construct(VesselTripDetailRepository $vesselTripDetailRepo)
    {
        $this->vesselTripDetailRepository = $vesselTripDetailRepo;
    }

    /**
     * Display a listing of the VesselTripDetail.
     * GET|HEAD /vesselTripDetails
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->vesselTripDetailRepository->pushCriteria(new RequestCriteria($request));
        $this->vesselTripDetailRepository->pushCriteria(new LimitOffsetCriteria($request));
        $vesselTripDetails = $this->vesselTripDetailRepository->all();

        return $this->sendResponse($vesselTripDetails->toArray(), 'Vessel Trip Details retrieved successfully');
    }

    /**
     * Store a newly created VesselTripDetail in storage.
     * POST /vesselTripDetails
     *
     * @param CreateVesselTripDetailAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVesselTripDetailAPIRequest $request)
    {
        $input = $request->all();

        $vesselTripDetails = $this->vesselTripDetailRepository->create($input);

        return $this->sendResponse($vesselTripDetails->toArray(), 'Vessel Trip Detail saved successfully');
    }

    /**
     * Display the specified VesselTripDetail.
     * GET|HEAD /vesselTripDetails/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var VesselTripDetail $vesselTripDetail */
        $vesselTripDetail = $this->vesselTripDetailRepository->findWithoutFail($id);

        if (empty($vesselTripDetail)) {
            return $this->sendError('Vessel Trip Detail not found');
        }

        return $this->sendResponse($vesselTripDetail->toArray(), 'Vessel Trip Detail retrieved successfully');
    }

    /**
     * Update the specified VesselTripDetail in storage.
     * PUT/PATCH /vesselTripDetails/{id}
     *
     * @param  int $id
     * @param UpdateVesselTripDetailAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVesselTripDetailAPIRequest $request)
    {
        $input = $request->all();

        /** @var VesselTripDetail $vesselTripDetail */
        $vesselTripDetail = $this->vesselTripDetailRepository->findWithoutFail($id);

        if (empty($vesselTripDetail)) {
            return $this->sendError('Vessel Trip Detail not found');
        }

        $vesselTripDetail = $this->vesselTripDetailRepository->update($input, $id);

        return $this->sendResponse($vesselTripDetail->toArray(), 'VesselTripDetail updated successfully');
    }

    /**
     * Remove the specified VesselTripDetail from storage.
     * DELETE /vesselTripDetails/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var VesselTripDetail $vesselTripDetail */
        $vesselTripDetail = $this->vesselTripDetailRepository->findWithoutFail($id);

        if (empty($vesselTripDetail)) {
            return $this->sendError('Vessel Trip Detail not found');
        }

        $vesselTripDetail->delete();

        return $this->sendResponse($id, 'Vessel Trip Detail deleted successfully');
    }
}
