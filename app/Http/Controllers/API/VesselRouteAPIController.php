<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVesselRouteAPIRequest;
use App\Http\Requests\API\UpdateVesselRouteAPIRequest;
use App\Models\VesselRoute;
use App\Repositories\VesselRouteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class VesselRouteController
 * @package App\Http\Controllers\API
 */
class VesselRouteAPIController extends AppBaseController
{
    /** @var  VesselRouteRepository */
    private $vesselRouteRepository;

    public function __construct(VesselRouteRepository $vesselRouteRepo)
    {
        $this->vesselRouteRepository = $vesselRouteRepo;
    }

    /**
     * Display a listing of the VesselRoute.
     * GET|HEAD /vesselRoutes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->vesselRouteRepository->pushCriteria(new RequestCriteria($request));
        $this->vesselRouteRepository->pushCriteria(new LimitOffsetCriteria($request));
        $vesselRoutes = $this->vesselRouteRepository->all();

        return $this->sendResponse($vesselRoutes->toArray(), 'Vessel Routes retrieved successfully');
    }

    /**
     * Store a newly created VesselRoute in storage.
     * POST /vesselRoutes
     *
     * @param CreateVesselRouteAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVesselRouteAPIRequest $request)
    {
        $input = $request->except('location');

        $data = $this->vesselRouteRepository->create($input);

        $vesselRoutes = $this->vesselRouteRepository->with('location')->findWithoutFail($data->id);


        return $this->sendResponse($vesselRoutes->toArray(), 'Vessel Route saved successfully');
    }

    /**
     * Display the specified VesselRoute.
     * GET|HEAD /vesselRoutes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var VesselRoute $vesselRoute */
        $vesselRoute = $this->vesselRouteRepository->findWithoutFail($id);

        if (empty($vesselRoute)) {
            return $this->sendError('Vessel Route not found');
        }

        return $this->sendResponse($vesselRoute->toArray(), 'Vessel Route retrieved successfully');
    }

    /**
     * Update the specified VesselRoute in storage.
     * PUT/PATCH /vesselRoutes/{id}
     *
     * @param  int $id
     * @param UpdateVesselRouteAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVesselRouteAPIRequest $request)
    {
        $input = $request->all();

        /** @var VesselRoute $vesselRoute */
        $vesselRoute = $this->vesselRouteRepository->findWithoutFail($id);

        if (empty($vesselRoute)) {
            return $this->sendError('Vessel Route not found');
        }

        $vesselRoute = $this->vesselRouteRepository->update($input, $id);

        return $this->sendResponse($vesselRoute->toArray(), 'VesselRoute updated successfully');
    }

    /**
     * Remove the specified VesselRoute from storage.
     * DELETE /vesselRoutes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var VesselRoute $vesselRoute */
        $vesselRoute = $this->vesselRouteRepository->findWithoutFail($id);

        if (empty($vesselRoute)) {
            return $this->sendError('Vessel Route not found');
        }

        $vesselRoute->delete();

        return $this->sendResponse($id, 'Vessel Route deleted successfully');
    }

    public function vessel($vessel_id)
    {
        $result = [];
        $routes = $this->vesselRouteRepository->findWhere(['parent_id' => NULL]);
        foreach ($routes as $route) {
            $tree = new \App\Modules\Tree\Tree($route);
            $route->child = $tree->getTree();
        }

        return $this->sendResponse($routes, 'VesselRoute list successfully');

    }
}
