<?php

namespace App\Http\Controllers\API;

use App\Criteria\Commission\ByMonthCriteria;
use App\Criteria\FilterColumnsCriteria;
use App\Criteria\WithTrashCriteria;
use App\Http\Requests\API\CreateCommissionPaymentAPIRequest;
use App\Http\Requests\API\UpdateCommissionPaymentAPIRequest;
use App\Models\AgentPaymentPost;
use App\Models\BankAccount;
use App\Models\Commission;
use App\Models\CommissionPayment;
use App\Repositories\CommissionPaymentCommissionRepository;
use App\Repositories\CommissionPaymentRepository;
use App\Repositories\CommissionRepository;
use App\Traits\Remark;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CommissionPaymentController
 * @package App\Http\Controllers\API
 */
class CommissionPaymentAPIController extends AppBaseController
{
	/** @var  CommissionPaymentRepository */
	private $commissionPaymentRepository;
	private $commissionPaymentCommissionRepository;
	private $commissionRepository;
	use Remark;


	public function __construct(
		CommissionPaymentRepository $commissionPaymentRepo,
		CommissionRepository $commissionRepo,
		CommissionPaymentCommissionRepository $commissionPaymentCommissionRepo
	)
	{
		$this->commissionPaymentRepository = $commissionPaymentRepo;
		$this->commissionRepository = $commissionRepo;
		$this->commissionPaymentCommissionRepository = $commissionPaymentCommissionRepo;
	}

	/**
	 * Display a listing of the CommissionPayment.
	 * GET|HEAD /commissionPayments
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->commissionPaymentRepository->pushCriteria(new RequestCriteria($request));
		$this->commissionPaymentRepository->pushCriteria(new LimitOffsetCriteria($request));
		$this->commissionPaymentRepository->pushCriteria(new WithTrashCriteria());
		$commissionPayments = $this->commissionPaymentRepository->with(['bankAccount.profile'])->paginate();

		return $this->sendResponse($commissionPayments->toArray(), 'Commission Payments retrieved successfully');
	}

	/**
	 * Store a newly created CommissionPayment in storage.
	 * POST /commissionPayments
	 *
	 * @param CreateCommissionPaymentAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$input = $request->only('reference_number', 'date', 'remarks');
		$AgentPaymentPost = AgentPaymentPost::find($request->agent_payment_post_id);
		// getting bank account number

		$bank_account = BankAccount::whereProfileId($AgentPaymentPost->profile_id)->first();

		if ($bank_account) {
			$input['bank_account_id'] = $bank_account->id;
			$input['profile_id'] = $AgentPaymentPost->profile_id;
		}

		$input['date'] = Carbon::parse($request->date);

		$AgentPaymentPostDate = Carbon::parse($AgentPaymentPost->date);

		// get all commission for
		$this->commissionRepository->pushCriteria(new FilterColumnsCriteria(['status' => 1, 'profile_id' => $AgentPaymentPost->profile_id]));
		$this->commissionRepository->pushCriteria(new ByMonthCriteria($AgentPaymentPostDate->month));
		$commissions = $this->commissionRepository->get();


		if (!count($commissions)) {
			return $this->sendResponse([], 'Invalid Request commission not found');
		}

		$input['amount'] = $commissions->sum('total');

		$commissionPayments = $this->commissionPaymentRepository->create($input);

		foreach ($commissions as $commission) {
			$data = [
				'commission_id' => $commission->id,
				'commission_payment_id' => $commissionPayments->id,
			];
			$this->commissionPaymentCommissionRepository->create($data);
			$commission->status = 2;
			$commission->save();
		}

		$AgentPaymentPost->status = 0;
		$AgentPaymentPost->save();


		return $this->sendResponse($commissionPayments->toArray(), 'Commission Payment saved successfully');
	}

	/**
	 * Display the specified CommissionPayment.
	 * GET|HEAD /commissionPayments/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var CommissionPayment $commissionPayment */
		$commissionPayment = $this->commissionPaymentRepository->findWithoutFail($id);

		if (empty($commissionPayment)) {
			return $this->sendError('Commission Payment not found');
		}

		return $this->sendResponse($commissionPayment->toArray(), 'Commission Payment retrieved successfully');
	}

	/**
	 * Update the specified CommissionPayment in storage.
	 * PUT/PATCH /commissionPayments/{id}
	 *
	 * @param  int $id
	 * @param UpdateCommissionPaymentAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateCommissionPaymentAPIRequest $request)
	{
		$input = $request->all();

		/** @var CommissionPayment $commissionPayment */
		$commissionPayment = $this->commissionPaymentRepository->findWithoutFail($id);

		if (empty($commissionPayment)) {
			return $this->sendError('Commission Payment not found');
		}

		$commissionPayment = $this->commissionPaymentRepository->update($input, $id);

		return $this->sendResponse($commissionPayment->toArray(), 'CommissionPayment updated successfully');
	}

	/**
	 * Remove the specified CommissionPayment from storage.
	 * DELETE /commissionPayments/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		/** @var CommissionPayment $commissionPayment */
		$commissionPayment = $this->commissionPaymentRepository->findWithoutFail($id);


		if (empty($commissionPayment)) {
			return $this->sendError('Commission Payment not found');
		}
		//creating remarks for the deleting
		$this->require_remarks($commissionPayment, $request);

		foreach ($commissionPayment->commissions as $commission) {
			$commission->commission()->update(['status' => 1]);
			$commission->delete();
		}

		$commissionPayment->delete();


		return $this->sendResponse($id, 'Commission Payment deleted successfully');
	}
}
