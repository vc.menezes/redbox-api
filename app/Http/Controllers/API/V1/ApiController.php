<?php

namespace App\Http\Controllers\API\V1;

use App\Models\Rate;
use App\Modules\Finance\Shipping;
use App\Repositories\RateRepository;
use App\Repositories\LocationRepository;
use App\Repositories\BulkOrderRepository;
use App\Repositories\BulkOrderPackageRepository;
use App\Repositories\PackageRepository;
use App\Repositories\TrackingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Commodity;
use App\Jobs\GiveSequence;
use App\Http\Requests\API\v1\CreateBulkOrder;
use App\Http\Requests\API\v1\CalculateRate;
use App\Http\Requests\API\v1\Authenticate;
use App\Traits\Jwt;
use App\Models\Setting;

/**
 * Class RateController
 * @package App\Http\Controllers\API
 */

class ApiController extends AppBaseController
{
    /** @var  RateRepository */
    private $rateRepository;
    private $locationRepository;
    private $packageRepository;
    private $bulkOrderRepository;
    private $bulkOrderPackageRepository;
    private $trackingRepository;
    use Jwt;


    public function __construct(RateRepository $rateRepo, 
                                LocationRepository $locationRepo,
                                BulkOrderRepository $bulkOrderRepo,
                                PackageRepository $packageRepo,
                                BulkOrderPackageRepository $bulkOrderPackageRepository,
                                TrackingRepository $trackingRepo
                                )
    {
        $this->rateRepository = $rateRepo;
        $this->locationRepository = $locationRepo;
        $this->packageRepository = $packageRepo;
        $this->bulkOrderRepository = $bulkOrderRepo;
        $this->bulkOrderPackageRepository = $bulkOrderPackageRepository;
        $this->trackingRepository = $trackingRepo;
    }

    /**
     * POST /v1/rates/calculate
     * 
     * @param Request $request
     * weight  :  required [100]
     * medium  :  required [sea or air]
     * type    :  required [local or international]
     * from_id :  [location_id]:nullable
     * to_id   :  [location_id]:nullable
     * @return mixed
     */
    public function calculate(CalculateRate $request){
        $input = $request->all();

        $input['weight'] = floor($request->weight);
        $input['operator'] = '=';

        if($request->weight<1){
            $input['weight'] = ceil($request->weight);
            $input['operator'] = '<';
        }

        $input['type'] = $request->type ? $request->type : 'local';
        
        $input['medium'] = $request->medium ? $request->medium : 'sea';
        

        if($request->from_id) {
            $input['from_id'] = $request->from_id;
        }

        if($request->to_id) {
            $input['to_id'] = $request->to_id;
        }

        $rate = $this->rateRepository->findWhere($input)->first();

        if(empty($rate)) {
            return $this->sendError('Rate not calculated');
        }

        $data['currency'] = 'MVR';
        $data['rate'] = $rate->rate;
        $tax = Setting::where('key', 'GST')->first();
        $data['tax'] = (int)$tax->value;
        $data['total'] = $rate->rate * (1 + ($data['tax'] / 100));

        return $this->sendResponse($data, 'Rate');

    }

    /**
     * Get all islands
     * GET /v1/islands
     *
     * @return Response
     */
    public function getIslands(Request $request) {
        $this->locationRepository->pushCriteria(new RequestCriteria($request));
       
        $location = $this->locationRepository->findByField('type', 'island')->all(['id', 'name', 'prefix', 'code']);
        
        return $this->sendResponse($location, 'Island retrieved successfully');
    }


    /**
     * Authenticate User
     * GET /v1/authenticate
     *
     * @return Response
     */
    public function authenticate(Authenticate $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = \JWTAuth::attempt($credentials)) {
                // return response()->json(['error' => 'invalid_credentials'], 401);
                return $this->sendError('Invalid Credentials', 401); // please follow same convention, otherwise this make trouble on mobile
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            // return response()->json(['error' => 'could_not_create_token'], 500);
            return $this->sendError('could_not_create_token', 500); // please follow same convention, otherwise this make trouble on mobile
        }

        return $this->sendResponse(['token'=>$token], 'Authenticated');
    }

    /**
     * Accepted external orders
     * POST v1/order/create
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function createOrder(CreateBulkOrder $request)
    {
        try {

            // Order: {
            //     weight: number | float,
            
            //     reference_no?: string,//for different tracking number, not required
            
            //     qty: number 
            
            //     UOM: string //eg. package | box | bag,
            
            //     item_name: string //max length 150,
            
            //     description?: string // not required, max length 190
            
            // receiver_name: string,
            
            // receiver_address: string,
            
            // receiver_contact_number: number,
            
            // receiver_location_id: string, // island name eg. S hithadhoo
            
            // }
            
            //TODO @adam
            // before proceed any db actin please validate all orders
            
            if(!is_array($request->orders) || !count($request->orders)) {
                return $this->sendError('Orders items are required or Orders should be an array');
            }
         

            $orders = collect($request->orders);
            
            

            $user = $this->getUser();
            $profile_id = $request->profile_id ? $request->profile_id : $user->profile_id;
            $input = $request->except('orders');
            $input['profile_id'] = $profile_id;
            $input['weight'] = $orders->sum('weight');


            $bulkOrders = $this->bulkOrderRepository->create($input);


            foreach ($request->orders as $order) {
                $package = [
                    'barcode' => '-',
                    'code' => '-',
                    'type' => 'order',
                    'flag' => 'bulk',
                    'profile_id' => $profile_id,
                    'weight' => $order['weight'],
                    'reference_no' => $order['reference_no'],
                    'description' => $order['description'],
                ];

                $packages = $this->packageRepository->create($package);

                GiveSequence::dispatch($packages, 'barcode', 'order_barcode');

                GiveSequence::dispatch($packages, 'code', 'order_serial');

                $this->trackingRepository->createNewTracking($packages, null, 8, 455);

                $this->bulkOrderPackageRepository->create(['bulk_order_id'=>$bulkOrders->id,'package_id'=>$packages->id]);


                //create of get commodity by name
                $commodity = Commodity::firstOrCreate(['name' => $order['item_name']]);

                $packages->items()->create([
                    'qty' => $order['qty'],
                    'description' => $order['description'],
                    'type' => 'parcel',
                    'UOM' => $order['UOM'],
                    'commodity_id' => $commodity->id,
                ]);

            
                $packages->package_destination()->create([
                    'name' => $order['receiver_name'],
                    'address' => $order['receiver_address'],
                    'contact_number' => $order['receiver_contact_number'],
                    'location_id' => $order['receiver_location_id'],
                    'medium' => 'sea', //air or sea ? @adam i dnt know
                ]);
            }

            return $this->sendResponse($bulkOrders->toArray(), 'Order created successfully');

        } catch (Exception $e) {
            return $this->sendError('Server error. Please contact redbox. LOL ;)');
            // return $this->sendError($e->getMessage());
        }
    }
}