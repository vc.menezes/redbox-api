<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBlockAPIRequest;
use App\Http\Requests\API\UpdateBlockAPIRequest;
use App\Models\Block;
use App\Repositories\BlockRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\FilterColumnsCriteria;
use Response;

/**
 * Class BlockController
 * @package App\Http\Controllers\API
 */

class BlockAPIController extends AppBaseController
{
    /** @var  BlockRepository */
    private $blockRepository;

    public function __construct(BlockRepository $blockRepo)
    {
        $this->blockRepository = $blockRepo;
    }

    /**
     * Display a listing of the Block.
     * GET|HEAD /blocks
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if($request->rack) {
            $racks = $this->blockRepository->pushCriteria(new FilterColumnsCriteria(['id' => $request->rack]));
        }
        $this->blockRepository->pushCriteria(new RequestCriteria($request));
        $this->blockRepository->pushCriteria(new LimitOffsetCriteria($request));
        $data['blocks'] = $this->blockRepository->with(['child.child.packages.package'])->findByField('name', 'rack')->all();
        $data['all'] =  Block::whereName('rack')->get();

        return $this->sendResponse($data, 'Blocks retrieved successfully');
    }

    /**
     * Store a newly created Block in storage.
     * POST /blocks
     *
     * @param CreateBlockAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBlockAPIRequest $request)
    {
        $input = $request->only('parent_id', 'name', 'code', 'type');
        
        if($request->order == 1) {
            $input['name'] = $request->type == 0 ? 'row' : 'column';
            $row = $this->blockRepository->create([
                'parent_id' => $input['parent_id'],
                'name' => $input['name'],
                'code' => $input['name']
            ]);
            $input['name'] = $request->type == 1 ? 'row' : 'column';
            $input['parent_id'] = $row->id;
            
            $blocks = $this->blockRepository->create($input);

        } else {
            $blocks = $this->blockRepository->create($input);
        }

        

        return $this->sendResponse($blocks->toArray(), 'Block saved successfully');
    }

    /**
     * Display the specified Block.
     * GET|HEAD /blocks/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Block $block */
        $block = $this->blockRepository->with(['child.child.packages.package'])->findWithoutFail($id);

        if (empty($block)) {
            return $this->sendError('Block not found');
        }

        return $this->sendResponse($block->toArray(), 'Block retrieved successfully');
    }

    /**
     * Update the specified Block in storage.
     * PUT/PATCH /blocks/{id}
     *
     * @param  int $id
     * @param UpdateBlockAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBlockAPIRequest $request)
    {
        $input = $request->only(['code']);

        /** @var Block $block */
        $block = $this->blockRepository->findWithoutFail($id);

        if (empty($block)) {
            return $this->sendError('Block not found');
        }

        $block = $this->blockRepository->with('packages')->update($input, $id);

        return $this->sendResponse($block->toArray(), 'Block updated successfully');
    }

    /**
     * Remove the specified Block from storage.
     * DELETE /blocks/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Block $block */
        $block = $this->blockRepository->findWithoutFail($id);
        if (empty($block)) {
            return $this->sendError('Block not found');
        }
        if($block->parent_id) {
            $parants = Block::with('child')->find($block->parent_id);
        }
        $block->delete();

        if($block->parent_id && count($parants->child) == 1) {
            $parants->delete();
        }
    

        return $this->sendResponse($id, 'Block deleted successfully');
    }

    public function getRacks(Request $request)
    {
        if($request->id) {
            $rack = \App\Models\Block::where(['id' => $request->id, 'name' => 'rack'])->first();
            if($rack) {
                $racks = [];
                if(count($rack->child)) {
                    foreach ($rack->child as $row) {
                        foreach($row->child as $col) {
                            $racks[] = $col;
                        }
                        
                    }
                }
    
                if(count($racks)) {
                    return $this->sendResponse($racks, 'Blocks retrieved successfully');
                }
            }
            
        }
        $racks = $this->blockRepository->pushCriteria(new FilterColumnsCriteria(['name' => 'rack']));

        return $this->sendResponse($racks->all(), 'Blocks retrieved successfully');
    }
}
