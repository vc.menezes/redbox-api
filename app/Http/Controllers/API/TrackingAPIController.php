<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTrackingAPIRequest;
use App\Http\Requests\API\UpdateTrackingAPIRequest;
use App\Models\Tracking;
use App\Repositories\TrackingRepository;
use App\Repositories\PackageRepository;
use App\Repositories\AgentCommissionRepository;
use App\Repositories\CommissionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TrackingController
 * @package App\Http\Controllers\API
 */
class TrackingAPIController extends AppBaseController
{
    /** @var  TrackingRepository */
    private $trackingRepository;

    public function __construct(TrackingRepository $trackingRepo, PackageRepository $packageRepo, AgentCommissionRepository $agentCommissionRepo, CommissionRepository $commissionRepo)
    {
        $this->trackingRepository = $trackingRepo;
        $this->packageRepository = $packageRepo;
        $this->commissionRepository = $commissionRepo;
        $this->agentCommissionRepository = $agentCommissionRepo;
    }

    /**
     * Display a listing of the Tracking.
     * GET|HEAD /trackings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->trackingRepository->pushCriteria(new RequestCriteria($request));
        $this->trackingRepository->pushCriteria(new LimitOffsetCriteria($request));
        $trackings = $this->trackingRepository->with('package')->paginate($request->pageSize);

        return $this->sendResponse($trackings->toArray(), 'Trackings retrieved successfully');
    }

    /**
     * Store a newly created Tracking in storage.
     * POST /trackings
     *
     * @param CreateTrackingAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTrackingAPIRequest $request)
    {
        $input = $request->all();

        $trackings = $this->trackingRepository->create($input);

        return $this->sendResponse($trackings->toArray(), 'Tracking saved successfully');
    }

    /**
     * Display the specified Tracking.
     * GET|HEAD /trackings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Tracking $tracking */
        $tracking = $this->trackingRepository->findWithoutFail($id);

        if (empty($tracking)) {
            return $this->sendError('Tracking not found');
        }

        return $this->sendResponse($tracking->toArray(), 'Tracking retrieved successfully');
    }

    /**
     * Update the specified Tracking in storage.
     * PUT/PATCH /trackings/{id}
     *
     * @param  int $id
     * @param UpdateTrackingAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->only('estimated_delivery_date', 'status');

        /** @var Tracking $tracking */
        $tracking = $this->trackingRepository->with('package.orders')->findWithoutFail($id);

        if (empty($tracking)) {
            return $this->sendError('Tracking not found');
        }
        $tracking = $this->trackingRepository->update($input, $id);
        if ($tracking->package->type == 'shipment') {
            $this->trackingRepository->updateETA($tracking->package, $input['estimated_delivery_date']);
            // foreach ($tracking->package->orders as $order) {
            //     $orderTracking = \App\Models\Tracking::where('package_id', $order->id)->first();
            //     $orderTracking->estimated_delivery_date = $input['estimated_delivery_date'];
            //     $orderTracking->save();
            // }
        }

        return $this->sendResponse($tracking->toArray(), 'Tracking updated successfully');
    }

    /**
     * Remove the specified Tracking from storage.
     * DELETE /trackings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Tracking $tracking */
        $tracking = $this->trackingRepository->findWithoutFail($id);

        if (empty($tracking)) {
            return $this->sendError('Tracking not found');
        }

        $tracking->delete();

        return $this->sendResponse($id, 'Tracking deleted successfully');
    }


    /**
     * Display the specified Tracking.
     * GET|HEAD /trackings/search/{itracking_nod}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function getTracking($tracking_no)
    {
        $tracking = $this->trackingRepository->with([
            'tracking_log',
            'tracking_log.location',
            'tracking_log.status',
            'tracking_log.country',
            'package',
            'package.items',
            'package.picking_location.address.location',
            'package.package_destination',
            'package.package_destination.location',
        ])->findByField('tracking_no', $tracking_no)->first();

        if (empty($tracking)) {
            return $this->sendError('Tracking not found');
        }

        $count = $tracking->package->items->count();
        $tracking->toArray();
        unset($tracking['package']['items']);
        $tracking['package']['items_count'] = $count;


        return $this->sendResponse($tracking, 'Tracking retrieved successfully');

    }


    public function log($code)
    {
        /** @var Tracking $tracking */
        $tracking = $this->trackingRepository->with(['tracking_log.country', 'package.profile.location', 'package.package_destination.location', 'package.item'])->findByField('tracking_no', $code)->first();

        if (empty($tracking)) {
            return $this->sendError('Tracking not found');
        }

        return $this->sendResponse($tracking->toArray(), 'Tracking retrieved successfully');
    }

    public function getPackageTrackingLogs($id)
    {
        /** @var Tracking $tracking */
        $tracking = $this->trackingRepository->with(['tracking_log.country', 'tracking_log.location', 'tracking_log.status'])->findByField('package_id', $id)->first();

        if (empty($tracking)) {
            return $this->sendError('Tracking not found');
        }

        return $this->sendResponse($tracking->toArray(), 'Tracking retrieved successfully');
    }

    /**
     * Agent update when shipment reveived
     * PUT shipments/update/tracking/{package_id}
     *
     * @param  int $package_id
     *
     * @return Response
     */
    public function updateShipmentTracking($id, Request $request)
    {

			  //find package for update
        $package = $this->packageRepository->findWithoutFail($id);

        if (empty($package)) {
            return $this->sendError('Tracking not found');
        }

        // return $request;

        try {
        	  //Setting tracking status
            $package->tracking()->update(['status' => 0]);

            //update all the children package tracking to parent tracking status
            $this->trackingRepository->updateChildTrackingLogs($package, 5, $request->location_id, $request->description);

            //get package to show :P why should i comment
            $package = $this->packageRepository->with(['child', 'package_destination.location', 'tracking.tracking_log', 'profile', 'commission.details.package_view', 'vessel.vessel'])->find($package->id);

            return $this->sendResponse(['update' => true, 'package' => $package->toArray()], 'Tracking updated successfully');
        } catch (\Exception $e) {
            // return $this->sendError($e->getMessage());
            return $this->sendError('Something went wrong. Please try again later');
        }

        // return $this->sendResponse(['update' => true], 'Tracking updated successfully');
    }


    public function returnToMale($id, Request $request)
    {
        $package = $this->packageRepository->findWithoutFail($id);

        if (empty($package)) {
            return $this->sendError('Something went wrong. Please try again later');
        }

        $package->remarks()->create([
            'body' => $request->description,
            'profile_id' => $request->profile_id
        ]);
        $package->tracking()->update(['status' => 0]);

        $this->trackingRepository->createTrackingLog($package, 9, $request->location_id, $request->description);
        return $this->sendResponse(['update' => true], 'Request submitted');

    }

}
