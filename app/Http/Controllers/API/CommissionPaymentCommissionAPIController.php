<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCommissionPaymentCommissionAPIRequest;
use App\Http\Requests\API\UpdateCommissionPaymentCommissionAPIRequest;
use App\Models\CommissionPaymentCommission;
use App\Repositories\CommissionPaymentCommissionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CommissionPaymentCommissionController
 * @package App\Http\Controllers\API
 */

class CommissionPaymentCommissionAPIController extends AppBaseController
{
    /** @var  CommissionPaymentCommissionRepository */
    private $commissionPaymentCommissionRepository;

    public function __construct(CommissionPaymentCommissionRepository $commissionPaymentCommissionRepo)
    {
        $this->commissionPaymentCommissionRepository = $commissionPaymentCommissionRepo;
    }

    /**
     * Display a listing of the CommissionPaymentCommission.
     * GET|HEAD /commissionPaymentCommissions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->commissionPaymentCommissionRepository->pushCriteria(new RequestCriteria($request));
        $this->commissionPaymentCommissionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $commissionPaymentCommissions = $this->commissionPaymentCommissionRepository->all();

        return $this->sendResponse($commissionPaymentCommissions->toArray(), 'Commission Payment Commissions retrieved successfully');
    }

    /**
     * Store a newly created CommissionPaymentCommission in storage.
     * POST /commissionPaymentCommissions
     *
     * @param CreateCommissionPaymentCommissionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCommissionPaymentCommissionAPIRequest $request)
    {
        $input = $request->all();

        $commissionPaymentCommissions = $this->commissionPaymentCommissionRepository->create($input);

        return $this->sendResponse($commissionPaymentCommissions->toArray(), 'Commission Payment Commission saved successfully');
    }

    /**
     * Display the specified CommissionPaymentCommission.
     * GET|HEAD /commissionPaymentCommissions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CommissionPaymentCommission $commissionPaymentCommission */
        $commissionPaymentCommission = $this->commissionPaymentCommissionRepository->findWithoutFail($id);

        if (empty($commissionPaymentCommission)) {
            return $this->sendError('Commission Payment Commission not found');
        }

        return $this->sendResponse($commissionPaymentCommission->toArray(), 'Commission Payment Commission retrieved successfully');
    }

    /**
     * Update the specified CommissionPaymentCommission in storage.
     * PUT/PATCH /commissionPaymentCommissions/{id}
     *
     * @param  int $id
     * @param UpdateCommissionPaymentCommissionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCommissionPaymentCommissionAPIRequest $request)
    {
        $input = $request->all();

        /** @var CommissionPaymentCommission $commissionPaymentCommission */
        $commissionPaymentCommission = $this->commissionPaymentCommissionRepository->findWithoutFail($id);

        if (empty($commissionPaymentCommission)) {
            return $this->sendError('Commission Payment Commission not found');
        }

        $commissionPaymentCommission = $this->commissionPaymentCommissionRepository->update($input, $id);

        return $this->sendResponse($commissionPaymentCommission->toArray(), 'CommissionPaymentCommission updated successfully');
    }

    /**
     * Remove the specified CommissionPaymentCommission from storage.
     * DELETE /commissionPaymentCommissions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CommissionPaymentCommission $commissionPaymentCommission */
        $commissionPaymentCommission = $this->commissionPaymentCommissionRepository->findWithoutFail($id);

        if (empty($commissionPaymentCommission)) {
            return $this->sendError('Commission Payment Commission not found');
        }

        $commissionPaymentCommission->delete();

        return $this->sendResponse($id, 'Commission Payment Commission deleted successfully');
    }
}
