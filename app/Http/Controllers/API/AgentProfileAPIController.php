<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAgentProfileAPIRequest;
use App\Http\Requests\API\UpdateAgentProfileAPIRequest;
use App\Repositories\AgentProfileRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\FilterColumnsCriteria;
use Response;

/**
 * Class AgentProfileController
 * @package App\Http\Controllers\API
 */

class  AgentProfileAPIController extends AppBaseController
{
    /** @var  AgentProfileRepository */
    private $agentProfileRepository;

    public function __construct(AgentProfileRepository $agentProfileRepo)
    {
        $this->agentProfileRepository = $agentProfileRepo;
    }

    /**
     * Display a listing of the AgentProfile.
     * GET|HEAD /agentProfiles
     *
     * @param Request $request
    * @return Response
     */
    public function index(Request $request)
    {
        $this->agentProfileRepository->pushCriteria(new RequestCriteria($request));
        $this->agentProfileRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->agentProfileRepository->pushCriteria(new FilterColumnsCriteria(['user_type' => 'agent']));
        $agentProfiles = $this->agentProfileRepository->paginate($request->pageSize);

        return $this->sendResponse($agentProfiles->toArray(), 'Agent Profiles retrieved successfully');
    }


    /**
     * Display the specified AgentProfile.
     * GET|HEAD /agentProfiles/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var AgentProfile $agentProfile */
        $agentProfile = $this->agentProfileRepository->with(['location', 'packages.package_destination.location'])->findWithoutFail($id);

        if (empty($agentProfile)) {
            return $this->sendError('Agent Profile not found');
        }

        return $this->sendResponse($agentProfile->toArray(), 'Agent Profile retrieved successfully');
    }

    
    /**
     * Remove the specified AgentProfile from storage.
     * DELETE /agentProfiles/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var AgentProfile $agentProfile */
        $agentProfile = $this->agentProfileRepository->findWithoutFail($id);

        if (empty($agentProfile)) {
            return $this->sendError('Agent Profile not found');
        }

        $agentProfile->delete();

        return $this->sendResponse($id, 'Agent Profile deleted successfully');
    }

    public function getAgentIslands($id){
        return $this->agentProfileRepository->with(['islands.location'])->findWithoutFail($id);
    }
}
