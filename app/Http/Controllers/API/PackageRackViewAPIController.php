<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePackageRackViewAPIRequest;
use App\Http\Requests\API\UpdatePackageRackViewAPIRequest;
use App\Views\PackageRackView;
use App\Repositories\PackageRackViewRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\FilterColumnsCriteria;
use Response;

/**
 * Class PackageRackViewController
 * @package App\Http\Controllers\API
 */

class PackageRackViewAPIController extends AppBaseController
{
    /** @var  PackageRackViewRepository */
    private $packageRackViewRepository;

    public function __construct(PackageRackViewRepository $packageRackViewRepo)
    {
        $this->packageRackViewRepository = $packageRackViewRepo;
    }

    /**
     * Display a listing of the PackageRackView.
     * GET|HEAD /packageRackViews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->packageRackViewRepository->pushCriteria(new RequestCriteria($request));
        $this->packageRackViewRepository->pushCriteria(new LimitOffsetCriteria($request));
        $packageRackViews = $this->packageRackViewRepository->all();

        return $this->sendResponse($packageRackViews->toArray(), 'Package Rack Views retrieved successfully');
    }

    /**
     * Store a newly created PackageRackView in storage.
     * POST /packageRackViews
     *
     * @param CreatePackageRackViewAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePackageRackViewAPIRequest $request)
    {
        $input = $request->all();

        $packageRackViews = $this->packageRackViewRepository->create($input);

        return $this->sendResponse($packageRackViews->toArray(), 'Package Rack View saved successfully');
    }

    /**
     * Display the specified PackageRackView.
     * GET|HEAD /packageRackViews/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PackageRackView $packageRackView */
        $packageRackView = $this->packageRackViewRepository->findWithoutFail($id);

        if (empty($packageRackView)) {
            return $this->sendError('Package Rack View not found');
        }

        return $this->sendResponse($packageRackView->toArray(), 'Package Rack View retrieved successfully');
    }

    /**
     * Update the specified PackageRackView in storage.
     * PUT/PATCH /packageRackViews/{id}
     *
     * @param  int $id
     * @param UpdatePackageRackViewAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePackageRackViewAPIRequest $request)
    {
        $input = $request->all();

        /** @var PackageRackView $packageRackView */
        $packageRackView = $this->packageRackViewRepository->findWithoutFail($id);

        if (empty($packageRackView)) {
            return $this->sendError('Package Rack View not found');
        }

        $packageRackView = $this->packageRackViewRepository->update($input, $id);

        return $this->sendResponse($packageRackView->toArray(), 'PackageRackView updated successfully');
    }

    /**
     * Remove the specified PackageRackView from storage.
     * DELETE /packageRackViews/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PackageRackView $packageRackView */
        $packageRackView = $this->packageRackViewRepository->findWithoutFail($id);

        if (empty($packageRackView)) {
            return $this->sendError('Package Rack View not found');
        }

        $packageRackView->delete();

        return $this->sendResponse($id, 'Package Rack View deleted successfully');
    }

    /**
     * Display the  packages for storage
     * last_tracking_status_id = 2(received to redbox)
     * block_id = NULL
     * GET|HEAD /package_view/package/storage
     *
     * @param  int $profile_id
     *
     * @return Response
     */
    public function getPackageForStorage(Request $request)
    {
        $conditions = [
            'last_tracking_status_id' => 2,
            'block_id' => NULL
        ];
        $this->packageRackViewRepository->pushCriteria(new FilterColumnsCriteria($conditions));
        $this->packageRackViewRepository->pushCriteria(new RequestCriteria($request));
        $this->packageRackViewRepository->pushCriteria(new LimitOffsetCriteria($request));

        $packages = $this->packageRackViewRepository->with('profile')->all();
        
        return $this->sendResponse($packages->toArray(), 'Package View retrieved successfully');
    }
}
