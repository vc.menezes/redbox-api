<?php

namespace App\Http\Controllers\API;

use App\Criteria\ByMediumCriteria;
use App\Criteria\FilterColumnsCriteria;
use App\Criteria\NotYetShippedCriteria;
use App\Events\OrderCreated;
use App\Http\Requests\API\CreatePackageAPIRequest;
use App\Http\Requests\API\CreateRemarkAPIRequest;
use App\Http\Requests\API\UpdatePackageAPIRequest;
use App\Jobs\GiveSequence;
use App\Models\Package;
use App\Models\Status;
use App\Models\StorageBlock;
use App\Models\TrackingLog;
use App\Modules\Finance\AgentCommission;
use App\Modules\Finance\Invoice;
use App\Modules\Finance\PickingCharge;
use App\Modules\Finance\Shipping;
use App\Repositories\AgentCommissionRepository;
use App\Repositories\CommissionRepository;
use App\Repositories\OrderRepository;
use App\Repositories\PackageDestinationRepository;
use App\Repositories\PackageRepository;
use App\Repositories\ProfileViewRepository;
use App\Repositories\TrackingRepository;
use App\Repositories\VesselRepository;
use App\Repositories\AttachableRepository;
use App\Traits\Jwt;
use App\Views\PackageView;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\FindOwnCriteria;
use App\Criteria\MyOngoingTrackingCriteria;
use Response;
use App\Modules\Commission\Commission;

/**
 * Class PackageController
 * @package App\Http\Controllers\API
 */
class PackageAPIController extends AppBaseController
{
	/** @var  PackageRepository */
	private $packageRepository;
	private $profileViewRepositry;
	private $packageDestinationRepository;
	private $vesselRepository;
	private $commissionRepository;
	private $agentCommissionRepository;
	private $orderRepository;
	private $attachableRepository;
	use Jwt;

	public function __construct(PackageRepository $packageRepo,
															CommissionRepository $commissionRepo,
															TrackingRepository $trackingRepo,
															ProfileViewRepository $profileViewRepo,
															PackageDestinationRepository $packageDestinationRepo,
															VesselRepository $vesselRepo,
															AgentCommissionRepository $agentCommissionRepo,
															OrderRepository $orderRep,
															AttachableRepository $attachableRepo
	)
	{
		$this->packageRepository = $packageRepo;
		$this->commissionRepository = $commissionRepo;
		$this->trackingRepository = $trackingRepo;
		$this->profileViewRepositry = $profileViewRepo;
		$this->packageDestinationRepository = $packageDestinationRepo;
		$this->vesselRepository = $vesselRepo;
		$this->agentCommissionRepository = $agentCommissionRepo;
		$this->orderRepository = $orderRep;
		$this->attachableRepository = $attachableRepo;
	}

	/**
	 * Display a listing of the Package.
	 * GET|HEAD /packages
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->packageRepository->pushCriteria(new RequestCriteria($request));
		$this->packageRepository->pushCriteria(new LimitOffsetCriteria($request));
		$packages = $this->packageRepository->paginate();

		return $this->sendResponse($packages->toArray(), 'Packages retrieved successfully');
	}

	/**
	 * Store a newly created Package in storage.
	 * POST /packages
	 *
	 * @param CreatePackageAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePackageAPIRequest $request)
	{
		$input = $request->only('type', 'reference_no', 'description', 'weight', 'profile_id', 'parent_id', 'status_id');
		// return $request->status_id;
		$input['barcode'] = '-';
		$input['code'] = '-';
		$input['type'] = 'order';

		if ($request->status_id && $request->status_id != 1) {
			$input['weight_approved'] = 1;
		}


		$packages = $this->packageRepository->create($input);



		GiveSequence::dispatch($packages, 'barcode', 'order_barcode');

		GiveSequence::dispatch($packages, 'code', 'order_serial'); // follow convention ;)

		if ($request->status_id && $request->status_id != 1) {
			$this->trackingRepository->createNewTracking($packages, null, $request->status_id, 455);
		} else {
			$this->trackingRepository->createNewTracking($packages);
		}

		event(new OrderCreated($packages, $request));

		return $this->sendResponse($packages->toArray(), 'Order placed successfully');
	}

	/**
	 * Display the specified Package.
	 * GET|HEAD /packages/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var Package $package */
		$package = $this->packageRepository->with([
			'handOver',
			'block',
			'attachables',
			'signature.attachables',
			'orders.profile',
			'profile.location',
			'commission',
			'items.commodity',
			'shipment.vessel',
			'pickingLocation.location',
			'pickingLocation.address.location',
			'package_destination.location',
			'tracking.tracking_log.location',
			'tracking.tracking_log.status',
			'tracking.tracking_log.country',
		])->findWithoutFail($id);

		if (empty($package)) {
			return $this->sendError('Package not found');
		}

		$log = \App\Models\TrackingLog::whereTrackingId($package->tracking->id)->orderBy('id', 'desc')->first();
		$package->last_log = $log->status_id;

		return $this->sendResponse($package->toArray(), 'Package retrieved successfully');
	}

	/**
	 * Update the specified Package in storage.
	 * PUT/PATCH /packages/{id}
	 *
	 * @param  int $id
	 * @param UpdatePackageAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatePackageAPIRequest $request)
	{
		$input = $request->only('code', 'description', 'profile_id', 'reference_no', 'package_destination', 'picking_location', 'block_id');
		$input_package_destination = $request->destination;
		$input_picking_location = $request->picking_location;
		$input_package_destination['package_id'] = $id;
		$input_picking_location['package_id'] = $id;

		/** @var Package $package */
		$package = $this->packageRepository->findWithoutFail($id);

		if (empty($package)) {
			return $this->sendError('Package not found');
		}
		unset($input_package_destination['location']);
		unset($input_picking_location['location']);
		if ($request->destination) {
			$package->package_destination()->update($input_package_destination);
		}


		if ($request->picking_location) {

			if (is_null($request->picking_location['id'])) {
				$package->picking_location()->create($input_picking_location);
			} else {
				$package->picking_location()->update($input_picking_location);
			}

		}

		if ($request->block_id) {
			if (count($package->block)) {
				$storageBlock = \App\Models\StorageBlock::where('package_id', $package->id)->orderBy('id', 'desc')->first();
				$storageBlock->block_id = $request->block_id;
				$storageBlock->save();
			} else {
				\App\Models\StorageBlock::create(['block_id' => $request->block_id, 'package_id' => $package->id]);
			}
		}


		$package = $this->packageRepository->update($input, $id);

		return $this->sendResponse($package->toArray(), 'Package updated successfully');
	}

	/**
	 * Remove the specified Package from storage.
	 * DELETE /packages/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		/** @var Package $package */
		$package = $this->packageRepository->findWithoutFail($id);

		if (empty($package) || $package->weight_approved) {
			return $this->sendError('Invoice already generated. Cancel invoice first.');
		}


		if ($request->remarks) {
			$package->remarks()->create([
				'body' => $request->remarks,
				'profile_id' => $this->getProfileId()
			]);
		}

		$package->packageRepository->cancelOrder($package);

		return $this->sendResponse($id, 'Package deleted successfully');
	}

	/**
	 * Get My Order
	 * GET /packages/my/order
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function getMyOrders(Request $request)
	{
		$user = \JWTAuth::parseToken()->authenticate();
		$this->packageRepository->pushCriteria(new RequestCriteria($request));
		$this->packageRepository->pushCriteria(new FindOwnCriteria($user));

		$orders = $this->packageRepository->with(['items', 'package_destination', 'pickingLocation', 'tracking.tracking_log.status'])->paginate($request->pageSize);

		return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
	}

	/**
	 * Get My Order Statistic and onoging track
	 * GET /packages/my/statistic
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function getStatistic(Request $request)
	{
		$user = \JWTAuth::parseToken()->authenticate();
		//filter request user packages
		$clone = $this->packageRepository->pushCriteria(new FindOwnCriteria($user));

		//request user package count
		$data['count'] = $clone->all()->count();

		//packages->tracking status == 1
		$this->packageRepository->pushCriteria(new MyOngoingTrackingCriteria);

		$data['trackings'] = $this->packageRepository->with([
			'package_destination.location',
			'tracking.tracking_log.location',
			'tracking.tracking_log.status',
		])->all();


		return $this->sendResponse($data, 'Stats retrieved successfully');
	}


	public function create_invoice($packages)
	{

		$input['weight'] = $packages->weight;
		$input['operator'] = '=';

		$medium = $packages->package_destination->medium;
		if ($medium != 'land') {
			$input['medium'] = $medium;
		}

		if ($packages->weight < 1) {
			$input['weight'] = ceil($packages->weight);
			$input['operator'] = '<';
		}

		$package = Package::find($packages->id);


		$shipping = new Shipping($input, $package);
		$invoice = new Invoice($shipping);
		$invoice = $invoice->create();

		if ($invoice->error) {
			return $invoice->error->getmessage();
		}


	}


	public function create_shipment(CreatePackageAPIRequest $request)
	{

		//getting basic input for package creation
		$input_package = $request->only('reference_no', 'description', 'weight', 'profile_id', 'order_id');
		//setting up initial values
		$input_package['barcode'] = '-';
		$input_package['code'] = '-';
		$input_package['type'] = 'shipment';
		$input_package['payment_status'] = null;

		//getting all package which one want shipp
		$orders = $this->packageRepository->findWhereIn('id', $request->order_id);


		//getting all package destination to check medium
		$package_destination = $this->packageDestinationRepository->findWhereIn('package_id', $request->order_id);


		$vessel = $this->vesselRepository->find($request->vessel_id);


		$medium = array_column($package_destination->toArray(), 'medium');


		if (count(array_unique($medium)) > 1) {

			return $this->sendResponse($package_destination->toArray(), 'all orders of shipment medium must be same');
		};


		// open profile
		$profile = $this->profileViewRepositry->find($request->profile_id);

		//create package
		$packages = $this->packageRepository->create($input_package);


		$destination = [
			'location_id' => $request->location_id,
			'medium' => $vessel->medium,
			'name' => $profile->name,
			'contact_number' => $profile->contact_number,
			'address' => $profile->location_name,
		];


		$packages->package_destination()->create($destination);

		$packages->shipment()->create(['vessel_id' => $request->vessel_id]);


		GiveSequence::dispatch($packages, 'barcode', 'shipment_barcode');

		GiveSequence::dispatch($packages, 'code', 'shipment_code'); // follow convention ;)
//      
		$this->trackingRepository->createNewTracking($packages, $request, 3, 455, '', $request->estimated_delivery_date);

		// $agent_commission = $this->agentCommissionRepository->raise($packages);

		// $this->commissionRepository->raise($packages, $agent_commission);

		// //remove item from block
		// StorageBlock::whereIn('package_id', $request->order_id)->delete();


		return $this->sendResponse($packages->toArray(), 'Order placed successfully');
	}


	public function show_shipment($id)
	{
		$package = $this->packageRepository->with(['child', 'package_destination.location', 'tracking.tracking_log', 'profile', 'commission.details.package_view', 'vessel.vessel'])->find($id);
		// $package->commissions = $this->packageRepository->list_commission($id);

		return $this->sendResponse($package->toArray(), 'shipment showed successfully');
	}

	public function getPackageByCode($code)
	{
		// return $code;
		/** @var Package $package */
		$package = \App\Models\Package::with([
			'handOver',
			'attachables',
			'signature.attachables',
			'orders.profile',
			'profile.location',
			'commission',
			'items.commodity',
			'shipment.vessel',
			'pickingLocation.location',
			'pickingLocation.address.location',
			'package_destination.location',
			'tracking.tracking_log.location',
			'tracking.tracking_log.status',
			'tracking.tracking_log.country',
		])->where('barcode', $code)->orWhere('code', $code)->first();


		if (empty($package)) {
			return $this->sendError('Package not found. Please check number');
		}

		// get agent profile -id
		$profile_id = $this->getProfileId();

		// if its order
		if ($package->type == 'shipment') {
			// return $package->profile_id;
			if ($package->profile_id != $profile_id) {
				return $this->sendError('Shipment is not belong to you');
			}

		} else {
			if ($package->parent_id) {
				// return $package->package;
				if ($package->package->profile_id != $profile_id) {
					return $this->sendError('Package is not belong to you');
				}
			} else {
				return $this->sendError('Package is not belong to you');
			}

		}


		// if it is order shipment than check profile_id and session profile_id
		//

		return $this->sendResponse($package->toArray(), 'Package retrieved successfully');
	}


	public function shipment_orders($shipment_id)
	{
//        $orders = $this->packageRepository->findWhere(['parent_id' => $shipment_id]);
		$orders = PackageView::with(['destination', 'storageBlock.block'])->whereParentId($shipment_id)->get();

		return $this->sendResponse($orders, 'orders retrieved successfully');
	}

	public function cancel_shipment($id)
	{
		$package = $this->packageRepository->find($id);

		if ($package->commission) {
			return $this->sendResponse($package->commission, 'Commission already generated');
		}


		$result = $this->packageRepository->delete_package($package);


		foreach ($package->child as $child) {
			\Redbox::PopTracking($child);
			$child->parent_id = null;
			$child->save();
		}

		return $this->sendResponse($package, $result);
	}

	public function shipments_index(Request $request)
	{
		$this->orderRepository->pushCriteria(new RequestCriteria($request));
		$this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
		$this->orderRepository->pushCriteria(new FilterColumnsCriteria(['type' => 'shipment']));

		if ($request->destinations_location_id) {
			$this->orderRepository->pushCriteria(new FilterColumnsCriteria(['destinations_location_id' => $request->destinations_location_id]));
		}

		if ($request->last_tracking_status_id) {
			$this->orderRepository->pushCriteria(new FilterColumnsCriteria(['last_tracking_status_id' => $request->last_tracking_status_id]));
		}

		if ($request->payment_status) {
			$this->orderRepository->pushCriteria(new FilterColumnsCriteria(['payment_status' => $request->payment_status]));
		}

		$orders = $this->orderRepository->with(['status', 'destination', 'profile'])->orderBy('id', 'desc')->paginate($request->pageSize);

		return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
	}

	public function shipment_return(Request $request)
	{
		$package = $this->packageRepository->findWithoutFail($request->id);

		if (empty($package)) {
			return $this->sendError('Package not found. Please check number');
		}

		$package->tracking->tracking_log()->create([
			'status_id' => 7,
			'location_id' => 455,
			'description' => $request->description
		]);

		$package->block()->attach($request->block_id);

		return $this->sendResponse($package->toArray(), 'Shipment return to office');


	}

	/**
	 * verify order
	 * GET /packages/verify/{id}
	 *
	 * @param  int $id
	 * @param  int $weight
	 * @param  int $remarks
	 *
	 * @return Response
	 */
	public function verifyOrder($id, Request $request)
	{
		$input = $request->only('weight');
		$package = $this->packageRepository->findWithoutFail($id);


		// if ($package->pickingLocation) {
		//     $this->create_invoice($package);
		// }

		if (empty($package) || $package->weight_approved) {
			return $this->sendError('Invoice already generated. Cancel invoice first.');
		}

		$oldWeight = $package->weight;


		$str = $oldWeight == $input['weight'] ? '' : 'Previous weight is ' . $oldWeight . '. ';

		$package->weight = $input['weight'];

		if ($request->flag == 'normal') {
			try {

				$d = $this->create_invoice($package);
			} catch (\Exception $e) {
				return $this->sendError('Invoice already generated. Cancel invoice first.');
			}

		}

		$input['weight_approved'] = 1;
		$package = $this->packageRepository->update($input, $id);

		if ($request->remarks) {
			$package->remarks()->create([
				'body' => $str . $request->remarks,
				'profile_id' => $this->getProfileId()
			]);
		}


		$this->trackingRepository->createTrackingLog($package, 2, 455);
		return $this->sendResponse($package->toArray(), 'Invoice raised');

	}

	public function cancelOrder(Request $request)
	{
		$package = $this->packageRepository->findWithoutFail($request->id);

		if (empty($package) || $package->weight_approved) {
			return $this->sendError('Invoice already generated. Cancel invoice first.');
		}

		if($package->checkCommission) {
			return $this->sendError('Commission already generated. Cancel Commision first.');
		}

		if ($request->body) {
			$package->remarks()->create([
				'body' => $request->body,
				'profile_id' => $this->getProfileId()
			]);
		}

		$this->packageRepository->cancelOrder($package);

		return $this->sendResponse($request->id, 'Package deleted successfully');

	}

	public function remove_package_from_shipment($id, CreateRemarkAPIRequest $request)
	{
		//finding the package
		$package = Package::find($id);

		$commission = $this->commissionRepository->findWhere(['package_id' => $id]);


		if (count($commission)) {
			return $this->sendError('Commission is there , please cancel it first');
		}


		$package->parent_id = null;

		if ($package->save() && $request->remarks) {
			$package->remarks()->create([
				'body' => $request->remarks,
				'profile_id' => $this->getProfileId()
			]);
		}

		$last_tracking_location = TrackingLog::where('tracking_id', $package->tracking->id)->orderBy('id', 'desc')->first();

		$package->tracking->tracking_log()->create([
			'status_id' => 2,
			'location_id' => $last_tracking_location->location_id,
			'description' => $request->remarks
		]);


		return $this->sendResponse($package, 'Package remove from the system');


	}

	public function not_shipped_order(Request $request)
	{
		if (is_null($request->medium))
			$request->medium = 'sea';

		// $this->orderRepository->pushCriteria(new ByMediumCriteria($request->medium));
		$this->orderRepository->pushCriteria(new NotYetShippedCriteria());
		$order = $this->orderRepository->get()->toArray();
		return $this->sendResponse($order, 'Package remove from the system');
	}

	public function add_order_to_shipment($id, Request $request)
	{

		Package::whereId($request->order_id)->update(['parent_id' => $id]);

		$package = Package::find($request->order_id);

		$last_tracking_location = TrackingLog::where('tracking_id', $package->tracking->id)->orderBy('id', 'desc')->first();

		if (!$package->tracking) {

		}

		$package->tracking->tracking_log()->create([
			'status_id' => 3,
			'location_id' => $last_tracking_location->location_id,
			'description' => $request->remarks
		]);

		return $this->sendResponse(Package::whereParentId($id)->get(), 'Updated');

	}

	public function updateShipment($id, Request $request)
	{
		$input_package = $request->only('reference_no', 'description', 'weight', 'profile_id');

		$package = $this->packageRepository->update($input_package, $id);

		$profile = $this->profileViewRepositry->findWithoutFail($request->profile);

		$destination = [
			'location_id' => $request->location,
			'medium' => $request->medium,
			'name' => $profile->name,
			'contact_number' => $profile->contact_number,
			'address' => $profile->location_name,
		];


		$package->package_destination()->update($destination);

		$package->shipment()->update(['vessel_id' => $request->package_shipments['vessel']]);
		return $this->sendResponse($package->toArray(), 'Shipment updated');
	}

	function yearly_statistics(Request $request)
	{
		$now = Carbon::now();

		$transport_medium = $request->transport_medium ? explode(',', $request->transport_medium) : 'sea';

		$start = $request->start ? Carbon::parse($request->start) : $now->startOfDay();

		$start_clone = clone $start;

		$end = $request->end ? Carbon::parse($request->end) : $start_clone->addMonths(1);

		if ($request->location_id) {
			$data = PackageView::select('tracking_status', \DB::raw('count(*) as count'))
				->groupBy('tracking_status')
				->whereIn('transport_medium', $transport_medium)
				->whereBetween('created_at', [$start, $end])
				->where('destinations_location_id', $request->location_id)
				->get();

		} else {
			$data = PackageView::select('tracking_status', \DB::raw('count(*) as count'))
				->groupBy('tracking_status')
				->whereIn('transport_medium', $transport_medium)
				->whereBetween('created_at', [$start, $end])
				->get();

		}


		foreach ($data as $item) {
			if ($item->tracking_status == 0) {
				$blackStatus = new \stdClass();
				$blackStatus->id = 0;
				$blackStatus->name = "Completed";

				$item->status_name = $blackStatus;
			} else {
				$item->status_name = Status::find($item->tracking_status);
			}

		}


		return $this->sendResponse($data, 'Yearly Sales Successfully');
	}

	public function filter(Request $request)
	{

		$now = Carbon::now();

		$transport_medium = $request->transport_medium ? explode(',', $request->transport_medium) : ['sea'];

		$start = $request->start ? Carbon::parse($request->start) : $now->startOfDay();

		$start_clone = clone $start;

		$end = $request->end ? Carbon::parse($request->end) : $start_clone->addMonths(1);


		$status_ids = Status::pluck('id')->toArray();

		$tracking_status = $request->tracking_status || $request->tracking_status == 0 ? explode(',', $request->tracking_status) : $status_ids;

		if ($request->location_id || !is_null($request->location_id)) {

			$data = PackageView::whereIn('transport_medium', $transport_medium)
				->whereBetween('created_at', [$start, $end])
				->where('destinations_location_id', $request->location_id)
				->whereIn('tracking_status', $tracking_status)->get();
		} else {

			$data = PackageView::whereIn('transport_medium', $transport_medium)
				->whereBetween('created_at', [$start, $end])
				->whereIn('tracking_status', $tracking_status)->get();
		}


		return $this->sendResponse($data, 'Yearly Sales Successfully');

	}

	public function download_packages_report(Request $request)
	{
		return \Excel::download(new \App\Exports\PackagesExport($request), 'Packages.xlsx');
		$new = new \App\Exports\PackagesExport($request);
		return $new->collection();
	}



	/**
	 * Generate Commission
	 * POST shipment/generate_commission
	 *
	 * @param  int $id (shipment / package)
	 *
	 * @return Response
	 */
	public function generateCommission(Request $request) {

		$package = $this->packageRepository->findWithoutFail($request->id);

		if (empty($package)) {
			return $this->sendError('Shipment not found');
		}
		$commission = new Commission($package);

		$commission->raiseAgentCommission()->raiseCommission();


		// $package = $this->packageRepository->with('commission.details.package_view')->find($package->id);

		return $this->sendResponse($package, 'Commission Generated Successfully');
	}

	/**
	 * Get Shipment Commissions
	 * POST shipment/commission/{id}
	 *
	 * @param  int $id (shipment / package)
	 *
	 * @return Response
	 */
	public function shipmentCommission($id) {

		$package = $this->packageRepository->with('commission.details.package_view')->find($id);

		return $this->sendResponse($package, 'Commission Generated Successfully');
	}

}

