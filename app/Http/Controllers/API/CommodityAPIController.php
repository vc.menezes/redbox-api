<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCommodityAPIRequest;
use App\Http\Requests\API\UpdateCommodityAPIRequest;
use App\Models\Commodity;
use App\Repositories\CommodityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CommodityController
 * @package App\Http\Controllers\API
 */

class CommodityAPIController extends AppBaseController
{
    /** @var  CommodityRepository */
    private $commodityRepository;

    public function __construct(CommodityRepository $commodityRepo)
    {
        $this->commodityRepository = $commodityRepo;
    }

    /**
     * Display a listing of the Commodity.
     * GET|HEAD /commodities
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->commodityRepository->pushCriteria(new RequestCriteria($request));
        $this->commodityRepository->pushCriteria(new LimitOffsetCriteria($request));
        $commodities = $this->commodityRepository->with(['category'])->paginate($request->pageSize);

        return $this->sendResponse($commodities->toArray(), 'Commodities retrieved successfully');
    }

    /**
     * Store a newly created Commodity in storage.
     * POST /commodities
     *
     * @param CreateCommodityAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCommodityAPIRequest $request)
    {
        $input = $request->except('category');

        $commodities = $this->commodityRepository->create($input);

        return $this->sendResponse($commodities->toArray(), 'Commodity saved successfully');
    }

    /**
     * Display the specified Commodity.
     * GET|HEAD /commodities/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Commodity $commodity */
        $commodity = $this->commodityRepository->findWithoutFail($id);

        if (empty($commodity)) {
            return $this->sendError('Commodity not found');
        }

        return $this->sendResponse($commodity->toArray(), 'Commodity retrieved successfully');
    }

    /**
     * Update the specified Commodity in storage.
     * PUT/PATCH /commodities/{id}
     *
     * @param  int $id
     * @param UpdateCommodityAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCommodityAPIRequest $request)
    {
        $input = $request->except('category');

        /** @var Commodity $commodity */
        $commodity = $this->commodityRepository->findWithoutFail($id);

        if (empty($commodity)) {
            return $this->sendError('Commodity not found');
        }

        $commodity = $this->commodityRepository->update($input, $id);

        return $this->sendResponse($commodity->toArray(), 'Commodity updated successfully');
    }

    /**
     * Remove the specified Commodity from storage.
     * DELETE /commodities/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Commodity $commodity */
        $commodity = $this->commodityRepository->findWithoutFail($id);

        if (empty($commodity)) {
            return $this->sendError('Commodity not found');
        }

        $commodity->delete();

        return $this->sendResponse($id, 'Commodity deleted successfully');
    }

    public function firstOrCreate(Request $request)
    {
        if($request->name) {
            $commodidty =  \App\Models\Commodity::firstOrCreate(['name' => $request->name]);
            return $this->sendResponse($commodidty, 'Commodity retrieved successfully');
        }
        return $this->sendError('Commodity not found');
    }
}
