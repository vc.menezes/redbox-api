<?php

namespace App\Http\Controllers\API;

use App\Criteria\ByMediumCriteria;
use App\Criteria\NotYetShippedCriteria;
use App\Http\Requests\API\CreateVesselAPIRequest;
use App\Http\Requests\API\UpdateVesselAPIRequest;
use App\Models\Agent;
use App\Models\Location;
use App\Models\Package;
use App\Models\PackageDestination;
use App\Models\PackageShipment;
use App\Models\Vessel;
use App\Models\VesselRoute;
use App\Repositories\AgentRepository;
use App\Repositories\LocationRepository;
use App\Repositories\OrderRepository;
use App\Repositories\VesselRepository;
use App\Repositories\VesselRouteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\FilterColumnsCriteria;
use Response;

/**
 * Class VesselController
 * @package App\Http\Controllers\API
 */
class VesselAPIController extends AppBaseController
{
	/** @var  VesselRepository */
	private $vesselRepository;
	private $orderRepository;
	private $vesselAgentRepository;
	private $locationRepository;


	public function __construct(
		VesselRepository $vesselRepo,
		AgentRepository $AgentRepo,
		OrderRepository $orderRepo,
		LocationRepository $locationRepo
	)
	{
		$this->vesselRepository = $vesselRepo;
		$this->vesselAgentRepository = $AgentRepo;
		$this->orderRepository = $orderRepo;
		$this->locationRepository = $locationRepo;
	}

	/**
	 * Display a listing of the Vessel.
	 * GET|HEAD /vessels
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->vesselRepository->pushCriteria(new RequestCriteria($request));
		$this->vesselRepository->pushCriteria(new LimitOffsetCriteria($request));
		if ($request->docked >= 0 && $request->docked != null) {
			$this->vesselRepository->pushCriteria(new FilterColumnsCriteria(['docked' => $request->docked]));
		}
		$vessels = $this->vesselRepository->with('routes.location')->paginate($request->pageSize);


		if ($request->searchType == 'island') {
			$location_ids = Location::whereType('island')->where('name', 'like', '%' . $request->search . '%')->pluck('id');
			$vessel_id = VesselRoute::whereIn('location_id', $location_ids)->pluck('vessel_id');
			$vessels = Vessel::whereIn('id', $vessel_id)->with('routes.location')->paginate();
			return $this->sendResponse($vessels->toArray(), 'Vessels retrieved successfully');

		}

		return $this->sendResponse($vessels->toArray(), 'Vessels retrieved successfully');
	}

	/**
	 * Store a newly created Vessel in storage.
	 * POST /vessels
	 *
	 * @param CreateVesselAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateVesselAPIRequest $request)
	{
		$input = $request->except('location');

		$vessels = $this->vesselRepository->create($input);

		return $this->sendResponse($vessels->toArray(), 'Vessel saved successfully');
	}

	/**
	 * Display the specified Vessel.
	 * GET|HEAD /vessels/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var Vessel $vessel */
		$vessel = $this->vesselRepository->with('location')->findWithoutFail($id);

		if (empty($vessel)) {
			return $this->sendError('Vessel not found');
		}

		return $this->sendResponse($vessel->toArray(), 'Vessel retrieved successfully');
	}

	/**
	 * Update the specified Vessel in storage.
	 * PUT/PATCH /vessels/{id}
	 *
	 * @param  int $id
	 * @param UpdateVesselAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateVesselAPIRequest $request)
	{
		$input = $request->except('location');

		/** @var Vessel $vessel */
		$vessel = $this->vesselRepository->findWithoutFail($id);

		if (empty($vessel)) {
			return $this->sendError('Vessel not found');
		}

		$vessel = $this->vesselRepository->update($input, $id);

		return $this->sendResponse($vessel->toArray(), 'Vessel updated successfully');
	}

	/**
	 * Remove the specified Vessel from storage.
	 * DELETE /vessels/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var Vessel $vessel */
		$vessel = $this->vesselRepository->findWithoutFail($id);

		if (empty($vessel)) {
			return $this->sendError('Vessel not found');
		}

		$vessel->delete();

		return $this->sendResponse($id, 'Vessel deleted successfully');
	}

	public function vessel_agents(Request $request)
	{
		//setting default medium to sea
		if (is_null($request->medium)) {
			$request->medium = 'sea';
		}


		if ($request->id && is_numeric($request->id)) {
			//get selected vessels
			$vessel = $this->vesselRepository->find($request->id);
			//get all route location ids of the selected vessels
			$location_ids = VesselRoute::whereIn('vessel_id', [$request->id])->pluck('location_id')->toArray();
		} else {
			$location_ids = Agent::where('profile_id', $request->agent_profile_id)->pluck('location_id')->toArray();
		}


		//get all agents which belongs to vessel routes
		$agents = Agent::groupBy('profile_id')->select(['profile_id'])->with('profile')->whereIn('location_id', $location_ids)->get();


		if ($request->agent_profile_id) {
			$location_ids = Agent::whereProfileId($request->agent_profile_id)->pluck('location_id')->toArray();
		}

		if ($request->destination_location_id) {
			$location_ids = [$request->destination_location_id];
		}


		//get all location of vessesl routes
		$agent_location = $this->locationRepository->findWhereIn('id', $location_ids);


		$this->orderRepository->pushCriteria(new ByMediumCriteria($request->medium));
		$this->orderRepository->pushCriteria(new NotYetShippedCriteria());
		$order = $this->orderRepository->with(['destination', 'status', 'storageBlock.block'])->findWhereIn('destinations_location_id', $location_ids);


//        return $location_ids;
		return $this->sendResponse(['agents' => $agents, 'orders' => $order, 'agent_locations' => $agent_location], 'Vessel Agents successfully');

	}

	public function toggle_docked(Request $request)
	{

		$vessel = $this->vesselRepository->findWithoutFail($request->id);

		if (empty($vessel)) {
			return $this->sendError('Vessel not found');
		}

		$input['docked'] = $request->docked ? 1 : 0;

		$vessel = $this->vesselRepository->update($input, $request->id);

		return $this->sendResponse($vessel->toArray(), 'Vessel docked updated successfully');
	}

	public function ready_to_ship_vessel_shipment_edit(Request $request)
	{

		$package_ids = Package::where(function ($query) use ($request) {
			$query->where('id', '=', $request->package_id)
				->orWhere('parent_id', '=', $request->package_id);
		})->pluck('id')->toArray();

		$location_ids = PackageDestination::whereIn('package_id', $package_ids)->pluck('location_id')->toArray();

		$vessels_ids = VesselRoute::whereIn('location_id', $location_ids)->pluck('vessel_id')->toArray();

		$data = Vessel::whereIn('id', $vessels_ids)->get();

		return $this->sendResponse($data, 'ready_to_ship_vessel_shipment_edit');
	}

	public function ready_to_ship_vessels(Request $request)
	{

		$this->orderRepository->pushCriteria(new NotYetShippedCriteria());
		//get all not shipped order
		$order = $this->orderRepository->get()->toArray();
		//getting all the location id
		$destinations_location_id = array_column($order, 'destinations_location_id');

		$vessels_ids = VesselRoute::whereIn('location_id', $destinations_location_id)->pluck('vessel_id')->toArray();


		if ($request->package_id) {

			$package_shipment = PackageShipment::wherePackageId($request->package_id)->pluck('vessel_id')->toArray();

			$vessels_ids = array_merge($vessels_ids, $package_shipment);
		}

		$data = Vessel::whereIn('id', $vessels_ids)->get();

		return $this->sendResponse($data, 'ready_to_ship_vessel');
	}
}
