<?php

namespace App\Http\Controllers\API;

use App\Criteria\BetweenTwoDateCriteria;
use App\Http\Requests\API\CreateReceiptAPIRequest;
use App\Http\Requests\API\UpdateReceiptAPIRequest;
use App\Models\Receipt;
use App\Repositories\PackageRepository;
use App\Repositories\ReceiptRepository;
use App\Repositories\RemarkRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ReceiptController
 * @package App\Http\Controllers\API
 */

class ReceiptAPIController extends AppBaseController
{
    /** @var  ReceiptRepository */
    private $receiptRepository;
    private $packageRepository;
    private $remarkableRepository;

    public function __construct(ReceiptRepository $receiptRepo,PackageRepository $packageRepo,RemarkRepository $remarkableRepo)
    {
        $this->receiptRepository = $receiptRepo;
        $this->packageRepository = $packageRepo;
        $this->remarkableRepository = $remarkableRepo;
    }

    /**
     * Display a listing of the Receipt.
     * GET|HEAD /receipts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        if ($request->from_date && $request->to_date) {
            $this->receiptRepository->pushCriteria(new BetweenTwoDateCriteria($request->from_date, $request->to_date));
        }

        $this->receiptRepository->pushCriteria(new RequestCriteria($request));
        $this->receiptRepository->pushCriteria(new LimitOffsetCriteria($request));
        $receipts = $this->receiptRepository->with(['profile'])->paginate();

        return $this->sendResponse($receipts->toArray(), 'Receipts retrieved successfully');
    }

    /**
     * Store a newly created Receipt in storage.
     * POST /receipts
     *
     * @param CreateReceiptAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateReceiptAPIRequest $request)
    {
        $input = $request->all();

        $receipts = $this->receiptRepository->create($input);

        return $this->sendResponse($receipts->toArray(), 'Receipt saved successfully');
    }

    /**
     * Display the specified Receipt.
     * GET|HEAD /receipts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Receipt $receipt */
        $receipt = $this->receiptRepository->with('invoices')->findWithoutFail($id);

        if (empty($receipt)) {
            return $this->sendError('Receipt not found');
        }

        return $this->sendResponse($receipt->toArray(), 'Receipt retrieved successfully');
    }

    /**
     * Update the specified Receipt in storage.
     * PUT/PATCH /receipts/{id}
     *
     * @param  int $id
     * @param UpdateReceiptAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReceiptAPIRequest $request)
    {
        $input = $request->all();

        /** @var Receipt $receipt */
        $receipt = $this->receiptRepository->findWithoutFail($id);

        if (empty($receipt)) {
            return $this->sendError('Receipt not found');
        }

        $receipt = $this->receiptRepository->update($input, $id);

        return $this->sendResponse($receipt->toArray(), 'Receipt updated successfully');
    }

    /**
     * Remove the specified Receipt from storage.
     * DELETE /receipts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request,$id)
    {
        /** @var Receipt $receipt */
        $receipt = $this->receiptRepository->findWithoutFail($id);

        if (empty($receipt)) {
            return $this->sendError('Receipt not found');
        }

        foreach ($receipt->invoice_receipts() as $invoice_receipt){
            $type = $invoice_receipt->invoice->type;
            $invoice_receipt->invoice()->update(['status'=>$type]);
            $this->packageRepository->OnPayReverse($invoice_receipt->invoice());
            $invoice_receipt->delete();
        }

        if($request->remarks){
            $this->remarkableRepository->raise($receipt ,$request->remarks);
        }





        return $this->sendResponse($id, 'Receipt Cancelled successfully');
    }
}
