<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePackageViewAPIRequest;
use App\Http\Requests\API\UpdatePackageViewAPIRequest;
use App\Models\PackageView;
use App\Views\PackageRackView;
use App\Repositories\PackageRackViewRepository;
use App\Repositories\PackageViewRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\FilterColumnsCriteria;
use App\Criteria\FindOwnCriteria;
use App\Criteria\OrWhereCriteria;
use Response;

/**
 * Class PackageViewController
 * @package App\Http\Controllers\API
 */

class PackageViewAPIController extends AppBaseController
{
    /** @var  PackageViewRepository */
    private $packageViewRepository;
    private $packageRackViewRepository;
    public function __construct(PackageViewRepository $packageViewRepo , PackageRackViewRepository $packageRackViewRepo)
    {
        $this->packageViewRepository = $packageViewRepo;
        $this->packageRackViewRepository =  $packageRackViewRepo;
    }

    /**
     * Display a listing of the PackageView.
     * GET|HEAD /packageViews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->packageViewRepository->pushCriteria(new RequestCriteria($request));
        $this->packageViewRepository->pushCriteria(new LimitOffsetCriteria($request));
        $packageViews = $this->packageViewRepository->all();

        return $this->sendResponse($packageViews->toArray(), 'Package Views retrieved successfully');
    }


    /**
     * Display the specified PackageView.
     * GET|HEAD /packageViews/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PackageView $packageView */
        $packageView = $this->packageViewRepository->with(['status', 'items.commodity', 'tracking.tracking_log' => function($query) {
            $query->with('status', 'location');
        }
        ])->findWithoutFail($id);

        if (empty($packageView)) {
            return $this->sendError('Package View not found');
        }

        return $this->sendResponse($packageView->toArray(), 'Package View retrieved successfully');
    }

    /**
     * Display the all packages belongs to profile_id.
     * GET|HEAD /package_view/profile/{profile_id}
     *
     * @param  int $profile_id
     *
     * @return Response
     */
    public function getPackages($profile_id, Request $request)
    {
        $this->packageViewRepository->pushCriteria(new FilterColumnsCriteria(['profile_id' => $profile_id]));
        $this->packageViewRepository->pushCriteria(new RequestCriteria($request));
        $this->packageViewRepository->pushCriteria(new LimitOffsetCriteria($request));

        $packages = $this->packageViewRepository->with(['destination', 'status'])->paginate($request->pageSize);
        
        return $this->sendResponse($packages->toArray(), 'Package View retrieved successfully');
    }
    

     /**
     * Get My Order
     * GET /packages/my/order
     *
     * @param  int $id
     *
     * @return Response
     */
    public function getMyOrders(Request $request)
    {
        $user = \JWTAuth::parseToken()->authenticate();
        $this->packageViewRepository->pushCriteria(new RequestCriteria($request));
        $this->packageViewRepository->pushCriteria(new FindOwnCriteria($user));

        $orders = $this->packageViewRepository->with(['status', 'destination', 'commission'])->orderBy('created_at', 'desc')->paginate($request->pageSize);

        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }

    /**
     * Display the specified Tracking.
     * GET|HEAD /trackings/check{tracking_no}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function check($number)
    {
        $this->packageViewRepository->pushCriteria(new OrWhereCriteria(['reference_no' => $number], ['tracking_no' => $number]));
        $tracking = $this->packageViewRepository->first();
        if (empty($tracking)) {
            return $this->sendError('Tracking not found');
        }

        return $this->sendResponse(['found' => true], 'Tracking retrieved successfully');
    }


     /**
     * Get My Order
     * GET /packages/my/order
     *
     * @param  int $id
     *
     * @return Response
     */
    public function getTracking($number)
    {
        $this->packageViewRepository->pushCriteria(new OrWhereCriteria(['reference_no' => $number], ['tracking_no' => $number]));
        $package = $this->packageViewRepository->with([
            'status', 
            'tracking_log' => function($q) {
                $q->with(['status', 'location']);
            },
            'picking_location' => function($q) {
                $q->with(['address.location', 'location']);
            }, 
            ])->first([
                    'id',
                    'reference_no',
                    'tracking_id',
                    'last_tracking_status_id',
                    'tracking_no',
                    'estimated_delivery_date',
                    'destination_island',
                    'destinations_address',
                    'destinations_contact_number',
                    'address_name',
                    'transport_medium'
                ]);
                

            if (empty($package)) {
                return $this->sendError('Tracking not found');
            }
            $package->item_count = $package->items->count();

            unset($package['items']);

        return $this->sendResponse($package, 'Tracking');
    }

}
