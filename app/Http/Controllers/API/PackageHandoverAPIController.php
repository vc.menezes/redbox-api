<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePackageHandoverAPIRequest;
use App\Http\Requests\API\UpdatePackageHandoverAPIRequest;
use App\Models\PackageHandover;
use App\Repositories\PackageHandoverRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PackageHandoverController
 * @package App\Http\Controllers\API
 */

class PackageHandoverAPIController extends AppBaseController
{
    /** @var  PackageHandoverRepository */
    private $packageHandoverRepository;

    public function __construct(PackageHandoverRepository $packageHandoverRepo)
    {
        $this->packageHandoverRepository = $packageHandoverRepo;
    }

    /**
     * Display a listing of the PackageHandover.
     * GET|HEAD /packageHandovers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->packageHandoverRepository->pushCriteria(new RequestCriteria($request));
        $this->packageHandoverRepository->pushCriteria(new LimitOffsetCriteria($request));
        $packageHandovers = $this->packageHandoverRepository->all();

        return $this->sendResponse($packageHandovers->toArray(), 'Package Handovers retrieved successfully');
    }

    /**
     * Store a newly created PackageHandover in storage.
     * POST /packageHandovers
     *
     * @param CreatePackageHandoverAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePackageHandoverAPIRequest $request)
    {
        $input = $request->all();

        $packageHandovers = $this->packageHandoverRepository->create($input);

        return $this->sendResponse($packageHandovers->toArray(), 'Package Handover saved successfully');
    }

    /**
     * Display the specified PackageHandover.
     * GET|HEAD /packageHandovers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PackageHandover $packageHandover */
        $packageHandover = $this->packageHandoverRepository->findWithoutFail($id);

        if (empty($packageHandover)) {
            return $this->sendError('Package Handover not found');
        }

        return $this->sendResponse($packageHandover->toArray(), 'Package Handover retrieved successfully');
    }

    /**
     * Update the specified PackageHandover in storage.
     * PUT/PATCH /packageHandovers/{id}
     *
     * @param  int $id
     * @param UpdatePackageHandoverAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePackageHandoverAPIRequest $request)
    {
        $input = $request->all();

        /** @var PackageHandover $packageHandover */
        $packageHandover = $this->packageHandoverRepository->findWithoutFail($id);

        if (empty($packageHandover)) {
            return $this->sendError('Package Handover not found');
        }

        $packageHandover = $this->packageHandoverRepository->update($input, $id);

        return $this->sendResponse($packageHandover->toArray(), 'PackageHandover updated successfully');
    }

    /**
     * Remove the specified PackageHandover from storage.
     * DELETE /packageHandovers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PackageHandover $packageHandover */
        $packageHandover = $this->packageHandoverRepository->findWithoutFail($id);

        if (empty($packageHandover)) {
            return $this->sendError('Package Handover not found');
        }

        $packageHandover->delete();

        return $this->sendResponse($id, 'Package Handover deleted successfully');
    }
}
