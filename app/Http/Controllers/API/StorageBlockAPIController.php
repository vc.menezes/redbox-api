<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStorageBlockAPIRequest;
use App\Http\Requests\API\UpdateStorageBlockAPIRequest;
use App\Models\StorageBlock;
use App\Repositories\StorageBlockRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StorageBlockController
 * @package App\Http\Controllers\API
 */

class StorageBlockAPIController extends AppBaseController
{
    /** @var  StorageBlockRepository */
    private $storageBlockRepository;

    public function __construct(StorageBlockRepository $storageBlockRepo)
    {
        $this->storageBlockRepository = $storageBlockRepo;
    }

    /**
     * Display a listing of the StorageBlock.
     * GET|HEAD /storageBlocks
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->storageBlockRepository->pushCriteria(new RequestCriteria($request));
        $this->storageBlockRepository->pushCriteria(new LimitOffsetCriteria($request));
        $storageBlocks = $this->storageBlockRepository->all();

        return $this->sendResponse($storageBlocks->toArray(), 'Storage Blocks retrieved successfully');
    }

    /**
     * Store a newly created StorageBlock in storage.
     * POST /storageBlocks
     *
     * @param CreateStorageBlockAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStorageBlockAPIRequest $request)
    {
        $input = $request->only('block_id', 'package_id');

        $storageBlocks = $this->storageBlockRepository->create($input);

        return $this->sendResponse($storageBlocks->toArray(), 'Storage Block saved successfully');
    }

    /**
     * Display the specified StorageBlock.
     * GET|HEAD /storageBlocks/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StorageBlock $storageBlock */
        $storageBlock = $this->storageBlockRepository->findWithoutFail($id);

        if (empty($storageBlock)) {
            return $this->sendError('Storage Block not found');
        }

        return $this->sendResponse($storageBlock->toArray(), 'Storage Block retrieved successfully');
    }

    /**
     * Update the specified StorageBlock in storage.
     * PUT/PATCH /storageBlocks/{id}
     *
     * @param  int $id
     * @param UpdateStorageBlockAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStorageBlockAPIRequest $request)
    {
        $input = $request->all();

        /** @var StorageBlock $storageBlock */
        $storageBlock = $this->storageBlockRepository->findWithoutFail($id);

        if (empty($storageBlock)) {
            return $this->sendError('Storage Block not found');
        }

        $storageBlock = $this->storageBlockRepository->update($input, $id);

        return $this->sendResponse($storageBlock->toArray(), 'StorageBlock updated successfully');
    }

    /**
     * Remove the specified StorageBlock from storage.
     * DELETE /storageBlocks/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StorageBlock $storageBlock */
        $storageBlock = $this->storageBlockRepository->findWithoutFail($id);

        if (empty($storageBlock)) {
            return $this->sendError('Storage Block not found');
        }

        $storageBlock->delete();

        return $this->sendResponse($id, 'Storage Block deleted successfully');
    }

    /**
     * Remove the  packages 
     * package_id
     * block_id
     * GET|HEAD package_view/package/remove
     *
     * @param  int $profile_id
     *
     * @return Response
     */
    public function removePackage(Request $request)
    {
        $conditions = $request->only('block_id', 'package_id');
       
        $package = $this->storageBlockRepository->findWhere($conditions)->first();

        if (empty($package)) {
            return $this->sendError('Storage Block not found');
        }

        $package->delete();
        
        return $this->sendResponse($conditions, 'Package removed successfully');
    }
}
