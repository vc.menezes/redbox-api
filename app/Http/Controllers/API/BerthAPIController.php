<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBerthAPIRequest;
use App\Http\Requests\API\UpdateBerthAPIRequest;
use App\Models\Berth;
use App\Repositories\BerthRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BerthController
 * @package App\Http\Controllers\API
 */

class BerthAPIController extends AppBaseController
{
    /** @var  BerthRepository */
    private $berthRepository;

    public function __construct(BerthRepository $berthRepo)
    {
        $this->berthRepository = $berthRepo;
    }

    /**
     * Display a listing of the Berth.
     * GET|HEAD /berths
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->berthRepository->pushCriteria(new RequestCriteria($request));
        $this->berthRepository->pushCriteria(new LimitOffsetCriteria($request));
        $berths = $this->berthRepository->with('location')->paginate();

        return $this->sendResponse($berths->toArray(), 'Berths retrieved successfully');
    }

    /**
     * Store a newly created Berth in storage.
     * POST /berths
     *
     * @param CreateBerthAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBerthAPIRequest $request)
    {
        $input = $request->except('location');

        $berths = $this->berthRepository->create($input);

        return $this->sendResponse($berths->toArray(), 'Berth saved successfully');
    }

    /**
     * Display the specified Berth.
     * GET|HEAD /berths/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Berth $berth */
        $berth = $this->berthRepository->findWithoutFail($id);

        if (empty($berth)) {
            return $this->sendError('Berth not found');
        }

        return $this->sendResponse($berth->toArray(), 'Berth retrieved successfully');
    }

    /**
     * Update the specified Berth in storage.
     * PUT/PATCH /berths/{id}
     *
     * @param  int $id
     * @param UpdateBerthAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBerthAPIRequest $request)
    {
        $input = $request->except('location');

        /** @var Berth $berth */
        $berth = $this->berthRepository->findWithoutFail($id);

        if (empty($berth)) {
            return $this->sendError('Berth not found');
        }

        $berth = $this->berthRepository->update($input, $id);

        return $this->sendResponse($berth->toArray(), 'Berth updated successfully');
    }

    /**
     * Remove the specified Berth from storage.
     * DELETE /berths/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Berth $berth */
        $berth = $this->berthRepository->findWithoutFail($id);

        if (empty($berth)) {
            return $this->sendError('Berth not found');
        }

        $berth->delete();

        return $this->sendResponse($id, 'Berth deleted successfully');
    }
}
