<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAttachableAPIRequest;
use App\Http\Requests\API\UpdateAttachableAPIRequest;
use App\Models\Attachable;
use App\Repositories\AttachableRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AttachableController
 * @package App\Http\Controllers\API
 */

class AttachableAPIController extends AppBaseController
{
    /** @var  AttachableRepository */
    private $attachableRepository;

    public function __construct(AttachableRepository $attachableRepo)
    {
        $this->attachableRepository = $attachableRepo;
    }

    /**
     * Display a listing of the Attachable.
     * GET|HEAD /attachables
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->attachableRepository->pushCriteria(new RequestCriteria($request));
        $this->attachableRepository->pushCriteria(new LimitOffsetCriteria($request));
        $attachables = $this->attachableRepository->all();

        return $this->sendResponse($attachables->toArray(), 'Attachables retrieved successfully');
    }

    /**
     * Store a newly created Attachable in storage.
     * POST /attachables
     *
     * @param CreateAttachableAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAttachableAPIRequest $request)
    {
        $input = $request->all();

        if($request->hasFile('files')) {
            $this->attachableRepository->attachFiles($request->file('files'), $request);
        }

        // $attachables = $this->attachableRepository->create($input);

        return $this->sendResponse([], 'Attachable saved successfully');
    }

    /**
     * Display the specified Attachable.
     * GET|HEAD /attachables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Attachable $attachable */
        $attachable = $this->attachableRepository->findWithoutFail($id);

        if (empty($attachable)) {
            return $this->sendError('Attachable not found');
        }
        return response()->file(storage_path().'/app/'.$attachable->type.'/'.$attachable->name);
        
        // $contents = \Storage::get($attachable->type.'/'.$attachable->name);
        // return \Response::make($contents, 200, [
        //     'Content-Type' => $attachable->mime_type."/".$attachable->extension,
        //     'Content-Disposition' => 'inline; ' . $attachable->filename,
        //     'Cache-Control' => 'no-cache, must-revalidate'
        // ]);

    }

    /**
     * Update the specified Attachable in storage.
     * PUT/PATCH /attachables/{id}
     *
     * @param  int $id
     * @param UpdateAttachableAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAttachableAPIRequest $request)
    {
        $input = $request->all();

        /** @var Attachable $attachable */
        $attachable = $this->attachableRepository->findWithoutFail($id);

        if (empty($attachable)) {
            return $this->sendError('Attachable not found');
        }

        $attachable = $this->attachableRepository->update($input, $id);

        return $this->sendResponse($attachable->toArray(), 'Attachable updated successfully');
    }

    /**
     * Remove the specified Attachable from storage.
     * DELETE /attachables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Attachable $attachable */
        $attachable = $this->attachableRepository->findWithoutFail($id);

        if (empty($attachable)) {
            return $this->sendError('Attachable not found');
        }

        if(!$this->attachableRepository->deleteFile($attachable->type.'/'.$attachable->name)) {
            return $this->sendError('Cant delete file');
        }
        
        $attachable->delete();

        return $this->sendResponse($id, 'Attachable deleted successfully');
    }

    public function getProfileAttachble($id, Request $request)
    {
        $files = $this->attachableRepository->findWhere([
            'attachable_id' => $id,
            'attachable_type' => 'App\\Models\\'.$request->type,
        ])->all();

        return $this->sendResponse($files, 'Attachable deleted successfully');
    }
}
