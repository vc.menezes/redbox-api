<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRemarkAPIRequest;
use App\Http\Requests\API\UpdateRemarkAPIRequest;
use App\Models\Remark;
use App\Repositories\RemarkRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RemarkController
 * @package App\Http\Controllers\API
 */

class RemarkAPIController extends AppBaseController
{
    /** @var  RemarkRepository */
    private $remarkRepository;

    public function __construct(RemarkRepository $remarkRepo)
    {
        $this->remarkRepository = $remarkRepo;
    }

    /**
     * Display a listing of the Remark.
     * GET|HEAD /remarks
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->remarkRepository->pushCriteria(new RequestCriteria($request));
        $this->remarkRepository->pushCriteria(new LimitOffsetCriteria($request));
        $remarks = $this->remarkRepository->all();

        return $this->sendResponse($remarks->toArray(), 'Remarks retrieved successfully');
    }

    /**
     * Store a newly created Remark in storage.
     * POST /remarks
     *
     * @param CreateRemarkAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRemarkAPIRequest $request)
    {
        $input = $request->all();
        $input['remarkable_type'] = 'App/Models/'.$request->model_name;
        $input['remarkable_id'] = $request->id;

        $remarks = $this->remarkRepository->create($input);

        return $this->sendResponse($remarks->toArray(), 'Remark saved successfully');
    }

    /**
     * Display the specified Remark.
     * GET|HEAD /remarks/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Remark $remark */
        $remark = $this->remarkRepository->findWithoutFail($id);

        if (empty($remark)) {
            return $this->sendError('Remark not found');
        }

        return $this->sendResponse($remark->toArray(), 'Remark retrieved successfully');
    }

    /**
     * Update the specified Remark in storage.
     * PUT/PATCH /remarks/{id}
     *
     * @param  int $id
     * @param UpdateRemarkAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRemarkAPIRequest $request)
    {
        $input = $request->all();

        /** @var Remark $remark */
        $remark = $this->remarkRepository->findWithoutFail($id);

        if (empty($remark)) {
            return $this->sendError('Remark not found');
        }

        $remark = $this->remarkRepository->update($input, $id);

        return $this->sendResponse($remark->toArray(), 'Remark updated successfully');
    }

    /**
     * Remove the specified Remark from storage.
     * DELETE /remarks/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Remark $remark */
        $remark = $this->remarkRepository->findWithoutFail($id);

        if (empty($remark)) {
            return $this->sendError('Remark not found');
        }

        $remark->delete();

        return $this->sendResponse($id, 'Remark deleted successfully');
    }
}
