<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSignatureAPIRequest;
use App\Http\Requests\API\UpdateSignatureAPIRequest;
use App\Models\Signature;
use App\Repositories\SignatureRepository;
use App\Repositories\AttachableRepository;
use App\Repositories\PackageHandoverRepository;
use App\Repositories\TrackingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SignatureController
 * @package App\Http\Controllers\API
 */

class SignatureAPIController extends AppBaseController
{
    /** @var  SignatureRepository */
    private $signatureRepository;
    private $attachebleRepository;
    private $packageHandoverRepository;

    public function __construct(SignatureRepository $signatureRepo, AttachableRepository $attachableRepo, PackageHandoverRepository $packageHandoverRepo, TrackingRepository $trackingRepo)
    {
        $this->signatureRepository = $signatureRepo;
        $this->attachebleRepository = $attachableRepo;
        $this->packageHandoverRepository = $packageHandoverRepo;
        $this->trackingRepository = $trackingRepo;
    }

    /**
     * Display a listing of the Signature.
     * GET|HEAD /signatures
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->signatureRepository->pushCriteria(new RequestCriteria($request));
        $this->signatureRepository->pushCriteria(new LimitOffsetCriteria($request));
        $signatures = $this->signatureRepository->all();

        return $this->sendResponse($signatures->toArray(), 'Signatures retrieved successfully');
    }

    /**
     * Store a newly created Signature in storage.
     * POST /signatures
     *
     * @param CreateSignatureAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSignatureAPIRequest $request)
    {
        $input = $request->all();

        $signature = $this->signatureRepository->create($input);
        $packageHandovers = $this->packageHandoverRepository->create($input);

        $image = $request->image;
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = time().'.'.'png';

        \File::put(storage_path(). '/app/signature/' . $imageName, base64_decode($image));


        $input['name'] = $imageName;
        $input['attachable_id'] = $signature->id;
        $input['attachable_type'] = 'App\\Models\\Signature';
        $input['extension'] = 'png';
        $input['type'] = 'signature';
        $attachebles = $this->attachebleRepository->create($input);

        $package = \App\Models\Package::find($request->package_id);
        $this->trackingRepository->createTrackingLog($package, 6, $request->location_id, 'Delivered to final destination');
        $package->tracking()->update(['status' => 0]);

        return $this->sendResponse($attachebles->toArray(), 'Signature saved successfully');
    }

    /**
     * Display the specified Signature.
     * GET|HEAD /signatures/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Signature $signature */
        $signature = $this->signatureRepository->findWithoutFail($id);

        if (empty($signature)) {
            return $this->sendError('Signature not found');
        }

        return $this->sendResponse($signature->toArray(), 'Signature retrieved successfully');
    }

    /**
     * Update the specified Signature in storage.
     * PUT/PATCH /signatures/{id}
     *
     * @param  int $id
     * @param UpdateSignatureAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSignatureAPIRequest $request)
    {
        $input = $request->all();

        /** @var Signature $signature */
        $signature = $this->signatureRepository->findWithoutFail($id);

        if (empty($signature)) {
            return $this->sendError('Signature not found');
        }

        $signature = $this->signatureRepository->update($input, $id);

        return $this->sendResponse($signature->toArray(), 'Signature updated successfully');
    }

    /**
     * Remove the specified Signature from storage.
     * DELETE /signatures/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Signature $signature */
        $signature = $this->signatureRepository->findWithoutFail($id);

        if (empty($signature)) {
            return $this->sendError('Signature not found');
        }

        $signature->delete();

        return $this->sendResponse($id, 'Signature deleted successfully');
    }
}
