<?php

namespace App\Http\Controllers\API;


use App\Repositories\ProfileViewRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\FilterColumnsCriteria;
use App\Criteria\OrWhereCriteria;
use Response;

/**
 * Class ProfileViewController
 * @package App\Http\Controllers\API
 */

class ProfileViewAPIController extends AppBaseController
{
    /** @var  ProfileViewRepository */
    private $profileViewRepository;

    public function __construct(ProfileViewRepository $profileViewRepo)
    {
        $this->profileViewRepository = $profileViewRepo;
    }

    /**
     * Display a listing of the ProfileView.
     * GET|HEAD /profileViews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if($request->user_type) {
            if($request->user_type == 'both') {
                $this->profileViewRepository->pushCriteria(new OrWhereCriteria(['user_type' => $request->one], ['user_type' => $request->two]));
            } else {
                $this->profileViewRepository->pushCriteria(new FilterColumnsCriteria($request->only('user_type')));
            }
        }
        $this->profileViewRepository->pushCriteria(new RequestCriteria($request));
        $this->profileViewRepository->pushCriteria(new LimitOffsetCriteria($request));
        $profileViews = $this->profileViewRepository->with('location')->paginate($request->pageSize);

        return $this->sendResponse($profileViews->toArray(), 'Profile Views retrieved successfully');
    }


    /**
     * Display the specified ProfileView.
     * GET|HEAD /profileViews/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ProfileView $profileView */
        $profileView = $this->profileViewRepository->with(['bankAccount.bank'])->findWithoutFail($id);
        $profileView->contacts = \App\Models\Contactable::where(['contactable_id' => $id, 'contactable_type' => 'App\\Models\\Profile'])->get();

        if (empty($profileView)) {
            return $this->sendError('Profile View not found');
        }

        return $this->sendResponse($profileView->toArray(), 'Profile View retrieved successfully');
    }

    
}
