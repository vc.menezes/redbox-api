<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTrackingLogAPIRequest;
use App\Http\Requests\API\UpdateTrackingLogAPIRequest;
use App\Models\TrackingLog;
use App\Repositories\TrackingLogRepository;
use App\Repositories\TrackingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TrackingLogController
 * @package App\Http\Controllers\API
 */

class TrackingLogAPIController extends AppBaseController
{
    use \App\Traits\Jwt;

    /** @var  TrackingLogRepository */
    private $trackingLogRepository;
    private $trackingRepository;

    public function __construct(TrackingLogRepository $trackingLogRepo, TrackingRepository $trackingRepo)
    {
        $this->trackingLogRepository = $trackingLogRepo;
        $this->trackingRepository = $trackingRepo;
    }

    /**
     * Display a listing of the TrackingLog.
     * GET|HEAD /trackingLogs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->trackingLogRepository->pushCriteria(new RequestCriteria($request));
        $this->trackingLogRepository->pushCriteria(new LimitOffsetCriteria($request));
        $trackingLogs = $this->trackingLogRepository->all();

        return $this->sendResponse($trackingLogs->toArray(), 'Tracking Logs retrieved successfully');
    }

    /**
     * Store a newly created TrackingLog in storage.
     * POST /trackingLogs
     *
     * @param CreateTrackingLogAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTrackingLogAPIRequest $request)
    {
        $input = $request->only('tracking_id', 'status_id', 'location_id', 'type', 'description', 'country_id');
        // $input['profile_id'] = $this->getProfileId();
        // $trackingLogs = $this->trackingLogRepository->create($input);
        
        // if($status_id == 6) {
        //     $this->trackingLogRepository->updateParent($trackingLogs);
        // }

        $package = \App\Models\Package::find($request->package_id);
        $this->trackingRepository->updateChildTrackingLogs($package, $request->status_id, $request->location_id, $request->description);

        // $trackingLog = $this->trackingLogRepository->with('tracking.package.child')->findByField('tracking_id', $trackingLogs->tracking_id)->last();
       
        // if ($trackingLog->tracking->package->type == 'shipment') {
        //     if($trackingLogs->status_id == 4) {
        //         $trackingLog->tracking->package->block()->detach();
        //     }
        //     foreach ($trackingLog->tracking->package->child as $order) {
        //         $this->trackingRepository->createTrackingLog($order, $input['status_id'], $request->location_id, $request->description);
        //     }
        // }
        
        
        
        return $this->sendResponse($package->toArray(), 'Tracking Log saved successfully');
    }

    /**
     * Display the specified TrackingLog.
     * GET|HEAD /trackingLogs/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TrackingLog $trackingLog */
        $trackingLog = $this->trackingLogRepository->findWithoutFail($id);

        if (empty($trackingLog)) {
            return $this->sendError('Tracking Log not found');
        }

        return $this->sendResponse($trackingLog->toArray(), 'Tracking Log retrieved successfully');
    }

    /**
     * Update the specified TrackingLog in storage.
     * PUT/PATCH /trackingLogs/{id}
     *
     * @param  int $id
     * @param UpdateTrackingLogAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTrackingLogAPIRequest $request)
    {
        $input = $request->only('tracking_id', 'status_id', 'location_id', 'type', 'description', 'country_id');

        /** @var TrackingLog $trackingLog */
        $trackingLog = $this->trackingLogRepository->with('tracking.package.child')->findWithoutFail($id);

        if (empty($trackingLog)) {
            return $this->sendError('Tracking Log not found');
        }

        $trackingLog = $this->trackingLogRepository->update($input, $id);
     
        if($trackingLog->status_id == 6) {
            $this->trackingLogRepository->updateParent($trackingLog);
        }

        if ($trackingLog->tracking->package->type == 'shipment') {
            foreach ($trackingLog->tracking->package->child as $order) {
                $log = $order->tracking->tracking_log->last();
                $log->status_id = $request->status_id;
                $log->location_id = $request->location_id;
                $log->description = $request->description;
                $log->save();
            }
        }

        return $this->sendResponse($trackingLog->toArray(), 'TrackingLog updated successfully');
    }

    /**
     * Remove the specified TrackingLog from storage.
     * DELETE /trackingLogs/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TrackingLog $trackingLog */
        $trackingLog = $this->trackingLogRepository->findWithoutFail($id);

        if (empty($trackingLog)) {
            return $this->sendError('Tracking Log not found');
        }

        $trackingLog->delete();

        return $this->sendResponse($id, 'Tracking Log deleted successfully');
    }
}
