<?php

namespace App\Http\Middleware;

use Closure;

class CheckAbility
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // get current routename
        $current_route = $request->route()->getName();

        $explode = explode(".",$current_route);

        $resource = $explode[1];

        $action = $explode[2];

        $permissions_name = $resource."_".$action;


        $user = \App\User::find(\JWTAuth::parseToken()->toUser()->id);

        if ($user->status == 0) {
            return response()->json([
                'message' => 'User not verified. Unauthorized',
                'code' => 401
            ],401);
        }

        if ($user->can($permissions_name)){
            return $next($request);

        } else {
            return response()->json([
              'message' => 'Unauthorized',
              'permissions_name' => $permissions_name,
              'code' => 401
            ],401);
        }

    }
}
