<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 18/10/2018
 * Time: 2:38 PM
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Redbox extends Facade
{
    protected static function getFacadeAccessor() { return 'redbox'; }
}