<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;

class TrackingUpdated extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $package;
    public $log = 'No remarks';

    public function __construct($package)
    {
        $this->package = $package;
        $status = \App\Models\TrackingLog::where(['tracking_id' => $package->tracking->id, 'status_id' => 4])->first();
        if($status) {
            $this->log = $status->description  ? $status->description : 'No remarks';
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['nexmo'];
    }

    public function toNexmo($notifiable)
    {
        return (new NexmoMessage)
            ->content(
                "New Shipment alart:\n".
                "\nShipment ID: ". $this->package->code. 
                "\nTo: ". $this->package->profile->name. ", ". ( $this->package->profile->location ? $this->package->profile->location->name : " ").
                "\nBoat: ". $this->package->shipment->vessel->name.
                "\nNo. of box: ". count($this->package->child).    
                "\nStatus: Shipped from male". 
                "\nRemarks: ". $this->log. 
                "\n\nPlease update the shipment status from the Redbox Mobile app. More info: 7911887"    
            )
            ->from('Redbox');

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
