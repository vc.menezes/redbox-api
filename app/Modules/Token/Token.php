<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 20/07/2018
 * Time: 7:29 PM
 */

namespace App\Modules\Token;


class Token
{
    public $new_token;
    public $current;
    public $generated;
    public function __construct($user_id=null)
    {
        /**
         * generate fresh random token
         */
        $this->new_token = rand(10000,99999);

        /**
         * looking for the newly generated token from database to prevent duplicates
         */
        $this->current = \App\Models\Token::whereExpired(0)->whereToken($this->new_token)->first();

        /**
         * if the newly generated token found from database which means , need another unique token for the process
         */
        while (!empty($this->current)){
            $this->new_token= rand(10000,99999);
            $this->current   = \App\Models\Token::whereExpired(0)->whereToken($this->new_token)->first();
        }

        /**
         * here we got new token and its time to store it as new token
         */
        $arra = [
            'token' =>$this->new_token,
            'medium' =>'sms',
            'expires_at' => \Carbon\Carbon::now()->addMinutes(5),
            'expired' =>0,
            'user_id'=>$user_id
        ];

        $new =new  \App\Models\Token($arra);
        $new->save();

        $this->generated = $new;


    }

    /**
     * @return string
     * get new token for the use
     */
    public function getToken(){
     return $this->generated->token;
    }


}