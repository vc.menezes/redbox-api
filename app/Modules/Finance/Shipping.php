<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 16/08/2018
 * Time: 2:38 AM
 */

namespace App\Modules\Finance;


use App\Exceptions\RateNotExist;
use App\Models\Rate;

class Shipping implements FinanceInterface
{
    public $input;
    public $rate;
    public $total;
    public $profile_id;
    public $change_name;
    public $charge;
    public $currency;
    public $status;
    public $package;
    public $invoice_data;
    public $invoice_items = [];

    public function __construct($input, $package = null, $change_name = null)
    {
        $this->input = $input;
        $this->input['weight'] = floor($input['weight']);
        $this->change_name = $change_name ? $change_name : 'Shipping Charge';
        $this->profile_id = $package ? $package->profile_id : null;
        $this->package = $package;


    }

    public function calculate()
    {
        $this->status = 'Rate Calculate Successfully';

        $this->charge = $this->getRate();
        

        $this->total = $this->getTotal($this->charge);

        $this->invoice_data = $this->build_invoice_data();

        $this->get_invoice_items($this->invoice_data);

        return $this;


    }

    public function getTotal($object)
    {

        $picking_charge = $this->add_picking_charges();
        if ($object) {

            $this->rate = $object->rate;

            if ($object->type == 'international') {
                $this->currency = 'USD';
                return round($object->rate * $object->weight, 2) + $picking_charge;

            } else {
                $this->currency = 'MVR';
                return $object->rate + $picking_charge;

            }
        } else {
            $this->status = "Cannot find valid rate";
        }
    }

    public function getRate()
    {
         
        $rate = Rate::where($this->input)->first();

        

        return $rate;
    }

    public function build_invoice_data()
    {
        $invoice = [];
        $invoice['profile_id'] = $this->profile_id;
        $invoice['details'] = $this->getInvoiceDetails();
        $invoice['qty'] = $this->charge->weight;
        $invoice['data_type'] = get_class($this->package);
        $invoice['data_id'] = $this->package->id;
        $invoice['rate'] = $this->charge->rate;
        $invoice['UOM'] = 'kg';
        $invoice['charge_type'] = 'shipping';
        $invoice['currency'] = $this->currency;
        return $invoice;
    }

    public function getInvoiceDetails()
    {
         
        $str = '';
        $str = $this->change_name . ' of ';
        $str .= $this->charge->weight . ' Kg ';
        $str .= ', from ' . $this->charge->from->name;
        $str .= ' to ' . $this->package->package_destination->location->name;
    
        return $str;

    }

    public function get_invoice_items($data)
    {
        $item = [
            'invoice_id' => null,
            'item_code' => $this->package->reference_no ? $this->package->reference_no : $this->package->tracking->tracking_no,
            'details' => $data['details'],
            'rate' => $data['rate'],
            'UOM' => $data['UOM'],
            'qty' => $data['qty'],
            'type' => 'charge',
            'charge_type' => 'shipping'
        ];

        array_push($this->invoice_items, $item);

    }

    public function add_picking_charges()
    {
        //createing invoice for picking hcarges
        if ($this->package->pickingLocation) {
            $pickingCharge = new PickingCharge($this->package);
            $pickingCharge->calculate();
            if ($pickingCharge->total > 0) {
                $item = [
                    'invoice_id' => null,
                    'item_code' => 'p'.$this->getItemCode($this->package),
                    'details' => $pickingCharge->details,
                    'rate' => $pickingCharge->total,
                    'UOM' => 'kg',
                    'qty' => 1,
                    'type' => 'charge',
                    'charge_type' => 'picking'
                ];
                array_push($this->invoice_items, $item);
                return $pickingCharge->total;
            }
        }
        return 0;
    }

    public function getItemCode($package)
    {
        if (is_null($package->reference_no)) {
            return $package->tracking->tracking_no;
        }
        return $package->reference_no;
    }
}