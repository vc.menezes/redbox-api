<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 16/08/2018
 * Time: 2:38 AM
 */

namespace App\Modules\Finance;

use App\Events\InvoiceGenerated;
use App\Exceptions\InvoiceAlreadyExist;
use App\Helpers\RedBox;
use App\Jobs\GiveItemCode;
use App\Jobs\GiveSequence;
use App\Models\CreditLimit;
use App\Models\Invoice as ModelsInvoice;
use App\Models\InvoiceItem;
use App\Models\Setting;

class Invoice
{
    public $result;
    public $GST;
    public $error;
    public $invoice;
    public $invoice_type;
    public $data_type;
    public $data_id;

    public function __construct(FinanceInterface $finnace)
    {
        //get
        $this->result = $finnace->calculate();
        $this->GST = Setting::where("key", "GST")->first();
        $this->error = null;


    }

    public function create()

    {

        $invoice = new  ModelsInvoice($this->result->invoice_data);
        $this->data_id = $this->result->invoice_data['data_id'];
        $this->data_type = $this->result->invoice_data['data_type'];
        $this->invoice_type = $this->result->invoice_data['charge_type'];

        $invoice->serial_number = "";
        $invoice->tax = \Redbox::get_tax($this->result->total);
        $invoice->total = \Redbox::get_tax($this->result->total) + $this->result->total;
        $invoice->type = $this->getType();
        $invoice->status = $this->getType();


        try {
            if ($this->checkRepeat()) {
              throw new InvoiceAlreadyExist();
            }
        } catch (InvoiceAlreadyExist $e) {
            $this->error = $e;
            return $this;
        }


        if ($invoice->save()) {
            $this->invoice = $invoice;
            $this->create_item($invoice,$this->result->invoice_items);
            GiveSequence::dispatch($invoice, 'serial_number', 'invoice_number');
            event(new InvoiceGenerated($invoice));
            \RedBox::RaisePayload($this->invoice->profile,$this->invoice);
        }

        return $this;
    }


    public function getInvoiceDetails()
    {
        $str = '';
        $str = $this->result->change_name . ' of ';
        $str .= $this->result->charge->weight . ' Kg';
        $str .= ', from' . $this->result->charge->from->name;
        $str .= ' to ' . $this->result->package->package_destination->location->name;
        return $str;

    }




    public function getType(): int
    {
        $totalPending = \App\Models\Invoice::whereProfileId($this->result->profile_id)->whereType(1)->whereIn('status', [2, 1])->sum('total');
        $creadit_limit = CreditLimit::whereProfileId($this->result->profile_id)->first();
        if ($creadit_limit) {
            if ($creadit_limit->limit > $totalPending) {
                return 1;
            } else {
                return 2;
            }
        }
        return 2;
    }


    public function checkRepeat(): bool
    {
        $invoice = \App\Models\Invoice::whereDataType($this->data_type)
            ->whereDataId($this->data_id)->whereChargeType($this->invoice_type)
            ->whereIn('status',[0,1,2])->first();
        if ($invoice)
            return true;

        return false;
    }

    public function create_item($invoice,$items){
        foreach ($items as $item){
            $item['invoice_id'] = $invoice->id;
            $newimte = new InvoiceItem($item);
            $newimte->save();
        }

        GiveItemCode::dispatch($invoice->id);
    }
}

