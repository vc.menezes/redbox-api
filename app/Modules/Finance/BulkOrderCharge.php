<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 16/08/2018
 * Time: 2:38 AM
 */

namespace App\Modules\Finance;


use App\Exceptions\RateNotExist;
use App\Models\BulkOrder;
use App\Models\Package;
use App\Models\Rate;
use App\Models\Setting;
use App\Repositories\LocationRepository;

class BulkOrderCharge implements FinanceInterface
{

    public $rate;
    public $total;
    public $profile_id;
    public $change_name;
    public $charge;
    public $currency;
    public $status;
    public $model;
    public $orders;
    public $locationRepository;
    public $origin;
    public $discount;
    public $invoice_data;
    public $invoice_items = [];
    public $dis_count_amount;
    public $discrete_tariff;

    /**
     * BulkOrderCharge constructor.
     * @param $input
     * @param BulkOrder $bulkOrder
     * @param null $change_name
     *
     * this bulk order and get
     * get all the order under bulk oder
     * calculate each total and sum up each
     *
     */

    public function __construct($model,$discrete_tariff=false)
    {
        //details for invoice
        $this->change_name = 'Bulk order';
        $this->profile_id = $model ? $model->profile_id : null;
        $this->model = $model;
        $this->discount = \Redbox::getSetting('AGENT_DISCOUNT');
        $this->currency = 'MVR';
        $this->dis_count_amount = 0;


    }

    public function calculate()
    {
        $this->status = 'Rate Calculate Successfully';

        $this->total = $this->getTotal($this->model->packages);

        $this->invoice_data = $this->build_invoice_data();


        return $this;

    }

    public function getTotal($packages)
    {

        $total = 0;

        foreach ($packages as $package) {
            $package = Package::find($package->id);
            //get rate object for each packages
            $rate = $this->getRate($package);

            $this->get_invoice_items($rate, $package);
            if ($rate) {

                if ($this->origin == 'international') {

                    $total = $total + (round($rate->rate * $package->weight, 2) - $this->discount);
                } else {
                    $total = $total + ($rate->rate - $this->discount);

                }
                $this->dis_count_amount = $this->dis_count_amount + $this->discount;


            }

        }
        $this->get_discount_items();
        return $total;

    }


    public function package_cost($package, $is_discount = true)
    {
        //take discount as temporary
        $discount = \Redbox::getSetting('AGENT_DISCOUNT');

        //if discount is disable set it to zero
        if (!$is_discount)
            $discount = 0;

        //get rate
        $rate = $this->getRate($package);

        if ($rate) {

            if ($this->origin == 'international') {
                return round($rate->rate * $package->weight, 2) - $discount;
            } else {
                return ($rate->rate - $discount);

            }

        }
    }


    public function getRate($package)
    {
        $package = Package::find($package->id);
        $input['weight'] = floor($package->weight);
        $input['operator'] = '=';

        if($this->discrete_tariff){
					$input['discrete_tariff_profile_id'] = $this->profile_id;
				}

        $DEFAULT_FROM_LOCATION = \Redbox::getSetting('DEFAULT_FROM_LOCATION');

        $parent_id = $package->package_destination->location->parent_id;

        if ($package->weight < 1) {
            $input['weight'] = ceil($package->weight);
            $input['operator'] = '<';
        }

        $input['type'] = \Redbox::getOrigin($parent_id);
        $input['medium'] = $package->package_destination->medium;

        $input['from_id'] = $DEFAULT_FROM_LOCATION;

        $input['to_id'] = '*';


        return Rate::where($input)->first();


    }

    public function setOrigin($packages)
    {
        $parent_id = $packages[0]->package_destination->location->parent_id;
        $this->origin = $this->locationRepository->GetOrigin($parent_id);
    }

    public function build_invoice_data()
    {
        $invoice = [];
        $invoice['profile_id'] = $this->profile_id;
        $invoice['details'] = $this->getInvoiceDetails();
        $invoice['qty'] = $this->model->weight;
        $invoice['data_type'] = get_class($this->model);
        $invoice['data_id'] = $this->model->id;
        $invoice['rate'] = $this->total;
        $invoice['UOM'] = 'kg';
        $invoice['charge_type'] = 'bulkorder';
        $invoice['currency'] = $this->currency;
        return $invoice;
    }

    public function getInvoiceDetails()
    {
        $str = '';
        $str = $this->change_name . ' of ';
        $str .= $this->model->description . ' - ';
        $str .= $this->model->weight . ' Kg';

        return $str;

    }

    public function itemDetails($rate, $package)
    {
        $str = 'shipping charge of ';
        $str .= $package->weight . ' Kg ';
        $str .= ', from ' . $rate->from->name;
        $str .= '  to  ' . $package->package_destination->location->name;
        return $str;
    }

    public function get_invoice_items($data, $package)
    {
        $item_code = $package->reference_no ? $package->reference_no : $package->tracking->tracking_no;

        $item = [
            'invoice_id' => null,
            'details' => $this->itemDetails($data, $package),
            'rate' => (\Redbox::get_total($package,false)-5),
            'UOM' => 'kg',
            'qty' => $package->weight,
            'type' => 'charge',
            'item_code' =>$item_code,
            'charge_type' =>'bulk_order_shipping',
        ];

        array_push($this->invoice_items, $item);

    }

    public function get_discount_items(){
        if($this->dis_count_amount > 0){

            $item = [
                'invoice_id' => null,
                'details' => 'discount for ' . count($this->model->packages) . ' packages',
                'rate' => ($this->dis_count_amount * -1),
                'UOM' => 'package',
                'qty' => count($this->model->packages),
                'type' => 'discount',
                'item_code' => 'D100',
                'charge_type' => 'discount'
            ];
            array_push($this->invoice_items, $item);
        }
    }
}
