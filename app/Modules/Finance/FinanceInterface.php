<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 16/08/2018
 * Time: 2:37 AM
 */
namespace App\Modules\Finance;


interface FinanceInterface
{
 public function calculate();
 public function getTotal($param);
 public function build_invoice_data();
 public function getInvoiceDetails();
}