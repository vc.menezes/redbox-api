<?php
namespace App\Modules\Finance;


use App\Exceptions\RateNotExist;
use App\Models\Rate;
use App\Models\Setting;
use App\Modules\Finance\FinanceInterface;

class AgentCommission implements FinanceInterface
{
    public $input;
    public $rate;
    public $total;
    public $profile_id;
    public $change_name;
    public $currency;
    public $status;
    public $invoice;

    public function __construct($input,$invoice=null,$change_name = null )
    {
        $this->input = $input;
        $this->change_name = $change_name ? $change_name : 'Agent Commission';
        $this->profile_id = $invoice ?  $invoice->profile_id : null;
        $this->invoice = $invoice;


    }

    public function calculate()
    {
        $this->status = 'agent Commission Calculate Successfully';

        $this->total = $this->getTotal($this->getRate());

        return $this;


    }

    public function getTotal($object)
    {
        if ($object) {

            $this->rate = $object->value;

            return $object->value / 100 * $this->invoice->total;

        }

    }

    public function getRate()
    {

        $rate = Setting::whereKey('AGENT_COMMISSION')->fist();

        return $rate;
    }

    public function build_invoice_data()
    {
        // TODO: Implement build_invoice_data() method.
    }

    public function getInvoiceDetails()
    {
        // TODO: Implement getInvoiceDetails() method.
    }
}