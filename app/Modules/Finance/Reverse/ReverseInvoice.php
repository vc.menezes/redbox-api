<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 24/10/2018
 * Time: 5:20 PM
 */

namespace App\Modules\Finance\Reverse;


use App\Events\InvoiceCancelled;
use App\Models\BulkOrder;
use App\Models\Invoice;
use App\Models\Package;
use App\Traits\Remark;

class ReverseInvoice
{

    use Remark;

    public $invoice;
    public $reflexion;
    public $remark;
    public $error;
    public $error_message;

    public function __construct($invoice, $remarks)
    {
        $this->invoice = $invoice;

        $this->error = false;

        $this->remark = $remarks;

        $this->ChangeStatus();

    }

    public function ChangeStatus()
    {


        if ($this->invoice->status == 0) {
            $this->error = true;
            $this->error_message = 'Already Paid';
            return $this;
        }


        if ($this->invoice->status == 3) {
            $this->error = true;
            $this->error_message = 'Already Cancelled';
            return $this;
        }

        $this->invoice->status = 3;


        if ($this->invoice->save()) {

            $this->add_remarks($this->invoice, $this->remark);

            event(new InvoiceCancelled($this->invoice));

            $this->error_message = 'reverse successfully';
            $this->error = false;

        }
        return $this;

    }

}