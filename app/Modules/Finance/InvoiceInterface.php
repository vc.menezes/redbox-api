<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 06/11/2018
 * Time: 1:32 PM
 */

namespace App\Modules\Finance;


interface InvoiceInterface
{
    public function BulkOrder();
    public function Package();
    public function PackagePickingLocation();
}