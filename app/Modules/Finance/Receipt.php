<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 18/08/2018
 * Time: 11:36 AM
 */

namespace App\Modules\Finance;
use App\Jobs\GiveSequence;
use App\Models\Receipt as Model;
use App\Models\ReceiptCard;
use App\Models\ReceiptCheque;
use App\Models\ReceiptTranfer;
use App\Traits\Jwt;

class Receipt
{
    use Jwt;
    /*
     * status
     * 1 = paid
     * 0 = cancelled
     * */
    public $invoices;
    public $input;
    public function __construct($invoices,$input)
    {
        $this->invoices = $invoices;
        $this->input  = $input;

    }

    public function create(){
        $receipt = new Model();
        $receipt->customer_profile_id = $this->input['customer_profile_id'];
        $receipt->created_by_profile_id = $this->getProfileId();
        $receipt->receipt_number = '-';
        $receipt->remarks =  $this->input['remarks'];
        $receipt->cash =  round($this->input['cash'],2);
        $receipt->cheque = round( $this->input['cheque'],2);
        $receipt->card =  round($this->input['cheque'],2);
        $receipt->transfer =  round($this->input['transfer'],2);
        $receipt->status =  1;

        if($receipt->save()){
            $this->create_receipt_cheque($receipt);
            $this->create_receipt_card($receipt);
            $this->create_receipt_tranfer($receipt);
            GiveSequence::dispatch($receipt,'receipt_number','receipt_number');
        }
    }

    public function create_receipt_cheque($receipt){
        if($this->input->receipt_cheque){
            $receipt_cheque = new ReceiptCheque();
            $receipt_cheque->receipt_id = $receipt->id;
            $receipt_cheque->cheque_account_no = $this->input->receipt_cheque->cheque_account_no;
            $receipt_cheque->cheque_no = $this->input->receipt_cheque->cheque_no;
            $receipt_cheque->bank_id = $this->input->receipt_cheque->bank_id;
            $receipt_cheque->save();
        }


    }
    public function create_receipt_card($receipt){
        $receipt_card = new ReceiptCard();
        if($this->input->receipt_card){
            $receipt_card->card_type_id = $this->input->receipt_card->card_type_id;
            $receipt_card->transaction_number = $this->input->receipt_card->transaction_number;
            $receipt_card->receipt_id = $receipt->id;
            $receipt_card->save();
        }
    }
    public function create_receipt_tranfer($receipt){
        $receipt_tranfer = new ReceiptTranfer();
        $receipt_tranfer->receipt_id = $receipt->id;
        $receipt_tranfer->ref = $this->input->receipt_tranfer->ref;
        $receipt_tranfer->from = $this->input->receipt_tranfer->from;
        $receipt_tranfer->amount =  $this->input->receipt_tranfer->amount;
        $receipt_tranfer->save();
    }



}


