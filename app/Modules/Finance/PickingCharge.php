<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 16/08/2018
 * Time: 2:38 AM
 */

namespace App\Modules\Finance;


use App\Exceptions\RateNotExist;
use App\Models\Location;
use App\Models\Rate;

class PickingCharge implements FinanceInterface
{
    public $input;
    public $rate;
    public $total;
    public $profile_id;
    public $change_name;
    public $charge;
    public $currency;
    public $status;
    public $package;
    public $invoice_data;
    public $details;
    public $invoice_items = [];
    public function __construct($package)
    {
        //charge name
        $this->change_name = 'Picking charges';
        //connecting invoice to customer
        $this->profile_id = $package ? $package->profile_id : null;

        //package for picking charges
        $this->package = $package;

        $this->currency = 'mvr';


    }

    public function calculate()
    {

        //initial status of rate calculate
        $this->status = 'Rate Calculate Successfully';
        //get charge from packages
        $this->rate = $this->getRate($this->package->pickingLocation->location_id);
        //get rate from obejct
        $this->total = $this->rate;
        //build invoice data
        $this->invoice_data = $this->build_invoice_data();

        $this->details = $this->getInvoiceDetails();

        $this->get_invoice_items($this->invoice_data);

        return $this;

    }

    //this function is useless in this class :P
    public function getTotal($object)
    {
        if ($object) {
            return $object->rate;
        }
    }

    //get most closest rate
    public function getRate($location_id)
    {

        if (is_null($location_id)) {
            return \Redbox::getSetting('DEFAULT_PICKING_CHARGE');
        }
        //get location with picking_charge
        $location = Location::whereId($location_id)->with('picking_charge')->first();


        if ($location && $location->picking_charge) {

            // if picking_charge found in location the app will use that one
            return $location->picking_charge->rate;
        } else {
            // else it will try to get rate from parent
            // so if we set to rate to H,dha atoll than all island will be charge as paren
            // of we set set rate to each island :P
            if ($location->parent_id) {

                return $this->getRate($location->parent_id);
            }

        }

        return \Redbox::getSetting('DEFAULT_PICKING_CHARGE');


    }

    public function build_invoice_data()
    {
        $invoice = [];
        //set profile ID to the invoice which taken from packages
        $invoice['profile_id'] = $this->profile_id;
        //add short details for invoice
        $invoice['details'] = $this->getInvoiceDetails();

        $invoice['qty'] = 1;
        $invoice['data_type'] = get_class($this->package->pickingLocation);
        $invoice['data_id'] = $this->package->pickingLocation->id;
        $invoice['rate'] = $this->rate;
        $invoice['UOM'] = 'kg';
        $invoice['charge_type'] = 'picking_charge';
        $invoice['currency'] = $this->currency;
        return $invoice;
    }

    public function getInvoiceDetails()
    {
        $str = ' Picking Charge ';
        if ($this->package->pickingLocatio) {
            $str .= ' - Pick From  ' . $this->package->pickingLocation->location->name;
        }
        return $str;

    }

    public function get_invoice_items($data)
    {
        $item = [
            'invoice_id' => null,
            'item_code' => $this->package->reference_no ? $this->package->reference_no : $this->package->tracking->tracking_no,
            'details' => $data['details'],
            'rate' => $data['rate'],
            'UOM' => $data['UOM'],
            'qty' => $data['qty'],
            'type' => 'charge',
            'charge_type' => 'picking_charge'
        ];
         
        array_push($this->invoice_items, $item);

    }
}