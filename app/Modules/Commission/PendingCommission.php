<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 2019-03-04
 * Time: 10:56
 */

namespace App\Modules\Commission;
use App\Models\Commission as Model;

class PendingCommission
{


	public function get_by_month($month)
	{
		return Model::with(['profile.location','bank_account'])
			->select(\DB::raw('sum(total) as total,profile_id'))->groupby('profile_id')
			->whereStatus(1)
			->whereMonth('created_at', '=', $month)
			->whereNull('deleted_at')->get();
	}

	public function sum_by_agent($profile_id){
		return Model::select(\DB::raw('sum(total) as total'))
			->whereStatus(1)
			->whereProfileId($profile_id)
			->whereNull('deleted_at')->first();
	}


}
