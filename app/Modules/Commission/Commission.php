<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 2019-02-23
 * Time: 13:08
 */

namespace App\Modules\Commission;


use App\Models\AgentCommission;
use App\Models\Invoice;
use App\Models\Package;
use App\Models\Setting;
use \App\Models\Commission as CommissionModel;
use Illuminate\Support\Facades\Log;

class Commission
{
	public $shipment;
	public $agentCommission;
	public $CommissionModel;


	public function __construct(Package $shipment)
	{
		$this->shipment = $shipment;
	}

	public function raiseAgentCommission()
	{
		//number of order in the shipment
		$total_packages = Package::where('parent_id', $this->shipment->id)->count();
		// getting default commission
		$AGENT_COMMISSION = Setting::where('key', 'AGENT_COMMISSION')->first();
		//all orders id to get total invoice total of all orders
		$package_ids = Package::where('parent_id', $this->shipment->id)->pluck('id');
		//getting total invoice of all packages
		$total_amount = \Redbox::shipment_commission_total($this->shipment);
		//building data for the agent commision
		$data = [
			'total_orders' => $total_packages,
			'agent_id' => $this->shipment->profile_id,
			'shipment_id' => $this->shipment->id,
			'total_amount' => $total_amount,
			'percentage' => $AGENT_COMMISSION->value,
			'agent_commission_total' => round((($AGENT_COMMISSION->value / 100) * $total_amount), 2),
		];

		$this->agentCommission  = AgentCommission::whereShipmentId($this->shipment->id)->first();
		\Log::info('$this->shipment->id',[$this->shipment->id]);
		if (empty($this->agentCommission)) {
			$agentCommission = new AgentCommission($data);
			$agentCommission->save();
			$this->agentCommission = $agentCommission;
		}
		return $this;
	}

	public function raiseCommission()
	{
		//getting percentage of agent commission
		$AGENT_COMMISSION = Setting::where('key', 'AGENT_COMMISSION')->first();

		//getting all the child packages of shipment
		foreach ($this->shipment->child as $child) {
			//getting invoice of  each child
			$invoice = Invoice::whereDataId($child->id)->whereIn('status',[0,1,2])->package()->first();

			//creating data set for new commission
			$data = [
				'profile_id' => $this->shipment->profile_id,
				'agent_commission_id' => $this->agentCommission->id,
				'invoice_id' => $invoice ? $invoice->id : null,
				'total' => round((($AGENT_COMMISSION->value / 100) * \Redbox::get_total($child)), 2),
				'details' => "commission of " . $child->description,
				'status' => 1,
				'package_id' => $child->id
			];


			$CommissionModel = CommissionModel::wherePackageId($child->id)->first();
			\Log::info('message',[$CommissionModel]);
			if (empty($CommissionModel)) {
				$CommissionModel = new  CommissionModel($data);
				\Log::info('Created commission $CommissionModel');
			}
			$this->CommissionModel = $CommissionModel->save();
		}
		\Log::info('Created commission');
		return $this;
	}

}
