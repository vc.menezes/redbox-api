<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 20/08/2018
 * Time: 12:08 PM
 */

namespace App\Modules\Tree;


use App\Models\VesselRoute;

class Tree
{

    public $tree = array();

    public function __construct($element)
    {

        $this->buildTree($element);
    }


    public function buildTree($element)
    {

        $route = VesselRoute::whereParentId($element->id)->first();

        if ($route) {
            $this->tree[] = $route;
            $this->buildTree($route);
        }
        return;
    }

    public function getTree(){return $this->tree;}

}