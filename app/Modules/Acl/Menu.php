<?php
namespace App\Modules\Acl;

use \App\User;

class Menu
{
    public $user;

    public $menu_items = [
        [
            'name' => 'Bulk Orders',
            'icon' => 'shopping_basket',
            'parent' => 'quick',
            'link' => '/home/my-orders',
            'permission' => 'menu_bulk_orders'
        ],
        [
            'name' => 'Orders',
            'icon' => 'ballot',
            'parent' => 'quick',
            'link' => '/home/orders',
            'permission' => 'menu_orders'
        ],
        [
            'name' => 'Shipments',
            'icon' => 'local_shipping',
            'parent' => 'quick',
            'link' => '/home/shipments',
            'permission' => 'menu_shipments'
        ],
        [
            'name' => 'Invoice',
            'icon' => 'receipt',
            'parent' => 'quick',
            'link' => '/home/invoices',
            'permission' => 'menu_invoices'
        ],
        [
            'name' => 'Bulk Orders',
            'icon' => 'shopping_basket',
            'parent' => 'application',
            'link' => '/home/my-orders',
            'permission' => 'menu_bulk_orders'
        ],
        [
            'name' => 'All Orders',
            'icon' => 'shopping_basket',
            'parent' => 'application',
            'link' => '/home/my-orders/my-all-orders',
            'permission' => 'menu_my_all_orders'
        ],
        [
            'name' => 'Invoice',
            'icon' => 'receipt',
            'parent' => 'application',
            'link' => '/home/my-orders/my-invoices',
            'permission' => 'menu_my_invoices'
        ],
        [
            'name' => 'Agents',
            'icon' => 'assignment_ind',
            'parent' => 'application',
            'link' => '/home/agents',
            'permission' => 'menu_agents'
        ],
        [
            'name' => 'Customers',
            'icon' => 'people',
            'parent' => 'application',
            'link' => '/home/customers',
            'permission' => 'menu_customers'
        ],
        [
            'name' => 'Organizations',
            'icon' => 'business',
            'parent' => 'application',
            'link' => '/home/organizations',
            'permission' => 'menu_organizations'
        ],
        [
            'name' => 'Invoice',
            'icon' => 'receipt',
            'parent' => 'application',
            'link' => '/home/invoices',
            'permission' => 'menu_invoices'
        ],
        [
            'name' => 'Receipts',
            'icon' => 'receipt',
            'parent' => 'application',
            'link' => '/home/receipts',
            'permission' => 'menu_receipts'
        ],
        [
            'name' => 'Orders',
            'icon' => 'ballot',
            'parent' => 'application',
            'link' => '/home/orders',
            'permission' => 'menu_orders'
        ],
        [
            'name' => 'Bulk Orders',
            'icon' => 'ballot',
            'parent' => 'application',
            'link' => '/home/my-orders',
            'permission' => 'menu_my_orders'
        ],
        [
            'name' => 'Shipments',
            'icon' => 'local_shipping',
            'parent' => 'application',
            'link' => '/home/shipments',
            'permission' => 'menu_shipments'
        ],
        // [
        //     'name' => 'Tracking',
        //     'icon' => 'track_changes',
        //     'parent' => 'application',
        //     'link' => '/home/tracking',
        //     'permission' => 'menu_tracking'
        // ],
        [
            'name' => 'Racks',
            'icon' => 'grid_on',
            'parent' => 'application',
            'link' => '/home/blocks',
            'permission' => 'menu_blocks'
        ],
        [
            'name' => 'Commission',
            'icon' => 'monetization_on',
            'parent' => 'application',
            'link' => '/home/commissions',
            'permission' => 'menu_commissions'
        ],
        [
            'name' => 'Pending Commission',
            'icon' => 'sms_failed',
            'parent' => 'application',
            'link' => '/home/commissions/pending',
            'permission' => 'menu_pending_commissions'
        ],
        [
            'name' => 'Users',
            'icon' => 'supervised_user_circle',
            'parent' => 'user',
            'link' => '/home/users',
            'permission' => 'menu_users'
        ],
        [
            'name' => 'Roles',
            'icon' => 'lock',
            'parent' => 'user',
            'link' => '/home/users/roles',
            'permission' => 'menu_roles'
        ],
        [
            'name' => 'Permissions',
            'icon' => 'verified_user',
            'parent' => 'user',
            'link' => '/home/users/permissions',
            'permission' => 'menu_permissions'
        ],
        // [
        //     'name' => 'Notifications',
        //     'icon' => 'notifications',
        //     'parent' => 'setting',
        //     'link' => '/home/notifications',
        //     'permission' => 'menu_notifications'
        // ],
        [
            'name' => 'Vessels',
            'icon' => 'directions_boat',
            'parent' => 'setting',
            'link' => '/home/vessels',
            'permission' => 'menu_vessels'
        ],
        [
            'name' => 'Locations',
            'icon' => 'location_on',
            'parent' => 'setting',
            'link' => '/home/locations',
            'permission' => 'menu_locations'
        ],
        [
            'name' => 'Bank',
            'icon' => 'business',
            'parent' => 'setting',
            'link' => '/home/banks',
            'permission' => 'menu_banks'
        ],
        [
            'name' => 'Card Type',
            'icon' => 'credit_card',
            'parent' => 'setting',
            'link' => '/home/card-types',
            'permission' => 'menu_card_types'
        ],
        [
            'name' => 'Category',
            'icon' => 'category',
            'parent' => 'setting',
            'link' => '/home/commodity-categories',
            'permission' => 'menu_commoditity_categories'
        ],
        [
            'name' => 'Commodities',
            'icon' => 'list',
            'parent' => 'setting',
            'link' => '/home/commodities',
            'permission' => 'menu_commodities'
        ],
        [
            'name' => 'Picking Charges',
            'icon' => 'beenhere',
            'parent' => 'setting',
            'link' => '/home/picking-charges',
            'permission' => 'picking_charges'
        ],
        [
            'name' => 'Rates',
            'icon' => 'attach_money',
            'parent' => 'setting',
            'link' => '/home/rates',
            'permission' => 'menu_rates'
        ],
        [
            'name' => 'Status',
            'icon' => 'label',
            'parent' => 'setting',
            'link' => '/home/status',
            'permission' => 'menu_status'
        ],
        [
            'name' => 'Settings',
            'icon' => 'settings',
            'parent' => 'setting',
            'link' => '/home/settings',
            'permission' => 'menu_settings'
        ],
        [
            'name' => 'Orders',
            'icon' => 'poll',
            'parent' => 'report',
            'link' => '/home/reports/packages',
            'permission' => 'menu_report_orders'
        ],
        [
            'name' => 'Sales',
            'icon' => 'poll',
            'parent' => 'report',
            'link' => '/home/reports/sales',
            'permission' => 'menu_report_sales'
        ],
        [
            'name' => 'Commissions',
            'icon' => 'poll',
            'parent' => 'report',
            'link' => '/home/reports/pending_commission',
            'permission' => 'menu_report_commission'
        ],
    ];


    public function __construct()
    {
        $this->user = User::find(\JWTAuth::parseToken()->toUser()->id);

        foreach ($this->menu_items as $index => $menu_item) {

            if (!is_null($menu_item['permission'])) {
                if (!$this->user->can($menu_item['permission'])) {
                    unset($this->menu_items[$index]);
                }

            }
        }
    }

    public function allowed()
    {
        $this->menu_items;
        $data = array_divide($this->menu_items);
        return $data[1];
    }
}


?>
