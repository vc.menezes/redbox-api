<?php

namespace App\Views;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PackageRackView
 * @package App\Models
 * @version October 15, 2018, 9:38 am UTC
 *
 */
class PackageRackView extends Model
{

    public $table = 'package_rack_views';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function profile() {
        return $this->belongsTo(ProfileView::class, 'profile_id', 'id');
    }
}
