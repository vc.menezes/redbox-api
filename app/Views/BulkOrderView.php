<?php

namespace App\Views;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BulkOrderView
 * @package App\Models
 * @version October 15, 2018, 10:16 am UTC
 *
 */
class BulkOrderView extends Model
{


    public $table = 'bulk_order_views';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function packages(){
        return $this->belongsToMany(PackageView::class,'bulk_order_packages','bulk_order_id','package_id');
    }

    
}
