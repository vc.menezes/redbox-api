<?php

namespace App\Views;


use App\Models\BaseModel;
use App\Models\Status;
use App\Models\Location;
use App\Models\TrackingLog;
use App\Models\PackageItem;
use App\Models\PackageHandover;
use App\Models\Signature;
use App\Models\PackagePickingLocation;
use App\Views\ProfileView;
use Laravel\Scout\Searchable;

class PackageView extends BaseModel
{
    public $table = 'package_view';



    public function status(){
        return $this->belongsTo(Status::class,'last_tracking_status_id','id');
    }

    
    public function destination() {
        return $this->hasOne(Location::class, 'id', 'destinations_location_id');
    }


    public function profile() {
        return $this->belongsTo(ProfileView::class, 'profile_id', 'id');
    }

    public function items() {
        return $this->hasMany(PackageItem::class, 'package_id', 'id');
    }

    public function picking_location() {
        return $this->hasOne(PackagePickingLocation::class, 'package_id', 'id');
    }

    public function tracking_log() {
        return $this->hasMany(TrackingLog::class, 'tracking_id', 'tracking_id');
    }

    public function commission() {
        return $this->hasOne(\App\Models\AgentCommission::class, 'shipment_id', 'id');
    }

    public function tracking()
    {
        return $this->hasOne(\App\Models\Tracking::class, 'package_id', 'id');
    }

    public function handOver()
    {
        return $this->hasOne(PackageHandover::class, 'package_id', 'id');
    }

    public function storageBlock()
    {
        return $this->hasOne(\App\Models\StorageBlock::class, 'package_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(\App\Models\Package::class);
    }

    public function signature()
    {
        return $this->hasOne(Signature::class, 'package_id', 'id');
    }

    public function attachables()
    {
        return $this->morphMany(\App\Models\Attachable::class, 'attachable');
    }

    public function getEstimatedDeliveryDateAttribute($value) {
        return $this->changeDateToIso8601String($value);
    }
}