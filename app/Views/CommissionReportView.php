<?php

namespace App\Views;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CommissionView
 * @package App\Models
 * @version October 3, 2018, 9:46 am UTC
 *
 */
class CommissionReportView extends Model
{

	public $table = 'commission_report_views';

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function profile()
	{
		return $this->belongsTo(\App\Models\Profile::class, 'profile_id', 'id');
	}


}
