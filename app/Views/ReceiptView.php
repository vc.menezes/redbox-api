<?php

namespace App\Views;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ReceiptView
 * @package App\Models
 * @version October 2, 2018, 9:57 am UTC
 *
 */
class ReceiptView extends Model
{

    public $table = 'receipt_views';

    
}
