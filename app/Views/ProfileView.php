<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 12/09/2018
 * Time: 11:33 AM
 */

namespace App\Views;

use App\Models\BaseModel;
use App\Models\Location;
use App\Models\Agent;
use App\Models\Package;
use App\Models\Invoice;
use App\Models\Person;
use App\Models\Addressable;
use App\Models\Organization;

class ProfileView extends BaseModel
{
    public $table = 'profile_view';

    public function location(){
        return $this->belongsTo(Location::class,'location_id','id');
    }

    public function packages(){
        return $this->hasMany(Package::class, 'profile_id', 'id');
    }

    public function invoices() {
        return $this->hasMany(Invoice::class, 'profile_id', 'id');
    }

    public function islands() {
        return $this->hasMany(Agent::class, 'profile_id', 'id');
    }

    public function address() {
        return $this->hasMany(Addressable::class, 'profile_id', 'id');
    }

    public function person() {
        return $this->belongsTo(Person::class, 'id', 'profile_id');
    }

    public function organization() {
        return $this->belongsTo(Organization::class, 'id', 'profile_id');
    }

    public function contacts()
    {
        return $this->morphMany(\App\Models\Contactable::class, 'contactable');
    }

    public function bankAccount() {
        return $this->hasMany(\App\Models\BankAccount::class, 'profile_id', 'id');
    }
}