<?php

namespace App\Console\Commands;

use App\Models\AgentCommission;
use App\Models\CommissionPayment;
use App\Models\CommissionPaymentCommission;
use App\Models\Commission;
use App\Models\Package;
use App\Views\PackageView;
use Illuminate\Console\Command;

class GenerateCommission extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'generate:commission';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		CommissionPaymentCommission::whereNotNull('id')->delete();
		CommissionPayment::whereNotNull('id')->delete();
		AgentCommission::whereNotNull('id')->delete();
		Commission::whereNotNull('id')->delete();

		$shipmentView = PackageView::whereIn('last_tracking_status_id',[4,5,6])
			->whereType('shipment')->get();
		foreach ($shipmentView as $shipment){
			$package = Package::find($shipment->id);
			$commision = new \App\Modules\Commission\Commission($package);
			$commision->raiseAgentCommission()->raiseCommission();
		}

	}
}
