<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Route;

class updatePermission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update-permission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Route::getRoutes() as $item) {
            $explode = explode(".", $item->getName());
            if(count($explode)>2){
                $name = $explode[1] . '_' . $explode[2];
                $permission = \App\Models\Permission::whereName($name)->first();
                if(!$permission){
                    $permission = new \App\Models\Permission();
                    $permission->name = $name;
                    $permission->display_name = ucfirst($explode[1]) . ' ' . ucfirst($explode[2]);
                    $permission->description = ucfirst($explode[1]) . ' ' . ucfirst($explode[2]);
                    $permission->save();
                }
            }
        }
    }

}
