<?php

namespace App\Models;

use App\Traits\Jwt;
use Eloquent as Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class BaseModel
 * @package App\Models
 * @version May 12, 2018, 09:40 am UTC
 *
 * @property string name
 */
class BaseModel extends Model
{
    use Jwt;



    public function getCreatedAtAttribute($value)
    {
        return $this->changeDateToIso8601String($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return $this->changeDateToIso8601String($value);
    }

    public function getDeletedAtAttribute($value)
    {
        return $this->changeDateToIso8601String($value);
    }

    public function changeDateToIso8601String($value)
    {
        return $value ? (new \Carbon\Carbon($value))->toIso8601String() : null;
    }



}
