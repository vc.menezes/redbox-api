<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class InvoiceItem
 * @package App\Models
 * @version October 18, 2018, 8:34 am UTC
 *
 * @property \App\Models\Invoice invoice
 * @property integer invoice_id
 * @property string details
 * @property double rate
 * @property string UOM
 * @property double qty
 * @property string type
 */
class InvoiceItem extends Model implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'invoice_items';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'invoice_id',
        'item_code',
        'details',
        'rate',
        'UOM',
        'qty',
        'charge_type',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'invoice_id' => 'integer',
        'details' => 'string',
        'rate' => 'double',
        'UOM' => 'string',
        'qty' => 'double',
        'type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'invoice_id' => 'required:integer',
        'details' => 'required,string',
        'rate' => 'required,integer',
        'UOM' => 'required,string',
        'qty' => 'required,integer',
        'type' => 'required,string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function invoice()
    {
        return $this->belongsTo(\App\Models\Invoice::class, 'invoice_id', 'id');
    }
}
