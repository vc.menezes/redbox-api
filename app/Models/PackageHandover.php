<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class PackageHandover
 * @package App\Models
 * @version September 9, 2018, 9:52 am UTC
 *
 * @property \App\Models\Package package
 * @property string name
 * @property string contact
 * @property string identifier
 * @property integer package_id
 */
class PackageHandover extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'package_handovers';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'contact',
        'identifier',
        'package_id',
        'other'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'contact' => 'string',
        'identifier' => 'string',
        'other' => 'integer',
        'package_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required:string',
        'contact' => 'required:string',
        'package_id' => 'required:integer',
        'other' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package()
    {
        return $this->belongsTo(\App\Models\Package::class, 'package_id', 'id');
    }

}
