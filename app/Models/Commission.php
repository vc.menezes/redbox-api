<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Commission
 * @package App\Models
 * @version September 15, 2018, 12:53 pm UTC
 *
 * @property \App\Models\Profile profile
 * @property \App\Models\Invoice invoice
 * @property \App\Models\Package package
 * @property integer profile_id
 * @property integer invoice_id
 * @property integer package_id
 * @property double total
 * @property string details
 * @property integer status
 */
class Commission extends Model implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'commissions';
    /**
     * Status
     * 1 = not resovled
     * 2 = resolved
    */

    protected $dates = ['deleted_at'];


    public $fillable = [
        'profile_id',
        'invoice_id',
        'package_id',
        'total',
        'details',
        'agent_commission_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'profile_id' => 'integer',
        'invoice_id' => 'integer',
        'package_id' => 'integer',
        'total' => 'double',
        'details' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'profile_id' => 'required:integer',
        'invoice_id' => 'required:integer',
        'package_id' => 'required:integer',
        'total' => 'required:double',
        'details' => 'string',
        'status' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function profile()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'profile_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function invoice()
    {
        return $this->belongsTo(\App\Models\Invoice::class, 'invoice_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package()
    {
        return $this->belongsTo(\App\Models\Package::class, 'package_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package_view()
    {
        return $this->belongsTo(\App\Views\PackageView::class, 'package_id', 'id');
    }

    public function commission_payment_commissions(){
        return $this->hasMany(CommissionPaymentCommission::class);
    }

    public function bank_account(){
    	return $this->HasOne(BankAccount::class,'profile_id','profile_id');
		}
}
