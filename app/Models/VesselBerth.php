<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class VesselBerth
 * @package App\Models
 * @version July 25, 2018, 4:49 am UTC
 *
 * @property \App\Models\Vessel vessel
 * @property \App\Models\Berth berth
 * @property string name
 * @property integer vessel_id
 * @property integer berth_id
 */
class VesselBerth extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'vessel_berths';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'vessel_id',
        'berth_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'vessel_id' => 'integer',
        'berth_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required,string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function vessel()
    {
        return $this->belongsTo(\App\Models\Vessel::class, 'vessel_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function berth()
    {
        return $this->belongsTo(\App\Models\Berth::class, 'berth_id', 'id');
    }
}
