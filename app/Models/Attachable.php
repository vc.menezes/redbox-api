<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Attachable
 * @package App\Models
 * @version July 25, 2018, 4:28 am UTC
 *
 * @property morphs attachable
 * @property string name
 * @property string extension
 * @property string type
 */
class Attachable extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'attachables';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'attachable_id',
        'attachable_type',
        'name',
        'extension',
        'type',
        'mime_type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'extension' => 'string',
        'type' => 'string',
        'mime_type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'attachable_id',
        'attachable_type',
        // 'name' => 'required',
        // 'extension' => 'required',
        // 'type' => 'required'
    ];

    public function attachable() {
        return $this->morphTo();
    }

    
}
