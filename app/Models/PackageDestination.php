<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class PackageDestination
 * @package App\Models
 * @version July 22, 2018, 5:12 am UTC
 *
 * @property \App\Models\Location location
 * @property \App\Models\Package package
 * @property string name
 * @property string address
 * @property string contact_number
 * @property integer location_id
 * @property integer package_id
 */
class PackageDestination extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'package_destinations';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'address',
        'contact_number',
        'location_id',
        'package_id',
        'medium',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'address' => 'string',
        'contact_number' => 'string',
        'location_id' => 'integer',
        'package_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required:string',
        'address' => 'required:string',
        'contact_number' => 'required:string',
        'location_id' => 'required:integer',
        'package_id' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function location()
    {
        return $this->belongsTo(\App\Models\Location::class, 'location_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package()
    {
        return $this->belongsTo(\App\Models\Package::class, 'package_id', 'id');
    }
}
