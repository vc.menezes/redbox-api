<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Location
 * @package App\Models
 * @version July 18, 2018, 6:32 am UTC
 *
 * @property \App\Models\location location
 * @property string name
 * @property integer parent_id
 * @property integer type
 * @property string longitude
 * @property string latitude
 */
class Location extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'locations';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'parent_id',
        'type',
        'longitude',
        'latitude',
        'code',
        'prefix',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'parent_id' => 'integer',
        'type' => 'string',
        'longitude' => 'string',
        'latitude' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required:string',
        'type' => 'required:string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function location()
    {
        return $this->belongsTo(\App\Models\Location::class, 'parent_id', 'id');
    }
    public function picking_charge()
    {
        return $this->hasOne(PickingCharge::class,'location_id','id');
    }

    public function routes(){
        return $this->hasMany(VesselRoute::class,'location_id','id');
    }
}
