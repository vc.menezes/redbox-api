<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class CommissionPaymentCommission
 * @package App\Models
 * @version October 7, 2018, 6:18 pm UTC
 *
 * @property \App\Models\Commission commission
 * @property \App\Models\CommissionPayment commissionPayment
 * @property integer commission_id
 * @property integer commission_payment_id
 */
class CommissionPaymentCommission extends Model implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'commission_payment_commissions';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'commission_id',
        'commission_payment_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'commission_id' => 'integer',
        'commission_payment_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'commission_id' => 'required:integer',
        'commission_payment_id' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function commission()
    {
        return $this->belongsTo(\App\Models\Commission::class, 'commission_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function commissionPayment()
    {
        return $this->belongsTo(\App\Models\CommissionPayment::class, 'commission_payment_id', 'id');
    }
}
