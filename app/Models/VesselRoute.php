<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class VesselRoute
 * @package App\Models
 * @version July 25, 2018, 4:57 am UTC
 *
 * @property \App\Models\Vessel vessel
 * @property \App\Models\Location location
 * @property \App\Models\VesselRoute vesselRoute
 * @property string name
 * @property integer vessel_id
 * @property integer location_id
 * @property integer parent_id
 * @property string contact_person
 * @property string contact
 */
class VesselRoute extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'vessel_routes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'vessel_id',
        'location_id',
        'parent_id',
        'contact_person',
        'contact'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'vessel_id' => 'integer',
        'location_id' => 'integer',
        'parent_id' => 'integer',
        'contact_person' => 'string',
        'contact' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'vessel_id' => 'required:integer',
        'location_id' => 'required:integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function vessel()
    {
        return $this->belongsTo(\App\Models\Vessel::class, 'vessel_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function location()
    {
        return $this->belongsTo(\App\Models\Location::class, 'location_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function vesselRoute()
    {
        return $this->belongsTo(\App\Models\VesselRoute::class, 'parent_id', 'id');
    }
}
