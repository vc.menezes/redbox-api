<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Key
 * @package App\Models
 * @version July 25, 2018, 4:31 am UTC
 *
 * @property string key_name
 */
class Key extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'keys';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'key_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'key_name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'key_name' => 'required,string'
    ];

    
}
