<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Vessel
 * @package App\Models
 * @version July 25, 2018, 4:46 am UTC
 *
 * @property \App\Models\Location location
 * @property string name
 * @property string registry_number
 * @property string contact_person
 * @property string contact_number
 * @property integer location_id
 */
class Vessel extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'vessels';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'registry_number',
        'contact_person',
        'contact_number',
        'location_id',
        'medium',
        'docked'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'registry_number' => 'string',
        'contact_person' => 'string',
        'contact_number' => 'string',
        'location_id' => 'integer',
        'docked' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'registry_number' => 'required',
        'contact_person' => 'required',
        'contact_number' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function location()
    {
        return $this->belongsTo(\App\Models\Location::class, 'location_id', 'id');
    }

    public function routes() {
        return $this->hasMany(VesselRoute::class);
    }
}
