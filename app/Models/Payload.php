<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Payload
 * @package App\Models
 * @version July 24, 2018, 6:56 pm UTC
 *
 * @property morphs data
 * @property string payload
 */
class Payload extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'payloads';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'data',
        'payload'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'payload' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'data' => 'required'
    ];

    
}
