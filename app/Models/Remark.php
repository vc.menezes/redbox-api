<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Remark
 * @package App\Models
 * @version October 14, 2018, 10:08 am UTC
 *
 * @property \App\Models\Profile profile
 * @property morphs remarkable
 * @property string body
 * @property integer profile_id
 */
class Remark extends Model implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'remarks';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'remarkable_type',
        'remarkable_id',
        'body',
        'profile_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'body' => 'string',
        'profile_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'body' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function profile()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'profile_id', 'id');
    }

    public function remarkable() {
        return $this->morphTo();
    }
}
