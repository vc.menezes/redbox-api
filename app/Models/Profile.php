<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Profile
 * @package App\Models
 * @version July 18, 2018, 7:03 am UTC
 *
 * @property \App\Models\address address
 * @property \App\Models\location location
 * @property string name
 * @property string identifier
 * @property integer address_id
 * @property string email
 * @property string contact_number
 * @property string type
 * @property integer location_id
 */
class Profile extends BaseModel implements Auditable
{
	use SoftDeletes;
	use Notifiable;
	public $table = 'profiles';

	use \OwenIt\Auditing\Auditable;
	protected $dates = ['deleted_at'];

	public $searchable = [
		'name' => 'like'
	];


	public $fillable = [
		'name',
		'identifier',
		'address_id',
		'email',
		'contact_number',
		'type',
		'location_id'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'name' => 'string',
		'identifier' => 'string',
		'address_id' => 'integer',
		'email' => 'string',
		'contact_number' => 'string',
		'type' => 'string',
		'location_id' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'name' => 'required',
		'identifier' => 'required',
		'email' => 'required',
		'contact_number' => 'required'
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function address()
	{
		return $this->belongsTo(\App\Models\Location::class, 'address_id', 'id');
	}

	public function addresses()
	{
		return $this->hasMany(\App\Models\Addressable::class);
	}

	public function agentsIslands()
	{
		return $this->hasMany(\App\Models\Agent::class);
	}

	public function package()
	{
		return $this->hasMany(\App\Models\Package::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function location()
	{
		return $this->belongsTo(\App\Models\Location::class, 'location_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function user()
	{
		return $this->belongsTo(\App\User::class, 'id', 'profile_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function person()
	{
		return $this->belongsTo(\App\Models\Person::class, 'id', 'profile_id');
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function agents()
	{
		return $this->belongsToMany(Location::class, 'agents', 'profile_id', 'location_id');
	}

	/**
	 * Route notifications for the Nexmo channel.
	 *
	 * @param  \Illuminate\Notifications\Notification $notification
	 * @return string
	 */
	public function routeNotificationForNexmo($notification)
	{
		return $this->contact_number;
	}

	public function shipments()
	{
		return $this->hasMany(\App\Views\PackageView::class, 'profile_id', 'id');
	}

	public function commission()
	{
		return $this->hasMany(\App\Models\AgentCommission::class, 'agent_id', 'id');
	}

	public function onGoingPakage()
	{
		return $this->hasMany(\App\Views\PackageView::class, 'profile_id', 'id')->where('tracking_status', 1);
	}

	public function finishedPackage()
	{
		return $this->hasMany(\App\Views\PackageView::class, 'profile_id', 'id')->where('tracking_status', 0);
	}

	public function contacts()
	{
		return $this->morphMany(Contactable::class, 'contactable');
	}

	public function bank_account()
	{
		return $this->hasOne(BankAccount::class, 'profile_id', 'id');
	}
}
