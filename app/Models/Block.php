<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Block
 * @package App\Models
 * @version July 22, 2018, 4:54 am UTC
 *
 * @property \App\Models\Block block
 * @property integer parent_id
 * @property string name
 * @property string code
 * @property integer type: 0 = 'horizontal/row' | 1 = 'vertical/column'
 */
class Block extends BaseModel implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    public $table = 'blocks';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'parent_id',
        'name',
        'code',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'parent_id' => 'integer',
        'name' => 'string',
        'code' => 'string',
        'type' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'code' => 'required:string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parent()
    {
        return $this->belongsTo(\App\Models\Block::class, 'parent_id', 'id');
    }

    public function child()
    {
        return $this->hasMany(Block::class, 'parent_id', 'id');
    }

    public function packages()
    {
        return $this->hasMany(StorageBlock::class);
        // return $this->belongsToMany(Package::class, 'storage_blocks', 'block_id', 'package_id')->withPivot('deleted_at');
    }
}
