<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Package
 * @package App\Models
 * @version July 21, 2018, 6:48 am UTC
 *
 * Status
 * payment_status
 * 0 = paid
 * 1 = credit
 * 2 = raise
 * 3 = invoice raise
 *
 *
 * @property \App\Models\Profile profile
 * @property \App\Models\Package package
 * @property string barcode
 * @property string reference_no
 * @property string code
 * @property string description
 * @property double weight
 * @property integer profile_id
 * @property integer parent_id
 */
class Package extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'packages';


    protected $dates = ['deleted_at'];

    public $searchable = [
        'description' => 'like'
    ];


    public $fillable = [
        'barcode',
        'reference_no',
        'code',
        'description',
        'weight',
        'profile_id',
        'parent_id',
        'type',
        'payment_status',
        'weight_approved',
        'flag',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'barcode' => 'string',
        'reference_no' => 'string',
        'code' => 'string',
        'description' => 'string',
        'weight' => 'double',
        'profile_id' => 'integer',
        'parent_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'description' => 'nullable:string',
        'profile_id' => 'required:integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function profile()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'profile_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package()
    {
        return $this->belongsTo(\App\Models\Package::class, 'parent_id', 'id');
    }


    public function shipment()
    {
        return $this->hasOne(\App\Models\PackageShipment::class,'package_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function child()
    {
        return $this->hasMany(\App\Models\Package::class, 'parent_id', 'id');
    }


    public function package_destination()
    {
        return $this->hasOne(PackageDestination::class, 'package_id', 'id');
    }

    public function picking_location()
    {
        return $this->hasOne(PackagePickingLocation::class, 'package_id', 'id');
    }


    public function items()
    {
        return $this->hasMany(\App\Models\PackageItem::class);
    }


    public function pickingLocation()
    {
        return $this->hasOne(\App\Models\PackagePickingLocation::class);

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tracking()
    {
        return $this->hasOne(\App\Models\Tracking::class, 'package_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function activeTracking()
    {
        return $this->hasOne(\App\Models\Tracking::class, 'package_id', 'id')->whereStatus(1);
    }


    public function commission()
    {
        return $this->hasOne(\App\Models\AgentCommission::class, 'shipment_id', 'id');
    }
    

    public function checkCommission()
    {
        return $this->hasOne(\App\Models\Commission::class, 'package_id', 'id');
    }
    
    public function handOver()
    {
        return $this->hasOne(PackageHandover::class, 'package_id', 'id');
    }

    public function signature()
    {
        return $this->hasOne(Signature::class, 'package_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany(\App\Views\PackageView::class, 'parent_id', 'id');
    }

    public function storageBlock(){
        return $this->hasMany(StorageBlock::class,'package_id','id');
    }

    public function block(){
        return $this->belongsToMany(Block::class, 'storage_blocks')->whereNull('storage_blocks.deleted_at');
        // return $this->hasOne(StorageBlock::class);
    }

    public function attachables()
    {
        return $this->morphMany(Attachable::class, 'attachable');
    }

    public function remarks()
    {
        return $this->morphMany(Remark::class, 'remarkable');
    }


    public function bulkOrderPackage()
    {
        return $this->hasOne(BulkOrderPackage::class, 'package_id' , 'id');
    }

    public function vessel() {
        return $this->hasOne(PackageShipment::class, 'package_id', 'id');
    }

}
