<?php

namespace App\Models;

use App\Events\TrackingLogUpdated;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class TrackingLog
 * @package App\Models
 * @version July 24, 2018, 9:51 am UTC
 *
 * @property \App\Models\Package package
 * @property \App\Models\Location location
 * @property \App\Models\Status status
 * @property integer tracking_id
 * @property integer location_id
 * @property integer status_id
 * @property string type
 * @property string description
 */
class TrackingLog extends BaseModel implements Auditable
{
	use SoftDeletes;
	use \OwenIt\Auditing\Auditable;

	public $table = 'tracking_logs';


	protected $dates = ['deleted_at'];

	protected $dispatchesEvents = [
		'created' => TrackingLogUpdated::class
	];


	public $fillable = [
		'tracking_id',
		'location_id',
		'status_id',
		'type',
		'description',
		'country_id',
		'profile_id',
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'tracking_id' => 'integer',
		'location_id' => 'integer',
		'profile_id' => 'integer',
		'status_id' => 'integer',
		'type' => 'string',
		'description' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'tracking_id' => 'required:integer',
		// 'location_id' => 'required:integer',
		'status_id' => 'required:integer'
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function tracking()
	{
		return $this->belongsTo(\App\Models\Tracking::class, 'tracking_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function profile()
	{
		return $this->belongsTo(\App\Models\Profile::class, 'profile_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function location()
	{
		return $this->belongsTo(\App\Models\Location::class, 'location_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function status()
	{
		return $this->belongsTo(\App\Models\Status::class, 'status_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class, 'country_id', 'id');
	}


}
