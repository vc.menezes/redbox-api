<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class AgentCommission
 * @package App\Models
 * @version September 27, 2018, 5:03 am UTC
 *
 * @property \App\Models\Profile profile
 * @property \App\Models\Package package
 * @property integer total_orders
 * @property integer agent_id
 * @property integer shipment_id
 * @property float total_amount
 * @property float percentage
 * @property float agent_commission_total
 */
class AgentCommission extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    public $table = 'agent_commissions';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'total_orders',
        'agent_id',
        'shipment_id',
        'total_amount',
        'percentage',
        'agent_commission_total'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'total_orders' => 'integer',
        'agent_id' => 'integer',
        'shipment_id' => 'integer',
        'total_amount' => 'float',
        'percentage' => 'float',
        'agent_commission_total' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'total_orders' => 'required,string',
        'total_amount' => 'required,integer',
        'percentage' => 'required,integer',
        'agent_commission_total' => 'required,integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function profile()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'agent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package()
    {
        return $this->belongsTo(\App\Models\Package::class, 'shipment_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function details()
    {
        return $this->hasMany(\App\Models\Commission::class, 'agent_commission_id');
    }



    
}
