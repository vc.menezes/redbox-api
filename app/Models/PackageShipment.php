<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class PackageShipment
 * @package App\Models
 * @version September 9, 2018, 9:49 am UTC
 *
 * @property \App\Models\Vessel vessel
 * @property \App\Models\Package package
 * @property integer vessel_id
 * @property integer package_id
 */
class PackageShipment extends Model implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'package_shipments';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'vessel_id',
        'package_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'vessel_id' => 'integer',
        'package_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'vessel_id' => 'required:integer',
        'package_id' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function vessel()
    {
        return $this->belongsTo(\App\Models\Vessel::class, 'vessel_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package()
    {
        return $this->belongsTo(\App\Models\Package::class, 'package_id', 'id');
    }
}
