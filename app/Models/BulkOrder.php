<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Views\PackageView;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class BulkOrder
 * @package App\Models
 * @version October 10, 2018, 2:12 am UTC
 *
 * @property \App\Models\Profile profile
 * @property string description
 * @property double weight
 * @property integer profile_id
 */
class BulkOrder extends Model implements Auditable
{
	use SoftDeletes;
	use \OwenIt\Auditing\Auditable;
	public $table = 'bulk_orders';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'description',
		'weight',
		'profile_id',
		'payment_status'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'description' => 'string',
		'weight' => 'double',
		'profile_id' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function profile()
	{
		return $this->belongsTo(\App\Models\Profile::class, 'profile_id', 'id');
	}

	public function packages()
	{
		return $this->belongsToMany(PackageView::class, 'bulk_order_packages', 'bulk_order_id', 'package_id');
	}

	public function packages_model()
	{
		return $this->belongsToMany(Package::class, 'bulk_order_packages', 'bulk_order_id', 'package_id');
	}
}
