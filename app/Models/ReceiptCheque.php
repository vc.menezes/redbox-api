<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class ReceiptCheque
 * @package App\Models
 * @version August 19, 2018, 5:07 am UTC
 *
 * @property \App\Models\Receipt receipt
 * @property \App\Models\Bank bank
 * @property integer receipt_id
 * @property string cheque_no
 * @property string cheque_account_no
 * @property integer bank_id
 */
class ReceiptCheque extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'receipt_cheques';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'receipt_id',
        'cheque_no',
        'cheque_account_no',
        'bank_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'receipt_id' => 'integer',
        'cheque_no' => 'string',
        'cheque_account_no' => 'string',
        'bank_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'receipt_id' => 'required:integer',
        'cheque_no' => 'string:integer',
        'cheque_account_no' => 'string:integer',
        'bank_id' => 'string:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function receipt()
    {
        return $this->belongsTo(\App\Models\Receipt::class, 'receipt_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function bank()
    {
        return $this->belongsTo(\App\Models\Bank::class, 'receipt_id', 'id');
    }
}
