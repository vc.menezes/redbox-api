<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Invoice
 * @package App\Models
 * @version July 24, 2018, 10:06 am UTC
 *
 * @property \App\Models\Profile profile
 * @property integer profile_id
 * @property string serial_number
 * @property string details
 * @property integer qty
 * @property double rate
 * @property double tax
 * @property double total
 * @property morphs data
 * @property integer type
 * @property integer status
 */
class Invoice extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'invoices';
    /*
     * Invoice type
     * 1 = credit
     * 2 = normal
     *
     * invoice status
     * 0 = paid
     * 1 = credit
     * 2 = raise
     * 3 = cancelled
     * */

    public $searchable = [
        'profile_id' => '=',
        'status' => '=',
    ];

    protected $dates = ['deleted_at'];

    public static $invoice_status = [
      'paid'=>0,
      'credit'=>1,
      'raise' =>2,
      'cancelled' =>3
    ];


    public $fillable = [
        'profile_id',
        'serial_number',
        'details',
        'qty',
        'rate',
        'tax',
        'total',
        'data_type',
        'data_id',
        'type',
        'status',
        'currency',
        'UOM',
        'charge_type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'profile_id' => 'integer',
        'serial_number' => 'string',
        'details' => 'string',
        'qty' => 'integer',
        'rate' => 'double',
        'tax' => 'double',
        'total' => 'double',
        'type' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'profile_id' => 'required:integer',
        'serial_number' => 'string',
        'details' => 'string',
        'qty' => 'required:integer',
        'rate' => 'required:double',
        'tax' => 'required:double',
        'total' => 'required:double',
        'data' => 'required:string',
        'type' => 'required:integer',
        'status' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function profile()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'profile_id', 'id');
    }

    public function profile_view()
    {
        return $this->belongsTo(\App\Views\ProfileView::class, 'profile_id', 'id');
    }


    public function scopePackage($q)
    {
        return $q->whereDataType("App\Models\Package");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package()
    {
        return $this->belongsTo(\App\Models\Package::class, 'data_id', 'id');

    }

    public function receipts(){
        return $this->belongsToMany(Receipt::class, 'invoice_receipts');
    }

    public function items(){
        return $this->hasMany(InvoiceItem::class);
    }


}
