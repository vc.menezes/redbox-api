<?php

namespace App\Models;

use Laratrust\Models\LaratrustPermission;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Permission
 * @package App\Models
 * @version September 19, 2018, 6:27 am UTC
 *
 * @property string name
 * @property string display_name
 * @property string description
 */
class Permission extends LaratrustPermission implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'permissions';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'display_name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'display_name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    
}
