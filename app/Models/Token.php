<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Token
 * @package App\Models
 * @version July 20, 2018, 8:31 am UTC
 *
 * @property \App\Models\User user
 * @property string token
 * @property string medium
 * @property integer user_id
 * @property timestamp expires_at
 * @property boolean expired
 * @property string payload
 */
class Token extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'tokens';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'token',
        'medium',
        'user_id',
        'expires_at',
        'expired',
        'payload'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'token' => 'string',
        'medium' => 'string',
        'user_id' => 'integer',
        'expired' => 'boolean',
        'payload' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'token' => 'required:string',
        'medium' => 'required:string',
        'user_id' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }



}
