<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Setting
 * @package App\Models
 * @version August 15, 2018, 9:40 pm UTC
 *
 * @property string key
 * @property string value
 */
class Setting extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'settings';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'key',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'key' => 'string',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'key' => 'required:string',
        'value' => 'required:string'
    ];

    public function  ScopeGst($query){
        return $query->whereKey('GST');
    }

    public function ScopeCommission($q){
        return $q->whereKey('AGENT_COMMISSION');
    }

    
}
