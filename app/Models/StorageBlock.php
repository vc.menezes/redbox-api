<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class StorageBlock
 * @package App\Models
 * @version July 22, 2018, 4:59 am UTC
 *
 * @property \App\Models\Block block
 * @property \App\Models\Package package
 * @property integer block_id
 * @property integer package_id
 */
class StorageBlock extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'storage_blocks';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'block_id',
        'package_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'block_id' => 'integer',
        'package_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'block_id' => 'required:integer',
        'package_id' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function block()
    {
        return $this->belongsTo(\App\Models\Block::class, 'block_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package()
    {
        return $this->belongsTo(\App\Views\PackageView::class, 'package_id', 'id');
    }
}
