<?php

namespace App\Models;

use Laratrust\Models\LaratrustRole;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Role
 * @package App\Models
 * @version September 19, 2018, 6:18 am UTC
 *
 * @property string name
 * @property string display_name
 * @property string description
 */
class Role extends LaratrustRole implements Auditable
{
		use \OwenIt\Auditing\Auditable;
    public $table = 'roles';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'display_name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'display_name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|unique:roles',
    ];

    
}
