<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Rate
 * @package App\Models
 * @version August 15, 2018, 8:27 am UTC
 *
 * @property \App\Models\Location location
 * @property float weight
 * @property float rate
 * @property string operator
 * @property string medium
 * @property string type
 * @property integer from_id
 * @property string to_id
 */
class Rate extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'rates';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'weight',
        'rate',
        'operator',
        'medium',
        'type',
        'from_id',
        'to_id',
		  	'discrete_tariff_profile_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'weight' => 'float',
        'rate' => 'float',
        'operator' => 'string',
        'medium' => 'string',
        'type' => 'string',
        'from_id' => 'integer',
        'to_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'weight' => 'required',
        'rate' => 'required',
        'operator' => 'required',
        'medium' => 'required',
        'type' => 'required',
        'from_id' => 'required:integer',
        'to_id' => 'integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function from()
    {
        return $this->belongsTo(\App\Models\Location::class, 'from_id', 'id');
    }

    public function to()
    {
        return $this->belongsTo(\App\Models\Location::class, 'to_id', 'id');
    }

}
