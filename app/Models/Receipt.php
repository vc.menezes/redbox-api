<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Receipt
 * @package App\Models
 * @version July 24, 2018, 6:53 pm UTC
 *
 * @property \App\Models\Profile profile
 * @property integer customer_profile_id
 * @property integer created_by_profile_id
 * @property string receipt_number
 * @property string remarks
 * @property double cash
 * @property double cheque
 * @property double card
 * @property double transfer
 * @property integer status
 */
class Receipt extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'receipts';
    

    protected $dates = ['deleted_at'];

    public $appends = ['total'];

    public $fillable = [
        'customer_profile_id',
        'created_by_profile_id',
        'receipt_number',
        'remarks',
        'cash',
        'cheque',
        'card',
        'transfer',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'customer_profile_id' => 'integer',
        'created_by_profile_id' => 'integer',
        'receipt_number' => 'string',
        'remarks' => 'string',
        'cash' => 'double',
        'cheque' => 'double',
        'card' => 'double',
        'transfer' => 'double',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customer_profile_id' => 'required:integer',
        'created_by_profile_id' => 'required:integer',
        'receipt_number' => 'string',
        'remarks' => 'string',
        'cash' => 'required:double',
        'cheque' => 'required:double',
        'card' => 'required:double',
        'transfer' => 'required:double',
        'status' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function profile()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'customer_profile_id', 'id');
    }


    public function issuedBy()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'created_by_profile_id', 'id');
    }

    public function invoice_receipts(){
        return $this->hasMany(InvoiceReceipt::class,'receipt_id','id');
    }

    public function invoices(){
        return $this->belongsToMany(Invoice::class, 'invoice_receipts');
    }

    public function getTotalAttribute(){
        return $this->cash + $this->card + $this->cheque + $this->transfer ;
    }


}
