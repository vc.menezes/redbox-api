<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class BankAccount
 * @package App\Models
 * @version July 18, 2018, 8:14 am UTC
 *
 * @property \App\Models\Profile profile
 * @property integer profile_id
 * @property string bank
 * @property string account_no
 * @property integer active
 * @property string currency
 */
class BankAccount extends BaseModel implements Auditable
{
	use SoftDeletes;
	use \OwenIt\Auditing\Auditable;
	public $table = 'bank_accounts';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'profile_id',
		'bank_id',
		'account_no',
		'active',
		'currency'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'profile_id' => 'integer',
		'bank_id' => 'integer',
		'account_no' => 'string',
		'active' => 'integer',
		'currency' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'profile_id' => 'required:integer',
		'account_no' => 'required',
		'active' => 'required',
		'currency' => 'required'
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function profile()
	{
		return $this->belongsTo(\App\Models\Profile::class, 'profile_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function bank()
	{
		return $this->belongsTo(\App\Models\Bank::class, 'bank_id', 'id');
	}
}
