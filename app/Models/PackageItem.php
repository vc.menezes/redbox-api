<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class PackageItem
 * @package App\Models
 * @version July 22, 2018, 7:03 am UTC
 *
 * @property \App\Models\Block block
 * @property \App\Models\Package package
 * @property integer qty
 * @property string description
 * @property string type
 * @property string UOM
 * @property integer commodity_id
 * @property integer package_id
 */
class PackageItem extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'package_items';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'qty',
        'description',
        'type',
        'UOM',
        'commodity_id',
        'package_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'qty' => 'integer',
        'description' => 'string',
        'type' => 'string',
        'UOM' => 'string',
        'commodity_id' => 'integer',
        'package_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'qty' => 'required:integer',
        'commodity_id' => 'integer',
        'package_id' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function block()
    {
        return $this->belongsTo(\App\Models\Block::class, 'commodity_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package()
    {
        return $this->belongsTo(\App\Models\Package::class, 'package_id', 'id');
    }

    public function commodity()
    {
        return $this->belongsTo(\App\Models\Commodity::class, 'commodity_id', 'id');
    }
}
