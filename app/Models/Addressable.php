<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Audit;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Addressable
 * @package App\Models
 * @version August 2, 2018, 4:47 am UTC
 *
 * @property \App\Models\Country country
 * @property morphs addressable
 * @property string address_line
 * @property string street
 * @property string district
 * @property string city
 * @property integer postal_code
 * @property integer country_id
 */
class Addressable extends BaseModel implements  Auditable
{
    use SoftDeletes;
    Use \OwenIt\Auditing\Auditable;

    public $table = 'addressables';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'profile_id',
        'address_line',
        'street',
        'district',
        'location_id',
        'postal_code',
        'country_id',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'address_line' => 'string',
        'street' => 'string',
        'district' => 'string',
        'type' => 'string',
        'location_id' => 'string',
        'profile_id' => 'integer',
        'postal_code' => 'string', //app ga malhi araathy change from interger to string
        'country_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'profile_id' => 'required:integer',
        'location_id' => 'required:integer',
        'type' => 'required:string',
        'address_line' => 'required:string',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function country()
    {
        return $this->belongsTo(\App\Models\Country::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function getTypeAttribute($value) {
        return ucfirst($value);
    }
}
