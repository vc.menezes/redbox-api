<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class BulkOrderPackage
 * @package App\Models
 * @version October 10, 2018, 2:14 am UTC
 *
 * @property \App\Models\BulkOrder bulkOrder
 * @property \App\Models\Package package
 * @property integer bulk_order_id
 * @property integer package_id
 */
class BulkOrderPackage extends Model implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'bulk_order_packages';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'bulk_order_id',
        'package_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'bulk_order_id' => 'integer',
        'package_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bulk_order_id' => 'required:integer',
        'package_id' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function bulkOrder()
    {
        return $this->belongsTo(\App\Models\BulkOrder::class, 'bulk_order_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package()
    {
        return $this->belongsTo(\App\Models\Package::class, 'package_id', 'id');
    }
}
