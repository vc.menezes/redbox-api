<?php

namespace App\Models;

use App\Events\TrackingUpdated;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Tracking
 * @package App\Models
 * @version July 24, 2018, 8:53 am UTC
 *
 * @property \App\Models\Package package
 * @property integer package_id
 * @property string tracking_no
 */
class Tracking extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'trackings';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'package_id',
        'tracking_no',
        'status',
        'estimated_delivery_date',
    ];



    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'package_id' => 'integer',
        'tracking_no' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'package_id' => 'required:integer',
        'tracking_no' => 'required:string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package()
    {
        return $this->belongsTo(\App\Models\Package::class, 'package_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tracking_log(){
        return $this->hasMany(TrackingLog::class);
    }

    public function getEstimatedDeliveryDateAttribute($value) {
        return $this->changeDateToIso8601String($value);
    }

    public function setEstimatedDeliveryDateAttribute($value) {
        $this->attributes['estimated_delivery_date'] = new \Carbon\Carbon($value);
    }
}
