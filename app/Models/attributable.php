<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class attributable
 * @package App\Models
 * @version July 25, 2018, 4:35 am UTC
 *
 * @property \App\Models\Key key
 * @property morphs attributable
 * @property string value
 * @property integer key_id
 */
class attributable extends BaseModel implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    public $table = 'attributables';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'attributable',
        'value',
        'key_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'value' => 'string',
        'key_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'attributable' => 'required',
        'value' => 'required,string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function key()
    {
        return $this->belongsTo(\App\Models\Key::class, 'key_id', 'id');
    }
}
