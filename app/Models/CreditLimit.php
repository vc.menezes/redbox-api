<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class CreditLimit
 * @package App\Models
 * @version August 16, 2018, 6:57 am UTC
 *
 * @property \App\Models\Profile profile
 * @property integer profile_id
 * @property float limit
 */
class CreditLimit extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'credit_limits';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'profile_id',
        'limit'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'profile_id' => 'integer',
        'limit' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'profile_id' => 'required:integer',
        'limit' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function profile()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'profile_id', 'id');
    }
}
