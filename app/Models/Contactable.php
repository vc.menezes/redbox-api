<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Contactable
 * @package App\Models
 * @version October 29, 2018, 6:35 am UTC
 *
 * @property string value
 * @property string key
 * @property string contactable_id
 * @property string contactable_type
 * @property string type
 */
class Contactable extends Model implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'contactables';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'value',
        'key',
        'contactable_id',
        'contactable_type',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'value' => 'string',
        'key' => 'string',
        'contactable_id' => 'string',
        'contactable_type' => 'string',
        'type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'value' => 'required',
        'key' => 'required',
        'contactable_id' => 'required:integer',
        'contactable_type' => 'required'
    ];

    public function contactable() {
        return $this->morphTo();
    }

    
}
