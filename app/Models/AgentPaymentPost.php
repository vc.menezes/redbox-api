<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AgentPaymentPost
 * @package App\Models
 * @version March 4, 2019, 5:50 am UTC
 *
 * @property \App\Models\Profile profile
 * @property integer profile_id
 * @property double total
 * @property string account_number
 * @property string email
 * @property string details
 * @property string contact_number
 * @property string identifier
 * @property double weight
 */
class AgentPaymentPost extends Model
{
    use SoftDeletes;

    public $table = 'agent_payment_posts';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'profile_id',
        'total',
        'account_number',
        'email',
        'details',
        'contact_number',
        'identifier',
        'weight',
        'date',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'profile_id' => 'integer',
        'total' => 'double',
        'account_number' => 'string',
        'email' => 'string',
        'details' => 'string',
        'contact_number' => 'string',
        'identifier' => 'string',
        'weight' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'profile_id' => 'required:integer',
        'total' => 'required:integer',
        'account_number' => 'required:integer',
        'email' => 'required:integer',
        'details' => 'required:string',
        'contact_number' => 'required:string',
        'identifier' => 'required:string',
        'weight' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function profile()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'profile_id', 'id');
    }
}
