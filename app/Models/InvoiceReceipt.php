<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class InvoiceReceipt
 * @package App\Models
 * @version July 24, 2018, 6:53 pm UTC
 *
 * @property \App\Models\Invoice invoice
 * @property \App\Models\Receipt receipt
 * @property integer invoice_id
 * @property integer receipt_id
 */
class InvoiceReceipt extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'invoice_receipts';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'invoice_id',
        'receipt_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'invoice_id' => 'integer',
        'receipt_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'invoice_id' => 'required:integer',
        'receipt_id' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function invoice()
    {
        return $this->belongsTo(\App\Models\Invoice::class, 'invoice_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function receipt()
    {
        return $this->belongsTo(\App\Models\Receipt::class, 'receipt_id', 'id');
    }
}
