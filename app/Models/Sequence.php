<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Sequence
 * @package App\Models
 * @version July 21, 2018, 7:14 am UTC
 *
 * @property string data_type
 * @property string current_number
 * @property string prefix
 * @property string post_fix
 * @property string template
 * @property string date
 * @property string initial_number
 * @property string reset_by
 */
class Sequence extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'sequences';

    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'data_type',
        'current_number',
        'prefix',
        'post_fix',
        'template',
        'date',
        'initial_number',
        'reset_by',
        'type',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'data_type' => 'string',
        'current_number' => 'string',
        'prefix' => 'string',
        'post_fix' => 'string',
        'template' => 'string',
        'date' => 'string',
        'initial_number' => 'string',
        'reset_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'data_type' => 'required:string',
        'current_number' => 'required:string',
        'prefix' => 'required:string',
        'post_fix' => 'required:string',
        'template' => 'required:string',
        'date' => 'required:string',
        'initial_number' => 'required:string',
        'reset_by' => 'required:string'
    ];



    
}
