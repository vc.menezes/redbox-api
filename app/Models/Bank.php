<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Bank
 * @package App\Models
 * @version August 19, 2018, 5:06 am UTC
 *
 * @property string acronym
 * @property string name
 */
class Bank extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'banks';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'acronym',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'acronym' => 'string',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'acronym' => 'required:string',
        'name' => 'required:string'
    ];

    
}
