<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class ReceiptTranfer
 * @package App\Models
 * @version August 19, 2018, 6:15 am UTC
 *
 * @property \App\Models\Receipt receipt
 * @property integer receipt_id
 * @property string ref
 * @property string from
 * @property float amount
 */
class ReceiptTranfer extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'receipt_tranfers';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'receipt_id',
        'ref',
        'from',
        'amount'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'receipt_id' => 'integer',
        'ref' => 'string',
        'from' => 'string',
        'amount' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'receipt_id' => 'required:integer',
        'ref' => 'required:string',
        'from' => 'required:string',
        'amount' => 'required:float'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function receipt()
    {
        return $this->belongsTo(\App\Models\Receipt::class, 'receipt_id', 'id');
    }
}
