<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Organization
 * @package App\Models
 * @version July 18, 2018, 7:22 am UTC
 *
 * @property \App\Models\Profile profile
 * @property integer profile_id
 * @property integer contact_person
 * @property string tin
 * @property string registry_number
 */
class Organization extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'organizations';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'profile_id',
        'contact_person',
        'tin',
        'registry_number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'profile_id' => 'integer',
        'contact_person' => 'integer',
        'tin' => 'string',
        'registry_number' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'profile_id' => 'required:integer',
        'contact_person' => 'required:integer',
        'registry_number' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function profile()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'profile_id', 'id');
    }


}
