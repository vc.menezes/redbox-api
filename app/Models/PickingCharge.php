<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class PickingCharge
 * @package App\Models
 * @version October 21, 2018, 3:40 am UTC
 *
 * @property \App\Models\Location location
 * @property integer location_id
 * @property double rate
 */
class PickingCharge extends Model implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'picking_charges';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'location_id',
        'rate'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'location_id' => 'integer',
        'rate' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'location_id' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function location()
    {
        return $this->belongsTo(\App\Models\Location::class, 'location_id', 'id');
    }
}
