<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class ReceiptCard
 * @package App\Models
 * @version August 19, 2018, 5:04 am UTC
 *
 * @property \App\Models\Receipt receipt
 * @property \App\Models\CardType cardType
 * @property integer receipt_id
 * @property integer card_type_id
 * @property string transaction_number
 */
class ReceiptCard extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'receipt_cards';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'receipt_id',
        'card_type_id',
        'transaction_number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'receipt_id' => 'integer',
        'card_type_id' => 'integer',
        'transaction_number' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'receipt_id' => 'required:integer',
        'card_type_id' => 'required:integer',
        'transaction_number' => 'string:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function receipt()
    {
        return $this->belongsTo(\App\Models\Receipt::class, 'receipt_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cardType()
    {
        return $this->belongsTo(\App\Models\CardType::class, 'card_type_id', 'id');
    }
}
