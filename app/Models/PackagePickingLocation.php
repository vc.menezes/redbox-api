<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Maatwebsite\Excel\Concerns\Importable;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class PackagePickingLocation
 * @package App\Models
 * @version July 22, 2018, 4:49 am UTC
 *
 * @property \App\Models\Locaton locaton
 * @property \App\Models\Package package
 * @property integer location_id
 * @property integer package_id
 * @property string remarks
 * @property string contact_number
 */
class PackagePickingLocation extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'package_picking_locations';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'location_id',
        'package_id',
        'remarks',
        'contact_number',
        'addressables_id',
        'payment_status',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'location_id' => 'integer',
        'addressables_id' => 'integer',
        'package_id' => 'integer',
        'remarks' => 'string',
        'contact_number' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'location_id' => 'required:integer',
        'package_id' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function location()
    {
        return $this->belongsTo(\App\Models\Location::class, 'location_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package()
    {
        return $this->belongsTo(\App\Models\Package::class, 'package_id', 'id');
    }

     /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function address()
    {
        return $this->belongsTo(\App\Models\Addressable::class, 'addressables_id', 'id');
    }
}
