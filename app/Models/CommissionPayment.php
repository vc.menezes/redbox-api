<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class CommissionPayment
 * @package App\Models
 * @version October 7, 2018, 6:12 pm UTC
 *
 * @property \App\Models\BankAccount bankAccount
 * @property integer bank_account_id
 * @property string reference_number
 * @property double amount
 * @property stimestamp date
 * @property string remarks
 */
class CommissionPayment extends Model implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'commission_payments';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'bank_account_id',
        'profile_id',
        'reference_number',
        'amount',
        'date',
        'remarks'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'bank_account_id' => 'integer',
        'reference_number' => 'string',
        'amount' => 'double',
        'remarks' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bank_account_id' => 'required:integer',
        'date' => 'required:date',
        'remarks' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function bankAccount()
    {
        return $this->belongsTo(\App\Models\BankAccount::class, 'bank_account_id', 'id');
    }

    public function commissions(){
        return $this->hasMany(CommissionPaymentCommission::class);
    }


}
