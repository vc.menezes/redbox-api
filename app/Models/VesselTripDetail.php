<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class VesselTripDetail
 * @package App\Models
 * @version September 23, 2018, 5:38 am UTC
 *
 * @property \App\Models\Vessel vessel
 * @property integer vessel_id
 * @property timestamp departure_at
 * @property timestamp eta
 */
class VesselTripDetail extends Model implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'vessel_trip_details';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'vessel_id',
        'departure_at',
        'eta'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'vessel_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'vessel_id' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function vessel()
    {
        return $this->belongsTo(\App\Models\Vessel::class, 'vessel_id', 'id');
    }
}
