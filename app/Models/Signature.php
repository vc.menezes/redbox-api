<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Signature
 * @package App\Models
 * @version July 24, 2018, 8:49 am UTC
 *
 * @property \App\Models\Package package
 * @property \App\Models\Profile profile
 * @property \App\Models\Agreement agreement
 * @property integer package_id
 * @property integer profile_id
 * @property integer agreement_id
 * @property integer attacheble_id
 */
class Signature extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    public $table = 'signatures';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'package_id',
        'profile_id',
        'agreement_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'package_id' => 'integer',
        'profile_id' => 'integer',
        'agreement_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'package_id' => 'required:integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package()
    {
        return $this->belongsTo(\App\Models\Package::class, 'package_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function profile()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'profile_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function agreement()
    {
        return $this->belongsTo(\App\Models\Agreement::class, 'agreement_id', 'id');
    }

    public function attachables()
    {
        return $this->morphMany(Attachable::class, 'attachable');
    }
}
