<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Commodity
 * @package App\Models
 * @version July 22, 2018, 6:16 am UTC
 *
 * @property \App\Models\CommodityCategory commodityCategory
 * @property string name
 * @property integer commodity_category_id
 */
class Commodity extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'commodities';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'commodity_category_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'required:string',
        'commodity_category_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required:string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Models\CommodityCategory::class, 'commodity_category_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function packageItem()
    {
        return $this->hasMany(\App\Models\PackageItem::class, 'id', 'package_id');
    }
}
