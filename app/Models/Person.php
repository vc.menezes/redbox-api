<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class Person
 * @package App\Models
 * @version July 18, 2018, 7:18 am UTC
 *
 * @property \App\Models\Profile profile
 * @property integer profile_id
 * @property date date_of_birth
 * @property string id_card_no
 */
class Person extends BaseModel implements Auditable
{
    use SoftDeletes;
		use \OwenIt\Auditing\Auditable;
    public $table = 'people';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'profile_id',
        'date_of_birth',
        'id_card_no'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'profile_id' => 'integer',
        'date_of_birth' => 'date',
        'id_card_no' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'profile_id' => 'required:integer',
        'id_card_no' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function profile()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'profile_id', 'id');
    }

    public function getDateOfBirthAttribute($value) {
        return $this->changeDateToIso8601String($value);
    }

    public function setDateOfBirthAttribute($value) {
        $this->attributes['date_of_birth'] = new \Carbon\Carbon($value);
    }
}
