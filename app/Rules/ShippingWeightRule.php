<?php

namespace App\Rules;

use App\Models\Rate;
use Illuminate\Contracts\Validation\Rule;

class ShippingWeightRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    public $max_weight;
    public $medium;

    public function __construct($medium)
    {
        $this->medium = $medium;
        $this->max_weight = Rate::whereMedium($medium)->max('weight');
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->max_weight >= $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Maximum weight allow in'. $this->medium .'medium is ' . $this->max_weight;
    }
}
