<?php

namespace App\Exceptions;

use Exception;

class RateNotExist extends Exception
{
    /**
     * @var string
     */
    protected $message = 'Rate Not Found';
    /**
     * @var int
     */
    protected $code = 403;
}
