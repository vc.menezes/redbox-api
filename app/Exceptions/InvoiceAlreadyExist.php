<?php

namespace App\Exceptions;

use Exception;

class InvoiceAlreadyExist extends Exception
{
    /**
     * @var string
     */
    protected $message = 'Invoice already generated for this services';
    /**
     * @var int
     */
    protected $code = 403;
}
