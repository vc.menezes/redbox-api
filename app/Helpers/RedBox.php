<?php
/**
 * Created by PhpStorm.
 * User: adamwaheed
 * Date: 18/10/2018
 * Time: 2:23 PM
 */

namespace App\Helpers;


use App\Models\Location;
use App\Models\Package;
use App\Models\Payload;
use App\Models\Rate;
use App\Models\Setting;
use App\Models\TrackingLog;

class RedBox
{
    public function getOrigin($parent_id)
    {
        $location = Location::whereId($parent_id)->first();

        if (!$location)
            return 'international';

        if ($location->type == 'country' && $location->code == 'MV') {

            return 'local';
        } else {

            return $this->getOrigin($location->parent_id);
        }

        return 'international';
    }

    public function getSetting($key)
    {
        $setting = Setting::where('key', $key)->first();
        if (!$setting)
            return null;
        return $setting->value;
    }

    public function Rate($package)
    {
        //package wight
        $input['weight'] = floor($package->weight);

        $input['operator'] = '=';

        $DEFAULT_FROM_LOCATION = $this->getSetting('DEFAULT_FROM_LOCATION');


        if ($package->weight < 1) {

            $input['weight'] = ceil($package->weight);

            $input['operator'] = '<';
        }

        $input['type'] = 'local';

        if ($parent_id = $package->package_destination->location->parent_id) {

            $input['type'] = $this->getOrigin($parent_id);
        }


        $input['medium'] = $package->package_destination->medium;

        $input['from_id'] = $DEFAULT_FROM_LOCATION;

        $input['to_id'] = '*';

        return Rate::where($input)->first();
    }

    public function get_total($package, $with_gst = true)
    {

        // get by package wight
        $rate = $this->Rate($package);
        //get GST percentage from setting
        $GST = $this->getSetting('GST');
        if ($rate) {
            $total = $rate->type == 'international' ? round(($rate->rate * $rate->weight), 2) : round($rate->rate);
            if ($with_gst) {
                return $this->get_tax($total) + $total;
            } else {
                return $total;
            }
        } else {
            return 0;
        }
    }

    public function get_tax($total)
    {
        $GST = $this->getSetting('GST');
        return round(($GST / 100) * $total, 2);
    }

    public function shipment_commission_total($shipment)
    {
        $total = 0;
        $packages = Package::where('parent_id', $shipment->id)->get();
        foreach ($packages as $package) {
            $total = $total + $this->get_total($package);
        }
        return $total;

    }

    public function PopTracking(Package $package)
    {
        if ($package->tracking && count($package->tracking->tracking_log) >= 2) {
            TrackingLog::whereTrackingId($package->tracking->id)->orderBy('id', 'desc')->first()->delete();
        }
    }

    public function AddTracking(Package $package, $status_id, $location_id, $description)
    {
        if ($package->tracking) {
            $package->tracking->tracking_log()->create([
                'status_id' => $status_id,
                'location_id' => $location_id,
                'description' => $description
            ]);
        }
    }


    public function RaisePayload($data, $model)
    {
        $new = new  Payload();
        $new->data_id = $model->id;
        $new->data_type = get_class($model);
        $new->payload = json_encode($data);
        $new->save();
    }

    public function GetPayload($model){
        $model->payload = Payload::whereDataId($model->id)->whereDataType(get_class($model))->first();
    }

    public function MonthName($nuber){
        $months = array(
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July ',
            'August',
            'September',
            'October',
            'November',
            'December',
        );
        return $months[$nuber-1];
    }


}
