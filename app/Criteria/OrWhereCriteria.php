<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class OrWhereCriteria.
 *
 * @package namespace App\Criteria;
 */
class OrWhereCriteria implements CriteriaInterface
{
    public $one;
    public $two;

    public function __construct($one, $two)
    {
        $this->one = $one;
        $this->two = $two;
    }


    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where($this->one)->orWhere($this->two);
    }
}
