<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class WhereInCriteria.
 *
 * @package namespace App\Criteria;
 */
class WhereInCriteria implements CriteriaInterface
{

    public $field;
    public $array;

    public function __construct($field, $array)
    {
        $this->field = $field;
        $this->array = $array;
    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn($this->field, $this->array);
    }
}
