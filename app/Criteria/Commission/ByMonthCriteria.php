<?php

namespace App\Criteria\Commission;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ByMonthCriteria.
 *
 * @package namespace App\Criteria\Commission;
 */
class ByMonthCriteria implements CriteriaInterface
{
    public $month;
    public function __construct($month)
    {
        $this->month = $month;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereMonth('created_at',$this->month);
    }
}
