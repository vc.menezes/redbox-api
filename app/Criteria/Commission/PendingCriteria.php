<?php

namespace App\Criteria\Commission;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PendingCriteria.
 *
 * @package namespace App\Criteria\Commision;
 */
class PendingCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->select(\DB::raw('sum(total) as total,profile_id'))->groupby('profile_id')
            ->with('profile.location')
            ->whereStatus(1)
            ->whereNull('deleted_at');
    }
}
