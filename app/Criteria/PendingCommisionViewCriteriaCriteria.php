<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PendingCommisionViewCriteriaCriteria.
 *
 * @package namespace App\Criteria;
 */
class PendingCommisionViewCriteriaCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
	public function apply($model, RepositoryInterface $repository)
	{
		return $model->select(\DB::raw('
		name,
		sum(total) as total,
		profile_id,
		max(account_no) as account_no,
		max(email) as email,
		max(details) as details,
		max(identifier) as identifier,
		max(contact_number) as contact_number,
		sum(weight) as weight,
		max(type) as type,
		max(created_at) as created_at
		'))->groupby('profile_id')
//            ->with('profile.location')
			->whereStatus(1);
//			->whereNull('deleted_at');
	}
}
