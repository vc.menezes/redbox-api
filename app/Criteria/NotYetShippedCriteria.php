<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class NotYetShippedCriteria.
 *
 * @package namespace App\Criteria;
 */
class NotYetShippedCriteria implements CriteriaInterface
{

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        return $model->whereNull('parent_id')->whereType('order')->whereIn('last_tracking_status_id', [2,8,3]);
    }
}
