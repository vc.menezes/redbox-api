<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FilterColumnsCriteria.
 *
 * @package namespace App\Criteria;
 */
class FilterColumnsCriteria implements CriteriaInterface
{
    public $condition;

    public function __construct($condition)
    {
        $this->condition = $condition;
    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where($this->condition);
    }
}
