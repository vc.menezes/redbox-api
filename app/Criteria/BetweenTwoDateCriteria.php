<?php

namespace App\Criteria;

use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class BetweenTwoDateCriteria.
 *
 * @package namespace App\Criteria;
 */
class BetweenTwoDateCriteria implements CriteriaInterface
{
    public $from;
    public $to;

    public function __construct($from, $to)
    {
        $this->from = Carbon::parse($from)->setTime(00, 00, 00);
        $this->to = Carbon::parse($to)->setTime(23, 59, 59);

    }

    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        return $model->whereBetween('created_at', array($this->from->toDateTimeString(), $this->to->toDateTimeString()));
    }
}
