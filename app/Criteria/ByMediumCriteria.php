<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ByMediumCriteria.
 *
 * @package namespace App\Criteria;
 */
class ByMediumCriteria implements CriteriaInterface
{

    public $medium;
    public function __construct($medium)
    {
        $this->medium = $medium;

    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereTransportMedium($this->medium);
    }
}
