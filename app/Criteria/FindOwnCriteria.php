<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FindOwnCriteria.
 *
 * @package namespace App\Criteria;
 */
class FindOwnCriteria implements CriteriaInterface
{
    public $user;

    public function __construct($user)
    {
        $this->user = $user;
    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereProfileId($this->user->profile_id);
    }
}
