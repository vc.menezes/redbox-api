<?php

namespace App\Criteria;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FindValidTokenCriteria.
 *
 * @package namespace App\Criteria;
 */
class FindValidTokenCriteria implements CriteriaInterface
{
    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('expires_at','>',Carbon::now())->whereExpired(0)->whereToken($this->request->token);
    }
}
