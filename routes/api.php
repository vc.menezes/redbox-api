<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('hello', function () {
	echo \Hash::make('admin');


});

//Route::get('package_destinations/list_unique','PackageDestinationAPIController@list_unique')->name('package_destinations.list_unique');


// routes for ooredoo
// Route::group(['prefix' => 'v1', 'middleware' => 'check.token', 'namespace' => 'V1'], function() {
Route::group(['prefix' => 'v1', 'namespace' => 'V1'], function () {
	Route::post('authenticate', 'ApiController@authenticate')->name('users.authenticate');
	Route::post('rates/calculate', 'ApiController@calculate')->name('v1.calculate');
	Route::get('islands', 'ApiController@getIslands')->name('v1.islands');
	Route::post('order/create', 'ApiController@createOrder')->name('v1.order-create');
});


Route::prefix('open')->group(function () {

	// get all island, this api used website customer register
	Route::get('/locations/{type}', 'LocationAPIController@getLocationByType')->name('location.get-by-type');
	// Route::get('/trackings/search/{tracking_no}', 'TrackingAPIController@getTracking')->name('tracking.get-tracking');
	Route::get('/trackings/search/{tracking_no}', 'PackageViewAPIController@getTracking')->name('tracking.get-tracking');
	Route::get('/trackings/check/{tracking_no}', 'PackageViewAPIController@check')->name('tracking.check-tracking');
	Route::post('rates/calculate', 'RateAPIController@calculate')->name('rates.calculate');
	Route::get('rates/locations', 'RateAPIController@locationRates')->name('rates.location');
	Route::get('rates/medium', 'RateAPIController@getMedium')->name('rates.medium');

	Route::post('/contact/sendmail/', 'ContactUsAPIControllerAPIController@sendContactUsMessage');
	Route::get('attachables/blaalbalaa/{id}', 'AttachableAPIController@show');
});

Route::post('users/register', 'UserAPIController@register')->name('users.register');
Route::post('users/authenticate', 'UserAPIController@authenticate')->name('users.authenticate');
Route::post('users/refresh/token', 'UserAPIController@refreshToken')->name('users.refresh_token');
Route::post('users/password/forget', 'UserAPIController@passwordForget')->name('users.password-forget');
Route::post('users/password/reset', 'ResetPasswordAPIController@passwordReset')->name('users.password-reset');

Route::get('countries', 'CountryAPIController@index');

// Route::middleware(['check.token', 'user.ability'])->group(function () {
Route::middleware(['check.token'])->group(function () {
// Route::middleware([])->group(function () {

	Route::get('users/profile', 'UserAPIController@profile')->name('users.profile'); //use app
	Route::put('users/update/password/{id}', 'UserAPIController@updatePassword')->name('users.update_password'); //use app
	Route::put('users/update_profile/{user_id}', 'UserAPIController@update_profile')->name('users.update_profile'); //use app
	Route::post('users/verify', 'UserAPIController@verify')->name('users.verify'); //use app
	Route::post('users/resend_token', 'UserAPIController@resend_token')->name('users.resend-token'); //use app
	Route::post('users/password/change', 'UserAPIController@passwordChange')->name('users.password-change'); //user app


	Route::get('dashboard/packages', 'DashboardAPIController@packages')->name('dashboard.packages'); //user app
	Route::get('dashboard/sales_over_view', 'DashboardAPIController@sales_over_view')->name('dashboard.sales_over_view'); //user app
	Route::get('dashboard/users_count', 'DashboardAPIController@users_count')->name('dashboard.users_count'); //user app
	Route::get('dashboard/packages_agent', 'DashboardAPIController@packages_agent')->name('dashboard.packages_agent'); //user app
	Route::get('dashboard/agent_routes', 'DashboardAPIController@agent_routes')->name('dashboard.agent_routes'); //user app
	Route::get('dashboard/agent_location', 'DashboardAPIController@agent_location')->name('dashboard.agent_location'); //user app


	Route::get('packages/my/orders', 'PackageViewAPIController@getMyOrders')->name('packages.my-orders'); // use app
	Route::get('packages/my/statistic', 'PackageAPIController@getStatistic')->name('packages.my-orders-stat-and-open-tracking'); //use app


	Route::get('packages/download_packages_report', 'PackageAPIController@download_packages_report')->name('packages.download_packages_report'); //use app


	Route::get('invoices/daily_sales', 'InvoiceAPIController@daily_sales')->name('invoices.daily_sales'); //use app
	Route::get('invoices/daily_sales_download', 'InvoiceAPIController@daily_sales_download')->name('invoices.daily_sales_download');
	//use app
	Route::get('invoices/yearly_sales', 'InvoiceAPIController@yearly_sales')->name('invoices.yearly_sales'); //use app

	Route::get('invoices/monthly_sales', 'InvoiceAPIController@monthly_sales')->name('invoices.monthly_sales'); //use app


	Route::get('packages/yearly_statistics', 'PackageAPIController@yearly_statistics')->name('packages.yearly_statistics'); //use app

	Route::get('packages/filter', 'PackageAPIController@filter')->name('packages.filter'); //use app

	Route::get('profiles/agent/{id}', 'ProfileAPIController@getAgent')->name('profile.agent-data'); // use app \agent
	Route::put('shipments/update/tracking/{package_id}', 'TrackingAPIController@updateShipmentTracking')->name('profile.shipments-update-tracking'); // use app \agent
	Route::post('package/return/{package_id}', 'TrackingAPIController@returnToMale')->name('profile.package-returnToMale'); // use app \agent
	Route::get('packages/code/{code}', 'PackageAPIController@getPackageByCode')->name('packages.get-by-code'); //use app \agent
	Route::post('commodities/firstOrCreate', 'CommodityAPIController@firstOrCreate')->name('commodity.find-or-create-package'); //use app \customer

	Route::get('package_destinations/list_unique', 'PackageDestinationAPIController@list_unique')->name('package_destinations.list_unique');

	Route::get('commissions/show_by_agent', 'CommissionAPIController@show_by_agent')->name('commissions.show_by_agent');

	Route::get('commissions/download_by_agent', 'CommissionAPIController@download_by_agent')->name('commissions.download_by_agent');

	Route::get('commissions/pending_commission', 'CommissionAPIController@pending_commission')->name('commissions.pending_commission');
	Route::get('commissions/download_pending_commission', 'CommissionAPIController@download_pending_commission')->name('commissions.download_pending_commission');


	Route::get('vessel_routes/vessel/{vessel_id}', 'VesselRouteAPIController@vessel')->name('vessel_routes.vessels');

	Route::post('packages/create_shipment', 'PackageAPIController@create_shipment')->name('packages.create_shipment');
	Route::put('packages/update_shipment/{id}', 'PackageAPIController@updateShipment')->name('packages.update_shipment');


	Route::delete('packages/remove_package_from_shipment/{id}', 'PackageAPIController@remove_package_from_shipment')->name('packages.remove_package_from_shipment');

	Route::get('packages/not_shipped_order', 'PackageAPIController@not_shipped_order')->name('packages.not_shipped_order');

	Route::post('packages/add_order_to_shipment/{shipment_id}', 'PackageAPIController@add_order_to_shipment')->name('packages.add_order_to_shipment');


	Route::get('packages/show_shipment/{id}', 'PackageAPIController@show_shipment')->name('packages.show_shipment');


	Route::get('tracking/log/{code}', 'TrackingAPIController@log')->name('tracking.log');
	Route::get('tracking/package/logs/{id}', 'TrackingAPIController@getPackageTrackingLogs')->name('tracking.getPackageTrackingLogs');

	Route::get('vessels/vessel_agents', 'VesselAPIController@vessel_agents')->name('vessels.vessel_agents');

	Route::get('agents/commissions/{agent_id}', 'AgentAPIController@commissions')->name('agents.commissions');

	Route::get('agents/not_ship_packages_agent', 'AgentAPIController@not_ship_packages_agent')->name('agents.not_ship_packages_agent');


	Route::get('dashboard/menu', 'PermissionAPIController@menu')->name('dashboard.menu');
	Route::resource('locations', 'LocationAPIController');


	Route::resource('people', 'PersonAPIController');

	Route::resource('organizations', 'OrganizationAPIController');
	Route::resource('agents', 'AgentAPIController');

	Route::resource('bank_accounts', 'BankAccountAPIController');


	Route::resource('users', 'UserAPIController');

	Route::resource('tokens', 'TokenAPIController');


	Route::get('profiles/agent/shipments/{id}', 'ProfileAPIController@getAgentShipments')->name('profile.agent-shipments');
	Route::get('packages/shipment_orders/{shipment_id}', 'PackageAPIController@shipment_orders')->name('packages.shipment_orders');
	Route::get('packages/shipments_index', 'PackageAPIController@shipments_index')->name('packages.shipments_index');
	Route::post('packages/shipment_return', 'PackageAPIController@shipment_return')->name('packages.shipment_return');

	Route::delete('packages/cancel_shipment/{id}', 'PackageAPIController@cancel_shipment')->name('packages.cancel_shipment');


	Route::get('vessels/ready_to_ship_vessels', 'VesselAPIController@ready_to_ship_vessels')->name('vessels.ready_to_ship_vessels');

	Route::get('vessels/ready_to_ship_vessel_shipment_edit', 'VesselAPIController@ready_to_ship_vessel_shipment_edit')->name('vessels.ready_to_ship_vessel_shipment_edit');


	Route::resource('sequences', 'SequenceAPIController');

	Route::resource('package_picking_locations', 'PackagePickingLocationAPIController');

	Route::get('blocks/racks', 'BlockAPIController@getRacks')->name('blocks.getRacks');
	Route::resource('blocks', 'BlockAPIController');

	Route::resource('storage_blocks', 'StorageBlockAPIController');
	Route::post('package_view/package/remove', 'StorageBlockAPIController@removePackage')->name('block.remove-package');

	Route::resource('package_destinations', 'PackageDestinationAPIController');

	Route::resource('commodity_categories', 'CommodityCategoryAPIController');

	Route::resource('commodities', 'CommodityAPIController');


	Route::resource('package_items', 'PackageItemAPIController');

	Route::resource('agreements', 'AgreementAPIController');


	Route::resource('tracking', 'TrackingAPIController');

	Route::resource('statuses', 'StatusAPIController');

	Route::resource('tracking_logs', 'TrackingLogAPIController');

	Route::get('invoices/package/{package_id}', 'InvoiceAPIController@getPackageInvoice')->name('invoice.package');
	Route::get('invoices/profile/{profile_id?}', 'InvoiceAPIController@getProfileInvoice')->name('invoice.profile');
	Route::post('invoices/sum_selected', 'InvoiceAPIController@sum_selected')->name('invoices.sum_selected');
	Route::post('invoices/pay', 'InvoiceAPIController@pay')->name('invoices.pay');

	Route::post('invoices/cancel/{id}', 'InvoiceAPIController@cancel')->name('invoices.cancel');

	Route::resource('invoices', 'InvoiceAPIController');


	Route::resource('receipts', 'ReceiptAPIController');

	Route::resource('invoice_receipts', 'InvoiceReceiptAPIController');

	Route::resource('payloads', 'PayloadAPIController');


	Route::get('attachables/all/{id}', 'AttachableAPIController@getProfileAttachble')->name('attachables.by-id');
	Route::resource('attachables', 'AttachableAPIController');


	Route::resource('keys', 'KeyAPIController');

	Route::resource('attributables', 'attributableAPIController');

	Route::resource('berths', 'BerthAPIController');

	Route::post('vessels/toggle_docked', 'VesselAPIController@toggle_docked')->name('vessels.toggle_docked');
	Route::resource('vessels', 'VesselAPIController');

	Route::resource('vessel_berths', 'VesselBerthAPIController');

	Route::resource('vessel_routes', 'VesselRouteAPIController');

// Route::resource('countries', 'CountryAPIController');

	Route::get('addressables/profile', 'AddressableAPIController@profileAddress')->name('addressables.profileAddress');

	Route::resource('rates', 'RateAPIController');

	Route::resource('settings', 'SettingAPIController');

	Route::resource('credit_limits', 'CreditLimitAPIController');


	Route::resource('card_types', 'CardTypeAPIController');

	Route::resource('banks', 'BankAPIController');

	Route::resource('receipt_cards', 'ReceiptCardAPIController');

	Route::resource('banks', 'BankAPIController');

	Route::resource('receipt_cheques', 'ReceiptChequeAPIController');

	Route::resource('receipt_tranfers', 'ReceiptTranferAPIController');

	Route::resource('package_shipments', 'PackageShipmentAPIController');

	Route::resource('package_handovers', 'PackageHandoverAPIController');

	Route::resource('orders', 'OrderAPIController');

	Route::resource('profile_views', 'ProfileViewAPIController');

	Route::get('package_view/{id}', 'PackageViewAPIController@show')->name('package_view.show');
	Route::get('package_view/profile/{profile_id}', 'PackageViewAPIController@getPackages')->name('package_view.profile-get');


	Route::get('customers', 'CustomerAPIController@index')->name('customers.index');
	Route::get('customers/{id}', 'CustomerAPIController@show')->name('customers.show');


	Route::resource('commissions', 'CommissionAPIController');

	Route::resource('vessel_trip_details', 'VesselTripDetailAPIController');

	Route::resource('vessel_trip_details', 'VesselTripDetailAPIController');


	Route::get('organizations-view', 'OrganizationViewAPIController@index')->name('organizations-view.show');
	Route::get('organizations-view/{id}', 'OrganizationViewAPIController@show')->name('organizations-view.show');


	Route::get('agents-profile', 'AgentProfileAPIController@index')->name('agents-profile.show');
	Route::get('agents-profile/{id}', 'AgentProfileAPIController@show')->name('agents-profile.show');
	Route::get('agents-profile/islands/{id}', 'AgentProfileAPIController@getAgentIslands')->name('agents-profile.islands');


	Route::resource('roles', 'RoleAPIController');

	Route::resource('permissions', 'PermissionAPIController');

	Route::get('roles/permissions/{id}', 'RoleAPIController@permissions')->name('roles.permissions');
	Route::post('roles/permissions_toggle', 'RoleAPIController@permissions_toggle')->name('roles.permissions_toggle');

	Route::get('users/permissions/{id}', 'UserAPIController@permissions')->name('users.permissions');
	Route::post('users/permissions_toggle', 'UserAPIController@permissions_toggle')->name('users.permissions_toggle');


	Route::get('users/get_user_roles/{id}', 'UserAPIController@get_user_roles')->name('users.get_user_roles');
	Route::post('users/role_toggle', 'UserAPIController@role_toggle')->name('users.role_toggle');

	Route::resource('packages', 'PackageAPIController');
	Route::post('packages/cancel-order', 'PackageAPIController@cancelOrder')->name('packages.cancel-order');

	Route::resource('agent_commissions', 'AgentCommissionAPIController');

	Route::resource('receipt_views', 'ReceiptViewAPIController');
	Route::resource('commission_views', 'CommissionViewAPIController');

	Route::resource('profiles', 'ProfileAPIController'); //use app @show \customer
	Route::resource('addressables', 'AddressableAPIController'); //use app @store @update \customer
	Route::resource('signatures', 'SignatureAPIController'); //use app @store \agent
	Route::resource('packages', 'PackageAPIController'); //use app @store @get \customer --- @get \agent

	Route::put('packages/order/verify/{id}', 'PackageAPIController@verifyOrder')->name('packages.verify-order');


	Route::resource('commission_payments', 'CommissionPaymentAPIController');

	Route::resource('commission_payment_commissions', 'CommissionPaymentCommissionAPIController');


	Route::post('bulk_orders/raise_invoice/{id}', 'BulkOrderAPIController@raise_invoice')->name('bulk_orders.raise_invoice');

	Route::resource('bulk_orders', 'BulkOrderAPIController');

	Route::resource('bulk_order_packages', 'BulkOrderPackageAPIController');


	Route::resource('remarks', 'RemarkAPIController');

	Route::resource('package_rack_views', 'PackageRackViewAPIController');
	Route::get('package_rack_views/package/storage', 'PackageRackViewAPIController@getPackageForStorage')->name('package_view.storage-get');

	Route::resource('bulk_order_views', 'BulkOrderViewAPIController');

	Route::resource('invoice_items', 'InvoiceItemAPIController');

	Route::resource('picking_charges', 'PickingChargeAPIController');

	Route::resource('contactables', 'ContactableAPIController');
	Route::get('contactaready_to_ship_vesselsbles/profile/{id}', 'ContactableAPIController@profileContact')->name('conatable.profile');
	Route::post('shipments/generate_commission', 'PackageAPIController@generateCommission')->name('package.commission');
	Route::get('shipments/shipment_commission/{id}', 'PackageAPIController@shipmentCommission')->name('package.shipmentCommission');

	Route::resource('agent_payment_posts', 'AgentPaymentPostAPIController');
});






