<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('print/label/{id}', 'PrintController@label'); // print order stick label
Route::get('print/invoice/{id}', 'PrintController@invoice'); // print normal invoive
Route::get('print/bulk/{id}', 'PrintController@bulk'); // print bulk invoice
Route::get('print/pod/{id}', 'PrintController@proofOfDelivery'); // print proofOfDelivery
Route::get('print/waybill/{id}', 'PrintController@waybill'); // print proofOfDelivery
Route::get('print/commission/{id}', 'PrintController@commission'); // print commission
Route::get('print/receipt/{id}', 'PrintController@receipt'); // print receipt
Route::get('print/handover/{id}', 'PrintController@handOver'); // print receipt

Route::get('print/daily_sales_download', 'PrintController@daily_sales_download'); // print receipt

Route::get('/', function () {
    return 'bello';
    // $pdf = PDF::loadView('package-invoice');
    // return $pdf->stream('invoice.pdf');
    // $data  = \App\Models\Invoice::with(['profile'])->find(3);


    // $pdf = PDF::loadView('package-order-label', compact('data'));
    // return $pdf->stream('invoice.pdf');
});