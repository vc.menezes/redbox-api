<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        //  factory(\App\Models\Location::class, 500)->create(); // this one need?
        \Illuminate\Support\Facades\Artisan::call('command:update-permission');

        $this->call(StarterSeeder::class);
        $this->call(Country::class);
        $this->call(AtollSeed::class);
        $this->call(IslandSeeder::class);
        $this->call(Address::class);
        $this->call(Permission::class);
        $this->call(Role::class);
        $this->call(SequenceSeeder::class);
        $this->call(RateSeeder::class);
        $this->call(Setting::class);
        $this->call(Profile::class);
    }

}
