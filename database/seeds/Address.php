<?php

use Illuminate\Database\Seeder;

class Address extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lists = [
            ["code" => "103", "name" => "Blue Heaven", 'parent_id' => 29],
            ["prefix" => "G", "name" => "G.Aanooraaanee", 'parent_id' => 23],
        ];

        foreach ($lists as $list){
            $list['type'] = 'address';
            $new = new \App\Models\Location($list);
            $new->save();
        }
    }
}
