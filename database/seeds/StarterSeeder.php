<?php

use Illuminate\Database\Seeder;

class StarterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //commodity_category
        $commodity_category = [
            ['name' => 'Perishables'],
            ['name' => 'Fragile'],
            ['name' => 'Chemical'],
            ['name' => 'Electronics']
        ];
        foreach ($commodity_category as $val) {
            \App\Models\CommodityCategory::create($val);
        }

        //commodity
        $commodity = [
            ['name' => 'Onion', 'commodity_category_id' => 1],
            ['name' => 'Fish Can', 'commodity_category_id' => 1],
            ['name' => 'Resin', 'commodity_category_id' => 3],
            ['name' => 'Laptop', 'commodity_category_id' => 4],
            ['name' => 'Washing Machine', 'commodity_category_id' => 4],
            ['name' => 'Mobile Phone', 'commodity_category_id' => 2],
        ];
        foreach ($commodity as $val) {
            \App\Models\Commodity::create($val);
        }

        // status
        $statuses = [
            ['name' => 'Waiting For Pickup'],
            ['name' => 'Received to Office'],
            ['name' => 'Ready to ship'],
            ['name' => 'Shipped from Male\''],
            ['name' => 'Received To Agent'],
            ['name' => 'Delivered'],
            ['name' => 'Return to office'],
            ['name' => 'Order created'],
            ['name' => 'Return to Male\'']
        ];
        foreach ($statuses as $val) {
            \App\Models\Status::create($val);
        }
        

        // Banks
        $banks = [
            ['name' => 'Maldives Islamic Bank', 'acronym' => 'MIB'],
            ['name' => 'Bank of Maldives', 'acronym' => 'BML'],
        ];

        foreach ($banks as $bank) {
            \App\Models\Bank::create($bank);
        }

        
    }
}
