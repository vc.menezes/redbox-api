<?php

use Illuminate\Database\Seeder;

class Permission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'menu_bulk_orders',
                'display_name' => 'Bulk order menu',
            ],
            [
                'name' => 'menu_my_invoices',
                'display_name' => 'Organaization Invoices',
            ],
            [
                'name' => 'menu_my_all_orders',
                'display_name' => 'Organaization all orders',
            ],
            [
                'name' => 'menu_agents',
                'display_name' => 'Agents menu',
            ],
            [
                'name' => 'menu_customers',
                'display_name' => 'Customers menu',
            ],
            [
                'name' => 'menu_organizations',
                'display_name' => 'Organizations menu',
            ],
            [
                'name' => 'menu_invoices',
                'display_name' => 'Invoices menu',
            ],
            [
                'name' => 'menu_receipts',
                'display_name' => 'Receipts menu',
            ],
            [
                'name' => 'menu_orders',
                'display_name' => 'Orders menu',
            ],
            [
                'name' => 'menu_shipments',
                'display_name' => 'Shipments menu',
            ],
            [
                'name' => 'menu_tracking',
                'display_name' => 'Tracking menu',
            ],
            [
                'name' => 'menu_blocks',
                'display_name' => 'Racks menu',
            ],
            [
                'name' => 'menu_commissions',
                'display_name' => 'Commission menu',
            ],
            [
                'name' => 'menu_pending_commissions',
                'display_name' => 'Pending Commission menu',
            ],
            [
                'name' => 'menu_users',
                'display_name' => 'Users menu',
            ],
            [
                'name' => 'menu_roles',
                'display_name' => 'Roles menu',
            ],
            [
                'name' => 'menu_permissions',
                'display_name' => 'Permissions menu',
            ],
            [
                'name' => 'menu_notifications',
                'display_name' => 'Notifications menu',
            ],
            [
                'name' => 'menu_vessels',
                'display_name' => 'Vessels menu',
            ],
            [
                'name' => 'menu_locations',
                'display_name' => 'Locations menu',
            ],
            [
                'name' => 'menu_location_picking_rate',
                'display_name' => 'Pickinh location rate menu',
            ],
            [
                'name' => 'menu_banks',
                'display_name' => 'Banks menu',
            ],
            [
                'name' => 'menu_card_types',
                'display_name' => 'Card types menu',
            ],
            [
                'name' => 'menu_commoditity_categories',
                'display_name' => 'Commodity categories menu',
            ],
            [
                'name' => 'menu_commodities',
                'display_name' => 'Commodities menu',
            ],
            [
                'name' => 'menu_rates',
                'display_name' => 'Rates menu',
            ],
            [
                'name' => 'menu_status',
                'display_name' => 'Status menu',
            ],
            [
                'name' => 'menu_settings',
                'display_name' => 'Settings menu',
            ],
            [
                'name' => 'picking_charges',
                'display_name' => 'Picking Charges menu',
            ],
            [
                'name' => 'menu_report_orders',
                'display_name' => 'Orders report menu',
            ],
            [
                'name' => 'menu_report_sales',
                'display_name' => 'Sales report menu',
            ],
            [
                'name' => 'menu_report_commission',
                'display_name' => 'Commission report menu',
            ],
        ];

        foreach ($permissions as $value){

            $p =  \App\Models\Permission::where('name', $value['name'])->first();
            if(!$p){
                $permission = new  \App\Models\Permission();
                $permission->name = $value['name'];
                $permission->display_name = $value['display_name'];
                $permission->description = $value['display_name'];
                $permission->save();
            }


        }
    }
}
