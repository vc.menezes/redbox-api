<?php

use Illuminate\Database\Seeder;

class Profile extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //admin
        $profile = \App\Models\Profile::create([
            'name'  => 'Admin',
            'identifier' => 'A111111',
            'email'  => 'admin@redbox.com',
            'contact_number'  => '9607827570',
            'type' => 'person',
            'location_id' => 455
        ]);

        $user = \App\User::create([
            'name'  => 'Admin',
            'email'  => 'admin@redbox.com',
            'password'  => bcrypt('password'),
            'profile_id'  => $profile->id,
            'status'  => 1,
            'user_type' => 'admin'
        ]);

        $user->attachRole('admin');


        \App\Models\Person::create([
            'profile_id'  => $profile->id,
            'id_card_no' => 'A111111',
        ]);


        //agent
        $profile = \App\Models\Profile::create([
            'name'  => 'Agent',
            'identifier' => 'A111112',
            'email'  => 'agent@redbox.com',
            'contact_number'  => '9607827570',
            'type' => 'person',
            'location_id' => 455
        ]);

        $profile->agents()->attach([79, 82]); 

        $user = \App\User::create([
            'name'  => 'Agent',
            'email'  => 'agent@redbox.com',
            'password'  => bcrypt('password'),
            'profile_id'  => $profile->id,
            'status'  => 1,
            'user_type' => 'agent'
        ]);

        $user->attachRole('agent');


        \App\Models\Person::create([
            'profile_id'  => $profile->id,
            'id_card_no' => 'A111112',
        ]);


        //customer
        $profile = \App\Models\Profile::create([
            'name'  => 'Customer',
            'identifier' => 'A111113',
            'email'  => 'customer@redbox.com',
            'contact_number'  => '9607827570',
            'type' => 'person',
            'location_id' => 455
        ]);

        $user = \App\User::create([
            'name'  => 'Customer',
            'email'  => 'customer@redbox.com',
            'password'  => bcrypt('password'),
            'profile_id'  => $profile->id,
            'status'  => 1,
            'user_type' => 'customer'
        ]);

        $user->attachRole('customer');


        \App\Models\Person::create([
            'profile_id'  => $profile->id,
            'id_card_no' => 'A111113',
        ]);
        
    }
}
