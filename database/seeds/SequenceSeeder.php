<?php

use Illuminate\Database\Seeder;

class SequenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            [
                'data_type' => 'Package',
                'current_number' => 0,
                'prefix' => '',
                'initial_number' => 0,
                'post_fix' => '',
                'template' => 'date current_number',
                'reset_by' => 'year',
                'type' => 'order_barcode',
            ],

            [
                'data_type' => 'Package',
                'current_number' => 0,
                'prefix' => 'OD',
                'initial_number' => 0,
                'post_fix' => 'ORD',
                'template' => 'prefix date current_number',
                'reset_by' => 'year',
                'type' => 'order_serial',
            ],
            [
                'data_type' => 'Package',
                'current_number' => 0,
                'prefix' => 'RBS',
                'initial_number' => 0,
                'post_fix' => 'SHP',
                'template' => 'prefix date current_number',
                'reset_by' => 'year',
                'type' => 'shipment_code',
            ], [
                'data_type' => 'Package',
                'current_number' => 0,
                'prefix' => '',
                'initial_number' => 0,
                'post_fix' => '',
                'template' => 'current_number date',
                'reset_by' => 'year',
                'type' => 'shipment_barcode',
            ], [
                'data_type' => 'Tracking',
                'current_number' => 0,
                'prefix' => 'RB',
                'initial_number' => 1,
                'post_fix' => 'P',
                'template' => 'prefix date current_number',
                'date' => 'P',
                'reset_by' => 'year',
                'type' => 'tracking_number',
            ], [
                'data_type' => 'Invoice',
                'current_number' => 1000,
                'prefix' => 'INV',
                'initial_number' => 1000,
                'post_fix' => '2018',
                'template' => 'prefix / current_number / post_fix',
                'reset_by' => 'year',
                'type' => 'invoice_number',
            ],
            [
                'data_type' => 'Receipt',
                'current_number' => 1000,
                'prefix' => 'RE',
                'initial_number' => 1000,
                'post_fix' => '2018',
                'template' => 'prefix / current_number / post_fix',
                'reset_by' => 'year',
                'type' => 'receipt_number',
            ],
        ];

        foreach ($array as $item) {
            $item['date'] = time();
            $new = new  \App\Models\Sequence($item);
            $new->save();

        }
    }
}

