<?php

use Illuminate\Database\Seeder;

class AtollSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        \App\Models\Location::truncate();

        $Asia = new \App\Models\Location();
        $Asia->name = "Asia";
        $Asia->code = "as";
        $Asia->prefix = "as";
        $Asia->code = "as";
        $Asia->type = 'region';
        $Asia->save();


        $country = new \App\Models\Location();
        $country->name = "Maldives";
        $country->code = "MV";
        $country->prefix = "MV";
        $country->code = "MV";
        $country->type = 'country';
        $country->parent_id = $Asia->id;
        $country->save();


        $lists = [
            ["code" => "A", "prefix" => "HA", "name" => "Haa Alif"],
            ["code" => "B", "prefix" => "HDh", "name" => "Haa Dhaalu"],
            ["code" => "C", "prefix" => "Sh", "name" => "Shaviyani"],
            ["code" => "D", "prefix" => "N", "name" => "Noonu"],
            ["code" => "E", "prefix" => "R", "name" => "Raa"],
            ["code" => "F", "prefix" => "B", "name" => "Baa"],
            ["code" => "G", "prefix" => "Lh", "name" => "Lhaviyani"],
            ["code" => "H", "prefix" => "K", "name" => "Kaafu"],
            ["code" => "U", "prefix" => "AA", "name" => "Alif Alif"],
            ["code" => "I", "prefix" => "Adh", "name" => "Alif Dhaal"],
            ["code" => "J", "prefix" => "V", "name" => "Vaavu"],
            ["code" => "K", "prefix" => "M", "name" => "Meemu"],
            ["code" => "L", "prefix" => "F", "name" => "Faafu"],
            ["code" => "M", "prefix" => "Dh", "name" => "Dhaalu"],
            ["code" => "N", "prefix" => "Th", "name" => "Thaa"],
            ["code" => "O", "prefix" => "L", "name" => "Laamu"],
            ["code" => "P", "prefix" => "GA", "name" => "Gaafu Alif"],
            ["code" => "Q", "prefix" => "GDh", "name" => "Gaafu Dhaalu"],
            ["code" => "R", "prefix" => "Gn", "name" => "Gnaviyani"],
            ["code" => "S", "prefix" => "S", "name" => "Seenu"],
            ["code" => "T", "prefix" => "Male", "name" => "Male"],
        ];

        foreach ($lists as $list){
            $list['parent_id'] = $country->id;
            $list['type'] = 'atoll';
            $new = new \App\Models\Location($list);
            $new->save();
        }
    }
}
