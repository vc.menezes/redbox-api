<?php

use Illuminate\Database\Seeder;

class Setting extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            ['key'=>'GST','value'=>6],
            ['key'=>'DEFAULT_SHIPPING','value'=>1],
            ['key'=>'AGENT_COMMISSION','value'=>20],
            ['key'=>'DEFAULT_CURRENCY','value'=>'MVR'],
            ['key'=>'DEFAULT_FROM_LOCATION','value'=>455],
            ['key'=>'AGENT_DISCOUNT','value'=>5],
            ['key'=>'DEFAULT_PICKING_CHARGE','value'=>20],
            ['key'=>'CREDIT_PERIOD','value'=>30],
        ];

        foreach ($array as $item){
            $new = new \App\Models\Setting($item);
            $new->save();
        }
    }
}
