<?php

use Illuminate\Database\Seeder;

class Role extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'agent',
                'display_name' => 'agent',
                'description' => 'agent',
            ],
            [
                'name' => 'customer',
                'display_name' => 'customer',
                'description' => 'customer',
            ],
            [
                'name' => 'admin',
                'display_name' => 'admin',
                'description' => 'admin',
            ],
            [
                'name' => 'commercial_customer',
                'display_name' => 'commercial_customer',
                'description' => 'commercial_customer',
            ],
            [
                'name' => 'office',
                'display_name' => 'office',
                'description' => 'office',
            ],
        ];
        foreach ($roles as $value){
            $role = new \App\Models\Role();
            $role->name = $value['name'];
            $role->display_name = $value['display_name'];
            $role->description = $value['description'];
            $role->save();
            if($role->name == 'admin') {
                $role->attachPermissions(\App\Models\Permission::all());
            }
        }
    }
}
