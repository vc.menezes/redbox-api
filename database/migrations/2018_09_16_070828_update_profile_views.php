<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProfileViews extends Migration
{
    /**
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $sql = "create or replace view profile_view as select 
                    p.id,
                    p.name,
                    p.identifier,
                    p.contact_number,
                    p.type,
                    p.location_id,
                    
                    u.email,
                    u.status as user_status,
                    u.user_type as user_type,
                    u.id as user_id,

                    pp.date_of_birth,

                    o.tin as tin,
                    o.contact_person,
                    
                    l.name as location_name,
                    
                    p.created_at

                    from profiles p 
                    left join users u on u.profile_id = p.id
                    left join locations l on l.id = p.location_id
                    left join organizations o on o.profile_id = p.id
                    left join people pp on pp.profile_id = p.id";


    public function up()
    {
        \DB::statement($this->sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // \DB::statement('drop view profile_view');
    }
}
