<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProfileView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $sql = "create or replace view profile_view as select 
                    p.id,
                    p.name,
                    p.identifier,
                    p.contact_number,
                    p.type,
                    p.location_id,
                    
                    u.email,
                    u.status as user_status,
                    u.user_type as user_type,
                    u.id as user_id,
                    
                    l.name as location_name
                    
                    from profiles p 
                    left join users u on u.profile_id = p.id
                    left join locations l on l.id = p.location_id";


    public function up()
    {
        \DB::statement($this->sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // \DB::statement('drop view profile_view');
    }
}
