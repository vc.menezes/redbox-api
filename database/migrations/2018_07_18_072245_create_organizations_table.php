<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrganizationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned()->nullable();
            $table->integer('contact_person')->unsigned()->nullable();
            $table->string('tin', 20)->nullable();
            $table->string('registry_number', 20);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->foreign('contact_person')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('organizations', function (Blueprint $table) {
//
//            $table->dropForeign(['profile_id','contact_person']);
//
//        });
        Schema::drop('organizations');
    }
}
