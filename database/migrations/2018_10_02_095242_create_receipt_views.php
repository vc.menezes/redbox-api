<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptViews extends Migration
{
    public $sql = "create or replace view receipt_views as select 
                    r.id,
                    r.receipt_number,
                    r.card,
                    r.cash,
                    r.cheque,
                    r.transfer,
                    r.customer_profile_id,
                    r.status as receipt_status,
                    (r.card + r.cash + r.cheque + r.transfer) as total,
                    p.name as customer_name,
                    p.contact_number,
                    p.email,
                    p.identifier
                    from receipts r 
                    left join profiles p on p.id = r.customer_profile_id;";


    public function up()
    {
        \DB::statement($this->sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//         \DB::statement('drop view receipt_views');
    }
}
