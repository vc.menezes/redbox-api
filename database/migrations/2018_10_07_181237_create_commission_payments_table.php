<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommissionPaymentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_account_id')->unsigned()->nullable();
            $table->integer('profile_id')->unsigned()->nullable();
            $table->string('reference_number', 230)->nullable();
            $table->double('amount');
            $table->timestamp('date');
            $table->string('remarks', 553)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
            $table->foreign('profile_id')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('commission_payments');
    }
}
