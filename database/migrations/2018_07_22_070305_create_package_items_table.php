<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePackageItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('package_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('qty');
            $table->string('description')->nullable();
            $table->string('type')->nullable();
            $table->string('UOM');
            $table->integer('commodity_id')->unsigned();
            $table->integer('package_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('commodity_id')->references('id')->on('commodities');
            $table->foreign('package_id')->references('id')->on('packages');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {


        Schema::drop('package_items');

    }
}
