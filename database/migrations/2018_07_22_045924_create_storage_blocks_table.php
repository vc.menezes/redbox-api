<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStorageBlocksTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storage_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('block_id')->unsigned()->nullable();
            $table->integer('package_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('block_id')->references('id')->on('blocks');
            $table->foreign('package_id')->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('storage_blocks', function (Blueprint $table) {
//            $table->dropForeign(['block_id','package_id']);
//        });

        Schema::drop('storage_blocks');
    }
}
