<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('type');
            $table->string('longitude', 100)->nullable();
            $table->string('latitude', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('parent_id')->references('id')->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

//        Schema::table('locations', function (Blueprint $table) {
//            $table->dropForeign(['parent_id']);
//        });

        Schema::drop('locations');
    }
}
