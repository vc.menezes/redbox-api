<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBulkOrdersView extends Migration
{
    public $sql = 'create or replace view bulk_order_views as select
                bo.id,description,bo.weight ,bo.profile_id,
                p.name,p.identifier,p.email , p.contact_number,
                bo.created_at,bo.updated_at,
                bo.payment_status,
                (select count(*)  from bulk_order_packages bop where bulk_order_id = bo.id and deleted_at is null) as  total_orders,
                (select count(*) from bulk_oder_package_packages_view where bulk_order_id = bo.id && weight_approved=1) as weight_approved
                from bulk_orders bo
                left join profiles p on p.id = bo.profile_id
                where bo.deleted_at is null';

    public function up()
    {
        \DB::statement($this->sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::drop('bulk_order_views');
    }
}
