<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommissionViewsTable extends Migration
{
    public $sql = "create or replace view commission_views as select 
                c.id,
                c.profile_id,
                c.invoice_id,
                c.package_id,
                c.total,
                c.details,
                c.status,
                p.identifier,
                p.name,
                p.contact_number,
                p.email,
                pkg.barcode,
                pkg.code,
                pkg.description,
                pkg.weight,
                pkg.type,
                c.created_at,
                c.updated_at,
                c.deleted_at
                from commissions c
                left join profiles p on p.id = c.profile_id
                left join packages pkg on pkg.id = c.package_id
                where c.deleted_at is null
                ";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement($this->sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('drop view commission_views');
    }
}
