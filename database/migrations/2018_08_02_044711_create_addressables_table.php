<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAddressablesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addressables', function (Blueprint $table) {
            $table->increments('id');
            $table->morphs('addressable');
            $table->string('address_line', 255);
            $table->string('street', 255)->nullable();
            $table->string('district', 255);
            $table->string('city', 255);
            $table->integer('postal_code')->nullable();
            $table->integer('country_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            // $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addressables');
    }
}
