<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReceiptsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_profile_id')->unsigned()->nullable();
            $table->integer('created_by_profile_id')->unsigned()->nullable();
            $table->string('receipt_number', 500)->nullable();
            $table->string('remarks', 500)->nullable();
            $table->double('cash')->default(0);
            $table->double('cheque')->default(0);
            $table->double('card')->default(0);
            $table->double('transfer')->default(0);
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('customer_profile_id')->references('id')->on('profiles');
            $table->foreign('created_by_profile_id')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('receipts');
    }
}
