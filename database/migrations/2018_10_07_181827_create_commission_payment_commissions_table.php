<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommissionPaymentCommissionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission_payment_commissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commission_id')->unsigned()->nullable();
            $table->integer('commission_payment_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('commission_id')->references('id')->on('commissions');
            $table->foreign('commission_payment_id')->references('id')->on('commission_payments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('commission_payment_commissions');
    }
}
