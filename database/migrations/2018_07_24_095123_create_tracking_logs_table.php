<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrackingLogsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tracking_id')->unsigned()->nullable();
            $table->integer('location_id')->unsigned()->nullable();
            $table->integer('status_id')->unsigned()->nullable();
            $table->string('type', 200)->nullable();
            $table->string('description', 500)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('tracking_id')->references('id')->on('trackings');
            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('status_id')->references('id')->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tracking_logs');
    }
}
