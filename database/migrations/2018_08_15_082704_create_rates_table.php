<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRatesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->increments('id');
            $table->float('weight');
            $table->float('rate');
            $table->string('operator', 2);
            $table->string('medium', 200);
            $table->string('type', 200);
            $table->integer('from_id')->nullable();
            $table->string('to_id', 10)->nullable();
            $table->timestamps();
            $table->softDeletes();
            // $table->foreign('from_id')->references('id')->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rates');
    }
}
