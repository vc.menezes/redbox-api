<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePackageView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public $sql = " create or replace view package_view as  select 
                    pk.id,
                    pk.barcode,
                    pk.reference_no,
                    pk.description,
                    pk.code,
                    pk.weight,
                    pk.profile_id,
                    pk.parent_id,
                    pk.type,
                    pk.payment_status,
                    pkd.location_id as destinations_location_id,
                    pkd.contact_number as destinations_contact_number,
                    pkd.address as  destinations_address,
                    pkd.name as address_name,
                    pkd.medium as transport_medium,
                    pks.vessel_id as shipped_vessel_id,
                    v.location_id as shipped_vessel_location_id,
                    v.name as shipped_vessel_name,
                    v.contact_number  as shipped_vessel_contact_number,
                    (select count(*) from packages where parent_id=pk.id) as child_package_count,
                    t.tracking_no,
                    (select status_id from tracking_logs tl where tl.tracking_id = t.id order by id desc limit 1) as last_tracking_status_id
                    from packages pk
                    left join package_destinations pkd on pk.id = pkd.package_id
                    left join package_shipments pks on pk.id = pks.vessel_id
                    left join vessels v on pks.vessel_id = v.id
                    left join trackings  t on t.package_id = pk.id
                    where pk.deleted_at is null";
    public function up()
    {
        \DB::statement($this->sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // \DB::statement('drop view [IF EXISTS] package_view');
    }

}
