<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePackageRackViewsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */

    public $sql="create or replace view package_rack_views as select 
        pk.id,
        pk.barcode,
        pk.reference_no,
        pk.code,
        pk.weight,
        pk.profile_id,
        pk.type,
        pk.payment_status,
        t.tracking_no,
        t.status as tracking_status,
        (select block_id from storage_blocks sb where sb.package_id = pk.id and sb.deleted_at is null) as block_id,
        (select status_id from tracking_logs tl where tl.tracking_id = t.id order by id desc limit 1) as last_tracking_status_id,  
        pk.created_at
        from packages pk
        left join trackings  t on t.package_id = pk.id
        where pk.deleted_at is null";

    public function up()
    {
        \DB::statement($this->sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
