<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePackageDestinationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_destinations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 233);
            $table->string('address', 233)->nullable();
            $table->string('contact_number', 233)->nullable();
            $table->integer('location_id')->unsigned()->nullable();
            $table->integer('package_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('package_id')->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('package_destinations', function (Blueprint $table) {
//            $table->dropForeign(['location_id','package_id']);
//        });

        Schema::drop('package_destinations');
    }
}
