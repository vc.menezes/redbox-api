<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlocksTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('name', 230)->nullable();
            $table->string('code', 230)->nullable();
            $table->integer('type')->nullable();;
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('parent_id')->references('id')->on('blocks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blocks', function (Blueprint $table) {

            $table->dropForeign(['parent_id']);

        });
        Schema::drop('blocks');
    }
}
