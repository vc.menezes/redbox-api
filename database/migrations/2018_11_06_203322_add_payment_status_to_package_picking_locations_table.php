<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentStatusToPackagePickingLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_picking_locations', function (Blueprint $table) {
            $table->enum('payment_status',['invoiced','paid','credited','cancelled','none'])
            ->default('none');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_picking_locations', function (Blueprint $table) {
            $table->dropColumn('payment_status');
        });
    }
}
