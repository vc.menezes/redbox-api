<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfilesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('identifier', 50);
            $table->integer('address_id')->unsigned()->nullable();
            $table->string('email', 39);
            $table->string('contact_number', 15);
            $table->string('type', 100);
            $table->integer('location_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('address_id')->references('id')->on('locations');
            $table->foreign('location_id')->references('id')->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('profiles', function (Blueprint $table) {
//
//            $table->dropForeign(['address_id','location_id']);
//
//        });

        Schema::drop('profiles');
    }
}
