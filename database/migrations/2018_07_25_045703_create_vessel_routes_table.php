<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVesselRoutesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vessel_routes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('vessel_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('contact_person', 233)->nullable();
            $table->string('contact', 233)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('vessel_id')->references('id')->on('vessels');
            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('parent_id')->references('id')->on('vessel_routes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vessel_routes');
    }
}
