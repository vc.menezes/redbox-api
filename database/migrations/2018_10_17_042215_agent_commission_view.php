<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgentCommissionView extends Migration
{
    public $sql = "create or replace view agent_commission_view as select
        ac.*,
        pv.code,
        pv.barcode,
        pv.reference_no,
        pv.tracking_no,
        av.name,
        av.identifier,
        av.contact_number
        from agent_commissions ac
        left join package_view pv on pv.id = ac.shipment_id
        left join agents_view av on ac.agent_id = av.id
        where ac.deleted_at is null";
    

    public function up()
    {
        \DB::statement($this->sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
