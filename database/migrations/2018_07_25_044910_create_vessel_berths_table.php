<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVesselBerthsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vessel_berths', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('vessel_id')->unsigned();
            $table->integer('berth_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('vessel_id')->references('id')->on('vessels');
            $table->foreign('berth_id')->references('id')->on('berths');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vessel_berths');
    }
}
