<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgentCommissionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_commissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('total_orders');
            $table->integer('agent_id')->unsigned()->nullable();
            $table->integer('shipment_id')->unsigned()->nullable();
            $table->float('total_amount');
            $table->float('percentage');
            $table->float('agent_commission_total');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('agent_id')->references('id')->on('profiles');
            $table->foreign('shipment_id')->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agent_commissions');
    }
}
