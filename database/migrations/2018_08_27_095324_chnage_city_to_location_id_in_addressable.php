<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChnageCityToLocationIdInAddressable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addressables', function (Blueprint $table) {
            $table->dropColumn('city');
            $table->unsignedInteger('location_id');
            // $table->foreign('location_id')->references('id')->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addressables', function (Blueprint $table) {
            $table->dropColumn('location_id');
        });
    }
}
