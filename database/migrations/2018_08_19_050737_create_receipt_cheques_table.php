<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReceiptChequesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_cheques', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('receipt_id')->unsigned();
            $table->string('cheque_no', 233);
            $table->string('cheque_account_no', 233);
            $table->integer('bank_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('receipt_id')->references('id')->on('receipts');
            $table->foreign('bank_id')->references('id')->on('banks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('receipt_cheques');
    }
}
