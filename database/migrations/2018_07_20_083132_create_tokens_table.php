<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTokensTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token', 12);
            $table->string('medium', 12);
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->boolean('expired');
            $table->text('payload')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

//        Schema::table('tokens', function (Blueprint $table) {
//
//            $table->dropForeign(['user_id']);
//        });
        Schema::drop('tokens');
    }
}
