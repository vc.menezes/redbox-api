<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtherColumnToPackageHandover extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_handovers', function (Blueprint $table) {
            $table->string('identifier')->nullable()->change();
            $table->tinyInteger('other')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_handovers', function (Blueprint $table) {
            $table->dropColumn(['other']);
        });
    }
}
