<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatessPackagessViewss extends Migration
{

    public $sql = "create or replace view package_view as select 
        pk.id,
        pk.barcode,
        pk.reference_no,
        pk.description,
        pk.code,
        pk.weight,
        pk.profile_id,
        pk.parent_id,
        pk.flag,
        pk.weight_approved,
        pk.type,
        pk.payment_status,
        pkd.location_id as destinations_location_id,
        pkd.contact_number as destinations_contact_number,
        lc.code as atoll_code,
        lc.name as destination_island,
        pkd.address as  destinations_address,
        pkd.name as address_name,
        pkd.medium as transport_medium,
        pks.vessel_id as shipped_vessel_id,
        v.location_id as shipped_vessel_location_id,
        v.name as shipped_vessel_name,
        v.contact_number  as shipped_vessel_contact_number,
        t.id as tracking_id,
        t.tracking_no,
        t.status as tracking_status,
        t.estimated_delivery_date as estimated_delivery_date,
        (select status_id from tracking_logs tl where tl.tracking_id = t.id and deleted_at is null order by id desc limit 1) as last_tracking_status_id,    
        pk.created_at
        from packages pk
        left join package_destinations pkd on pk.id = pkd.package_id
        left join locations lc on pkd.location_id = lc.id
        left join package_shipments pks on pk.id = pks.package_id
        left join vessels v on pks.vessel_id = v.id
        left join trackings  t on t.package_id = pk.id
        where pk.deleted_at is null";
    

    public function up()
    {
        \DB::statement($this->sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
