<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $sql =  "CREATE OR REPLACE VIEW agents_view as select
                    p.id,
                    p.name,
                    p.identifier,
                    p.address_id,
                    p.email,
                    p.contact_number,
                    p.type,
                    p.location_id,
                    l.name as location_name,
                    l.code as atoll_code,
                    p.created_at,
                    p.updated_at,
                    p.deleted_at
                    from agents a
                    left join profiles p on p.id = a.profile_id
                    left join locations l on l.id = p.location_id
                    where p.deleted_at is null
                    ";

    public function up()
    {
        \DB::statement($this->sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('DROP VIEW agents_view');
    }
}
