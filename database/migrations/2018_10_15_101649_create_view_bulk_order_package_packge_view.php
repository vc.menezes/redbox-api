<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewBulkOrderPackagePackgeView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $sql ='create or replace view bulk_oder_package_packages_view as select
                    bop.id,bop.bulk_order_id,bop.package_id,bop.created_at,bop.updated_at,
                    p.type,p.payment_status,p.flag,p.weight_approved,p.weight
                    from bulk_order_packages bop
                    left join packages p on p.id = bop.package_id
                    where bop.deleted_at is null';

    public function up()
    {
        \DB::statement($this->sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
