<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePackagesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('barcode', 300);
            $table->string('reference_no', 300)->nullable();
            $table->string('code', 300);
            $table->string('description', 300)->nullable();
            $table->double('weight');
            $table->integer('profile_id')->unsigned()->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->foreign('parent_id')->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('packages', function (Blueprint $table) {
//            $table->dropForeign(['profile_id','parent_id']);
//        });


        Schema::drop('packages');
    }
}
