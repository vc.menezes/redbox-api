<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressableIdColumnToPickingLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_picking_locations', function (Blueprint $table) {
            $table->integer('addressables_id')->nullable();
//            $table->foreign('addressables_id')->references('id')->on('addressables');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_picking_locations', function (Blueprint $table) {
            $table->dropColumn(['addressables_id']);
        });
    }
}
