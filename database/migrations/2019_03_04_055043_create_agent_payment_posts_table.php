<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgentPaymentPostsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_payment_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned()->default(0)->nullable();
            $table->double('total');
            $table->string('account_number',100);
            $table->string('email',100);
            $table->string('details',200);
            $table->string('contact_number',10);
            $table->string('identifier',200);
            $table->double('weight');
            $table->integer('status')->default(1);
            $table->timestamp('date');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('profile_id')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agent_payment_posts');
    }
}
